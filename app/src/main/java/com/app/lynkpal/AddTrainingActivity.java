package com.app.lynkpal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.app.lynkpal.Application.Utils;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.FilePath;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.bumptech.glide.Glide;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class AddTrainingActivity extends AppCompatActivity {
    LoaderDiloag loaderDiloag;

    private TextView tv_uploadCv,tvBtnSubmit,tvCvName,tvPkgName,tvPkgAmt;
    private EditText edFirstName,edLastName,edEmail,edPhone,edSummary,edJobs;
    private String strFirstName,strLastName,strEmail,strPhone,strSummary,strJobs;
    Spinner spinner;
    String[] spinnerValue = {"1", "2", "3", "4","5","6", "7", "8", "9","10","11", "12", "13", "14","15","16", "17", "18", "19","20",
            "21", "22", "23", "24","25","26", "27", "28", "29","30","31", "32", "33", "34","35","36", "37", "38", "39","40",};
    //String[] spinnerValue = new String[50];

    FileBody filepath;

    SharedPreferences sharedPreferences;
    private String strExp,packageName;
    private String imageBase="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_training);
        loaderDiloag = new LoaderDiloag(AddTrainingActivity.this);

        sharedPreferences = AddTrainingActivity.this.getSharedPreferences(Constant.PREFS_NAME, 0);
        AndroidNetworking.initialize(getApplicationContext());

        spinner =(Spinner)findViewById(R.id.spinnerAddTraining);
        tvPkgName =(TextView) findViewById(R.id.tvPkgName);
        tvPkgAmt =(TextView) findViewById(R.id.tvPkgAmt);
        edFirstName =(EditText) findViewById(R.id.edFirstName);
        edLastName =(EditText) findViewById(R.id.edLastName);
        edEmail =(EditText) findViewById(R.id.edEmail);
        edPhone =(EditText) findViewById(R.id.edPhone);
        edSummary =(EditText) findViewById(R.id.edSummary);
        edJobs =(EditText) findViewById(R.id.edJobs);
        tvCvName =(TextView) findViewById(R.id.tvCvName);
        tv_uploadCv =(TextView) findViewById(R.id.tv_uploadCv);
        tvBtnSubmit =(TextView) findViewById(R.id.tvSubmit);

        getDetailsApi();



        Intent i = getIntent();
         packageName = i.getStringExtra("key");

         if (packageName.equalsIgnoreCase("bronze")){
             tvPkgName.setText("Bronze CV");
             tvPkgAmt.setText("# 10,900 or 109.00 ¢");

         }else   if (packageName.equalsIgnoreCase("silver")){
             tvPkgName.setText("Silver CV");
             tvPkgAmt.setText("# 18,900 or 189.00 ¢");

         }else  if (packageName.equalsIgnoreCase("gold")){
             tvPkgName.setText("Gold CV");
             tvPkgAmt.setText("# 30,900 or 309.00 ¢");
         }


             tv_uploadCv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                startActivityForResult(intent, 7);

            }
        });


        tvBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strFirstName = edFirstName.getText().toString();
                strLastName = edLastName.getText().toString();
                strEmail = edEmail.getText().toString();
                strPhone = edPhone.getText().toString();
                strSummary = edSummary.getText().toString();
                strJobs = edJobs.getText().toString();

                if (validate(strFirstName,strLastName,strEmail,strSummary,strJobs)) {
                    // new VideoUploader().execute();
                    getConfirmPackageApi();

                }
             /*   Intent intent=new Intent(AddTrainingActivity.this, CardDetailActivity.class);
                intent.putExtra("trainingKey","gold");
                startActivity(intent);*/
            }
        });

        spinnerAdapter adapter = new spinnerAdapter(AddTrainingActivity.this, android.R.layout.simple_list_item_1);
        adapter.addAll(spinnerValue);
        adapter.add("Experience");
        spinner.setAdapter(adapter);
        spinner.setSelection(adapter.getCount());

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(spinner.getSelectedItem() == "Experience")
                {
                    strExp="";
                    //Do nothing.
                }
                else{

                    strExp=spinner.getSelectedItem().toString().trim();
                   // Toast.makeText(AddTrainingActivity.this, spinner.getSelectedItem().toString(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    /*   //customSpinnerAdapter

        String [] spinnerArray={"Self","Newspaper","Website"};
        CustomSpinnerAdapter adapter=new CustomSpinnerAdapter(AddTrainingActivity.this, android.R.layout.simple_spinner_item, spinnerArray,
                "This is hint displayed in spinner");
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
       // Spinner spinner =(Spinner) findViewById(R.id.spinnerAddTraining);
        spinner.setAdapter(adapter);
        spinner.setSelection(adapter.getCount());

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(spinner.getSelectedItem() == "This is hint displayed in spinner")
                {

                    //Do nothing.
                }
                else{

                    Toast.makeText(AddTrainingActivity.this, spinner.getSelectedItem().toString(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode){

            case 7:

                if(resultCode==RESULT_OK){

                    try {


                        String PathHolder = data.getData().getPath();
                        //convertToBase64(PathHolder);

                        PathHolder = data.getData().getLastPathSegment();
                        //String[] a=    PathHolder.split("\\.");
                        // PathHolder=a[0];
                        tvCvName.setText(PathHolder);
                        Uri selectedFileUri = data.getData();
                        //  convertImage(AddTrainingActivity.this,selectedFileUri);

                        final InputStream imageStream = getContentResolver().openInputStream(selectedFileUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        //ivChooseTrainingImage.setImageBitmap(selectedImage);
                        //  Glide.with(AddNewTraining.this).load(imageUri).centerCrop().into(ivChooseTrainingImage);


                        imageBase = bitmapToBase64(selectedImage);
                        // Toast.makeText(AddTrainingActivity.this, PathHolder , Toast.LENGTH_SHORT).show();

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                break;

        }
    }

    public class spinnerAdapter extends ArrayAdapter<String> {

        public spinnerAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
            // TODO Auto-generated constructor stub

        }

        @Override
        public int getCount() {

            // TODO Auto-generated method stub
            int count = super.getCount();

            return count>0 ? count-1 : count ;


        }


    }

    public class CustomSpinnerAdapter extends ArrayAdapter {

        String hint;
        public CustomSpinnerAdapter(Activity act, int layoutResId, String [] dataArray,String hint)
        {
            super(act,layoutResId,dataArray);
            this.hint=hint;
        }
        @Override    public View getView(int position, View convertView, ViewGroup parent) {

            View v = null;
            if (position >= getCount()) { //logic to display hint
                v =super.getView(0, convertView, parent);
                ((TextView)v.findViewById(android.R.id.text1)).setText("");
                ((TextView)v.findViewById(android.R.id.text1)).setHint(hint); //"Hint to be displayed"
                 }

                 else
            v =super.getView(position, convertView, parent);
                return v;
            }

            @Override    public int getCount() {
                return super.getCount();
            }

        }




    private void getDetailsApi() {
        if (ApplicationGlobles.isConnectingToInternet(AddTrainingActivity.this)) {

            loaderDiloag.displayDiloag();

            com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(AddTrainingActivity.this);
      /*  Map<String, String> postParam = new HashMap<String, String>();
        postParam.put("toFollow", user);

        Log.d("Training", "frndParam " + postParam.toString());*/


            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    WebApis.USERDETAILS, null,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {

                            loaderDiloag.dismissDiloag();
                            Log.d("Training", "TrainingResponse " + response.toString());
                            try {
                                String status = response.getString("status");
                                // String message = response.getString("message");
                                if (status.contains("success")) {


                                    edFirstName.setText(response.getString("firstname"));
                                    edLastName.setText(response.getString("lastname"));
                                    edEmail.setText(response.getString("email"));

                                    if (response.getString("career_summary") != null || response.getString("career_summary").equalsIgnoreCase("null")) {


                                    } else {
                                        edSummary.setText(response.getString("career_summary"));

                                    }
                                    if (response.getString("phone") != null || response.getString("phone").equalsIgnoreCase("null")) {

                                    } else {
                                        edPhone.setText(response.getString("phone"));

                                    }
                                    if (response.getString("job_roles").equals(null) || response.getString("job_roles").equalsIgnoreCase("null")) {

                                    } else {
                                        edJobs.setText(response.getString("job_roles"));

                                    }
                              /*  mFriendAdapter.notifyDataSetChanged();
                                suggestionApi(context, settings.getString("token",""));
                                Toast.makeText(context, "Followed successfully ", Toast.LENGTH_LONG).show();*/
                                } else {

                                    loaderDiloag.dismissDiloag();

                                    Log.d("Training", "frndError " + response.toString());
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                loaderDiloag.dismissDiloag();

                            }


                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    loaderDiloag.dismissDiloag();
                    error.printStackTrace();
                    if (error.toString().contains(Constant.timeout_error_string)) ;
                    // Toast.makeText(AddTrainingActivity.this,getString(R.string.slow_internet), Toast.LENGTH_SHORT).show();
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> data = new HashMap<String, String>();
                    data.put("Authorization", sharedPreferences.getString("token", ""));
                    data.put("Content-Type", "application/json");
                    Log.d("Training", "header " + data.toString());
                    return data;
                }

            };

            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    Constant.VOLLEY_TIMEOUT_MS,
                    Constant.VOLLEY_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


            jsonObjReq.setTag("Training");
            // Adding request to request queue
            requestQueue.add(jsonObjReq);

        }else{
            Toast.makeText(AddTrainingActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }
    }

    private void convertImage(Context context, Uri uri)
    {
        String selectedFilePath = FilePath.getPath(context,uri);
        File file = new File(selectedFilePath);
        Log.d("fileIs", file+"");
        filepath = new FileBody(file);
        //extension = "png";
    }

    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }


    private String convertToBase64(String imagePath)

    {

        Bitmap bm = BitmapFactory.decodeFile(imagePath);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] byteArrayImage = baos.toByteArray();

        String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

        Toast.makeText(AddTrainingActivity.this, encodedImage , Toast.LENGTH_LONG).show();

        return encodedImage;

    }

    public boolean validate(String strFirstName,String strLastName,String strEmail,String strSummary,String strJobs){
       if (strFirstName.equalsIgnoreCase("")||strFirstName.isEmpty()){
          edFirstName.setError("Please fill the field");
           return false;
       }else  if (strLastName.equalsIgnoreCase("")||strLastName.isEmpty()){
           edLastName.setError("Please fill the field");
           return false;
       }else  if (strEmail.equalsIgnoreCase("")||strEmail.isEmpty()){
           edEmail.setError("Please fill the field");
           return false;
       }else  if (strSummary.equalsIgnoreCase("")||strSummary.isEmpty()){
           edSummary.setError("Please fill the field");
           return false;
       }else  if (strJobs.equalsIgnoreCase("")||strJobs.isEmpty()){
           edJobs.setError("Please fill the field");
           return false;
       }else  if (strExp.equalsIgnoreCase("")||strExp.isEmpty()){
           Toast.makeText(AddTrainingActivity.this,"Please Select Experience",Toast.LENGTH_SHORT).show();
           return false;
       }else  if (imageBase.equalsIgnoreCase("")||imageBase.isEmpty()){
           Toast.makeText(AddTrainingActivity.this,"Please Select CV",Toast.LENGTH_SHORT).show();
           return false;
       }
        return true;
    }

  public void uploadData(){

        AndroidNetworking.upload("")
                //.addMultipartFile("image",file)
                .addMultipartParameter("key","value")
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {

                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
  }

    private void getConfirmPackageApi() {
        if (ApplicationGlobles.isConnectingToInternet(AddTrainingActivity.this)) {

            loaderDiloag.displayDiloag();

        com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(AddTrainingActivity.this);

      /*  Map<String, String> postParam = new HashMap<String, String>();
        postParam.put("package", packageName);
        postParam.put("firstname", strFirstName);
        postParam.put("lastname", strLastName);
        postParam.put("phone", strPhone);
        postParam.put("experience", strExp);
        postParam.put("career_summary", strSummary);
        postParam.put("job_roles", strJobs);
        postParam.put("cvfile", imageBase);

        Log.d("Training",    postParam.toString());
*/
        JSONObject json = new JSONObject();;
        try {
            json.put("package", packageName);
            json.put("firstname", strFirstName);
            json.put("lastname", strLastName);
            json.put("phone", strPhone);
            json.put("experience", strExp);
            json.put("career_summary", strSummary);
            json.put("job_roles", strJobs);
            json.put("cvfile", imageBase);

            Log.d("Params",    json.toString());
        }catch (Exception e){

        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                WebApis.AddTrainingUpdateCv, json ,  //new JSONObject(postParam)
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response)
                    {
                        loaderDiloag.dismissDiloag();
                        Log.d("AddTrainingActivityRes", "AddTrainingActivity " + response.toString());
                        try {
                            String status = response.getString("status");
                             String message = response.getString("message");
                            if (status.contains("success"))
                            {
                                //showPopup(AddTrainingActivity.this,response.getString("message"));

                                new MaterialDialog.Builder(AddTrainingActivity.this)
                                        .title("Lynkpal")
                                        .content(message)
                                        .iconRes(R.drawable.ic_check_circle)
                                        .positiveText("OK")
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                Intent intent=new Intent(AddTrainingActivity.this, CardDetailActivity.class);
                                                intent.putExtra("trainingKey",packageName);
                                                startActivity(intent);
                                                finish();
                                            }
                                        })
                                        .show();

                            } else
                            {

                                Log.d("Training", "frndError " + response.toString());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener()
        {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                loaderDiloag.dismissDiloag();
                Toast.makeText(AddTrainingActivity.this,"Something went wrong",Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                if (error.toString().contains(Constant.timeout_error_string));
                // Toast.makeText(AddTrainingActivity.this,getString(R.string.slow_internet), Toast.LENGTH_SHORT).show();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String,String> data = new HashMap<String, String>();
                data.put("Authorization",sharedPreferences.getString("token",""));
                data.put("Content-Type","application/json");
                Log.d("AddTrainingActivity","header " + data.toString());
                return data;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                Constant.VOLLEY_TIMEOUT_MS,
                Constant.VOLLEY_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        jsonObjReq.setTag("AddTrainingActivity");
        // Adding request to request queue
        requestQueue.add(jsonObjReq);

        } else {
            Toast.makeText(AddTrainingActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }
    }

    private class VideoUploader extends AsyncTask<Void, Void, Void>
    {
        String url;
        String status = " ";
        String msg = " ";

        @Override
        protected void onPreExecute()
        {
            //dialog.dismiss();
             loaderDiloag.displayDiloag();
            url = WebApis.AddTrainingUpdateCv;
            url = url.replace(" ", "%20");
            //Log.e("extension", extension + "");
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            // TODO Auto-generated method stub

            // Client-side HTTP transport library

            HttpClient httpClient = new DefaultHttpClient();
            // using POST method
            HttpPost httpPostRequest = new HttpPost(url);
            httpPostRequest.setHeader("authorization", Constant.token);
            try {
                MultipartEntityBuilder multiPartEntityBuilder = MultipartEntityBuilder.create();

                if (filepath != null)
                {
                    multiPartEntityBuilder.addPart("cvfile", filepath);

                    multiPartEntityBuilder.addTextBody("package", packageName);
                    multiPartEntityBuilder.addTextBody("firstname", strFirstName);
                    multiPartEntityBuilder.addTextBody("lastname", strLastName);
                    multiPartEntityBuilder.addTextBody("phone", strPhone);
                    multiPartEntityBuilder.addTextBody("experience", strExp);
                    multiPartEntityBuilder.addTextBody("career_summary", strSummary);
                    multiPartEntityBuilder.addTextBody("job_roles", strJobs);

                } else
                {
                   // extension = "text";
                }
               // Log.d("AddTrainingActivity", "doInBackground: Post data " + post_data);
               // multiPartEntityBuilder.addTextBody("description", Utils.encodeMessage(post_data));
               // Log.d("description",Utils.encodeMessage(post_data) +"");

                httpPostRequest.setEntity(multiPartEntityBuilder.build());
                Log.e("response", multiPartEntityBuilder.toString());
                HttpResponse httpResponse = null;
                httpResponse = httpClient.execute(httpPostRequest);
                HttpEntity httpEntity = httpResponse.getEntity();
                String jsonStr = EntityUtils.toString(httpEntity);
                Log.e("response", jsonStr);
                if (jsonStr != null)
                {

                    try {

                        loaderDiloag.dismissDiloag();
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        Log.d("AddTrainingActivity", "doInBackground: Status " + status) ;
                        status = jsonObj.getString("status");
                        msg = jsonObj.getString("message");
                        if (status.equals("error"))
                        {
                            //mDialog.dismissDiloag();
                            Log.d("AddTrainingActivity", "Error");
                            Log.d("AddTrainingActivity", "doInBackground: Error");
                        }
                        else
                        {
                            //mDialog.dismissDiloag();
                            Log.d("AddTrainingActivity", "doInBackground: Success");
                        }
                    } catch (Exception e)
                    {
                        loaderDiloag.dismissDiloag();
                        Log.e("Error", e.toString());
                    }

                } else {
                    Log.e("ServiceHandler", "Couldn't get any data from the url");
                }
            } catch (Exception e) {
                loaderDiloag.dismissDiloag();
                Log.e("Error", e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
           /* page = 0;
            visibleItemCount = 0;
            totalItemCount = 0;
            pastVisiblesItems = 0;
            filePath = null;
            extension = "";
            postText = "";
            layImage.setVisibility(View.GONE);
            loaderDiloag.dismissDiloag();
            homeBeanList.clear();
            getAllPost();*/
            if (status.contains("success"))
            {
                Toast.makeText(AddTrainingActivity.this,"Post Successfully", Toast.LENGTH_SHORT).show();

//                finish();
//                startActivity(new Intent(AddTrainingActivity.this,MainActivity.class));
            }
            showPopup(AddTrainingActivity.this,msg);
            super.onPostExecute(aVoid);
            //  }
        }
    }


    public void showPopup(Context context,String msg) {
        LayoutInflater layoutInflater
                = (LayoutInflater) context
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_group_join_leave, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        final TextView textText = (TextView) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(context.getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        textTitle.setText("Lynkpal");
        textText.setText(msg);
        btnCancel.setVisibility(View.GONE);
        btnSumbmit.setText("Ok");
        // textText.setText("Are you sure, you want to leave " + '"' + text + '"' + " Group?");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // delete(id, pos);
                Intent intent=new Intent(AddTrainingActivity.this, CardDetailActivity.class);
                intent.putExtra("trainingKey",packageName);
                startActivity(intent);
                popupWindow.dismiss();

            }
        });
    }


}
