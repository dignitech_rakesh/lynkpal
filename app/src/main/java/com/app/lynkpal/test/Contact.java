package com.app.lynkpal.test;

/**
 * Created by user on 11/6/2017.
 */

public class Contact {

     int id;
     String name;
     String email;

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    Boolean isSelected;

    public Contact(){}

    public Contact(String name, String email,Boolean isSelected) {
        super();
        this.name = name;
        this.email = email;
        this.isSelected = isSelected;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }



}