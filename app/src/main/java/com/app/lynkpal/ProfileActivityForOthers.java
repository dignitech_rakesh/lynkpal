package com.app.lynkpal;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Adapter.EducationAdapter;
import com.app.lynkpal.Adapter.ExperienceAdapter;
import com.app.lynkpal.Adapter.MultiSelectionList;
import com.app.lynkpal.Bean.ProfileEducationBean;
import com.app.lynkpal.Bean.ProfileExperienceBean;
import com.app.lynkpal.Bean.SkillListBean;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.ExpandedHightListview;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.EditInterface;
import com.app.lynkpal.Interface.SelectInterface;
import com.app.lynkpal.Interface.deleteEduExpInterface;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class ProfileActivityForOthers extends AppCompatActivity implements SelectInterface, EditInterface, deleteEduExpInterface {
    private static final int REQUEST_CAMERA_ACCESS_PERMISSION = 5674;
    public static List<SkillListBean> list = new ArrayList<SkillListBean>();
    ListView listView;
    ImageView ic_editImage;
    Bitmap bitmap = null;
    String selectedIndustry;
    SkillListBean skillListBean;
    ScrollView scrollView;
    String attachmentStr = "";
    MultiSelectionList customAdapter;
    TextView spnr, head, txtName, edu, exp, myPerDet, txtDOB, Gender;
    String message = "";
    ImageView ic_back, imgImage;
    ProfileEducationBean profileEducationBean;
    EducationAdapter educationAdapter;
    ExperienceAdapter experienceAdapter;
    List<ProfileEducationBean> educationBeanList = new ArrayList<ProfileEducationBean>();
    ProfileExperienceBean profileExperienceBean;
    List<ProfileExperienceBean> experienceBeanArrayList = new ArrayList<ProfileExperienceBean>();
    String profile_slug = "";
    String profile_pic = "";
    String skills = "";
    String dob = "";
    String country = "";
    String gender = "";
    String hobby = "";
    String next_of_kin = "";
    String no_of_children = "";
    String power_statement = "";
    String first_name = "";
    String last_name = "";
    String userId = "";
    ExpandedHightListview exp_eduList;
    ExpandedHightListview exp_expList;
    TextView edtChild, edtCountry, edtKin, edtHobby, edtDetails;
    RadioButton radMale, radFemale;
    Button btnUpdate;
    // Calendar myCalendar, myCalSTREDU, myCalENDEDU, myCalSTREXP, myCalENDEXP;
    ImageView addEdu, addExp;
    TextView edtFirstName, edtLastName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_for_others);
        customAdapter = new MultiSelectionList(ProfileActivityForOthers.this, list, this);
        exp_eduList = (ExpandedHightListview) findViewById(R.id.exp_eduList);
        exp_expList = (ExpandedHightListview) findViewById(R.id.exp_expList);
        // scrollView = (ScrollView) findViewById(R.id.scroll);
        head = (TextView) findViewById(R.id.head);
        txtName = (TextView) findViewById(R.id.txtName);
        spnr = (TextView) findViewById(R.id.spnr);
        edu = (TextView) findViewById(R.id.edu);
        exp = (TextView) findViewById(R.id.exp);
        myPerDet = (TextView) findViewById(R.id.myPerDet);
        txtDOB = (TextView) findViewById(R.id.txtDOB);
        Gender = (TextView) findViewById(R.id.Gender);
        ic_back = (ImageView) findViewById(R.id.ic_back);
        ic_editImage = (ImageView) findViewById(R.id.ic_editImage);
        addEdu = (ImageView) findViewById(R.id.addEdu);
        addExp = (ImageView) findViewById(R.id.addExp);
        imgImage = (ImageView) findViewById(R.id.imgImage);
        edtChild = (TextView) findViewById(R.id.edtChild);
        edtCountry = (TextView) findViewById(R.id.edtCountry);
        edtKin = (TextView) findViewById(R.id.edtKin);
        edtHobby = (TextView) findViewById(R.id.edtHobby);
        edtDetails = (TextView) findViewById(R.id.edtDetails);
        edtFirstName = (TextView) findViewById(R.id.edtFirstName);
        edtLastName = (TextView) findViewById(R.id.edtLastName);
        radMale = (RadioButton) findViewById(R.id.radMale);
        radFemale = (RadioButton) findViewById(R.id.radFemale);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);

        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        head.setTypeface(tf_bold);
        txtName.setTypeface(tf_bold);
        edu.setTypeface(tf_bold);
        exp.setTypeface(tf_bold);
        myPerDet.setTypeface(tf_bold);
        btnUpdate.setTypeface(tf_bold);
        spnr.setTypeface(tf_reg);
        txtDOB.setTypeface(tf_reg);
        Gender.setTypeface(tf_med);
        edtChild.setTypeface(tf_reg);
        edtCountry.setTypeface(tf_reg);
        edtKin.setTypeface(tf_reg);
        edtLastName.setTypeface(tf_reg);
        edtFirstName.setTypeface(tf_reg);
        edtHobby.setTypeface(tf_reg);
        edtDetails.setTypeface(tf_reg);
        radMale.setTypeface(tf_reg);
        radFemale.setTypeface(tf_reg);
        //getSkills();

        try {
            userId = getIntent().getStringExtra("userId");
        } catch (Exception e) {
            userId = "";
        }
        getProfileInformaition();
    }


    public void getProfileInformaition() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(ProfileActivityForOthers.this);
        if (ApplicationGlobles.isConnectingToInternet(ProfileActivityForOthers.this)) {
            loaderDiloag.displayDiloag();
            educationBeanList.clear();
            experienceBeanArrayList.clear();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", Constant.token);
            Request request = new Request.Builder()
                    .url(WebApis.USERPROFILE + userId)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (ProfileActivityForOthers.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                JSONObject jsonObject = new JSONObject(jsonData);
                                final String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("personal_profile");
                                    profile_slug = jsonObject1.getString("profile_slug");
                                    profile_pic = jsonObject1.getString("profile_pic");
                                    skills = jsonObject1.getString("skills");
                                    dob = jsonObject1.getString("dob");
                                    country = jsonObject1.getString("country");
                                    gender = jsonObject1.getString("gender");
                                    hobby = jsonObject1.getString("hobby");
                                    next_of_kin = jsonObject1.getString("next_of_kin");
                                    no_of_children = jsonObject1.getString("no_of_children");
                                    power_statement = jsonObject1.getString("power_statement");
                                    first_name = jsonObject1.getString("first_name");
                                    last_name = jsonObject1.getString("last_name");

                                    JSONArray array = jsonObject.getJSONArray("education");
                                    for (int a = 0; a < array.length(); a++) {
                                        JSONObject jsonObject2 = array.getJSONObject(a);
                                        String id = jsonObject2.getString("id");
                                        String board_university = jsonObject2.getString("board_university");
                                        String class_degree = jsonObject2.getString("class_degree");
                                        String end_date = jsonObject2.getString("end_date");
                                        String start_date = jsonObject2.getString("start_date");
                                        String subject_course = jsonObject2.getString("subject_course");
                                        profileEducationBean = new ProfileEducationBean();
                                        profileEducationBean.setId(id);
                                        profileEducationBean.setBoard_university(board_university);
                                        profileEducationBean.setClass_degree(class_degree);
                                        profileEducationBean.setEnd_date(parseDateToddMMyyyy(end_date));
                                        profileEducationBean.setStart_date(parseDateToddMMyyyy(start_date));
                                        profileEducationBean.setSubject_course(subject_course);
                                        profileEducationBean.setDeleteEdit(false);
                                        educationBeanList.add(profileEducationBean);
                                    }
                                    JSONArray array1 = jsonObject.getJSONArray("experience");
                                    for (int a = 0; a < array1.length(); a++) {
                                        JSONObject jsonObject3 = array1.getJSONObject(a);
                                        String id = jsonObject3.getString("id");
                                        String title = jsonObject3.getString("title");
                                        String company = jsonObject3.getString("company");
                                        String location = jsonObject3.getString("location");
                                        String start_date = jsonObject3.getString("start_date");
                                        String end_date = jsonObject3.getString("end_date");
                                        profileExperienceBean = new ProfileExperienceBean();
                                        profileExperienceBean.setId(id);
                                        profileExperienceBean.setTitle(title);
                                        profileExperienceBean.setCompany(company);
                                        profileExperienceBean.setLocation(location);
                                        profileExperienceBean.setStart_date(parseDateToddMMyyyy(start_date));
                                        profileExperienceBean.setEnd_date(parseDateToddMMyyyy(end_date));
                                        profileExperienceBean.setDeleteEdit(false);
                                        experienceBeanArrayList.add(profileExperienceBean);
                                    }

                                }

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        loaderDiloag.dismissDiloag();
                                        if (!skills.equals("null")) {
                                            spnr.setText("Skills : " + skills + "");
                                        }
                                        if (!dob.equals("null")) {
                                            txtDOB.setText("DOB : " + dob + "");
                                        }
                                        if (!country.equals("null")) {
                                            edtCountry.setText("Country : " + country + "");
                                        }
                                        if (!no_of_children.equals("null")) {
                                            edtChild.setText("Number of Children : " + no_of_children + "");
                                        }
                                        if (!next_of_kin.equals("null")) {
                                            edtKin.setText("Next Of Kin/Referee : " + next_of_kin + "");
                                        }
                                        if (!hobby.equals("null")) {
                                            edtHobby.setText("Hobbies : " + hobby + "");
                                        }
                                        if (!power_statement.equals("null")) {
                                            edtDetails.setText("Power Statement : " + power_statement + "");
                                        }
                                        if (!first_name.equals("null")) {
                                            edtFirstName.setText("First Name : " + first_name + "");
                                        }
                                        if (!last_name.equals("null")) {
                                            edtLastName.setText("Last Name : " + last_name + "");
                                        }
                                        if (!gender.equals("null")) {
                                            if (gender.toLowerCase().equals("m")) {
                                                radMale.setChecked(true);
                                                radFemale.setChecked(false);
                                            } else {
                                                radMale.setChecked(false);
                                                radFemale.setChecked(true);
                                            }
                                        }

                                        if (!profile_slug.equals("null")) {
                                            // profile_slug = profile_slug.replace("-", " ");
                                            // profile_slug = profile_slug.replace(" " + Constant.user_id, "");
                                            txtName.setText(first_name + " " + last_name);
                                        }
                                        if (!profile_pic.equals("null")) {
                                            Glide.with(ProfileActivityForOthers.this).load(WebApis.userProfileImage + profile_pic).into(imgImage);
                                        }

                                        if (exp_eduList.getAdapter() == null) {
                                            if (educationBeanList.size() > 0) {
                                                exp_eduList.setExpanded(true);
                                                educationAdapter = new EducationAdapter(ProfileActivityForOthers.this, educationBeanList, ProfileActivityForOthers.this, ProfileActivityForOthers.this);
                                                exp_eduList.setAdapter(educationAdapter);
                                            }
                                        } else {
                                            try {
                                                educationAdapter.notifyDataSetChanged();
                                            } catch (Exception e) {
                                            }
                                        }
                                        if (exp_expList.getAdapter() == null) {
                                            if (experienceBeanArrayList.size() > 0) {
                                                exp_expList.setExpanded(true);
                                                experienceAdapter = new ExperienceAdapter(ProfileActivityForOthers.this, experienceBeanArrayList, ProfileActivityForOthers.this, ProfileActivityForOthers.this);
                                                exp_expList.setAdapter(experienceAdapter);
                                            }
                                        } else {
                                            try {
                                                experienceAdapter.notifyDataSetChanged();
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                });


                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(ProfileActivityForOthers.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        String outputPattern = "yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    @Override
    public void select(String position, int yes) {
        if (yes == 1) {
            list.get(Integer.parseInt(position)).setSelected(true);
            customAdapter.notifyDataSetChanged();
        } else {
            list.get(Integer.parseInt(position)).setSelected(false);
            customAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void Edit(String id, String type) {
        if (type.equals("Edu")) {
            // getEduDetails(id);
        } else {
            //  getExpDetails(id);
        }
    }


    public String yyMMdd(String time) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        String outputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    @Override
    public void delete(String id, int pos, String type) {

    }
}
