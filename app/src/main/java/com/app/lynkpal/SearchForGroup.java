package com.app.lynkpal;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Adapter.SearchGroupAdapter;
import com.app.lynkpal.Bean.SearchGroupBean;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.JoinGroup;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class SearchForGroup extends AppCompatActivity implements JoinGroup {
    ImageView imgBack;
    ProgressBar progress;
    TextView txtNoResult;
    ListView listSearch;
    SearchGroupAdapter searchGroupAdapter;
    EditText edtSearch;
    List<SearchGroupBean> searchGroupBeanArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_for_group);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        progress = (ProgressBar) findViewById(R.id.progress);
        txtNoResult = (TextView) findViewById(R.id.txtNoResult);
        listSearch = (ListView) findViewById(R.id.listSearch);
        edtSearch = (EditText) findViewById(R.id.edtSearch);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        edtSearch.setTypeface(tf_bold);
        txtNoResult.setTypeface(tf_med);
        progress.setVisibility(View.GONE);
        txtNoResult.setVisibility(View.INVISIBLE);
        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!edtSearch.getText().toString().trim().equals("")) {
                        String text = edtSearch.getText().toString().replace(" ", "%20");
                        getSearchResult(text);

                    } else {
                        try {
                            txtNoResult.setVisibility(View.INVISIBLE);
                            searchGroupBeanArrayList.clear();
                            searchGroupAdapter = new SearchGroupAdapter(SearchForGroup.this, searchGroupBeanArrayList, SearchForGroup.this);
                            listSearch.setAdapter(searchGroupAdapter);
                            searchGroupAdapter.notifyDataSetChanged();
                        } catch (Exception e) {

                        }
                    }
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edtSearch.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    return true;
                }
                return false;
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                SearchForGroup.this.overridePendingTransition(0, 0);

            }
        });
        try {
            edtSearch.setFocusable(true);
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(edtSearch, InputMethodManager.SHOW_IMPLICIT);
        } catch (Exception e) {

            Log.e("error", "r" + e.toString());
        }
    }

    public void getSearchResult(String search) {
        // final LoaderDiloag loaderDiloag = new LoaderDiloag(SearchForGroup.this);
        if (ApplicationGlobles.isConnectingToInternet(SearchForGroup.this)) {
            //  loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            progress.setVisibility(View.VISIBLE);
            txtNoResult.setVisibility(View.INVISIBLE);
            searchGroupBeanArrayList.clear();
            try {
                searchGroupAdapter.notifyDataSetChanged();
            } catch (Exception e) {
            }
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.SearchGroup", WebApis.SearchGroup + search);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.SearchGroup + search)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (SearchForGroup.this == null)
                            return;
                        progress.setVisibility(View.GONE);

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getDetails", jsonData + "");
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {
                                JSONObject jsonObject = array.getJSONObject(a);
                                SearchGroupBean searchGroupBean = new SearchGroupBean();
                                searchGroupBean.setId(jsonObject.getString("id"));
                                searchGroupBean.setName(jsonObject.getString("name"));
                                searchGroupBean.setGrouptype(jsonObject.getString("grouptype"));
                                searchGroupBean.setImage(jsonObject.getString("image"));
                                searchGroupBean.setMembershipstatus(jsonObject.getString("membershipstatus"));
                                searchGroupBeanArrayList.add(searchGroupBean);
                            }

                        } catch (Exception e) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress.setVisibility(View.GONE);
                                }
                            });
                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progress.setVisibility(View.GONE);
                                if (searchGroupBeanArrayList.size() > 0) {
                                    searchGroupAdapter = new SearchGroupAdapter(SearchForGroup.this, searchGroupBeanArrayList, SearchForGroup.this);
                                    listSearch.setAdapter(searchGroupAdapter);
                                } else {
                                    txtNoResult.setVisibility(View.VISIBLE);
                                }
                            }
                        });


                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress.setVisibility(View.GONE);

                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    @Override
    public void onBackPressed() {
        finish();
        SearchForGroup.this.overridePendingTransition(0, 0);
    }

    @Override
    public void Join(int pos, String type) {
        if (searchGroupBeanArrayList.get(pos).getGrouptype().toLowerCase().equals("open")) {
            String id = searchGroupBeanArrayList.get(pos).getId();
            String text = searchGroupBeanArrayList.get(pos).getName();
            String status = searchGroupBeanArrayList.get(pos).getMembershipstatus();
            if (status.equals("0")) {
                JoinOpenGroup(id);
            } else {
                showPopup(id, text, type);
            }
        } else {
            String id = searchGroupBeanArrayList.get(pos).getId();
            String text = searchGroupBeanArrayList.get(pos).getName();
            String status = searchGroupBeanArrayList.get(pos).getMembershipstatus();
            if (status.equals("0")) {
                JoinClosedGroup(id);
            } else if (status.equals("1")) {
                showPopup(id, text, type);
            } else {
                Toast.makeText(this, "Request Pending", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void showPopup(final String id, final String text, final String type) {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_group_join_leave, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        TextView textText = (TextView) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        textText.setText("Are you sure, you want to leave " + '"' + text + '"' + " Group?");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.equals("open")) {
                    JoinOpenGroup(id);
                } else {
                    JoinClosedGroup(id);
                }
                popupWindow.dismiss();

            }
        });
    }

    public void JoinOpenGroup(String id) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(SearchForGroup.this);
        if (ApplicationGlobles.isConnectingToInternet(SearchForGroup.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            //  gridBeanList.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.joinleave", WebApis.JOINLEAVEOPENGROUP + id);
            Request request = new Request.Builder()
                    .url(WebApis.JOINLEAVEOPENGROUP + id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (SearchForGroup.this == null)
                            return;
                        SearchForGroup.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                            }
                        });

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getMessage", jsonData + "");
                        try {
                            JSONObject jsonObject = new JSONObject(jsonData);
                            String status = jsonObject.getString("status");
                            if (status.equals("success")) {
                                final String message = jsonObject.getString("message");
                                SearchForGroup.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        loaderDiloag.dismissDiloag();
                                        if (!edtSearch.getText().toString().trim().equals("")) {
                                            String text = edtSearch.getText().toString().replace(" ", "%20");
                                            getSearchResult(text);

                                        }
                                        Toast.makeText(SearchForGroup.this, message, Toast.LENGTH_SHORT).show();
                                    }
                                });

                            } else {
                                Toast.makeText(SearchForGroup.this, "Something went wrong ,Try later...", Toast.LENGTH_SHORT).show();
                            }


                            Log.e("json", jsonObject.toString());

                        } catch (Exception e) {
                            SearchForGroup.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loaderDiloag.dismissDiloag();
                                }
                            });
                        }


                    }
                });

            } catch (Exception e) {
                SearchForGroup.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(SearchForGroup.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    public void JoinClosedGroup(String id) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(SearchForGroup.this);
        if (ApplicationGlobles.isConnectingToInternet(SearchForGroup.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            //  gridBeanList.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.JOINVECLOSE", WebApis.JOINLEAVECLOSEGROUP + id);
            Request request = new Request.Builder()
                    .url(WebApis.JOINLEAVECLOSEGROUP + id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (SearchForGroup.this == null)
                            return;
                        SearchForGroup.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                            }
                        });

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getMessage", jsonData + "");
                        try {
                            JSONObject jsonObject = new JSONObject(jsonData);
                            String status = jsonObject.getString("status");
                            if (status.equals("success")) {
                                final String message = jsonObject.getString("message");
                                SearchForGroup.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        loaderDiloag.dismissDiloag();
                                        if (!edtSearch.getText().toString().trim().equals("")) {
                                            String text = edtSearch.getText().toString().replace(" ", "%20");
                                            getSearchResult(text);

                                        }
                                        Toast.makeText(SearchForGroup.this, message, Toast.LENGTH_SHORT).show();
                                    }
                                });

                            } else {
                                Toast.makeText(SearchForGroup.this, "Something went wrong ,Try later...", Toast.LENGTH_SHORT).show();
                            }


                            Log.e("json", jsonObject.toString());

                        } catch (Exception e) {
                            SearchForGroup.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loaderDiloag.dismissDiloag();
                                }
                            });
                        }


                    }
                });

            } catch (Exception e) {
                SearchForGroup.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(SearchForGroup.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }
}

