package com.app.lynkpal;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Adapter.CompanyReviewAdapter;
import com.app.lynkpal.Adapter.GalleryGridAdapter;
import com.app.lynkpal.Adapter.GroupMembersAdapter;
import com.app.lynkpal.Bean.CompanyFollowersBean;
import com.app.lynkpal.Bean.CompanyReviewsBean;
import com.app.lynkpal.Bean.GridBean;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Base64;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.ExpandadHeightGridview;
import com.app.lynkpal.Helper.ExpandedHightListview;
import com.app.lynkpal.Helper.FilePath;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.deleteFollowers;
import com.app.lynkpal.Interface.showImagePopup;
import com.bumptech.glide.Glide;
/*import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;*/
import com.squareup.picasso.Picasso;

import net.karthikraj.shapesimage.ShapesImage;

import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import static android.util.Base64.encodeToString;

public class UserAndCompanyPageDetails extends AppCompatActivity implements View.OnClickListener {
    private static final int REQUEST_CAMERA_ACCESS_PERMISSION = 5674;
    private static final String TAG = UserAndCompanyPageDetails.class.getSimpleName();
    TextView head;
   // GoogleMap mMap;
    String message = "";
    ImageView imgDetails, imgReviews, imgAddress, imgGallery, imgFollowers, imgCover, imgProfile, imgEdit, imgSave, ic_back, follow_unfollow;
    TextView txtDetails, txtReviews, txtAddress, txtGallery, txtFollowers, txtAddMore, txtAddMoreReview;
    TextView txtMainName, txtSubName, txtFollowing, textContent, textLocation, txtSave, txtCancel;
    LinearLayout linDetails, linReviews, linAddress, linGallery, linFollowers, linDesGallery, addMore, linSaveCancel, linDesReviews, linDesFollowers, lin_Location;
    String profile_pic, company_link, company_description = "", mobile, company_lon, followers_count, company_name, address = "", company_lat, industryname, company_banner, follow_status;
    String status;
    RelativeLayout addMoreReview;
    List<GridBean> gridBeanList = new ArrayList<>();
    List<CompanyReviewsBean> companyReviewsArrayList = new ArrayList<CompanyReviewsBean>();
    List<CompanyFollowersBean> companyFollowersBeanList = new ArrayList<CompanyFollowersBean>();
    CompanyReviewsBean companyReviewsBean;
    CompanyFollowersBean companyFollowersBean;
    ExpandadHeightGridview gridView;
    GalleryGridAdapter gridViewAdapter;
    CompanyReviewAdapter companyReviewAdapter;
    // CompanyFollowersAdapter companyFollowersAdapter;
    GridBean gridBean;
    int height, width;
    String encodedString = "";
    FileBody bin;
    ExpandedHightListview expReviewsList, expFollowersList;
    ScrollView mainScrollView;
    String User_id, userProfile;
    SharedPreferences settings;
    SharedPreferences mPref ;
    SharedPreferences.Editor mEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_and_company_page_details);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        settings = getSharedPreferences(Constant.PREFS_NAME, 0);
        userProfile = settings.getString("profile_pic", "");




        //initialize Imageview
        imgDetails = (ImageView) findViewById(R.id.imgDetail);
        imgReviews = (ImageView) findViewById(R.id.imgReviews);
        imgAddress = (ImageView) findViewById(R.id.imgAddress);
        imgGallery = (ImageView) findViewById(R.id.imgGallery);
        imgFollowers = (ImageView) findViewById(R.id.imgFollowers);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        imgCover = (ImageView) findViewById(R.id.imgCover);
        imgEdit = (ImageView) findViewById(R.id.imgEdit);
        imgSave = (ImageView) findViewById(R.id.imgSave);
        ic_back = (ImageView) findViewById(R.id.ic_back);
        follow_unfollow = (ImageView) findViewById(R.id.follow_unfollow);

        //initialize Textview
        head = (TextView) findViewById(R.id.head);
        txtMainName = (TextView) findViewById(R.id.txtMainName);
        txtSubName = (TextView) findViewById(R.id.txtSubName);
        txtFollowing = (TextView) findViewById(R.id.txtFollowing);
        textContent = (TextView) findViewById(R.id.textContent);
        textLocation = (TextView) findViewById(R.id.textLocation);
        txtDetails = (TextView) findViewById(R.id.txtDetails);
        txtReviews = (TextView) findViewById(R.id.txtReviews);
        txtAddress = (TextView) findViewById(R.id.txtAddress);
        txtGallery = (TextView) findViewById(R.id.txtGallery);
        txtFollowers = (TextView) findViewById(R.id.txtFollowers);
        txtSave = (TextView) findViewById(R.id.txtSave);
        txtCancel = (TextView) findViewById(R.id.txtCancel);
        txtAddMore = (TextView) findViewById(R.id.txtAddMore);
        txtAddMoreReview = (TextView) findViewById(R.id.txtAddMoreReview);



        //initialize Layout
        linDetails = (LinearLayout) findViewById(R.id.linDetail);
        linReviews = (LinearLayout) findViewById(R.id.linReviews);
        linAddress = (LinearLayout) findViewById(R.id.linAddress);
        linGallery = (LinearLayout) findViewById(R.id.linGallery);
        linFollowers = (LinearLayout) findViewById(R.id.linFollowers);
        linDesGallery = (LinearLayout) findViewById(R.id.linDesGallery);
        linDesReviews = (LinearLayout) findViewById(R.id.linDesReviews);
        linDesFollowers = (LinearLayout) findViewById(R.id.linDesFollowers);
        lin_Location = (LinearLayout) findViewById(R.id.lin_Location);
        addMore = (LinearLayout) findViewById(R.id.addMore);
        linSaveCancel = (LinearLayout) findViewById(R.id.linSaveCancel);
        addMoreReview = (RelativeLayout) findViewById(R.id.addMoreReview);

        gridView = (ExpandadHeightGridview) findViewById(R.id.grid);
        expReviewsList = (ExpandedHightListview) findViewById(R.id.expReviewsList);
        expFollowersList = (ExpandedHightListview) findViewById(R.id.expFollowersList);
        mainScrollView = (ScrollView) findViewById(R.id.mainScrollView);
        //Change Fonts
        head.setTypeface(tf_bold);
        txtMainName.setTypeface(tf_bold);
        txtSubName.setTypeface(tf_reg);
        txtFollowing.setTypeface(tf_reg);
        textContent.setTypeface(tf_reg);
        textLocation.setTypeface(tf_reg);
        txtDetails.setTypeface(tf_reg);
        txtReviews.setTypeface(tf_reg);
        txtAddress.setTypeface(tf_reg);
        txtGallery.setTypeface(tf_reg);
        txtFollowers.setTypeface(tf_reg);
        txtSave.setTypeface(tf_reg);
        txtCancel.setTypeface(tf_reg);
        txtAddMore.setTypeface(tf_reg);
        txtAddMoreReview.setTypeface(tf_reg);

        //Visible and unvisible
        linDesGallery.setVisibility(View.GONE);
        linDesReviews.setVisibility(View.GONE);
        linDesFollowers.setVisibility(View.GONE);
        lin_Location.setVisibility(View.GONE);
        textContent.setVisibility(View.VISIBLE);


        //Change Color FirstTime
        ChangeColorImageView(imgDetails);
        ChangeColorTextView(txtDetails);
        ChangeColorLinear(linDetails);

        //set OnClickListener
        linDetails.setOnClickListener(this);
        linReviews.setOnClickListener(this);
        linAddress.setOnClickListener(this);
        linGallery.setOnClickListener(this);
        linFollowers.setOnClickListener(this);
        imgEdit.setOnClickListener(this);
        addMore.setOnClickListener(this);
        linSaveCancel.setOnClickListener(this);
        txtSave.setOnClickListener(this);
        txtCancel.setOnClickListener(this);
        txtFollowing.setOnClickListener(this);
        addMoreReview.setOnClickListener(this);
        ic_back.setOnClickListener(this);
        // Picasso.with(getActivity()).load(WebApis.userProfileImage+profile_pic)
        //kishan getDetails();
        // Inflate the layout for this fragment

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;


      




    }


    public void ChangeColorImageView(ImageView imageView) {
        imgDetails.setColorFilter(getResources().getColor(R.color.black_overlay));
        imgReviews.setColorFilter(getResources().getColor(R.color.black_overlay));
        imgAddress.setColorFilter(getResources().getColor(R.color.black_overlay));
        imgGallery.setColorFilter(getResources().getColor(R.color.black_overlay));
        imgFollowers.setColorFilter(getResources().getColor(R.color.black_overlay));
        imageView.setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_IN);

    }

    public void ChangeColorTextView(TextView txtView) {
        txtDetails.setTextColor(getResources().getColor(R.color.black_overlay));
        txtReviews.setTextColor(getResources().getColor(R.color.black_overlay));
        txtAddress.setTextColor(getResources().getColor(R.color.black_overlay));
        txtGallery.setTextColor(getResources().getColor(R.color.black_overlay));
        txtFollowers.setTextColor(getResources().getColor(R.color.black_overlay));
        txtView.setTextColor(Color.parseColor("#ffffff"));

    }

    public void ChangeColorLinear(LinearLayout linView) {
        linDetails.setBackgroundColor(Color.parseColor("#ffffff"));
        linReviews.setBackgroundColor(Color.parseColor("#ffffff"));
        linAddress.setBackgroundColor(Color.parseColor("#ffffff"));
        linGallery.setBackgroundColor(Color.parseColor("#ffffff"));
        linFollowers.setBackgroundColor(Color.parseColor("#ffffff"));
        linView.setBackgroundColor(Color.parseColor("#375cc8"));

    }


    @Override
    protected void onResume()
    {
        super.onResume();
        getDetails();
    }

    @Override
    public void onClick(View view)
    {
        if (view == linDetails) {
            if (!company_description.equals("null"))
            {
                textContent.setText(company_description + "");
            }
            linDesGallery.setVisibility(View.GONE);
            linDesReviews.setVisibility(View.GONE);
            linDesFollowers.setVisibility(View.GONE);
            lin_Location.setVisibility(View.GONE);
            textContent.setVisibility(View.VISIBLE);
            ChangeColorImageView(imgDetails);
            ChangeColorTextView(txtDetails);
            ChangeColorLinear(linDetails);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mainScrollView.fullScroll(ScrollView.FOCUS_UP);
                }
            }, 1000);
        } else if (view == linReviews) {
            linDesGallery.setVisibility(View.GONE);
            linDesReviews.setVisibility(View.VISIBLE);
            linDesFollowers.setVisibility(View.GONE);
            lin_Location.setVisibility(View.GONE);
            textContent.setVisibility(View.GONE);
            ChangeColorImageView(imgReviews);
            ChangeColorTextView(txtReviews);
            ChangeColorLinear(linReviews);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mainScrollView.fullScroll(ScrollView.FOCUS_UP);
                }
            }, 1000);

        } else if (view == linAddress) {
            if (!address.equals("null")) {
                textLocation.setText(address + "");
            }
            linDesGallery.setVisibility(View.GONE);
            linDesReviews.setVisibility(View.GONE);
            linDesFollowers.setVisibility(View.GONE);
            textContent.setVisibility(View.GONE);
            lin_Location.setVisibility(View.VISIBLE);
            ChangeColorImageView(imgAddress);
            ChangeColorTextView(txtAddress);
            ChangeColorLinear(linAddress);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mainScrollView.fullScroll(ScrollView.FOCUS_UP);
                }
            }, 1000);

        } else if (view == linGallery) {
            linDesGallery.setVisibility(View.VISIBLE);
            linDesReviews.setVisibility(View.GONE);
            linDesFollowers.setVisibility(View.GONE);
            lin_Location.setVisibility(View.GONE);
            textContent.setVisibility(View.GONE);
            ChangeColorImageView(imgGallery);
            ChangeColorTextView(txtGallery);
            ChangeColorLinear(linGallery);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mainScrollView.fullScroll(ScrollView.FOCUS_UP);
                }
            }, 1000);
        } else if (view == linFollowers) {
            linDesGallery.setVisibility(View.GONE);
            linDesReviews.setVisibility(View.GONE);
            linDesFollowers.setVisibility(View.VISIBLE);
            textContent.setVisibility(View.GONE);
            ChangeColorImageView(imgFollowers);
            ChangeColorTextView(txtFollowers);
            ChangeColorLinear(linFollowers);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mainScrollView.fullScroll(ScrollView.FOCUS_UP);
                }
            }, 1000);
        } else if (view == txtFollowers) {

        } else if (view == imgEdit) {
            startActivity(new Intent(UserAndCompanyPageDetails.this, UpdateCompanyProfile.class));
        } else if (view == addMore) {
           // selectImage();
        } else if (view == txtCancel) {
            linSaveCancel.setVisibility(View.GONE);
            addMore.setVisibility(View.VISIBLE);
            bin = null;
            encodedString = "";
        } else if (view == txtSave) {
          //  ImageUploader();
        } else if (view == addMoreReview) {
            //showPopup();

        } else if (view == ic_back) {
            finish();
        } else if (view == txtFollowing)
        {
           // followUnfollow();
        }
    }





    public void getDetails() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(UserAndCompanyPageDetails.this);
        if (ApplicationGlobles.isConnectingToInternet(UserAndCompanyPageDetails.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            status = "false";
            Log.e("Constant.token", Constant.token);
            Log.e("Constant.api", WebApis.CompanyDetails + User_id);
            Request request = new Request.Builder()
                    .url(WebApis.CompanyDetails + User_id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (UserAndCompanyPageDetails.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.d(TAG, "onResponse: All Details " + jsonData);
                        loaderDiloag.dismissDiloag();
                        try {
                            JSONObject jsonObject = new JSONObject(jsonData);
                            status = jsonObject.getString("status");
                            if (status.equals("success"))
                            {
                               follow_status = jsonObject.getString("follow_status");
                               if (follow_status.equals("1"))
                               {
                                   Log.d(TAG, "onResponse: good");
                                   txtFollowing.setText("Following");
                               }
                               else
                               {
                                   txtFollowing.setText("Follow");
                               }
                               profile_pic = jsonObject.getString("profile_pic");
                               company_link = jsonObject.getString("company_link");
                               company_description = jsonObject.getString("company_description");
                               mobile = jsonObject.getString("mobile");
                               company_lon = jsonObject.getString("company_lon");
                               followers_count = jsonObject.getString("followers_count");
                               company_name = jsonObject.getString("company_name");
                                Log.d(TAG, "onResponse: Company name " + company_name);
                                txtMainName.setText(company_name);
                               if (!company_name.isEmpty())
                               {

                               }

                                address = jsonObject.getString("address");
                                company_lat = jsonObject.getString("company_lat");
                                industryname = jsonObject.getString("industryname");
                                company_banner = jsonObject.getString("company_banner");
                            }
                        } catch (Exception e) {

                        }




                    }
                });

            } catch (Exception e) {
                UserAndCompanyPageDetails.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(UserAndCompanyPageDetails.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }






















}
