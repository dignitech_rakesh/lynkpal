package com.app.lynkpal.Interface;

import com.app.lynkpal.Bean.LynkaplLearningModel;
import com.app.lynkpal.Bean.MyTrainingModel;

public interface MyTrainingInterface {
    public void editTraining(String id, int pos, MyTrainingModel myTrainingModel);
    public void deleteTraining(String id,int pos);
    public void detailTraining(String id,int pos, MyTrainingModel myTrainingModel);
    public void detailAllTraining(String id,int pos, LynkaplLearningModel allTrainingModel);
}
