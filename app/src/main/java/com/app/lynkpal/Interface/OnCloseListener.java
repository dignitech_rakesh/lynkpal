package com.app.lynkpal.Interface;

public interface OnCloseListener
{
   public void onCloseClick(int id);
   public void onFriendAdd(String id);
}
