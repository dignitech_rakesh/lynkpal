package com.app.lynkpal.Interface;

import com.app.lynkpal.Bean.CommentsBean;

public interface InterCmntRply {
    public void likeComment(int comment_id);
    public void replyClick(int comment_id, CommentsBean group);
}
