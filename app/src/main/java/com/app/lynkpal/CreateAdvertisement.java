package com.app.lynkpal;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.FilePath;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import static android.media.CamcorderProfile.get;

public class CreateAdvertisement extends AppCompatActivity {
    TextView head;
    EditText edtName, edtAddTags, edtAddLink;
    TextView startLabel, startDate, endLabel, endDate, txtChrgPerDay, txtChrgPerDayLabel, txtTotalPayAmount, txtTotalPayAmountLabel;
    Button btnCreateAndPay, btnBrowse;
    DatePicker picker;
    ImageView imgImage, ic_back;
    Calendar myCalendarstart, myCalendarend;
    Intent pickPhoto;
    String encodedString = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_advertisment);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        head = (TextView) findViewById(R.id.head);
        edtName = (EditText) findViewById(R.id.edtName);
        edtAddTags = (EditText) findViewById(R.id.edtAddTags);
        //edtBrowse = (EditText) findViewById(R.id.edtBrowse);
        edtAddLink = (EditText) findViewById(R.id.edtAddLink);
        startLabel = (TextView) findViewById(R.id.startLabel);
        startDate = (TextView) findViewById(R.id.startDate);
        endLabel = (TextView) findViewById(R.id.endLabel);
        endDate = (TextView) findViewById(R.id.endDate);
        imgImage = (ImageView) findViewById(R.id.imgImage);
        txtChrgPerDay = (TextView) findViewById(R.id.txtChrgPerDay);
        txtChrgPerDayLabel = (TextView) findViewById(R.id.txtChrgPerDayLabel);
        txtTotalPayAmount = (TextView) findViewById(R.id.txtTotalPayAmount);
        txtTotalPayAmountLabel = (TextView) findViewById(R.id.txtTotalPayAmountLabel);
        btnCreateAndPay = (Button) findViewById(R.id.btnCreateAndPay);
        btnBrowse = (Button) findViewById(R.id.btnBrowse);
        ic_back = (ImageView) findViewById(R.id.ic_back);
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnBrowse.setTypeface(tf_bold);
        head.setTypeface(tf_bold);
        btnCreateAndPay.setTypeface(tf_bold);
        edtName.setTypeface(tf_reg);
        edtAddTags.setTypeface(tf_reg);
        edtAddLink.setTypeface(tf_reg);
        // edtBrowse.setTypeface(tf_reg);
        startLabel.setTypeface(tf_reg);
        startDate.setTypeface(tf_reg);
        endLabel.setTypeface(tf_reg);
        endDate.setTypeface(tf_reg);
        txtChrgPerDay.setTypeface(tf_reg);
        txtChrgPerDayLabel.setTypeface(tf_reg);
        txtTotalPayAmount.setTypeface(tf_reg);
        txtTotalPayAmountLabel.setTypeface(tf_reg);
        myCalendarstart = Calendar.getInstance();
        myCalendarend = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener datestart = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendarstart.set(Calendar.YEAR, year);
                myCalendarstart.set(Calendar.MONTH, monthOfYear);
                myCalendarstart.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateStartLabel();
            }

        };
        final DatePickerDialog.OnDateSetListener dateend = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendarend.set(Calendar.YEAR, year);
                myCalendarend.set(Calendar.MONTH, monthOfYear);
                myCalendarend.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                //Set yesterday time milliseconds as date pickers minimum date
                updateEndLabel();
            }

        };
        startDate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                /*new DatePickerDialog(CreateAdvertisement.this, datestart, myCalendarstart.get(Calendar.YEAR), myCalendarstart.get(Calendar.MONTH),
                        myCalendarstart.get(Calendar.DAY_OF_MONTH)).getDatePicker().setMinDate(System.currentTimeMillis() - 1000)
                        */
                DatePickerDialog datePickerDialog = new DatePickerDialog(CreateAdvertisement.this, datestart, myCalendarstart
                        .get(Calendar.YEAR), myCalendarstart.get(Calendar.MONTH),
                        myCalendarstart.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(myCalendarstart.getTimeInMillis());
                datePickerDialog.show();

            }
        });
        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (startDate.getText().toString().toLowerCase().contains("select")) {
                    Toast.makeText(CreateAdvertisement.this, "Please select start date first", Toast.LENGTH_SHORT).show();
                } else {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(CreateAdvertisement.this, dateend, myCalendarend
                            .get(Calendar.YEAR), myCalendarend.get(Calendar.MONTH),
                            myCalendarend.get(Calendar.DAY_OF_MONTH));
                    datePickerDialog.getDatePicker().setMinDate(myCalendarstart.getTimeInMillis());
                    datePickerDialog.show();
                }
                //  new DatePickerDialog(CreateAdvertisement.this, dateend, )
                // .getDatePicker().setMinDate(calendar.getTimeInMillis()).show();
            }
        });
        btnCreateAndPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = edtName.getText().toString();
                String tags = edtAddTags.getText().toString();
                String link = edtAddLink.getText().toString();
                String startdate = startDate.getText().toString();
                String enddate = endDate.getText().toString();

                if (name.trim().length() > 0) {
                    if (tags.trim().length() > 0) {
                        if (link.trim().length() > 0) {
                            if (!startdate.toLowerCase().contains("start")) {
                                if (!enddate.toLowerCase().contains("end")) {
                                    if (!startdate.toLowerCase().equals(enddate.toLowerCase())) {
                                        if (encodedString!=null)
                                        {
                                            CreateAdvertisment(name, tags, link, parseDateToddMMyyyy(startdate), parseDateToddMMyyyy(enddate));
                                        }
                                        else {
                                            Toast.makeText(CreateAdvertisement.this, "Please Select a Image", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(CreateAdvertisement.this, "Please Select at least 1 or more then 1 days", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(CreateAdvertisement.this, "Please Select End Date", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(CreateAdvertisement.this, "Please Select Start Date", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            edtAddLink.setError("Required");
                        }
                    } else {
                        edtAddTags.setError("Required");
                    }
                } else {
                    edtName.setError("Required");
                }
                //showPopup();
            }
        });


        btnBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED || checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.v("TAG", "Permission is granted");
                        pickPhoto = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, 0);//one can be replaced with any action code

                    } else {

                        Log.v("TAG", "Permission is revoked");
                        ActivityCompat.requestPermissions(CreateAdvertisement.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                    }
                } else {

                    Log.v("TAG", "Permission is granted");
                    pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 0);//one can be replaced with any action code
                }
            }
        });
        getChargeDetails();
    }

    public void CreateAdvertisment(String name, String tags, String link, String startdate, String enddate) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(CreateAdvertisement.this);

        if (ApplicationGlobles.isConnectingToInternet(CreateAdvertisement.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            //String params="{\r\n\"description\":\"Write text to post\",\r\n\"postFile\":\"sadc\",\r\n\"extension\":\"jpeg\"\r\n}";
            JSONObject params = new JSONObject();
            try {
                params.put("name", name);
                params.put("tags", tags);
                params.put("link", link);
                params.put("startdate", startdate);
                params.put("enddate", enddate);
                params.put("adPic", encodedString);
                Log.e("param", params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(mediaType, params + "");
            Request request = new Request.Builder()
                    .url(WebApis.ADDADV)
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (CreateAdvertisement.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                encodedString = null;
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            showPopup();
                                        }
                                    });
                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(CreateAdvertisement.this, "Something went wrong try later...", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                        loaderDiloag.dismissDiloag();
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(CreateAdvertisement.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "MM/dd/yy";
        String outputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public void getChargeDetails() {
        if (ApplicationGlobles.isConnectingToInternet(CreateAdvertisement.this)) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(WebApis.GETPLANS)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (CreateAdvertisement.this == null)
                            return;
                        // loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response", jsonData + "");
                        if (jsonData.length() > 0) {
                            try {
                                JSONArray array = new JSONArray(jsonData);
                                if (array.length() > 0) {
                                   /* for (int i = 0; i < array.length(); i++) {*/
                                    JSONObject jsonObject1 = array.getJSONObject(1);
                                    final String amount = jsonObject1.getString("amount");
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            txtChrgPerDay.setText("$" + amount);
                                            txtTotalPayAmount.setText("$" + amount);
                                        }
                                    });
                                    //   String id=jsonObject1.getString("id");
                                    // }

                                }

                            } catch (Exception je) {
                                je.printStackTrace();
                            }
                        } else {
                            // loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //swipeRefreshLayout.setRefreshing(false);
                        //loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(CreateAdvertisement.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            try {
                Uri selectedFileUri = data.getData();
                Picasso.with(CreateAdvertisement.this).load(selectedFileUri.toString()).into(imgImage);
                imgImage.setVisibility(View.VISIBLE);
                String selectedFilePath = FilePath.getPath(CreateAdvertisement.this, selectedFileUri);
                // File file = new File(selectedFilePath);
                //bin = new FileBody(file);
                InputStream inputStream = new FileInputStream(selectedFilePath);//You can get an inputStream using any IO API
                byte[] bytes;
                byte[] buffer = new byte[8192];
                int bytesRead;
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        output.write(buffer, 0, bytesRead);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                bytes = output.toByteArray();
                encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);
                Log.e("encodedString", encodedString);

            } catch (Exception e) {
                Log.e("eroore", e.toString());
                e.printStackTrace();
            }
        }

    }

    private void updateStartLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        startDate.setText(sdf.format(myCalendarstart.getTime()));
    }

    private void updateEndLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        endDate.setText(sdf.format(myCalendarend.getTime()));

        int diff = getCountOfDays(startDate.getText().toString(), endDate.getText().toString());
        if (diff > 0) {
            String ontDay = txtChrgPerDay.getText().toString().replace("$", "");
            float amount = Float.parseFloat(ontDay) * diff;
            txtTotalPayAmount.setText("$" + amount);
        } else {
            Toast.makeText(this, "Please Select at least 1 or more then 1 days", Toast.LENGTH_SHORT).show();
        }
        Log.e("days", getCountOfDays(startDate.getText().toString(), endDate.getText().toString()) + "");
    }

    public void showPopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.custom_dialog_postjob, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        TextView textView = (TextView) popupView.findViewById(R.id.txtText);
        textView.setTypeface(tf_med);
        textView.setText("Advertisement has been created and published successfully");
        Button button = (Button) popupView.findViewById(R.id.btnOk);
        button.setTypeface(tf_bold);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                finish();
            }
        });
    }


    public int getCountOfDays(String createdDateString, String expireDateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy", Locale.getDefault());

        Date createdConvertedDate = null, expireCovertedDate = null, todayWithZeroTime = null;
        try {
            createdConvertedDate = dateFormat.parse(createdDateString);
            expireCovertedDate = dateFormat.parse(expireDateString);

            Date today = new Date();

            todayWithZeroTime = dateFormat.parse(dateFormat.format(today));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int cYear = 0, cMonth = 0, cDay = 0;

        if (createdConvertedDate.after(todayWithZeroTime)) {
            Calendar cCal = Calendar.getInstance();
            cCal.setTime(createdConvertedDate);
            cYear = cCal.get(Calendar.YEAR);
            cMonth = cCal.get(Calendar.MONTH);
            cDay = cCal.get(Calendar.DAY_OF_MONTH);

        } else {
            Calendar cCal = Calendar.getInstance();
            cCal.setTime(todayWithZeroTime);
            cYear = cCal.get(Calendar.YEAR);
            cMonth = cCal.get(Calendar.MONTH);
            cDay = cCal.get(Calendar.DAY_OF_MONTH);
        }


    /*Calendar todayCal = Calendar.getInstance();
    int todayYear = todayCal.get(Calendar.YEAR);
    int today = todayCal.get(Calendar.MONTH);
    int todayDay = todayCal.get(Calendar.DAY_OF_MONTH);
    */

        Calendar eCal = Calendar.getInstance();
        eCal.setTime(expireCovertedDate);

        int eYear = eCal.get(Calendar.YEAR);
        int eMonth = eCal.get(Calendar.MONTH);
        int eDay = eCal.get(Calendar.DAY_OF_MONTH);

        Calendar date1 = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();

        date1.clear();
        date1.set(cYear, cMonth, cDay);
        date2.clear();
        date2.set(eYear, eMonth, eDay);

        long diff = date2.getTimeInMillis() - date1.getTimeInMillis();

        float dayCount = (float) diff / (24 * 60 * 60 * 1000);

        return ((int) dayCount);
    }
}
