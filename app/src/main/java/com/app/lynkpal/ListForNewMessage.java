package com.app.lynkpal;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Adapter.ListNewMessageAdapter;
import com.app.lynkpal.Bean.CompanyFollowersBean;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.WebApis;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class ListForNewMessage extends AppCompatActivity {
    ImageView imgBack;
    ProgressBar progress;
    TextView txtNoResult;
    ListView listSearch;
    ListNewMessageAdapter companyFollowersAdapter;
    EditText edtSearch;
    List<CompanyFollowersBean> companyFollowersBeanList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_for_new_message);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        progress = (ProgressBar) findViewById(R.id.progress);
        txtNoResult = (TextView) findViewById(R.id.txtNoResult);
        listSearch = (ListView) findViewById(R.id.listSearch);
        edtSearch = (EditText) findViewById(R.id.edtSearch);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        edtSearch.setTypeface(tf_bold);
        txtNoResult.setTypeface(tf_med);
        progress.setVisibility(View.GONE);
        txtNoResult.setVisibility(View.INVISIBLE);
        // Capture Text in EditText
        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                try {
                    String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                    companyFollowersAdapter.filter(text);
                } catch (Exception e) {
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });
        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edtSearch.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    return true;
                }
                return false;
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(0, 0);

            }
        });
        try {
            edtSearch.setFocusable(true);
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(edtSearch, InputMethodManager.SHOW_IMPLICIT);
        } catch (Exception e) {

            Log.e("error", "r" + e.toString());
        }
        getAllUserResult();
    }

    public void getAllUserResult() {
        // final LoaderDiloag loaderDiloag = new LoaderDiloag(SearchActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(this)) {
            //  loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            progress.setVisibility(View.VISIBLE);
            txtNoResult.setVisibility(View.INVISIBLE);
            companyFollowersBeanList.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.ALLUSERS", WebApis.ALLUSERS);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.ALLUSERS)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (this == null)
                            return;
                        progress.setVisibility(View.GONE);

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getDetails", jsonData + "");
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {
                                JSONObject jsonObject = array.getJSONObject(a);
                                CompanyFollowersBean companyFollowersBean = new CompanyFollowersBean();
                                companyFollowersBean.setFullName(jsonObject.getString("first_name") + " " + jsonObject.getString("last_name"));
                                companyFollowersBean.setProfilePic(jsonObject.getString("profile_pic"));
                                companyFollowersBean.setUserid(jsonObject.getString("id"));
                                companyFollowersBean.setDescription(jsonObject.getString("email"));
                                companyFollowersBeanList.add(companyFollowersBean);
                            }

                        } catch (Exception e) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress.setVisibility(View.GONE);
                                }
                            });
                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progress.setVisibility(View.GONE);
                                if (companyFollowersBeanList.size() > 0) {
                                    companyFollowersAdapter = new ListNewMessageAdapter(ListForNewMessage.this, companyFollowersBeanList);
                                    listSearch.setAdapter(companyFollowersAdapter);
                                } else {
                                    txtNoResult.setVisibility(View.VISIBLE);
                                }
                            }
                        });


                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress.setVisibility(View.GONE);

                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
