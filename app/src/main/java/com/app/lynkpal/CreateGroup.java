package com.app.lynkpal;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Adapter.MultipleSelectorPeoples;
import com.app.lynkpal.Bean.AllUsersBean;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Base64;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.FilePath;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.SelectInterface;
import com.squareup.picasso.Picasso;

import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import static android.util.Base64.encodeToString;

public class CreateGroup extends AppCompatActivity implements SelectInterface {
    private static final String TAG = CreateGroup.class.getSimpleName() ;
    private final int REQUEST_CAMERA_ACCESS_PERMISSION = 5674;
    TextView head, Type, edtFriends;
    ImageView ic_edit, ic_back, ic_image;
    EditText edtName;
    String encodedString = null;
    FileBody bin;
    RadioButton radOpen, radClosed;
    Button btnBrowse, btnPostJob;
    MultipleSelectorPeoples customAdapter;
    List<AllUsersBean> list = new ArrayList<>();
    AllUsersBean allUsersBean;
    String add = "";
    String name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);
        head = (TextView) findViewById(R.id.head);
        Type = (TextView) findViewById(R.id.Type);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        head.setTypeface(tf_bold);
        Type.setTypeface(tf_med);
        ic_back = (ImageView) findViewById(R.id.ic_back);
        ic_image = (ImageView) findViewById(R.id.imgImage);
        edtName = (EditText) findViewById(R.id.edtName);
        edtFriends = (TextView) findViewById(R.id.edtFriends);
        radOpen = (RadioButton) findViewById(R.id.radOpen);
        radClosed = (RadioButton) findViewById(R.id.radClosed);
        btnBrowse = (Button) findViewById(R.id.btnBrowse);
        btnPostJob = (Button) findViewById(R.id.btnPostJob);
        edtName.setTypeface(tf_reg);
        edtFriends.setTypeface(tf_reg);
        edtFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup();
            }
        });
        radOpen.setTypeface(tf_reg);
        radClosed.setTypeface(tf_reg);
        btnPostJob.setTypeface(tf_bold);
        btnBrowse.setTypeface(tf_bold);
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
        btnPostJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = edtName.getText().toString();
                String type = "";
                if (radOpen.isChecked()) {
                    type = "open";
                } else {
                    type = "closed";
                }

                if (!ApplicationGlobles.isNullOrEmpty(name.trim())) {
                    if (!add.equals("")) {
                        CreateGroup(name, type, add);
                    } else {
                        Toast.makeText(CreateGroup.this, "Please select at least 1 or more friends.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    edtName.setError("Group Name can't be Empty");
                }

            }
        });
        getUsers();
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateGroup.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                            && ActivityCompat.checkSelfPermission(CreateGroup.this, Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                REQUEST_CAMERA_ACCESS_PERMISSION);
                    } else {
                        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePicture.resolveActivity(CreateGroup.this.getPackageManager()) != null) {
                            startActivityForResult(takePicture, 1);
                        }
                    }

                } else if (options[item].equals("Choose from Gallery")) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        if (CreateGroup.this.checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                                == PackageManager.PERMISSION_GRANTED || CreateGroup.this.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            Log.v("TAG", "Permission is granted");
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            //  Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(photoPickerIntent, 2);//one can be replaced with any action code

                        } else {

                            Log.v("TAG", "Permission is revoked");
                            ActivityCompat.requestPermissions(CreateGroup.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                        }
                    } else {

                        Log.v("TAG", "Permission is granted");
                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                        photoPickerIntent.setType("image/*");
                        //  Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(photoPickerIntent, 2);//one can be replaced with any action code
                    }

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void getUsers() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(CreateGroup.this);
        if (ApplicationGlobles.isConnectingToInternet(CreateGroup.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            list.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.ALLUSERS", WebApis.ALLUSERS);
            Request request = new Request.Builder()
                    .url(WebApis.ALLUSERS)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (CreateGroup.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.d(TAG, "onResponse: getAll Users " + jsonData);
                        loaderDiloag.dismissDiloag();
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {

                                JSONObject jsonObject = array.getJSONObject(a);
                                allUsersBean = new AllUsersBean();
                                allUsersBean.setId(jsonObject.getString("id"));
                                allUsersBean.setUsername(jsonObject.getString("username"));
                                allUsersBean.setFirst_name(jsonObject.getString("first_name"));
                                allUsersBean.setLast_name(jsonObject.getString("last_name"));
                                allUsersBean.setEmail(jsonObject.getString("email"));
                                allUsersBean.setProfile_pic(jsonObject.getString("profile_pic"));
                                allUsersBean.setSelected(false);
                                list.add(allUsersBean);
                            }


                        } catch (Exception e) {

                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                               /* loaderDiloag.dismissDiloag();
                                if (list.size() > 0) {
                                    advertismentAdapter = new AdvertismentAdapter(CreateGroup.this, advertisementsBeanList, Advertisements.this, Advertisements.this);
                                    list.setAdapter(advertismentAdapter);
                                }*/
                            }
                        });


                    }
                });

            } catch (Exception e) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void showPopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_add_peoples, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        Typeface tf = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        TextView textView = (TextView) popupView.findViewById(R.id.txtMain);
        textView.setTypeface(tf);
        Button btnOK = (Button) popupView.findViewById(R.id.btnOK);
        btnOK.setTypeface(tf);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        btnCancel.setTypeface(tf);
        ListView listView = (ListView) popupView.findViewById(R.id.list);
        Log.e("list", list.size() + "");

        customAdapter = new MultipleSelectorPeoples(CreateGroup.this, list, this);
        listView.setAdapter(customAdapter);
        // customAdapter.notifyDataSetChanged();
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add = "";
                name = "";
                for (int a = 0; a < list.size(); a++) {
                    if (list.get(a).getSelected()) {
                        if (add.equals("")) {
                            add = list.get(a).getId();
                        } else {
                            add = add + "," + list.get(a).getId();
                        }
                        if (name.equals("")) {
                            name = list.get(a).getFirst_name();
                        } else {
                            name = name + "," + list.get(a).getFirst_name();
                        }
                    }

                }
                if (name.equals("")) {
                    edtFriends.setText("Add Peoples");

                } else {
                    edtFriends.setText(name);
                }
                popupWindow.dismiss();

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
    }

    public void CreateGroup(String name, String grouptype, String members) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(CreateGroup.this);

        if (ApplicationGlobles.isConnectingToInternet(CreateGroup.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            //String params="{\r\n\"description\":\"Write text to post\",\r\n\"postFile\":\"sadc\",\r\n\"extension\":\"jpeg\"\r\n}";
            JSONObject params = new JSONObject();
            try {
                params.put("name", name);
                params.put("grouptype", grouptype);
                params.put("members", members);
                params.put("groupPic", encodedString);
                Log.e("param", params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(mediaType, params + "");
            Request request = new Request.Builder()
                    .url(WebApis.CREATEGROUP)
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (CreateGroup.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("responseCreateGroup", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                encodedString = null;
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                final String message = jsonObject.getString("message");
                                if (status.equals("success")) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(CreateGroup.this, message + "", Toast.LENGTH_SHORT).show();
                                            Constant.update_myCreatedGroup = true;
                                            finish();
                                        }
                                    });
                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(CreateGroup.this, "Something went wrong try later...", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                        loaderDiloag.dismissDiloag();
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(CreateGroup.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Bundle extras = data.getExtras();
                Bitmap bitmap = (Bitmap) extras.get("data");
                encodedString = convertPreImagetBitmapToString(bitmap);
                ic_image.setImageBitmap(bitmap);
                File file = new File(String.valueOf(bitmap));
                bin = new FileBody(file);
            } else if (requestCode == 2) {
                try {
                    Uri selectedFileUri = data.getData();
                    Picasso.with(CreateGroup.this).load(selectedFileUri.toString()).into(ic_image);
                    String selectedFilePath = FilePath.getPath(CreateGroup.this, selectedFileUri);
                    File file = new File(selectedFilePath);
                    bin = new FileBody(file);
                    InputStream inputStream = new FileInputStream(selectedFilePath);//You can get an inputStream using any IO API
                    byte[] bytes;
                    byte[] buffer = new byte[8192];
                    int bytesRead;
                    ByteArrayOutputStream output = new ByteArrayOutputStream();
                    try {
                        while ((bytesRead = inputStream.read(buffer)) != -1) {
                            output.write(buffer, 0, bytesRead);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    bytes = output.toByteArray();
                    encodedString = encodeToString(bytes, android.util.Base64.DEFAULT);
                    Log.e("encodedString", encodedString);

                } catch (Exception e) {
                    Log.e("eroore", e.toString());
                    e.printStackTrace();
                }
            }
        }
    }

    public String convertPreImagetBitmapToString(Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream); //compress to which format you want.
        byte[] byte_arr = stream.toByteArray();
        String attachmentStr = Base64.encodeBytes(byte_arr);
        Log.e("imageStr", attachmentStr);
        // showPopup(ChatActivity.this, attachmentStr, bmp);
        return attachmentStr;

    }

    @Override
    public void select(String position, int yes) {
        if (yes == 1) {
            list.get(Integer.parseInt(position)).setSelected(true);
            customAdapter.notifyDataSetChanged();
        } else {
            list.get(Integer.parseInt(position)).setSelected(false);
            customAdapter.notifyDataSetChanged();
        }
    }
}
