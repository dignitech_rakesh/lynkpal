package com.app.lynkpal;

import android.app.DownloadManager;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.app.lynkpal.Helper.LoaderDiloag;

import java.io.File;

public class VideoActivity extends AppCompatActivity {
    private static final String TAG = VideoActivity.class.getSimpleName();
    TextView mainShare;
    LoaderDiloag loaderDiloag;
    VideoView vidView;
    String FileURl;
    ImageView ic_close, ic_download;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        Log.d(TAG, "onCreate: VideoActivity");

        FileURl = getIntent().getStringExtra("url");
        vidView = (VideoView) findViewById(R.id.myVideo);
        findViewById(R.id.ic_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.ic_download).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                file_download(FileURl);
            }
        });
        loaderDiloag = new LoaderDiloag(VideoActivity.this);
        loaderDiloag.setCancelable(true);
        loaderDiloag.displayDiloag();
        vidView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                if (loaderDiloag.isShowing())
                    loaderDiloag.dismissDiloag();
            }
        });

      /*  new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (loaderDiloag.isShowing())
                    loaderDiloag.dismissDiloag();
            }
        }, 7000);*/
        MediaController vidControl = new MediaController(this);
        vidControl.setAnchorView(vidView);
        vidView.setMediaController(vidControl);
        vidView.setVideoPath(FileURl);
        vidView.start();
    }

    public void file_download(String uRl) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/lynkpal");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        DownloadManager mgr = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        Uri downloadUri = Uri.parse(uRl);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("Lynkpal")
                .setDescription("Video Downloading...")
                .setDestinationInExternalPublicDir("/lynkpal", "video.mp4");

        mgr.enqueue(request);

        Toast.makeText(this, "Video will Successfully Download", Toast.LENGTH_SHORT).show();

    }

}
