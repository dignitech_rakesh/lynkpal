package com.app.lynkpal;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.lynkpal.Adapter.CareerAdviceAdapter;
import com.app.lynkpal.Adapter.LynkpalLearningListAdapter;
import com.app.lynkpal.Adapter.MyTrainingListAdapter;
import com.app.lynkpal.Bean.CareerAdviceModel;
import com.app.lynkpal.Bean.JobCreatedListBean;
import com.app.lynkpal.Bean.LynkaplLearningModel;
import com.app.lynkpal.Bean.MyTrainingModel;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.ExpandedHightListview;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.MyTrainingInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Callback;
import okhttp3.OkHttpClient;

public class LynkpalLearning extends AppCompatActivity implements MyTrainingInterface {

    private TextView txtAllTraining,txtMyTraining;
    FloatingActionButton floatAddTraining;

    //ExpandedHightListview listLynkpalLearning;
    RecyclerView rvAllTraining,rvMyTraining;
    List<LynkaplLearningModel> lynkpalLearningList = new ArrayList<LynkaplLearningModel>();
    List<MyTrainingModel> myTrainingList = new ArrayList<MyTrainingModel>();

    SharedPreferences sharedPreferences;
    private MyTrainingListAdapter myTrainingListAdapter;
    private int agreement_status= -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lynkpal_learning);
        sharedPreferences = LynkpalLearning.this.getSharedPreferences(Constant.PREFS_NAME, 0);

       // listLynkpalLearning=(ExpandedHightListview)findViewById(R.id.listLynkpalLearning);
        rvAllTraining=(RecyclerView)findViewById(R.id.rvAllTraining);
        rvMyTraining=(RecyclerView)findViewById(R.id.rvMyTraining);
        txtAllTraining=(TextView) findViewById(R.id.txtAllTraining);
        txtMyTraining=(TextView) findViewById(R.id.txtMyTraining);
        floatAddTraining = (FloatingActionButton) findViewById(R.id.floatAddTraining);

        floatAddTraining.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LynkpalLearning.this, AddNewTraining.class).putExtra("type","Add"));
            }
        });


        txtAllTraining.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view)
            {
                txtAllTraining.setBackground(getResources().getDrawable(R.drawable.left_dashboard));
                txtMyTraining.setBackground(null);
                floatAddTraining.setVisibility(View.GONE);
                txtAllTraining.setTextColor(Color.WHITE);
                txtMyTraining.setTextColor(Color.parseColor("#375cc8"));
                rvAllTraining.setVisibility(View.VISIBLE);
                rvMyTraining.setVisibility(View.GONE);
            }
        });

        txtMyTraining.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view)
            {

                if (sharedPreferences.getString("premiumMember", "").equalsIgnoreCase("yes")) {
                    //tv_upgradeToPremium.setVisibility(View.GONE);
                    //showPopup("",-1,"Alert","Alert!! Del","This service is not available for you");

                    if (agreement_status==1){

                        txtMyTraining.setBackground(getResources().getDrawable(R.drawable.right_dashboard));
                        txtAllTraining.setBackground(null);
                        floatAddTraining.setVisibility(View.VISIBLE);
                        txtMyTraining.setTextColor(Color.WHITE);
                        txtAllTraining.setTextColor(Color.parseColor("#375cc8"));
                        rvAllTraining.setVisibility(View.GONE);
                        rvMyTraining.setVisibility(View.VISIBLE);

                    }else {

                        String msg= "Hi "+ sharedPreferences.getString("firstname","") + sharedPreferences.getString("lastname","") +
                                getResources().getString(R.string.mytraining_string);
                        showPopup("",-1,"Alert","Alert !!",msg);

                    }
                } else{
                   // tv_upgradeToPremium.setVisibility(View.VISIBLE);

                    showPopup("",-1,"Alert","Alert!!","Please Upgrade your membership");

                }


               /* txtMyTraining.setBackground(getResources().getDrawable(R.drawable.right_dashboard));
                txtAllTraining.setBackground(null);
                floatAddTraining.setVisibility(View.VISIBLE);
                txtMyTraining.setTextColor(Color.WHITE);
                txtAllTraining.setTextColor(Color.parseColor("#375cc8"));
                rvAllTraining.setVisibility(View.GONE);
                rvMyTraining.setVisibility(View.VISIBLE);*/
                //getApplied();
            }
        });

        //getAllTrainingApi();
        //getMyTrainingApi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ApplicationGlobles.isConnectingToInternet(LynkpalLearning.this)) {

            getMyTrainingStatusApi();


            lynkpalLearningList.clear();
            getAllTrainingApi();
            myTrainingList.clear();
            getMyTrainingApi();
        }else {
            Toast.makeText(LynkpalLearning.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }
    }

    private void getAllTrainingApi() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(LynkpalLearning.this);
        if (ApplicationGlobles.isConnectingToInternet(LynkpalLearning.this)) {

            loaderDiloag.displayDiloag();

            com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(LynkpalLearning.this);
      /*  Map<String, String> postParam = new HashMap<String, String>();
        postParam.put("toFollow", user);

        Log.d("Training", "frndParam " + postParam.toString());*/


            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                    WebApis.ALLTRAINING, null,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {
                            loaderDiloag.dismissDiloag();
                            Log.d("Training", "TrainingResponse " + response.toString());
                            try {
                                //String status = response.getString("status");
                                // String message = response.getString("message");
                                // if (status.contains("success"))
                                if (response.length() > 0) {
                                    // Loop through the array elements
                                    for (int i = 0; i < response.length(); i++) {
                                        // Get current json object
                                        LynkaplLearningModel lynkaplLearningModel = new LynkaplLearningModel();
                                        JSONObject jsonObject = response.getJSONObject(i);

                                        // Get the current student (json object) data
                                        lynkaplLearningModel.setId(jsonObject.getInt("id"));
                                        lynkaplLearningModel.setName(jsonObject.getString("name"));
                                        lynkaplLearningModel.setDuration(jsonObject.getInt("duration"));
                                        lynkaplLearningModel.setLectures(jsonObject.getInt("lectures"));
                                        lynkaplLearningModel.setSpecialization_name(jsonObject.getString("specialization_name"));
                                        lynkaplLearningModel.setAmount(jsonObject.getInt("amount"));
                                        lynkaplLearningModel.setPurchase_status(jsonObject.getInt("purchase_status"));
                                        lynkaplLearningModel.setTraining_image(jsonObject.getString("training_image"));
                                        lynkaplLearningModel.setCreated_at(jsonObject.getString("created_at"));
                                        lynkaplLearningModel.setFirst_name(jsonObject.getString("first_name"));
                                        lynkaplLearningModel.setLast_name(jsonObject.getString("last_name"));
                                        lynkaplLearningModel.setProfile_pic(jsonObject.getString("profile_pic"));

                                        lynkpalLearningList.add(lynkaplLearningModel);

                                        Log.d("lynkpalLearningListSize", lynkpalLearningList.size() + "");
                                    }


                                    if (lynkpalLearningList.size() > 0) {
                                   /* listLynkpalLearning.setExpanded(true);
                                    LynkpalLearningListAdapter lynkpalLearningListAdapter = new LynkpalLearningListAdapter(LynkpalLearning.this, lynkpalLearningList);
                                    listLynkpalLearning.setAdapter(lynkpalLearningListAdapter);*/


                                        LynkpalLearningListAdapter mAdapter = new LynkpalLearningListAdapter(LynkpalLearning.this, lynkpalLearningList, LynkpalLearning.this);
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                        rvAllTraining.setLayoutManager(mLayoutManager);
                                        rvAllTraining.setItemAnimator(new DefaultItemAnimator());
                                        rvAllTraining.setAdapter(mAdapter);
                                    }
                              /*  mFriendAdapter.notifyDataSetChanged();
                                suggestionApi(context, settings.getString("token",""));
                                Toast.makeText(context, "Followed successfully ", Toast.LENGTH_LONG).show();*/
                                } else {

                                    Log.d("Training", "frndError " + response.toString());
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    loaderDiloag.dismissDiloag();
                    error.printStackTrace();
                    // if (error.toString().contains(Constant.timeout_error_string));
                    // Toast.makeText(AddTrainingActivity.this,getString(R.string.slow_internet), Toast.LENGTH_SHORT).show();
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> data = new HashMap<String, String>();
                    data.put("Authorization", sharedPreferences.getString("token", ""));
                    data.put("Content-Type", "application/json");
                    Log.d("Training", "header " + data.toString());
                    return data;
                }

            };

            jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                    Constant.VOLLEY_TIMEOUT_MS,
                    Constant.VOLLEY_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


            jsonArrayRequest.setTag("Training");
            // Adding request to request queue
            requestQueue.add(jsonArrayRequest);
        }else{
            Toast.makeText(LynkpalLearning.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }
    }


    private void getMyTrainingApi() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(LynkpalLearning.this);
        if (ApplicationGlobles.isConnectingToInternet(LynkpalLearning.this)) {

        loaderDiloag.displayDiloag();

        com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(LynkpalLearning.this);
      /*  Map<String, String> postParam = new HashMap<String, String>();
        postParam.put("toFollow", user);

        Log.d("Training", "frndParam " + postParam.toString());*/


        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                WebApis.MYTRAINING, null,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response)
                    {
                        loaderDiloag.dismissDiloag();
                        Log.d("MyTraining", "MyTrainingResponse " + response.toString());
                        try {
                            //String status = response.getString("status");
                            // String message = response.getString("message");
                            // if (status.contains("success"))
                            if (response.length()>0)
                            {
                                // Loop through the array elements
                                for(int i=0;i<response.length();i++){
                                    // Get current json object
                                    MyTrainingModel myTrainingModel=new MyTrainingModel();
                                    JSONObject jsonObject = response.getJSONObject(i);

                                    // Get the current student (json object) data
                                    myTrainingModel.setId(jsonObject.getInt("id"));
                                    myTrainingModel.setName(jsonObject.getString("name"));
                                    myTrainingModel.setDuration(jsonObject.getInt("duration"));
                                    myTrainingModel.setLectures(jsonObject.getInt("lectures"));
                                    myTrainingModel.setSpecialization_name(jsonObject.getString("specialization_name"));
                                    myTrainingModel.setAmount(jsonObject.getInt("amount"));
                                    myTrainingModel.setTraining_image(jsonObject.getString("training_image"));
                                    myTrainingModel.setCreated_at(jsonObject.getString("created_at"));
                                    myTrainingModel.setFirst_name(jsonObject.getString("first_name"));
                                    myTrainingModel.setLast_name(jsonObject.getString("last_name"));
                                    myTrainingModel.setProfile_pic(jsonObject.getString("profile_pic"));

                                    myTrainingList.add(myTrainingModel);

                                    Log.d("lynkpalLearningListSize",lynkpalLearningList.size()+"");
                                }


                                if (myTrainingList.size() > 0) {

                                    myTrainingListAdapter = new MyTrainingListAdapter(LynkpalLearning.this, myTrainingList,LynkpalLearning.this);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                    rvMyTraining.setLayoutManager(mLayoutManager);
                                    rvMyTraining.setItemAnimator(new DefaultItemAnimator());
                                    rvMyTraining.setAdapter(myTrainingListAdapter);
                                }
                            } else
                            {

                                Log.d("Training", "frndError " + response.toString());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener()
        {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                loaderDiloag.dismissDiloag();
                error.printStackTrace();
                // if (error.toString().contains(Constant.timeout_error_string));
                // Toast.makeText(AddTrainingActivity.this,getString(R.string.slow_internet), Toast.LENGTH_SHORT).show();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String,String> data = new HashMap<String, String>();
                data.put("Authorization",sharedPreferences.getString("token",""));
                data.put("Content-Type","application/json");
                Log.d("Training","header " + data.toString());
                return data;
            }

        };

        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.VOLLEY_TIMEOUT_MS,
                Constant.VOLLEY_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        jsonArrayRequest.setTag("Training");
        // Adding request to request queue
        requestQueue.add(jsonArrayRequest);

        }else{
            Toast.makeText(LynkpalLearning.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }
    }


    private void getMyTrainingStatusApi() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(LynkpalLearning.this);

        if (ApplicationGlobles.isConnectingToInternet(LynkpalLearning.this)) {

            loaderDiloag.displayDiloag();

            com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(LynkpalLearning.this);
      /*  Map<String, String> postParam = new HashMap<String, String>();
        postParam.put("toFollow", user);

        Log.d("Training", "frndParam " + postParam.toString());*/


            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                    WebApis.MYTRAININGSTATUS, null,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response)
                        {
                            loaderDiloag.dismissDiloag();
                            Log.d("CAREERADVICE", "CAREERADVICE " + response.toString());
                            try {
                                //String status = response.getString("status");
                                // String message = response.getString("message");
                                // if (status.contains("success"))
                                String status = response.getString("status");
                                // String message = response.getString("message");
                                if (status.contains("success"))
                                {
                                    agreement_status=  response.getInt("agreement_status");


                                } else
                                {

                                    Log.d("Training", "frndError " + response.toString());
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    }, new Response.ErrorListener()
            {

                @Override
                public void onErrorResponse(VolleyError error)
                {
                    loaderDiloag.dismissDiloag();
                    error.printStackTrace();
                    // if (error.toString().contains(Constant.timeout_error_string));
                    // Toast.makeText(AddTrainingActivity.this,getString(R.string.slow_internet), Toast.LENGTH_SHORT).show();
                }
            })

            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    HashMap<String,String> data = new HashMap<String, String>();
                    data.put("Authorization",sharedPreferences.getString("token",""));
                    data.put("Content-Type","application/json");
                    Log.d("Training","header " + data.toString());
                    return data;
                }

            };

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    Constant.VOLLEY_TIMEOUT_MS,
                    Constant.VOLLEY_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


            jsonObjectRequest.setTag("Training");
            // Adding request to request queue
            requestQueue.add(jsonObjectRequest);

        } else {
            Toast.makeText(LynkpalLearning.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }
    }


    public void showPopup(final String id, final int pos, final String popUpFor, String title, String msg) {
        LayoutInflater layoutInflater
                = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_group_join_leave, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        final TextView textText = (TextView) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        textTitle.setText(title);
        textText.setText(msg);
        // textText.setText("Are you sure, you want to leave " + '"' + text + '"' + " Group?");
        if (popUpFor.equalsIgnoreCase("deleteTraining")) {

        }else {
            btnCancel.setVisibility(View.GONE);
            btnSumbmit.setText("Ok");
        }
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popUpFor.equalsIgnoreCase("deleteTraining")) {
                    deleteTrainingApi(id, pos);
                }else {

                }
                popupWindow.dismiss();

            }
        });
    }




    public void deleteTrainingApi(String id, final int pos) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(LynkpalLearning.this);
        if (ApplicationGlobles.isConnectingToInternet(LynkpalLearning.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", sharedPreferences.getString("token",""));
            Log.e("WebApis.JOBINTERESTED", WebApis.DELETETRAINING + id);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(WebApis.DELETETRAINING + id)
                    .get()
                    .addHeader("authorization",  sharedPreferences.getString("token",""))
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (LynkpalLearning.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("Apply", jsonData + "");

                        try {
                            final JSONObject jsonObject = new JSONObject(jsonData);
                            final String status = jsonObject.getString("status");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (status.equals("success")) {
                                        myTrainingList.remove(pos);
                                        myTrainingListAdapter.notifyDataSetChanged();
                                        //getMyTrainingApi();
                                        try {
                                            Toast.makeText(LynkpalLearning.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });


                        } catch (Exception e) {

                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                            }
                        });


                    }
                });

            } catch (Exception e) {
                LynkpalLearning.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(LynkpalLearning.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }


    @Override
    public void editTraining(String id, int pos,MyTrainingModel myTrainingModel) {
        //Toast.makeText(LynkpalLearning.this,"editTraining"+"--"+myTrainingModel.getName(),Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(LynkpalLearning.this,AddNewTraining.class);
        intent.putExtra("type","update");
        intent.putExtra("model",myTrainingModel);
        startActivity(intent);
    }

    @Override
    public void deleteTraining(String id, int pos) {
        //Toast.makeText(LynkpalLearning.this,"deleteTraining",Toast.LENGTH_SHORT).show();

        showPopup(id,pos,"deleteTraining","Delete Application","Are you sure, you want to delete your Application from this Job?");
    }

    @Override
    public void detailTraining(String id, int pos,MyTrainingModel myTrainingModel) {
       // Toast.makeText(LynkpalLearning.this,"detailTraining",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(LynkpalLearning.this,TrainingDetailActivity.class);
        intent.putExtra("type","myTraining");
        intent.putExtra("model",myTrainingModel);
        startActivity(intent);
    }

    @Override
    public void detailAllTraining(String id, int pos, LynkaplLearningModel allTrainingModel) {
        Intent intent = new Intent(LynkpalLearning.this,TrainingDetailActivity.class);
        intent.putExtra("type","allTraining");
        intent.putExtra("LynkaplLearningModel",allTrainingModel);
        startActivity(intent);
    }
}
