package com.app.lynkpal;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.lynkpal.Adapter.GridViewApplicationsAdapter;
import com.app.lynkpal.Bean.GridBean;
import com.app.lynkpal.Bean.JobDetailBean;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.ExpandadHeightGridview;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.bumptech.glide.Glide;
//import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class JobDetail extends AppCompatActivity {
    private static final String TAG = JobDetail.class.getSimpleName();
    ExpandadHeightGridview gridView;
    TextView head;
    GridViewApplicationsAdapter gridViewAdapter;
    List<GridBean> gridBeanList = new ArrayList<>();
    GridBean gridBean;
    int height, width;
    TextView txtJobName, txtComName, txtCityName, txtJobDesTitle, txtJobDes, txtApplications, txtViewAll, txtJobEmpTitleSub,
            txtJobEmp, txtJobExpTitleSub, txtJobExp, txtJobDesTitleSub, txtJobCreatedTitleSub, txtJobCreated;
    Button btnApply;
    String id = "", name = "", com_name = "", from = "", image = "", dec = "", location = "", empType = "", exp = "", time = "", hot = "";
    ImageView ic_edit, ic_back, ic_image;

    RelativeLayout mParentRelative;
    LinearLayout mParentLinear;
    JobDetailBean detailBean;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_detail);
        gridView = (ExpandadHeightGridview) findViewById(R.id.grid);

        mParentRelative = (RelativeLayout) findViewById(R.id.parent_relative);
        mParentLinear = (LinearLayout) findViewById(R.id.parent_linear);

        head = (TextView) findViewById(R.id.head);
        ic_edit = (ImageView) findViewById(R.id.ic_edit);
        ic_back = (ImageView) findViewById(R.id.ic_back);
        ic_image = (ImageView) findViewById(R.id.ic_image);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(new Intent(JobDetail.this, ProfileActivityForOthers.class).putExtra("userId", gridBeanList.get(i).getId()+""));
            }
        });
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ic_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("hot", hot + "");
                startActivity(new Intent(JobDetail.this, EditJob.class)
                        .putExtra("id", id)
                        .putExtra("name", name)
                        .putExtra("com_name", com_name)
                        .putExtra("image", image)
                        .putExtra("hot", hot)
                        .putExtra("dec", dec)
                        .putExtra("exp", exp)
                        .putExtra("empType", empType)
                        .putExtra("time", time)
                        .putExtra("location", location));
            }
        });
        txtJobName = (TextView) findViewById(R.id.txtJobName);
        txtJobEmpTitleSub = (TextView) findViewById(R.id.txtJobEmpTitleSub);
        txtJobEmp = (TextView) findViewById(R.id.txtJobEmp);
        txtJobExpTitleSub = (TextView) findViewById(R.id.txtJobExpTitleSub);
        txtJobDesTitleSub = (TextView) findViewById(R.id.txtJobDesTitleSub);
        txtJobExp = (TextView) findViewById(R.id.txtJobExp);
        txtJobCreatedTitleSub = (TextView) findViewById(R.id.txtJobCreatedTitleSub);
        txtJobCreated = (TextView) findViewById(R.id.txtJobCreated);

        txtComName = (TextView) findViewById(R.id.txtComName);
        txtCityName = (TextView) findViewById(R.id.txtCityName);
        txtJobDesTitle = (TextView) findViewById(R.id.txtJobDesTitle);
        txtJobDes = (TextView) findViewById(R.id.txtJobDes);
        txtApplications = (TextView) findViewById(R.id.txtApplications);
        txtViewAll = (TextView) findViewById(R.id.txtViewAll);
        btnApply = (Button) findViewById(R.id.btnApply);
        final Intent intent = getIntent();
        id =intent.getStringExtra("id");
        Log.d(TAG, "onCreate: id"+ id);

        if(intent.getStringExtra("id")!=null)
        {
            if (mParentLinear.getVisibility()==View.GONE)
            {
                mParentRelative.setVisibility(View.GONE);
                mParentLinear.setVisibility(View.VISIBLE);

            }
            Log.d(TAG,"oldjobid " + id);
                name = intent.getStringExtra("name");
                com_name = intent.getStringExtra("com_name");
                from = intent.getStringExtra("from");
                image = intent.getStringExtra("image");
                dec = intent.getStringExtra("dec");
                location = intent.getStringExtra("location");
                empType = intent.getStringExtra("empType");
                exp = intent.getStringExtra("exp");
                time = intent.getStringExtra("time");
                hot = intent.getStringExtra("hot");
                Log.e("hot", hot + "");
                txtJobName.setText(name);
                txtComName.setText(com_name);
                txtCityName.setText(location);
                txtJobDes.setText(dec);
                txtJobEmp.setText(empType);
                txtJobExp.setText(exp + " Years");
                time = time.replace("From:", "");
                txtJobCreated.setText(time);
                Log.e("id", id + "");
                if (from.equals("0"))
                {
                    btnApply.setText("Delete Job");
                }
                Glide.with(this).load(WebApis.JobImages + image)
                        .into(ic_image);
        }
        else
        {
            String job_id = getIntent().getStringExtra("job_id");
            Log.d(TAG,"new job id " + job_id);
              jobDetailApi(JobDetail.this,job_id);
        }

        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        head.setTypeface(tf_bold);
        txtViewAll.setTypeface(tf_bold);
        btnApply.setTypeface(tf_bold);
        txtJobName.setTypeface(tf_bold);
        txtJobDesTitle.setTypeface(tf_bold);
        txtApplications.setTypeface(tf_bold);
        txtComName.setTypeface(tf_med);
        txtCityName.setTypeface(tf_med);
        txtJobDes.setTypeface(tf_reg);
        txtViewAll.setVisibility(View.GONE);
        txtApplications.setVisibility(View.GONE);
        txtJobEmpTitleSub.setTypeface(tf_bold);
        txtJobEmp.setTypeface(tf_reg);
        txtJobExpTitleSub.setTypeface(tf_bold);
        txtJobDesTitleSub.setTypeface(tf_bold);
        txtJobExp.setTypeface(tf_reg);
        txtJobCreatedTitleSub.setTypeface(tf_bold);
        txtJobCreated.setTypeface(tf_reg);

        if (btnApply.getText().toString().contains("Applied"))
        {
            btnApply.setEnabled(false);
        }

        btnApply.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (intent.getStringExtra("id")!=null)
                {
                    showPopup();
                }
                else
                    {

                   applyjobApi(JobDetail.this,detailBean.getId());
                }
            }
        });
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
       // getApplications(id);
    }

    private void applyjobApi(final Context context, String jobid)
    {
        Log.d(TAG,"applied_job_id " + jobid);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, WebApis.APPLYJOB + jobid, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    String status = response.getString("status");
                    String message = response.getString("message");
                    Log.d(TAG,"serverResponse " + status + message);
                    if (status.contains("success"))
                    {
                        btnApply.setText("Applied");
                        btnApply.setEnabled(false);

                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d(TAG,"ServerError " + error.toString());
                Toast.makeText(context,"Something went wrong",Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization",Constant.token);
                return params;
            }
        };
        Volley.newRequestQueue(context).add(jsonObjectRequest);
    }

    private void jobDetailApi(final Context context, String id)
    {

        Log.d(TAG,"auth" + Constant.token);
        StringRequest request = new StringRequest(com.android.volley.Request.Method.GET,WebApis.JOBDETAIL + id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                if (mParentLinear.getVisibility()==View.GONE)
                {
                    mParentRelative.setVisibility(View.GONE);
                    mParentLinear.setVisibility(View.VISIBLE);
                    if (ic_edit.getVisibility()==View.VISIBLE)
                        ic_edit.setVisibility(View.GONE);
                }
                Log.d(TAG,"jobResponse " + response.toString());
                try
                {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i=0;i<jsonArray.length();i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String location = jsonObject.getString("title");
                        String title = jsonObject.getString("location");
                        String description = jsonObject.getString("description");
                        String experience = jsonObject.getString("experience");
                        String employment = jsonObject.getString("employment_type");
                        if(employment.equals("part-time"))
                        { // compare for the key-value
                            ((JSONObject)jsonArray.get(i)).put("employment_type","Part Time"); // put the new value for the key
                        }
                        else if (employment.equals("full-time"))
                        {
                            ((JSONObject)jsonArray.get(i)).put("employment_type","Full Time");
                        }

                        Log.d(TAG,"Employment " + employment);
                        String added = jsonObject.getString("added");
                        String image = jsonObject.getString("image");
                        String hot = jsonObject.getString("hot");
                        String applications_count = jsonObject.getString("applications_count");
                        String paid = jsonObject.getString("paid");

                        detailBean = new JobDetailBean();
                        detailBean.setId(id);
                        detailBean.setLocation(location);
                        detailBean.setTitle(title);
                        detailBean.setDescription(description);
                        detailBean.setExperience(experience);
                        detailBean.setEmployment_type(employment);
                        detailBean.setAdded(added);
                        detailBean.setImage(image);
                        detailBean.setHot(hot);
                        detailBean.setApplications_count(applications_count);
                        detailBean.setPaid(paid);





                        txtJobName.setText(title);
                        txtComName.setText(com_name);
                        txtCityName.setText(location);
                        txtJobDes.setText(description);
                        txtJobEmp.setText(employment);
                        txtJobExp.setText(experience);


                        Glide.with(context).load(WebApis.JobImages + image)/*.centerCrop().placeholder(R.drawable.noimage).error(R.drawable.noimage)*/
                                .into(ic_image);

                        // getApplications(id);
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.d(TAG,"joberror " + error.toString());
                if (mParentLinear.getVisibility()==View.GONE)
                {
                    mParentRelative.setVisibility(View.GONE);
                    mParentLinear.setVisibility(View.VISIBLE);
                    Toast.makeText(context,"Something Went Wrong",Toast.LENGTH_SHORT).show();
                }

            }
        }) {

            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization",Constant.token);
                return params;
            }

            //Pass Your Parameters here

        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
    }


    public void getApplications(String id) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(JobDetail.this);
        if (ApplicationGlobles.isConnectingToInternet(JobDetail.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";

            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.APPLICANTS", WebApis.APPLICANTS + id);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.APPLICANTS + id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (JobDetail.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getDetails", jsonData + "");
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {
                                JSONObject jsonObject = array.getJSONObject(a);
                                gridBean = new GridBean();
                                gridBean.setName(jsonObject.getString("first_name") + " " + jsonObject.getString("last_name"));
                                gridBean.setImage(WebApis.userProfileImage + jsonObject.getString("profile_pic"));
                                gridBean.setId(jsonObject.getString("user"));
                                gridBeanList.add(gridBean);
                            }

                        } catch (Exception e) {
                            loaderDiloag.dismissDiloag();
                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                                if (gridBeanList.size() > 0) {
                                    gridView.setExpanded(true);
                                    gridViewAdapter = new GridViewApplicationsAdapter(JobDetail.this, gridBeanList, height);
                                    gridView.setAdapter(gridViewAdapter);
                                    //txtViewAll.setVisibility(View.VISIBLE);
                                    txtApplications.setVisibility(View.VISIBLE);
                                }
                            }
                        });


                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    public void showPopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_group_join_leave, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        final TextView textText = (TextView) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        textTitle.setText("Delete Job");
        textText.setText("Are you sure, you want to delete this Job?");
        // textText.setText("Are you sure, you want to leave " + '"' + text + '"' + " Group?");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteJob();
                popupWindow.dismiss();

            }
        });
    }

    public void deleteJob() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(JobDetail.this);
        if (ApplicationGlobles.isConnectingToInternet(JobDetail.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            JSONObject jsonObject = new JSONObject();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, jsonObject + "");
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.JOBDELETE", WebApis.JOBDELETE + id + "/");
            Request request = new Request.Builder()
                    .url(WebApis.JOBDELETE + id + "/")
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (JobDetail.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.d("DeleteJobDetails", jsonData + "");

                        try {
                            if (jsonData.length() > 0) {
                                try {
                                    final JSONObject jsonObject = new JSONObject(jsonData);
                                    String status = jsonObject.getString("status");
                                    if (status.equals("success")) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                finish();
                                                try {
                                                    Toast.makeText(JobDetail.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                                } catch (Exception e) {
                                                    Toast.makeText(JobDetail.this, "Something went wrong try later...", Toast.LENGTH_SHORT).show();
                                                }
                                                Constant.update_created = true;
                                                finish();
                                            }
                                        });
                                    } else {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(JobDetail.this, "Something went wrong try later...", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                    Log.e("json", jsonObject.toString());
                                } catch (Exception je) {
                                    loaderDiloag.dismissDiloag();
                                    je.printStackTrace();
                                }
                                loaderDiloag.dismissDiloag();
                            } else {
                                loaderDiloag.dismissDiloag();
                            }


                        } catch (Exception e) {

                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();

                            }
                        });


                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(JobDetail.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }

        return;
    }

    @Override
    protected void onResume() {
        if (Constant.update_created) {
            finish();
        }
        super.onResume();
    }


}
