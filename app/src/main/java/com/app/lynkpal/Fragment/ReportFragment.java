package com.app.lynkpal.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.LoginActivity;
import com.app.lynkpal.R;
//import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportFragment extends Fragment {
    private static final String TAG = ReportFragment.class.getSimpleName();
    TextView mReport, mUnfollow, mCancel;
    RelativeLayout mLayout;
    int postId;
    public static int user;
    GetRefreshagain getRefreshagain;
    LoaderDiloag mDialogue;

    public ReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_demo, container, false);
        mReport = view.findViewById(R.id.report_text);
        mUnfollow = view.findViewById(R.id.unfollow_text);
        mCancel = view.findViewById(R.id.cancel_text);
        mLayout = view.findViewById(R.id.demo_frag_container);
        mDialogue = new LoaderDiloag(getContext());

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            postId = bundle.getInt("post_id", 0);
            user = bundle.getInt("user", 0);
            Log.d(TAG, "user_post_id " + postId + " " + user);
        }


        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "cancel");
               // addFragment();
                if (mLayout.getVisibility()==View.VISIBLE)
                {
                    mLayout.setVisibility(View.GONE);
                }
            }
        });

        mReport.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (LoginActivity.isNetworkConnected(getContext()))
                {
                    mReport.setEnabled(false);
                    mDialogue.displayDiloag();
                    reportApi(getContext());
                }
                else
                {
                    Toast.makeText(getContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                }


            }
        });

        mUnfollow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (LoginActivity.isNetworkConnected(getContext()))
                {
                    mUnfollow.setEnabled(false);
                    mDialogue.displayDiloag();
                    unfollowApi(getContext(), user);
                }
                else
                {
                    Toast.makeText(getContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                }

            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if ( context instanceof DeleteFragment.GetRefresh) {
            getRefreshagain = (GetRefreshagain) context;
        } else {
            throw new RuntimeException(context.getClass().getSimpleName()
                    + " must implement Callback");
        }
    }

    private void reportApi(final Context context)
    {
        final MaterialDialog.Builder newDialog = new MaterialDialog.Builder(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, WebApis.REPORT_URL + postId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                mDialogue.dismissDiloag();
                //dialog.dismiss();
                Log.d(TAG, "PostResponse " + response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.contains("success")) {

                        newDialog.title("Success")
                                .content(message)
                                .positiveText("OK")
                                .cancelable(false)
                                .iconRes(R.drawable.ic_check_circle)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                    {
                                       getRefreshagain.refreshOnce();

                                    }
                                })
                                //.icon(R.drawable.ic_check_circle)
                                .show();

                    } else {

                        newDialog.title("Fail")
                                .content(message)
                                .positiveText("OK")
                                .cancelable(false)
                                .iconRes(R.drawable.ic_cancel)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        dialog.dismiss();
                                    }
                                })
                                //.icon(R.drawable.ic_check_circle)
                                .show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                //dialog.dismiss();
                mDialogue.dismissDiloag();
                Log.d(TAG, "Error " + error.toString());
                Toast.makeText(context, "Please refresh the page", Toast.LENGTH_LONG).show();
            }
        }
        ) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", Constant.token);
                return params;
            }
        };

        Volley.newRequestQueue(context).add(stringRequest);
    }

    public interface GetRefreshagain
    {
       public void refreshOnce();
    }



    public  void unfollowApi(final Context context, final int user)
    {
        final MaterialDialog.Builder newDialog = new MaterialDialog.Builder(context);

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        Map<String, String> postParam= new HashMap<String, String>();
        postParam.put("toFollow",String.valueOf(user));

        Log.d(TAG,"postParam " + postParam.toString());


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                WebApis.FollowUnfollow, new JSONObject(postParam),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response)
                    {
                        mDialogue.dismissDiloag();
                        Log.d(TAG,"PostResponse " + response.toString());
                        try
                        {
                            String status = response.getString("status");
                            String message = response.getString("message");
                            if (status.contains("success"))
                            {
                                newDialog.title("Success")
                                        .content(message)
                                        .positiveText("OK")
                                        .cancelable(false)
                                        .iconRes(R.drawable.ic_check_circle)
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                            {
                                                getRefreshagain.refreshOnce();
                                            }
                                        })
                                        //.icon(R.drawable.ic_check_circle)
                                        .show();
                            }

                            else
                            {
                                newDialog.title("Fail")
                                        .content(message)
                                        .positiveText("OK")
                                        .cancelable(false)
                                        .iconRes(R.drawable.ic_cancel)
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                            {
                                                getRefreshagain.refreshOnce();
                                            }
                                        })
                                        //.icon(R.drawable.ic_check_circle)
                                        .show();
                            }

                        } catch (JSONException e)
                        {
                            e.printStackTrace();
                        }



                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                mDialogue.dismissDiloag();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                newDialog.title("Fail")
                        .content("Please refresh the page")
                        .positiveText("OK")
                        .iconRes(R.drawable.ic_cancel)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        //.icon(R.drawable.ic_check_circle)
                        .show();

            }
        }) {





            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String,String> data = new HashMap<String, String>();
                data.put("Authorization",Constant.token);
                data.put("Content-Type","application/json");
                Log.d(TAG,"header " + data.toString());
                return data;
            }


        };

        jsonObjReq.setTag(TAG);
        // Adding request to request queue
        requestQueue.add(jsonObjReq);

    }
}
