package com.app.lynkpal.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.app.lynkpal.Adapter.GridGroupsAdapter;
import com.app.lynkpal.Adapter.GridGroupsClosedAdapter;
import com.app.lynkpal.Adapter.GridViewAdapter;
import com.app.lynkpal.Bean.GridBean;
import com.app.lynkpal.Bean.GridBeanGroups;
import com.app.lynkpal.CreateGroup;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.ExpandadHeightGridview;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.JoinGroup;
import com.app.lynkpal.R;
import com.app.lynkpal.SearchForGroup;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;


public class Groups extends Fragment implements JoinGroup {
    private static final String TAG = Groups.class.getSimpleName();
    TextView head;
    TextView edtSearch;
    GridView gridView;
    GridViewAdapter gridViewAdapter;
    GridGroupsAdapter gridGroupsAdapter;
    GridGroupsClosedAdapter groupsClosedAdapter;
    List<GridBean> gridBeanList = new ArrayList<>();
    List<GridBeanGroups> gridBeanGroupsArrayList = new ArrayList<>();
    List<GridBeanGroups> gridBeanGroupsClosedArrayList = new ArrayList<>();
    GridBean gridBean;
    GridBeanGroups gridBeanGroups;
    GridBeanGroups gridBeanGroupsClosed;
    TextView txtCreated, txtApplied, txtOpen, txtClosed;
    int height, width;
    FloatingActionButton floatAdd;
    RelativeLayout relAllGroup;
    //LinearLayout linCreated, linApplied;
    ExpandadHeightGridview expOpe, expClose;
    ScrollView scrollView;

    public Groups() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_groups, container, false);
        head = (TextView) rootView.findViewById(R.id.head);
        edtSearch = (TextView) rootView.findViewById(R.id.edtSearch);
        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), SearchForGroup.class)/*.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)*/);
            }
        });
        expOpe = (ExpandadHeightGridview) rootView.findViewById(R.id.expOpe);
        expClose = (ExpandadHeightGridview) rootView.findViewById(R.id.expClose);
        floatAdd = (FloatingActionButton) rootView.findViewById(R.id.floatAdd);

        floatAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), CreateGroup.class));
            }
        });

        txtCreated = (TextView) rootView.findViewById(R.id.txtAllGroup);
        txtOpen = (TextView) rootView.findViewById(R.id.txtOpen);
        txtClosed = (TextView) rootView.findViewById(R.id.txtClosed);
        txtApplied = (TextView) rootView.findViewById(R.id.txtMyGroup);
        relAllGroup = (RelativeLayout) rootView.findViewById(R.id.relAllGroup);
        Typeface tf_reg = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Bold.ttf");
        head.setTypeface(tf_bold);
        txtCreated.setTypeface(tf_bold);
        txtOpen.setTypeface(tf_bold);
        txtClosed.setTypeface(tf_bold);
        txtApplied.setTypeface(tf_bold);
        edtSearch.setTypeface(tf_bold);
        gridView = (GridView) rootView.findViewById(R.id.grid);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        txtCreated.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                txtCreated.setBackground(getResources().getDrawable(R.drawable.left_dashboard));
                txtApplied.setBackground(null);
                // floatAdd.setVisibility(View.VISIBLE);
                txtCreated.setTextColor(Color.WHITE);
                txtApplied.setTextColor(Color.parseColor("#375cc8"));
                relAllGroup.setVisibility(View.VISIBLE);
                gridView.setVisibility(View.GONE);
            }
        });
        txtApplied.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                txtApplied.setBackground(getResources().getDrawable(R.drawable.right_dashboard));
                txtCreated.setBackground(null);
                //   floatAdd.setVisibility(View.GONE);
                txtApplied.setTextColor(Color.WHITE);
                txtCreated.setTextColor(Color.parseColor("#375cc8"));
                relAllGroup.setVisibility(View.GONE);
                gridView.setVisibility(View.VISIBLE);
            }
        });
        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onResume() {
        if (Constant.update_myCreatedGroup) {
            getGroups();
        }
        super.onResume();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            Log.e("visible", isVisibleToUser + "visible");
            try {
                getGroups();
                getOtherGroup();
            } catch (Exception e) {

            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    public void getOtherGroup() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            gridBeanGroupsArrayList.clear();
            gridBeanGroupsClosedArrayList.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.OTHERGROUP", WebApis.OTHERGROUP);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.OTHERGROUP)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getDetails", jsonData + "");
                        try {
                            JSONObject jsonObject12 = new JSONObject(jsonData);
                            String status = jsonObject12.getString("status");
                            if (status.equals("success")) {
                                JSONArray array = jsonObject12.getJSONArray("opengroups");
                                for (int a = 0; a < array.length(); a++) {
                                    JSONObject jsonObject = array.getJSONObject(a);
                                    gridBeanGroups = new GridBeanGroups();
                                    gridBeanGroups.setId(jsonObject.getString("id"));
                                    gridBeanGroups.setName(jsonObject.getString("name"));
                                    gridBeanGroups.setGrouptype(jsonObject.getString("grouptype"));
                                    gridBeanGroups.setImage(jsonObject.getString("image"));
                                    gridBeanGroups.setMembershipstatus(jsonObject.getString("membershipstatus"));
                                    gridBeanGroupsArrayList.add(gridBeanGroups);
                                }
                                JSONArray array1 = jsonObject12.getJSONArray("closedgroups");
                                for (int a = 0; a < array1.length(); a++) {
                                    JSONObject jsonObject = array1.getJSONObject(a);
                                    gridBeanGroupsClosed = new GridBeanGroups();
                                    gridBeanGroupsClosed.setId(jsonObject.getString("id"));
                                    gridBeanGroupsClosed.setName(jsonObject.getString("name"));
                                    gridBeanGroupsClosed.setGrouptype(jsonObject.getString("grouptype"));
                                    gridBeanGroupsClosed.setImage(jsonObject.getString("image"));
                                    gridBeanGroupsClosed.setMembershipstatus(jsonObject.getString("membershipstatus"));
                                    gridBeanGroupsClosedArrayList.add(gridBeanGroupsClosed);
                                }
                            }

                        } catch (Exception e) {
                            loaderDiloag.dismissDiloag();
                        }


                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                                if (gridBeanGroupsArrayList.size() > 0) {
                                    expOpe.setExpanded(true);
                                    gridGroupsAdapter = new GridGroupsAdapter(getActivity(), gridBeanGroupsArrayList, height, Groups.this);
                                    expOpe.setAdapter(gridGroupsAdapter);
                                }
                                if (gridBeanGroupsClosedArrayList.size() > 0) {
                                    expClose.setExpanded(true);
                                    groupsClosedAdapter = new GridGroupsClosedAdapter(getActivity(), gridBeanGroupsClosedArrayList, height, Groups.this);
                                    expClose.setAdapter(groupsClosedAdapter);
                                }
                            }
                        });


                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    public void getGroups() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            loaderDiloag.displayDiloag();
           // OkHttpClient client = new OkHttpClient();
            OkHttpClient client;
            client = new OkHttpClient.Builder()
                    .connectTimeout(Constant.VOLLEY_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                    .writeTimeout(Constant.WRITE_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                    .readTimeout(Constant.READ_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                    .build();
            //status = "false";
            gridBeanList.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.TALKED", WebApis.GroupsCreatedByMe);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.GroupsCreatedByMe)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (e.toString().contains(Constant.timeout_error_string)) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(), getString(R.string.slow_internet), Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                        if (getActivity() == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getMessage", jsonData + "");
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {
                                JSONObject jsonObject = array.getJSONObject(a);
                                gridBean = new GridBean();
                                gridBean.setName(jsonObject.getString("name"));
                                gridBean.setImage(jsonObject.getString("image"));
                                gridBean.setId(jsonObject.getString("id"));
                                gridBeanList.add(gridBean);
                            }

                        } catch (Exception e) {
                            loaderDiloag.dismissDiloag();
                        }


                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (gridBeanList.size() > 0) {
                                    gridViewAdapter = new GridViewAdapter(getActivity(), gridBeanList, height);
                                    gridView.setAdapter(gridViewAdapter);
                                }
                                loaderDiloag.dismissDiloag();
                            }
                        });


                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }


    @Override
    public void Join(int pos, String type) {

        if (type.equals("open")) {
            String id = gridBeanGroupsArrayList.get(pos).getId();
            String text = gridBeanGroupsArrayList.get(pos).getName();
            String status = gridBeanGroupsArrayList.get(pos).getMembershipstatus();
            if (status.equals("0")) {
                JoinOpenGroup(id);
            } else {
                showPopup(id, text, type);
            }
        } else {
            String id = gridBeanGroupsClosedArrayList.get(pos).getId();
            String text = gridBeanGroupsClosedArrayList.get(pos).getName();
            String status = gridBeanGroupsClosedArrayList.get(pos).getMembershipstatus();
            Log.d(TAG,"group_status " + status );
            if (status.equals("0")) {
                JoinClosedGroup(id);
            } else if (status.equals("1")) {
                showPopup(id, text, type);
            } else {
                Toast.makeText(getActivity(), "Request Pending", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void showPopup(final String id, final String text, final String type) {
        LayoutInflater layoutInflater
                = (LayoutInflater) ((Activity) getActivity()).getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_group_join_leave, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        TextView textText = (TextView) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        textText.setText("Are you sure, you want to leave " + '"' + text + '"' + " Group?");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.equals("open")) {
                    JoinOpenGroup(id);
                } else {
                    JoinClosedGroup(id);
                }
                popupWindow.dismiss();

            }
        });
    }

    public void JoinOpenGroup(String id) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            //  gridBeanList.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.joinleave", WebApis.JOINLEAVEOPENGROUP + id);
            Request request = new Request.Builder()
                    .url(WebApis.JOINLEAVEOPENGROUP + id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                            }
                        });

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getMessage", jsonData + "");
                        try {
                            JSONObject jsonObject = new JSONObject(jsonData);
                            String status = jsonObject.getString("status");
                            if (status.equals("success")) {
                                final String message = jsonObject.getString("message");
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        loaderDiloag.dismissDiloag();
                                        getOtherGroup();
                                        new MaterialDialog.Builder(getContext())
                                                .title("Success")
                                                .content(message)
                                                .positiveText("ok")
                                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                    @Override
                                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                                    {
                                                        dialog.dismiss();
                                                    }
                                                }).show();
                                    }
                                });

                            } else {
                                new MaterialDialog.Builder(getContext())
                                        .title("Failure")
                                        .content("Something Went Wrong,Try Later")
                                        .positiveText("ok")
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                            {
                                                dialog.dismiss();
                                            }
                                        }).show();
                            }


                            Log.e("json", jsonObject.toString());

                        } catch (Exception e) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loaderDiloag.dismissDiloag();
                                }
                            });
                        }


                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    public void JoinClosedGroup(String id) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            //  gridBeanList.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.JOINVECLOSE", WebApis.JOINLEAVECLOSEGROUP + id);
            Request request = new Request.Builder()
                    .url(WebApis.JOINLEAVECLOSEGROUP + id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                            }
                        });

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getMessage", jsonData + "");
                        try {
                            JSONObject jsonObject = new JSONObject(jsonData);
                            String status = jsonObject.getString("status");
                            if (status.equals("success")) {
                                final String message = jsonObject.getString("message");
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        loaderDiloag.dismissDiloag();
                                        getOtherGroup();
                                        /*Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();*/
                                        new MaterialDialog.Builder(getContext())
                                                .title("Success")
                                                .content(message)
                                                .positiveText("ok")
                                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                    @Override
                                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                                    {
                                                        dialog.dismiss();
                                                    }
                                                }).show();
                                    }
                                });

                            } else {
                                new MaterialDialog.Builder(getContext())
                                        .title("Failure")
                                        .content("Something went wrong, Try L")
                                        .positiveText("ok")
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                            {
                                                dialog.dismiss();
                                            }
                                        }).show();
                            }


                            Log.e("json", jsonObject.toString());

                        } catch (Exception e) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loaderDiloag.dismissDiloag();
                                }
                            });
                        }


                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }
}
