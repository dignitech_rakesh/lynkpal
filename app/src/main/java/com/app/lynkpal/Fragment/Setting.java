package com.app.lynkpal.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Advertisements;
import com.app.lynkpal.CareerAdvice;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.JobDashboard;
import com.app.lynkpal.LoginActivity;
import com.app.lynkpal.LynkpalLearning;
import com.app.lynkpal.MainActivity;
import com.app.lynkpal.ProfileActivity;
import com.app.lynkpal.R;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;


public class Setting extends Fragment {
    private static final String TAG = Setting.class.getSimpleName() ;
    TextView head, adv, noti, pro, job, invite, log;
    LinearLayout lin_job_das, lin_logout, lin_adv, lin_profile, lin_inv_peo,lin_careerAdvice,lin_linkpalLearning;
    SharedPreferences settings;
    SharedPreferences.Editor editor;

    public Setting() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_setting, container, false);
        head = (TextView) rootView.findViewById(R.id.head);
        settings = getActivity().getSharedPreferences(Constant.PREFS_NAME, 0);
        adv = (TextView) rootView.findViewById(R.id.adv);
        noti = (TextView) rootView.findViewById(R.id.noti);
        pro = (TextView) rootView.findViewById(R.id.pro);
        job = (TextView) rootView.findViewById(R.id.jobs);
        invite = (TextView) rootView.findViewById(R.id.invite);
        log = (TextView) rootView.findViewById(R.id.log);
        lin_job_das = (LinearLayout) rootView.findViewById(R.id.lin_job_das);
        lin_linkpalLearning = (LinearLayout) rootView.findViewById(R.id.lin_linkpalLearning);
        lin_careerAdvice = (LinearLayout) rootView.findViewById(R.id.lin_careerAdvice);
        lin_logout = (LinearLayout) rootView.findViewById(R.id.lin_logout);
        lin_adv = (LinearLayout) rootView.findViewById(R.id.lin_adv);
        lin_profile = (LinearLayout) rootView.findViewById(R.id.lin_profile);
        lin_inv_peo = (LinearLayout) rootView.findViewById(R.id.lin_inv_peo);

        lin_job_das.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), JobDashboard.class));
            }
        });
        lin_adv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Advertisements.class));
            }
        });
        lin_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ProfileActivity.class));
            }
        });

        lin_linkpalLearning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), LynkpalLearning.class));
            }
        });

        lin_careerAdvice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), CareerAdvice.class));
            }
        });

        lin_job_das.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), JobDashboard.class));
            }
        });

        lin_inv_peo.setVisibility(View.GONE);   // remove to view invite people
      /*  lin_inv_peo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Contacts.class));
            }
        });*/
        lin_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(rootView);
            }
        });
        Typeface tf_reg = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Bold.ttf");
        head.setTypeface(tf_bold);
        adv.setTypeface(tf_reg);
        noti.setTypeface(tf_reg);
        pro.setTypeface(tf_reg);
        job.setTypeface(tf_reg);
        log.setTypeface(tf_reg);
        invite.setTypeface(tf_reg);
        return rootView;
    }

    public void showPopup(View v) {

        Log.e("here", "here");
        LayoutInflater layoutInflater
                = (LayoutInflater) getActivity().getBaseContext()
                .getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.custom_dialog, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        RelativeLayout linearLayout = (RelativeLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        TextView textText = (TextView) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                popupWindow.dismiss();
                callLogout();

               /* SharedPreferences.Editor editor1 = settings.edit();
                editor1.putString("url", Url);
                Log.e("newurl", Url);
                //Log.e("mainurl", Constant.MAIN_URL);
                editor1.commit();

                if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
                    new Log_out().execute();


                } else {
                    Toast.makeText(getActivity(), "Please Check your Internet connection", Toast.LENGTH_SHORT).show();
                }*/
            }
        });

    }

    public void callLogout()
    {
        editor = settings.edit();
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());

        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            loaderDiloag.displayDiloag();

            OkHttpClient client = new OkHttpClient();

            //MediaType mediaType = MediaType.parse("application/json");
            //RequestBody body = RequestBody.create(mediaType, "{}");
            Request request = new Request.Builder()
                    .url(WebApis.logoutURL)
                    .get()
                    .addHeader("content-type", "application/json")
                    .addHeader("Authorization", Constant.token)
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException
                    {
                      loaderDiloag.dismissDiloag();
                        String jsonData = response.body().string();
                        Log.d(TAG, "onResponse: " + jsonData);


                        if (jsonData.length() > 0)
                        {
                            try {
                                JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success"))
                                {


                                    getActivity().runOnUiThread(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {

                                            editor.putString("token", "");
                                            editor.putString("email", "");
                                            editor.putString("fullname", "");
                                            editor.putString("profilePic", "");
                                            editor.putString("userid", "");
                                            editor.commit();
                                            getActivity().finish();
                                            startActivity(new Intent(getActivity(), LoginActivity.class));

                                        }
                                    });

                                }

                                else if (status.equals("error"))
                                {
                                    editor.clear();
                                    editor.apply();
                                    getActivity().finish();
                                    startActivity(new Intent(getContext(),LoginActivity.class));
                                }

                                Log.e("json", jsonObject.toString() + "");
                            }
                            catch (Exception je)
                            {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else
                            {
                            loaderDiloag.dismissDiloag();

                        }
                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                Log.d(TAG, "callLogout: " + e.getMessage());
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

}
