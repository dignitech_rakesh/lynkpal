package com.app.lynkpal.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Adapter.MessageAdapter;
import com.app.lynkpal.Bean.MessageBean;
import com.app.lynkpal.ChatActivity;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.MarkRead;
import com.app.lynkpal.ListForNewMessage;
import com.app.lynkpal.R;



import com.inscripts.interfaces.Callbacks;
import com.inscripts.interfaces.LaunchCallbacks;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;


import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

import static com.app.lynkpal.MainActivity.show_counter_message;
import static com.app.lynkpal.MainActivity.tab_text_message;

public class Messaging extends Fragment implements MarkRead
{
    private static final String url = "https://www.lynkpal.com/php/cometchat/";
    private static final String licence = "BM5BT-NDQX1-4VJOG-BL8NV-KBA1C";
    private static final String api = "75a2071c8c281cccc5d7782c20709d68";
    private static final String TAG = Messaging.class.getSimpleName();
    private boolean isConnect = false;
    TextView head,text_message;
    ProgressBar mProgress;
    RecyclerView notificationRecycler;
    List<MessageBean> messageBeenList = new ArrayList<MessageBean>();
    MessageBean messageBean;
    MessageAdapter messageAdapter;
    FloatingActionButton floatAdd;
    SharedPreferences mPref;
    SharedPreferences.Editor mEditor;
    CometChat cometChat;
    public Messaging() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_messaging, container, false);
        Log.d("checkActivity","Messaging");
        head = (TextView) rootView.findViewById(R.id.head);
        text_message = rootView.findViewById(R.id.text_msg);
        floatAdd = (FloatingActionButton) rootView.findViewById(R.id.floatAdd);
        Typeface tf_reg = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Bold.ttf");
        head.setTypeface(tf_bold);
        notificationRecycler = (RecyclerView) rootView.findViewById(R.id.notification_recycler_view);
        mProgress = rootView.findViewById(R.id.show_message_progressbar);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        notificationRecycler.setLayoutManager(mLayoutManager);
        floatAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ListForNewMessage.class));
            }
        });
        // getMessagesDone();
        // Inflate the layout for this fragment
       cometChat = CometChat.getInstance(getActivity());
        mPref = getContext().getSharedPreferences(Constant.PREFS_NAME,0);
        mEditor = mPref.edit();


        return rootView;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible)
    {
        super.setMenuVisibility(menuVisible);
        if (menuVisible)
        {
            mProgress.setVisibility(View.VISIBLE);
            cometChat.initializeCometChat(url, licence, api, isConnect, new Callbacks()
            {
                @Override
                public void successCallback(JSONObject jsonObject)
                {
                    mProgress.setVisibility(View.GONE);
                    text_message.setVisibility(View.GONE);
                   // Toast.makeText(getActivity(),"Success",Toast.LENGTH_SHORT).show();
                    Log.d(TAG,"response " + jsonObject.toString());
                    if (!isConnect)
                    {
                        login();
                    }

                }

                @Override
                public void failCallback(JSONObject jsonObject)
                {
                    mProgress.setVisibility(View.GONE);
                    text_message.setVisibility(View.GONE);
                    Log.d(TAG,"response " + jsonObject.toString());
                    Toast.makeText(getActivity(),"Failed",Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void login()
    {
        final CometChat cometChat1 = CometChat.getInstance(getActivity());
        if (!isConnect)
        {
            String user_name = mPref.getString("email",null);
            String password = mPref.getString("user_pass",null);
            Log.d(TAG,"credential " + user_name + password);
            //cometChat = CometChat.getInstance(MainActivity.this);
            cometChat1.login(user_name, password, new Callbacks()
            {
                @Override
                public void successCallback(JSONObject jsonObject)
                {
                   // Toast.makeText(getActivity()," login Success",Toast.LENGTH_SHORT).show();
                    Log.d(TAG,"msg" + jsonObject.toString());
                    Log.d("checkActivity","MessagingFrag");

                    mEditor.putString("Json",jsonObject.toString());
              mEditor.commit();

                    groupchat();
                }

                @Override
                public void failCallback(JSONObject jsonObject)
                {
                    Toast.makeText(getActivity(),"login Fail",Toast.LENGTH_SHORT).show();
                    Log.d(TAG,"Failed" + jsonObject.toString());
                }
            });

        }
    }

    public void groupchat()
    {
        mProgress.setVisibility(View.GONE);
        cometChat.launchCometChat(getActivity(), true, new LaunchCallbacks()
        {
            @Override
            public void successCallback(JSONObject jsonObject)
            {
                Log.d(TAG,"chatSuccess" + jsonObject.toString() + jsonObject.toString());
            }

            @Override
            public void failCallback(JSONObject jsonObject)
            {
                Log.d(TAG,"chatfailed" + jsonObject.toString() + jsonObject.toString());
            }

            @Override
            public void userInfoCallback(JSONObject jsonObject)
            {
                Log.d(TAG,"userinfochat" + jsonObject.toString());
            }

            @Override
            public void chatroomInfoCallback(JSONObject jsonObject)
            {
                Log.d(TAG,"chatroominfo" + jsonObject.toString());
            }

            @Override
            public void onMessageReceive(JSONObject jsonObject)
            {
                Log.d(TAG,"msgrcv" + jsonObject.toString());
            }

            @Override
            public void error(JSONObject jsonObject)
            {
                Log.d(TAG,"error" + jsonObject.toString());
            }

            @Override
            public void onWindowClose(JSONObject jsonObject) {

            }

            @Override
            public void onLogout()
            {
                Log.d(TAG,"logout");
            }
        });
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            Log.e("visible", isVisibleToUser + "visible");
            try {
               // getMessagesDone();
               // countMessage();
            } catch (Exception e) {

            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    public void countMessage() {
      //  final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            //loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";

            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.COUNTMESSAGE", WebApis.COUNTMESSAGE);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.COUNTMESSAGE)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                     //   loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getMsgDetails", jsonData + "");
                        try {
                            final JSONObject jsonObject = new JSONObject(jsonData);
                            String status = jsonObject.getString("status");
                            if (status.equals("success")) {

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            int count = Integer.parseInt(jsonObject.getString("unread_messages_count"));
                                            Log.e("count", count + "");

                                            if (count > 99) {
                                                show_counter_message.setVisibility(View.VISIBLE);
                                                tab_text_message.setText("99+");
                                            } else if (count == 0) {
                                                show_counter_message.setVisibility(View.GONE);
                                            } else {
                                                show_counter_message.setVisibility(View.VISIBLE);
                                                tab_text_message.setText(count + "");
                                            }

                                        } catch (Exception e) {

                                        }
                                    }
                                });
                            }

                        } catch (Exception e) {
                           // loaderDiloag.dismissDiloag();
                        }


                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                               // loaderDiloag.dismissDiloag();
                            }
                        });


                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                       // loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();

            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    public void getMessagesDone() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
           // loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            messageBeenList.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.TALKED", WebApis.TALKED);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.TALKED)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                       // loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getMessage", jsonData + "");
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {
                                JSONObject jsonObject = array.getJSONObject(a);
                                messageBean = new MessageBean();
                                String rec = jsonObject.getString("reciever");
                                String send = jsonObject.getString("sender");
                                String rec_image = jsonObject.getString("reciever_pic");
                                String sen_image = jsonObject.getString("sender_pic");
                                String rec_name = jsonObject.getString("reciever_name");
                                String sen_name = jsonObject.getString("sender_name");

                                if (rec.equals(Constant.user_id)) {
                                    messageBean.setId(send);
                                    messageBean.setImage(sen_image);
                                    messageBean.setName(sen_name);
                                } else {
                                    messageBean.setId(rec);
                                    messageBean.setImage(rec_image);
                                    messageBean.setName(rec_name);
                                }


                                messageBean.setStatus(jsonObject.getString("status"));
                                messageBean.setText(jsonObject.getString("message").trim());
                                String created = jsonObject.getString("created_at");
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//2016-01-24T16:00:00.000Z
                                sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                                long time = sdf.parse(created).getTime();
                                long now = System.currentTimeMillis();
                                CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
                                messageBean.setTime(String.valueOf(ago));
                                messageBeenList.add(messageBean);
                            }

                        } catch (Exception e) {
                           // load erDiloag.dismissDiloag();
                        }


                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (messageBeenList.size() > 0) {
                                    messageAdapter = new MessageAdapter(getActivity(), messageBeenList, Messaging.this);
                                    notificationRecycler.setAdapter(messageAdapter);
                                }
                                //loaderDiloag.dismissDiloag();
                            }
                        });


                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                      //  loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    public void readMessage(final int pos) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
           // loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";

            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.READMESSAGE", WebApis.READMESSAGE + messageBeenList.get(pos).getId());
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.READMESSAGE + messageBeenList.get(pos).getId())
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                       // loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getDetails", jsonData + "");
                        try {
                            final JSONObject jsonObject = new JSONObject(jsonData);
                            String status = jsonObject.getString("status");
                            if (status.equals("success")) {

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {

                                            try {
                                                int count = Integer.parseInt(tab_text_message.getText().toString());
                                                tab_text_message.setText((count - 1) + "");
                                            } catch (Exception e) {
                                            }

                                        } catch (Exception e) {

                                        }
                                        startActivity(new Intent(getActivity(), ChatActivity.class).putExtra("id", messageBeenList.get(pos).getId()).putExtra("name", messageBeenList.get(pos).getName()).putExtra("image", messageBeenList.get(pos).getImage()));

                                    }
                                });
                            }

                        } catch (Exception e) {

                           // loaderDiloag.dismissDiloag();
                        }


                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                               // loaderDiloag.dismissDiloag();
                            }
                        });


                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                      //  loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    @Override
    public void onResume() {
        if (Constant.update_chat) {
            Constant.update_chat = false;
          //  getMessagesDone();
            //countMessage();
        }
        try {
            //countMessage();
        } catch (Exception e) {
        }
        super.onResume();
    }



    @Override
    public void Mark(int pos) {
        if (messageBeenList.get(pos).getStatus().equals("read")) {
            startActivity(new Intent(getActivity(), ChatActivity.class).putExtra("id", messageBeenList.get(pos).getId()).putExtra("name", messageBeenList.get(pos).getName()).putExtra("image", messageBeenList.get(pos).getImage()));
        } else {
            readMessage(pos);
        }
    }


}
