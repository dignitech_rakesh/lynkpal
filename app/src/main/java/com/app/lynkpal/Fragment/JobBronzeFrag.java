package com.app.lynkpal.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.AddTrainingActivity;
import com.app.lynkpal.R;


public class JobBronzeFrag extends Fragment {


    private TextView tv_one,tv_two,tv_three,tv_four,tv_five,tv_six,tv_seven,tvBtn;
    private CardView llSeven;

    public JobBronzeFrag() {
        // Required empty public constructor
    }


 /*   public static JobBronzeFrag newInstance(String param1, String param2) {
        JobBronzeFrag fragment = new JobBronzeFrag();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_job_gold, container, false);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_job_gold, container, false);
         tv_one = view.findViewById(R.id.headingOne);
         tv_two = view.findViewById(R.id.headingTwo);
         tv_three = view.findViewById(R.id.headingThree);
         tv_four = view.findViewById(R.id.headingFour);
         tv_five = view.findViewById(R.id.headingFive);
         tv_six= view.findViewById(R.id.headingSix);
         tv_seven = view.findViewById(R.id.headingSeven);
        llSeven = view.findViewById(R.id.llSeven);
        tvBtn = view.findViewById(R.id.tvBtn);


        tv_one.setText(Html.fromHtml("<b>" + "# 10,900 or 109.00 ¢" + "</b> " ));
        tv_two.setText(Html.fromHtml("<b>" + "Professionally written -" + "</b> " + "A top-standard CV that showcases your strengths & achievements"));
        tv_three.setText(Html.fromHtml("<b>" + "Fine-tuned to suit your industry - " + "</b> " + "Your CV will be well-tailored to suit the specific industry you are in, or aspire to be in."));
        tv_four.setText(Html.fromHtml("<b>" + "Turnaround Time: " + "</b> " + "5 – 7"));
        tv_five.setText(Html.fromHtml("<b>" + "Working Days -" + "</b> " + "The first draft of your CV will be sent to you within 5 – 7 working days"));
        tv_six.setText(Html.fromHtml("<b>" + "1 Revision on your order -" + "</b> " + "You can give us feedback on your first draft, and we’ll make the needed adjustments."));
        tv_seven.setVisibility(View.GONE);
        llSeven.setVisibility(View.GONE);


        tvBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(getActivity(),"click",Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(getActivity(), AddTrainingActivity.class);
                intent.putExtra("key","bronze");
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    /*  // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    *//**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     *//*
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }*/
}
