package com.app.lynkpal.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.LoginActivity;
import com.app.lynkpal.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class DeleteFragment extends Fragment
{

   int postId;
    private static final String TAG = DeleteFragment.class.getSimpleName();
   private RelativeLayout mLayout;
   LoaderDiloag mDialog;
   GetRefresh getRefresh;
    public DeleteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_delete, container, false);
        final TextView delete_text = view.findViewById(R.id.delete_text);
        TextView cancle_text = view.findViewById(R.id.cancel_delete_text);
        mLayout = view.findViewById(R.id.frag_container);
        mDialog = new LoaderDiloag(getContext());
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            postId = bundle.getInt("post_id", 0);

            Log.d(TAG, "user_post_id " + postId);
        }

        delete_text.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (LoginActivity.isNetworkConnected(getContext()))
                {
                    delete_text.setEnabled(false);
                    mDialog.displayDiloag();
                    deleteApi(getContext());
                }
                else
                {
                    Toast.makeText(getContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                }

            }
        });

        cancle_text.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (mLayout.getVisibility()==View.VISIBLE)
                {
                   getActivity().getSupportFragmentManager().beginTransaction().remove(DeleteFragment.this).commitAllowingStateLoss();
                   // mLayout.setVisibility(View.GONE);
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if ( context instanceof GetRefresh ) {
            getRefresh = (GetRefresh) context;
        } else {
            throw new RuntimeException(context.getClass().getSimpleName()
                    + " must implement Callback");
        }
    }

    public interface GetRefresh
    {
        public void clicktoRefresh();
    }

    private void deleteApi(final Context context)
    {
        final MaterialDialog.Builder newDialog = new MaterialDialog.Builder(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, WebApis.DeletePOST + postId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                //dialog.dismiss();
                mDialog.dismissDiloag();
                Log.d(TAG, "PostResponse " + response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.contains("success")) {

                        newDialog.title("Success")
                                .content(message)
                                .positiveText("OK")
                                .iconRes(R.drawable.ic_check_circle)
                                .cancelable(false)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                    {
                                        if (mLayout.getVisibility()==View.VISIBLE)
                                        {
                                            //getActivity().getSupportFragmentManager().beginTransaction().remove(new Home()).commitAllowingStateLoss();
                                            getRefresh.clicktoRefresh();
                                          //  getActivity().getSupportFragmentManager().beginTransaction().remove(DeleteFragment.this).commitAllowingStateLoss();
                                           // addFragment();
                                        }

                                    }
                                })
                                //.icon(R.drawable.ic_check_circle)
                                .show();

                    } else {

                        newDialog.title("Fail")
                                .content(message)
                                .positiveText("OK")
                                .cancelable(false)
                                .iconRes(R.drawable.ic_cancel)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        newDialog.cancelable(true);
                                    }
                                })
                                //.icon(R.drawable.ic_check_circle)
                                .show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                //dialog.dismiss();
                mDialog.dismissDiloag();
                Log.d(TAG, "Error " + error.toString());
                Toast.makeText(context, "Please refresh the page", Toast.LENGTH_LONG).show();
            }
        }
        ) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", Constant.token);
                return params;
            }
        };

        Volley.newRequestQueue(context).add(stringRequest);
    }



    @Override
    public void onDetach()
    {
        super.onDetach();
        Log.d(TAG,"ondetach");
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.d(TAG,"ondestroy");
    }
}
