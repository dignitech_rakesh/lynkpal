package com.app.lynkpal.Fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Adapter.NotificationAdapter;
import com.app.lynkpal.Bean.NotificationBean;
import com.app.lynkpal.CommentActivity;
import com.app.lynkpal.GroupActivity;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.deleteAndreadNotification;
import com.app.lynkpal.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static com.app.lynkpal.MainActivity.show_counter_notification;
import static com.app.lynkpal.MainActivity.tab_text_notification;


public class Notification extends Fragment implements deleteAndreadNotification {
    TextView head;
    RecyclerView notificationRecycler;
    RelativeLayout noNotification;
    NotificationAdapter notificationAdapter;
    ArrayList<NotificationBean> notificationBeanArrayList = new ArrayList<>();
    NotificationBean notificationBean;

    public Notification() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            countNotification();
        } catch (Exception e) {
        }
    }

    public void countNotification() {
        //  final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            //   loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";

            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.COUNTNOTIFI", WebApis.COUNTNOTIFICATION);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.COUNTNOTIFICATION)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                        //   loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getDetails_COUNTNOTIFI", jsonData + "");
                        try {
                            final JSONObject jsonObject = new JSONObject(jsonData);
                            String status = jsonObject.getString("status");
                            if (status.equals("success")) {

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            int count = Integer.parseInt(jsonObject.getString("unread_notifications_count"));
                                            Log.e("count", count + "");

                                            if (count > 99) {
                                                show_counter_notification.setVisibility(View.VISIBLE);
                                                tab_text_notification.setText("99+");
                                            } else if (count == 0) {
                                                show_counter_notification.setVisibility(View.GONE);
                                            } else {
                                                show_counter_notification.setVisibility(View.VISIBLE);
                                                tab_text_notification.setText(count + "");
                                            }

                                        } catch (Exception e) {

                                        }
                                    }
                                });
                            }

                        } catch (Exception e) {
                            //   loaderDiloag.dismissDiloag();
                        }


                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //     loaderDiloag.dismissDiloag();
                            }
                        });


                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //   loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
            }
        } else {
            Log.d("Nointernet","Toast removed");
            //Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_notification, container, false);
        head = (TextView) rootView.findViewById(R.id.head);
        Typeface tf_reg = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Bold.ttf");
        notificationRecycler = (RecyclerView) rootView.findViewById(R.id.notification_recycler_view);
        noNotification = (RelativeLayout) rootView.findViewById(R.id.noNotification);
        head.setTypeface(tf_bold);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        notificationRecycler.setLayoutManager(mLayoutManager);
        //  new getNotification().execute();
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            Log.e("visible", isVisibleToUser + "visible");
            try {
                getNotification();
                countNotification();
            } catch (Exception e) {

            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }


    public void getNotification() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            notificationBeanArrayList.clear();
            Log.e("WebApis.GETNOTIFICATION", WebApis.GETNOTIFICATION);
            //    Log.e("WebApis.TALKED", WebApis.TALKED);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.GETNOTIFICATION)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getMessage", jsonData + "");
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {
                                JSONObject jsonObject = array.getJSONObject(a);
                                notificationBean = new NotificationBean();
                                notificationBean.setProfile_photo(jsonObject.getString("profile_pic"));
                                String fullname = jsonObject.getString("fullname");
                                String message = jsonObject.getString("message");
                                notificationBean.setText(fullname + " " + message);
                                notificationBean.setFullname(fullname);
                                String created = jsonObject.getString("added");
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//2016-01-24T16:00:00.000Z
                                sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                                long time = sdf.parse(created).getTime();
                                long now = System.currentTimeMillis();
                                CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
                                notificationBean.setTime(String.valueOf(ago));
                                notificationBean.setStatus(jsonObject.getString("status"));
                                notificationBean.setId(jsonObject.getString("id"));
                                notificationBean.setPostid(jsonObject.getString("postid"));
                                notificationBean.setNotification_type(jsonObject.getString("notification_type"));
                                notificationBean.setGroupid(jsonObject.getString("groupid"));
                                notificationBean.setAdded_by(jsonObject.getString("added_by"));
                                notificationBean.setGroup_name(jsonObject.getString("group_name"));
                                notificationBean.setGroup_image(jsonObject.getString("group_image"));
                                notificationBeanArrayList.add(notificationBean);
                            }

                        } catch (Exception e) {
                            loaderDiloag.dismissDiloag();
                        }


                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (notificationBeanArrayList.size() > 0) {
                                    notificationAdapter = new NotificationAdapter(getActivity(), notificationBeanArrayList, Notification.this);
                                    notificationRecycler.setAdapter(notificationAdapter);
                                }else {
                                    notificationRecycler.setVisibility(View.GONE);
                                    noNotification.setVisibility(View.VISIBLE);
                                }
                                loaderDiloag.dismissDiloag();
                            }
                        });


                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    @Override
    public void readANDdelete(String type, int pos) {
        if (type.equals("D")) {
            String id = notificationBeanArrayList.get(pos).getId();
            showPopup(id, pos);
        } else {
            if (notificationBeanArrayList.get(pos).getStatus().equals("read")) {
                if (notificationBeanArrayList.get(pos).getNotification_type().equals("Join Group")) {
                    startActivity(new Intent(getActivity(), GroupActivity.class).putExtra("type", "me").putExtra("name", notificationBeanArrayList.get(pos).getGroup_name())
                            .putExtra("id", notificationBeanArrayList.get(pos).getGroupid()).putExtra("image", notificationBeanArrayList.get(pos).getGroup_image()));

                }
                if (notificationBeanArrayList.get(pos).getNotification_type().equals("Join Group Approved")) {
                    startActivity(new Intent(getActivity(), GroupActivity.class).putExtra("type", "other").putExtra("name", notificationBeanArrayList.get(pos).getGroup_name())
                            .putExtra("id", notificationBeanArrayList.get(pos).getGroupid()).putExtra("image", notificationBeanArrayList.get(pos).getGroup_image()));

                } else if (notificationBeanArrayList.get(pos).getNotification_type().equals("Like")) {
                    Intent intent = new Intent(getActivity(), CommentActivity.class);
                    Log.e("postBean.getId()", String.valueOf(notificationBeanArrayList.get(pos).getPostid()));
                    intent.putExtra("id_", String.valueOf(notificationBeanArrayList.get(pos).getPostid()));
                    startActivity(intent);
                    // startActivity(new Intent(getActivity(), GroupActivity.class).putExtra("type", "other").putExtra("name", "Group Detail").putExtra("id", notificationBeanArrayList.get(pos).getGroupid()).putExtra("image", "null"));

                } else if (notificationBeanArrayList.get(pos).getNotification_type().equals("Share")) {
                    Intent intent = new Intent(getActivity(), CommentActivity.class);
                    Log.e("postBean.getId()", String.valueOf(notificationBeanArrayList.get(pos).getPostid()));
                    intent.putExtra("id_", String.valueOf(notificationBeanArrayList.get(pos).getPostid()));
                    startActivity(intent);
                    // startActivity(new Intent(getActivity(), GroupActivity.class).putExtra("type", "other").putExtra("name", "Group Detail").putExtra("id", notificationBeanArrayList.get(pos).getGroupid()).putExtra("image", "null"));

                } else if (notificationBeanArrayList.get(pos).getNotification_type().equals("Comment")) {
                    Intent intent = new Intent(getActivity(), CommentActivity.class);
                    Log.e("postBean.getId()", String.valueOf(notificationBeanArrayList.get(pos).getPostid()));
                    intent.putExtra("id_", String.valueOf(notificationBeanArrayList.get(pos).getPostid()));
                    startActivity(intent);
                    // startActivity(new Intent(getActivity(), GroupActivity.class).putExtra("type", "other").putExtra("name", "Group Detail").putExtra("id", notificationBeanArrayList.get(pos).getGroupid()).putExtra("image", "null"));

                } else if (notificationBeanArrayList.get(pos).getNotification_type().equals("Join Group Request")) {
                    showPopupApprove(pos, notificationBeanArrayList.get(pos).getFullname());
                    // startActivity(new Intent(getActivity(), GroupActivity.class).putExtra("type", "other").putExtra("name", "Group Detail").putExtra("id", notificationBeanArrayList.get(pos).getGroupid()).putExtra("image", "null"));

                }
            } else {
                read(notificationBeanArrayList.get(pos).getId(), pos);
            }
        }
    }

    public void showPopup(final String id, final int pos) {
        LayoutInflater layoutInflater
                = (LayoutInflater) getActivity().getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_group_join_leave, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        final TextView textText = (TextView) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        textTitle.setText("Delete Notification");
        textText.setText("Are you sure, you want to delete this notification?");
        // textText.setText("Are you sure, you want to leave " + '"' + text + '"' + " Group?");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delete(id, pos);
                popupWindow.dismiss();

            }
        });
    }

    public void delete(final String postID, final int pos) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", Constant.token + "");
            Request request = new Request.Builder()
                    .url(WebApis.DELETENOTIFICATION + postID)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("respons", jsonData + "");
                        try {
                            final JSONObject jsonObject = new JSONObject(jsonData);
                            final String success = jsonObject.getString("status");
                            final String message = jsonObject.getString("message");
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (success.equals("success")) {
                                        notificationBeanArrayList.remove(pos);
                                        Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();
                                        notificationAdapter.notifyDataSetChanged();
                                    }
                                    loaderDiloag.dismissDiloag();
                                }
                            });
                        } catch (Exception e) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loaderDiloag.dismissDiloag();
                                }
                            });
                        }
                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void read(final String postID, final int pos) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", Constant.token + "");
            Log.e("Constant.READNOTIFI", WebApis.READNOTIFICATION + postID);
            Request request = new Request.Builder()
                    .url(WebApis.READNOTIFICATION + postID)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("respons", jsonData + "");
                        try {
                            final JSONObject jsonObject = new JSONObject(jsonData);
                            final String success = jsonObject.getString("status");
                            final String message = jsonObject.getString("message");
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (success.equals("success")) {

                                        try {
                                            int count = Integer.parseInt(tab_text_notification.getText().toString());
                                            tab_text_notification.setText((count - 1) + "");
                                        } catch (Exception e) {
                                        }
                                        //Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();
                                        notificationBeanArrayList.get(pos).setStatus("read");
                                        notificationAdapter.notifyDataSetChanged();
                                        if (notificationBeanArrayList.get(pos).getNotification_type().equals("Join Group")) {
                                            startActivity(new Intent(getActivity(), GroupActivity.class).putExtra("type", "me").putExtra("name", notificationBeanArrayList.get(pos).getGroup_name())
                                                    .putExtra("id", notificationBeanArrayList.get(pos).getGroupid()).putExtra("image", notificationBeanArrayList.get(pos).getGroup_image()));

                                        }
                                        if (notificationBeanArrayList.get(pos).getNotification_type().equals("Join Group Approved")) {
                                            startActivity(new Intent(getActivity(), GroupActivity.class).putExtra("type", "other").putExtra("name", notificationBeanArrayList.get(pos).getGroup_name())
                                                    .putExtra("id", notificationBeanArrayList.get(pos).getGroupid()).putExtra("image", notificationBeanArrayList.get(pos).getGroup_image()));

                                        } else if (notificationBeanArrayList.get(pos).getNotification_type().equals("Like")) {
                                            Intent intent = new Intent(getActivity(), CommentActivity.class);
                                            Log.e("postBean.getId()", String.valueOf(notificationBeanArrayList.get(pos).getPostid()));
                                            intent.putExtra("id_", String.valueOf(notificationBeanArrayList.get(pos).getPostid()));
                                            startActivity(intent);
                                            // startActivity(new Intent(getActivity(), GroupActivity.class).putExtra("type", "other").putExtra("name", "Group Detail").putExtra("id", notificationBeanArrayList.get(pos).getGroupid()).putExtra("image", "null"));

                                        } else if (notificationBeanArrayList.get(pos).getNotification_type().equals("Comment")) {
                                            Intent intent = new Intent(getActivity(), CommentActivity.class);
                                            Log.e("postBean.getId()", String.valueOf(notificationBeanArrayList.get(pos).getPostid()));
                                            intent.putExtra("id_", String.valueOf(notificationBeanArrayList.get(pos).getPostid()));
                                            startActivity(intent);
                                            // startActivity(new Intent(getActivity(), GroupActivity.class).putExtra("type", "other").putExtra("name", "Group Detail").putExtra("id", notificationBeanArrayList.get(pos).getGroupid()).putExtra("image", "null"));

                                        } else if (notificationBeanArrayList.get(pos).getNotification_type().equals("Join Group Request")) {
                                            showPopupApprove(pos, notificationBeanArrayList.get(pos).getFullname());
                                            // startActivity(new Intent(getActivity(), GroupActivity.class).putExtra("type", "other").putExtra("name", "Group Detail").putExtra("id", notificationBeanArrayList.get(pos).getGroupid()).putExtra("image", "null"));

                                        }
                                    }
                                    loaderDiloag.dismissDiloag();
                                }
                            });
                        } catch (Exception e) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loaderDiloag.dismissDiloag();
                                }
                            });
                        }
                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }


    public void showPopupApprove(final int pos, final String text) {
        LayoutInflater layoutInflater
                = (LayoutInflater) getActivity()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_group_join_leave, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        TextView textText = (TextView) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        textText.setText("Are you sure, you want to add " + '"' + text + '"' + " in your Group?");
        textTitle.setText("Join Approval request");
        btnSumbmit.setText("Approve");
        btnCancel.setText("Cancel");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //DeleteGroup(id);
                approve(pos);

                popupWindow.dismiss();

            }
        });
    }


    public void approve(final int pos) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", Constant.token + "");
            Request request = new Request.Builder()
                    .url(WebApis.APPROVECLOSEGROUP + notificationBeanArrayList.get(pos).getAdded_by() + "/" + notificationBeanArrayList.get(pos).getGroupid() + "/" + notificationBeanArrayList.get(pos).getId())
                    .get()
                    .addHeader("authorization", Constant.token)
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("respons", jsonData + "");
                        try {
                            final JSONObject jsonObject = new JSONObject(jsonData);
                            final String success = jsonObject.getString("status");
                            final String message = jsonObject.getString("message");
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (success.equals("success")) {
                                        notificationBeanArrayList.remove(pos);
                                        Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();
                                        notificationAdapter.notifyDataSetChanged();
                                    }
                                    loaderDiloag.dismissDiloag();
                                }
                            });
                        } catch (Exception e) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loaderDiloag.dismissDiloag();
                                }
                            });
                        }
                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }
}
