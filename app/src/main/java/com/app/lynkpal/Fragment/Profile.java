package com.app.lynkpal.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Adapter.CompanyFollowersAdapter;
import com.app.lynkpal.Adapter.CompanyReviewAdapter;
import com.app.lynkpal.Adapter.GalleryGridAdapter;
import com.app.lynkpal.Bean.CompanyFollowersBean;
import com.app.lynkpal.Bean.CompanyReviewsBean;
import com.app.lynkpal.Bean.GridBean;
//import com.app.lynkpal.BuildConfig;
import com.app.lynkpal.Helper.AlbumStorageDirFactory;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Base64;
import com.app.lynkpal.Helper.BaseAlbumDirFactory;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.ExpandadHeightGridview;
import com.app.lynkpal.Helper.ExpandedHightListview;
import com.app.lynkpal.Helper.FilePath;
import com.app.lynkpal.Helper.FroyoAlbumDirFactory;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.deleteFollowers;
import com.app.lynkpal.Interface.showImagePopup;
import com.app.lynkpal.LoginActivity;
import com.app.lynkpal.MapActivity;
import com.app.lynkpal.R;
import com.app.lynkpal.UpdateCompanyProfile;
import com.app.lynkpal.storage.DbHelper;
import com.bumptech.glide.Glide;
/*
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
*/
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.squareup.picasso.Picasso;

import net.karthikraj.shapesimage.ShapesImage;

import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static android.content.Context.MODE_PRIVATE;
import static android.util.Base64.encodeToString;

public class Profile extends Fragment implements View.OnClickListener, deleteFollowers, showImagePopup,OnMapReadyCallback {
    private static final int REQUEST_CAMERA_ACCESS_PERMISSION = 5674;
    private static final String TAG = Profile.class.getSimpleName();
    TextView head;
    String message = "";
    ImageView imgDetails, imgReviews, imgAddress, imgGallery, imgFollowers, imgCover, imgProfile, imgEdit, imgSave;
    TextView txtDetails, txtReviews, txtAddress, txtGallery, txtFollowers, txtAddMore, txtAddMoreReview, textLocation, galleryTxt;
    TextView txtMainName, txtSubName, txtFollowing, textContent, txtSave, txtCancel, followertxt;
    LinearLayout linDetails, linReviews, linAddress, linGallery, linFollowers, linDesGallery, addMore, linSaveCancel, linDesReviews, linDesFollowers, lin_Location;
    String profile_pic, company_link, company_description = "", mobile, company_lon, followers_count, company_name, company_lat, industryname, company_banner, follow_status;
    private  String address = " ";
    String status;
    RelativeLayout addMoreReview, mNewLayout;
    TextView mNewText;
    List<GridBean> gridBeanList = new ArrayList<>();
    List<CompanyReviewsBean> companyReviewsArrayList = new ArrayList<CompanyReviewsBean>();
    List<CompanyFollowersBean> companyFollowersBeanList = new ArrayList<CompanyFollowersBean>();
    CompanyReviewsBean companyReviewsBean;
    CompanyFollowersBean companyFollowersBean;
    ExpandadHeightGridview gridView;
    GalleryGridAdapter gridViewAdapter;
    CompanyReviewAdapter companyReviewAdapter;
    CompanyFollowersAdapter companyFollowersAdapter;
    GridBean gridBean;
    int height, width;
    String encodedString = "";
    FileBody bin;
    ExpandedHightListview expReviewsList, expFollowersList;
    ScrollView mainScrollView;
    ArrayList<String> imagesEncodedList;
    String imageEncoded;
    //GoogleMap mMap;
    private String company_detail = " ";
    FrameLayout mframe;
    Boolean moveToDetails=false;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;


    public Boolean mLocationpermissionGranted = false;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    GoogleMap mMap;
    SharedPreferences mPref;
    SharedPreferences.Editor mEdit;


    private static final String JPEG_FILE_PREFIX = "IMG_";
    private static final String JPEG_FILE_SUFFIX = ".jpg";
    AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    private String mCurrentPhotoPath;
    private  LatLng mylocationValue;

    public  String adrs;
    public String  new_address;
    private LinearLayout mCompanyLinear;

    DbHelper dbHelper;

    //SharedPreferences settings;
    public Profile() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        // settings = (getActivity()).getSharedPreferences(Constant.PREFS_NAME, 0);
        //   settings.getString("profile_pic", "");
        dbHelper = new DbHelper(getContext());
        //Declare Fonts Typeface
        Typeface tf_reg = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Bold.ttf");

        getLocationPermissions(getContext());


        //initialize Imageview
      //  mCompanyLinear = rootView.findViewById(R.id.company_linear);
        imgDetails = (ImageView) rootView.findViewById(R.id.imgDetail);
        imgReviews = (ImageView) rootView.findViewById(R.id.imgReviews);
        imgAddress = (ImageView) rootView.findViewById(R.id.imgAddress);
        imgGallery = (ImageView) rootView.findViewById(R.id.imgGallery);
        imgFollowers = (ImageView) rootView.findViewById(R.id.imgFollowers);
        imgProfile = (ImageView) rootView.findViewById(R.id.imgProfile);
        imgCover = (ImageView) rootView.findViewById(R.id.imgCover);
        imgEdit = (ImageView) rootView.findViewById(R.id.imgEdit);
        imgSave = (ImageView) rootView.findViewById(R.id.imgSave);
        mframe = rootView.findViewById(R.id.frame);

        //initialize Textview
        head = (TextView) rootView.findViewById(R.id.head);
        txtMainName = (TextView) rootView.findViewById(R.id.txtMainName);
        txtSubName = (TextView) rootView.findViewById(R.id.txtSubName);
        txtFollowing = (TextView) rootView.findViewById(R.id.txtFollowing);
        textContent = (TextView) rootView.findViewById(R.id.textContent);
        txtDetails = (TextView) rootView.findViewById(R.id.txtDetails);
        txtReviews = (TextView) rootView.findViewById(R.id.txtReviews);
        txtAddress = (TextView) rootView.findViewById(R.id.txtAddress);
        txtGallery = (TextView) rootView.findViewById(R.id.txtGallery);
        txtFollowers = (TextView) rootView.findViewById(R.id.txtFollowers);
        txtSave = (TextView) rootView.findViewById(R.id.txtSave);
        textLocation = (TextView) rootView.findViewById(R.id.textLocation);
        txtCancel = (TextView) rootView.findViewById(R.id.txtCancel);
        txtAddMore = (TextView) rootView.findViewById(R.id.txtAddMore);
        txtAddMoreReview = (TextView) rootView.findViewById(R.id.txtAddMoreReview);
        followertxt = rootView.findViewById(R.id.follower_text);
        mNewLayout = rootView.findViewById(R.id.newLayout);
        mNewText = rootView.findViewById(R.id.dynamic_text);

        //initialize Layout
        linDetails = (LinearLayout) rootView.findViewById(R.id.linDetail);
        linReviews = (LinearLayout) rootView.findViewById(R.id.linReviews);
        linAddress = (LinearLayout) rootView.findViewById(R.id.linAddress);
        linGallery = (LinearLayout) rootView.findViewById(R.id.linGallery);
        linFollowers = (LinearLayout) rootView.findViewById(R.id.linFollowers);
        linDesGallery = (LinearLayout) rootView.findViewById(R.id.linDesGallery);
        linDesReviews = (LinearLayout) rootView.findViewById(R.id.linDesReviews);
        linDesFollowers = (LinearLayout) rootView.findViewById(R.id.linDesFollowers);
        addMore = (LinearLayout) rootView.findViewById(R.id.addMore);
        lin_Location = (LinearLayout) rootView.findViewById(R.id.lin_Location);
        linSaveCancel = (LinearLayout) rootView.findViewById(R.id.linSaveCancel);
        addMoreReview = (RelativeLayout) rootView.findViewById(R.id.addMoreReview);

        gridView = (ExpandadHeightGridview) rootView.findViewById(R.id.grid);
        expReviewsList = (ExpandedHightListview) rootView.findViewById(R.id.expReviewsList);
        expFollowersList = (ExpandedHightListview) rootView.findViewById(R.id.expFollowersList);
        mainScrollView = (ScrollView) rootView.findViewById(R.id.mainScrollView);
        galleryTxt = rootView.findViewById(R.id.gallery_txt);
        //Change Fonts
        head.setTypeface(tf_bold);
        txtMainName.setTypeface(tf_bold);
        txtSubName.setTypeface(tf_reg);
        txtFollowing.setTypeface(tf_reg);
        textContent.setTypeface(tf_reg);
        txtDetails.setTypeface(tf_reg);
        txtReviews.setTypeface(tf_reg);
        txtAddress.setTypeface(tf_reg);
        txtGallery.setTypeface(tf_reg);
        txtFollowers.setTypeface(tf_reg);
        txtSave.setTypeface(tf_reg);
        txtCancel.setTypeface(tf_reg);
        txtAddMore.setTypeface(tf_reg);
        txtAddMoreReview.setTypeface(tf_reg);
        textLocation.setTypeface(tf_reg);

        //Change Color FirstTime
        ChangeColorImageView(imgDetails);
        ChangeColorTextView(txtDetails);
        ChangeColorLinear(linDetails);


        /*SharedPreferences pref = getContext().getSharedPreferences("Company_Prof",MODE_PRIVATE);
        company_detail = pref.getString("Company_Detail",null);*/
        //Visible and unvisible

        linDesGallery.setVisibility(View.GONE);
        linDesReviews.setVisibility(View.GONE);
        linDesFollowers.setVisibility(View.GONE);
        lin_Location.setVisibility(View.GONE);
        textContent.setVisibility(View.VISIBLE);

        //set OnClickListener
        linDetails.setOnClickListener(this);
        linReviews.setOnClickListener(this);
        linAddress.setOnClickListener(this);
        linGallery.setOnClickListener(this);
        linFollowers.setOnClickListener(this);
        imgEdit.setOnClickListener(this);
        addMore.setOnClickListener(this);
        linSaveCancel.setOnClickListener(this);
        txtSave.setOnClickListener(this);
        txtCancel.setOnClickListener(this);
        addMoreReview.setOnClickListener(this);
        // Picasso.with(getActivity()).load(WebApis.userProfileImage+profile_pic)
        //kishan getDetails();
        // Inflate the layout for this fragment


        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }
        return rootView;

    }


    private void getLocationPermissions(Context context) {
        Log.d(TAG, "getting Location permissions");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        if (ContextCompat.checkSelfPermission(context, FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(context, COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLocationpermissionGranted = true;
                initMap();
            } else {
                ActivityCompat.requestPermissions(getActivity(), permissions, LOCATION_PERMISSION_REQUEST_CODE);
            }
        } else {
            ActivityCompat.requestPermissions(getActivity(), permissions, LOCATION_PERMISSION_REQUEST_CODE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "onRequest Permission result called");
        mLocationpermissionGranted = false;
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            mLocationpermissionGranted = false;
                            return;
                        }
                        mLocationpermissionGranted = true;
                        initMap();
                    }
                }
            }
        }
    }


    private void getDeviceLocation(LatLng mylocationValue) {
        Log.d(TAG, "getting current location");

        try {
            if (mLocationpermissionGranted)
            {
              // getDetails();

                Log.d(TAG, "getDeviceLocation: ChangeAddress " + new_address + mylocationValue);
                if (mylocationValue!=null)
                {
                    if (mMap!=null)
                    {
                        Marker marker = mMap.addMarker(new MarkerOptions().position(mylocationValue).title(address));
                        marker.showInfoWindow();
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mylocationValue,15));
                    }

                }
                else
                {
                    return;
                }
               //  mMap.setMyLocationEnabled(true);
              /*  if (MapActivity.mLocation!=null)
                {
                    markerOptions.position(MapActivity.mLocation);
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(MapActivity.mLocation));
                    mMap.addMarker(markerOptions);
                    Log.d(TAG, "getDeviceLocation: " + MapActivity.mLocation);
                }
                else
                {
                    Log.d(TAG, "getDeviceLocation: no Location " +MapActivity.mLocation);
                    return;
                }*/
            }
        } catch (SecurityException e) {
            Log.d(TAG, "getDeviceLocation: " + e.getMessage());
        }


    }


    private void moveCamera(LatLng latLng, float zoom, String title) {
        Log.d(TAG, "moveCamera: latlong " + latLng.longitude + "longitude " + latLng.latitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            Log.d(TAG, "setUserVisibleHint: visible");
            try {
                  getDetails();
                //getGallery();
                getReviews();
                galleryTxt.setVisibility(View.GONE);
                getFollowers();
            } catch (Exception e) {

            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    public void ChangeColorImageView(ImageView imageView) {
        imgDetails.setColorFilter(getContext().getResources().getColor(R.color.black_overlay));
        imgReviews.setColorFilter(getContext().getResources().getColor(R.color.black_overlay));
        imgAddress.setColorFilter(getContext().getResources().getColor(R.color.black_overlay));
        imgGallery.setColorFilter(getContext().getResources().getColor(R.color.black_overlay));
        imgFollowers.setColorFilter(getContext().getResources().getColor(R.color.black_overlay));
        imageView.setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_IN);

    }

    public void ChangeColorTextView(TextView txtView) {
        txtDetails.setTextColor(getContext().getResources().getColor(R.color.black_overlay));
        txtReviews.setTextColor(getContext().getResources().getColor(R.color.black_overlay));
        txtAddress.setTextColor(getContext().getResources().getColor(R.color.black_overlay));
        txtGallery.setTextColor(getContext().getResources().getColor(R.color.black_overlay));
        txtFollowers.setTextColor(getContext().getResources().getColor(R.color.black_overlay));
        txtView.setTextColor(Color.parseColor("#ffffff"));

    }

    public void ChangeColorLinear(LinearLayout linView) {
        linDetails.setBackgroundColor(Color.parseColor("#ffffff"));
        linReviews.setBackgroundColor(Color.parseColor("#ffffff"));
        linAddress.setBackgroundColor(Color.parseColor("#ffffff"));
        linGallery.setBackgroundColor(Color.parseColor("#ffffff"));
        linFollowers.setBackgroundColor(Color.parseColor("#ffffff"));
        linView.setBackgroundColor(Color.parseColor("#375cc8"));

    }

    @Override
    public void onClick(View view) {
        if (view == linDetails) {
            if (!company_description.equals("null")) {
                lin_Location.setVisibility(View.GONE);
                galleryTxt.setVisibility(View.GONE);
                textContent.setText(company_description + "");
                linDesGallery.setVisibility(View.GONE);
                linDesReviews.setVisibility(View.GONE);
                lin_Location.setVisibility(View.GONE);
                linDesFollowers.setVisibility(View.GONE);
                textContent.setVisibility(View.VISIBLE);
                ChangeColorImageView(imgDetails);
                ChangeColorTextView(txtDetails);
                ChangeColorLinear(linDetails);
            } else {
                lin_Location.setVisibility(View.GONE);
                galleryTxt.setVisibility(View.GONE);
                textContent.setText("You haven’t added this information yet.");
                linDesGallery.setVisibility(View.GONE);
                linDesReviews.setVisibility(View.GONE);
                lin_Location.setVisibility(View.GONE);
                linDesFollowers.setVisibility(View.GONE);
                textContent.setVisibility(View.VISIBLE);
                ChangeColorImageView(imgDetails);
                ChangeColorTextView(txtDetails);
                ChangeColorLinear(linDetails);
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mainScrollView.fullScroll(ScrollView.FOCUS_UP);
                }
            }, 1000);
        } else if (view == linReviews)
        {
           txtGallery.setVisibility(View.GONE);
            galleryTxt.setVisibility(View.GONE);
            getReviews();

            textLocation.setVisibility(View.VISIBLE);
            linDesGallery.setVisibility(View.GONE);
            linDesReviews.setVisibility(View.VISIBLE);
            linDesFollowers.setVisibility(View.GONE);
            textContent.setVisibility(View.GONE);
            lin_Location.setVisibility(View.GONE);
            ChangeColorImageView(imgReviews);
            ChangeColorTextView(txtReviews);
            ChangeColorLinear(linReviews);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run()
                {

                    Log.d(TAG, "run: Scrolling");
                }
            }, 1000);
        } else if (view == linAddress)
        {


            if (!address.equals("null"))
            {
                /*if (new_address!="null")
                {

                }*/
                Log.d(TAG, "Address " + address);
                textLocation.setText(address + "");

                if (mframe.getVisibility() == View.GONE)
                {
                    mframe.setVisibility(View.VISIBLE);
                }
            } else if (address.equals("null"))
            {
                textLocation.setText("You haven’t added Location information yet.");
            }
            txtGallery.setVisibility(View.GONE);
            galleryTxt.setVisibility(View.GONE);


            linDesGallery.setVisibility(View.GONE);
            linDesReviews.setVisibility(View.GONE);
            linDesFollowers.setVisibility(View.GONE);
            textContent.setVisibility(View.GONE);
            lin_Location.setVisibility(View.VISIBLE);
            ChangeColorImageView(imgAddress);
            ChangeColorTextView(txtAddress);
            ChangeColorLinear(linAddress);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mainScrollView.fullScroll(ScrollView.FOCUS_UP);
                }
            }, 1000);

        } else if (view == linGallery)
        {
            getGallery();
            linDesGallery.setVisibility(View.VISIBLE);
            linDesReviews.setVisibility(View.GONE);
            lin_Location.setVisibility(View.GONE);
            linDesFollowers.setVisibility(View.GONE);
            textContent.setVisibility(View.GONE);
            ChangeColorImageView(imgGallery);
            ChangeColorTextView(txtGallery);
            ChangeColorLinear(linGallery);
           /* new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mainScrollView.fullScroll(ScrollView.FOCUS_UP);
                }
            }, 1000);*/
        } else if (view == linFollowers)
        {
            txtGallery.setVisibility(View.GONE);
            galleryTxt.setVisibility(View.GONE);
            textLocation.setVisibility(View.GONE);
            linDesGallery.setVisibility(View.GONE);
            linDesReviews.setVisibility(View.GONE);
            linDesFollowers.setVisibility(View.VISIBLE);
            lin_Location.setVisibility(View.GONE);
            textContent.setVisibility(View.GONE);
            ChangeColorImageView(imgFollowers);
            ChangeColorTextView(txtFollowers);
            ChangeColorLinear(linFollowers);
            new Handler().postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    mainScrollView.fullScroll(ScrollView.FOCUS_UP);
                }
            }, 1000);
        }
        else if (view == txtFollowers)
        {

        }
        else if (view == imgEdit)
        {
            startActivity(new Intent(getActivity(), UpdateCompanyProfile.class));
        } else if (view == addMore) {
            selectImage();
        } else if (view == txtCancel) {
            linSaveCancel.setVisibility(View.GONE);
            addMore.setVisibility(View.VISIBLE);
            bin = null;
            encodedString = "";
        } else if (view == txtSave) {
            ImageUploader();
        } else if (view == addMoreReview) {
            showPopup();

        }
    }

    private void initMap() {
        Log.d(TAG, "initializing Map");
        MapFragment mapFragment = (MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(Profile.this);
    }

    public void ImageUploader() {
        if (txtGallery.getVisibility() == View.VISIBLE) {
            txtGallery.setVisibility(View.GONE);
        }
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            //String params="{\r\n\"description\":\"Write text to post\",\r\n\"postFile\":\"sadc\",\r\n\"extension\":\"jpeg\"\r\n}";
            JSONObject params = new JSONObject();
            JSONArray array = new JSONArray();
            array.put(encodedString);
            try {
                params.put("images", array);
                Log.e("param", params.toString() + "");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(mediaType, params + "");
            Request request = new Request.Builder()
                    .url(WebApis.UploadGalleryAPI)
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    message = jsonObject.getString("message");
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            linSaveCancel.setVisibility(View.GONE);
                                            addMore.setVisibility(View.VISIBLE);
                                            bin = null;
                                            encodedString = "";
                                            Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();
                                            getGallery();
                                        }
                                    });
                                } else {
                                    message = jsonObject.getString("message");
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
          //  Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void ImageUploaderDirect() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            //String params="{\r\n\"description\":\"Write text to post\",\r\n\"postFile\":\"sadc\",\r\n\"extension\":\"jpeg\"\r\n}";
            JSONObject params = new JSONObject();
            JSONArray array = new JSONArray();
            Log.e("LOG_TAG", "imagesEncodedList>>" + imagesEncodedList.size());
            String lla = "";
            for (int a = 0; a < imagesEncodedList.size(); a++) {
                array.put(imagesEncodedList.get(a));
            }
            Log.e("LOG_lla2", lla);

            try {
                params.put("images", array);
                Log.e("param", params.toString() + "");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(mediaType, params + "");
            Request request = new Request.Builder()
                    .url(WebApis.UploadGalleryAPI)
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    message = jsonObject.getString("message");
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            linSaveCancel.setVisibility(View.GONE);
                                            addMore.setVisibility(View.VISIBLE);
                                            bin = null;
                                            encodedString = "";
                                            imagesEncodedList.clear();
                                            Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();
                                            getGallery();
                                        }
                                    });
                                } else {
                                    message = jsonObject.getString("message");
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                            && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                REQUEST_CAMERA_ACCESS_PERMISSION);
                    } else {
                      /*  Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePicture.resolveActivity(getActivity().getPackageManager()) != null) {
                            startActivityForResult(takePicture, 1);
                        }*/
                        dispatchTakePictureIntent();
                    }

                } else if (options[item].equals("Choose from Gallery")) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        if (getActivity().checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                                == PackageManager.PERMISSION_GRANTED || getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                           /* Log.v("TAG", "Permission is granted");
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image*//*");
                            //  Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(photoPickerIntent, 2);//one can be replaced with any action code*/
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), 2);
                        } else {

                            Log.v("TAG", "Permission is revoked");
                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                        }
                    } else {

                        Log.v("TAG", "Permission is granted");
                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                        photoPickerIntent.setType("image/*");
                        //  Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(photoPickerIntent, 2);//one can be replaced with any action code
                    }

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void dispatchTakePictureIntent() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);


        File f = null;
        /*try {
            Uri photoURI = FileProvider.getUriForFile(getActivity(),
                    BuildConfig.APPLICATION_ID + ".provider",
                    setUpPhotoFile());
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        } catch (IOException e) {
            e.printStackTrace();
        }*/


        startActivityForResult(takePictureIntent, 1);
    }

    private File setUpPhotoFile() throws IOException {

        File f = createImageFile();
        mCurrentPhotoPath = f.getAbsolutePath();

        return f;
    }

    private void handleBigCameraPhoto() {

        if (mCurrentPhotoPath != null) {
            setPic();
            galleryAddPic();
            mCurrentPhotoPath = null;
        }

    }

    private void setPic() {

        /* There isn't enough memory to open up more than a camera photos */
        /* So pre-scale the target bitmap into which the file is decoded */

        /* Get the size of the ImageView */
        int targetW = 500;//.getWidth();
        int targetH = 500;////mImageView.getHeight();

        /* Get the size of the image */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        /* Figure out which way needs to be reduced less */
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        }

        /* Set bitmap options to scale the image decode target */
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        /* Decode the JPEG file into a Bitmap */
        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        encodedString = convertPreImagetBitmapToString(bitmap);
        imgSave.setImageBitmap(bitmap);
        File file1 = new File(String.valueOf(bitmap));
        bin = new FileBody(file1);
        // imgSave.setVisibility(View.VISIBLE);
        addMore.setVisibility(View.GONE);
        linSaveCancel.setVisibility(View.VISIBLE);
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
        return imageF;
    }

    private File getAlbumDir() {
        File storageDir = null;

        storageDir = new File(Environment.getExternalStorageDirectory()
                + "/lynkpal");

        return storageDir;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        Log.d(TAG, "onDetach: profile Fragment");
    }

    public  void getDetails()
    {
        Log.d(TAG, "getDetails: ");
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            loaderDiloag.displayDiloag();
            //OkHttpClient client = new OkHttpClient();
            OkHttpClient client;
            client = new OkHttpClient.Builder()
                    .connectTimeout(Constant.VOLLEY_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                    .writeTimeout(Constant.WRITE_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                    .readTimeout(Constant.READ_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                    .build();
            status = "false";
            Log.e("Constant.token", Constant.token);
            Log.e("Constant.api", WebApis.CompanyDetails + Constant.user_id);
            Request request = new Request.Builder()
                    .url(WebApis.CompanyDetails + Constant.user_id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (e.toString().contains(Constant.timeout_error_string)) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(), getString(R.string.slow_internet), Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                        if (getActivity() == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getDetails", jsonData + "");
                        loaderDiloag.dismissDiloag();
                        try {
                            JSONObject jsonObject = new JSONObject(jsonData);
                            status = jsonObject.getString("status");
                            if (status.equals("success")) {
                                profile_pic = jsonObject.getString("profile_pic");
                                Log.d(TAG, "onResponse: Profilee "  + WebApis.userProfileImage + profile_pic);
                                company_link = jsonObject.getString("company_link");
                                company_description = jsonObject.getString("company_description");
                                mobile = jsonObject.getString("mobile");
                                company_lon = jsonObject.getString("company_lon");
                                followers_count = jsonObject.getString("followers_count");
                                company_name = jsonObject.getString("company_name");
                                address = jsonObject.getString("address");
                                company_lat = jsonObject.getString("company_lat");
                                industryname = jsonObject.getString("industryname");
                                company_banner = jsonObject.getString("company_banner");

                                if (company_banner!="null")
                                {
                                    Log.d(TAG, "onResponse:CompanyBanner " + WebApis.UPLOAD_COMPANY_BANNER + company_banner);


                                }
                                follow_status = jsonObject.getString("follow_status");

                                SharedPreferences gpref = getActivity().getSharedPreferences("Location",MODE_PRIVATE);
                                SharedPreferences.Editor editor = gpref.edit();
                                editor.putString("UserAddress",address);
                                editor.commit();
                                Log.d(TAG, "onResponse: AddressDetail " + address );
                              //  getLocationFromAddress(getContext(),address);
                              //  getDeviceLocation();



                            }
                        } catch (Exception e) {

                        }


                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                                if (status.equals("success")) {
                                    Glide.with(getActivity()).load(WebApis.userProfileImage + profile_pic).into(imgProfile);
                                    Glide.with(getContext()).load(WebApis.UPLOAD_COMPANY_BANNER + company_banner).into(imgCover);
                                    Log.e("profile_pic_us", WebApis.userProfileImage + profile_pic);
                                   // Picasso.with(getActivity()).load(WebApis.CmpBannerImage + company_banner).placeholder(R.drawable.noimage).error(R.drawable.noimage).into(imgProfile);
                                   // Picasso.with(getActivity()).load(WebApis.CmpBannerImage + company_banner).placeholder(R.drawable.noimage).error(R.drawable.noimage).into(imgCover);
                                    if (!company_description.equals("null")) {
                                        textContent.setText(company_description);
                                    } else {
                                        textContent.setText("You haven’t added this information yet.");
                                        linDesReviews.setVisibility(View.GONE);
                                    }
                                    if (!company_name.equals("null")) {
                                        txtMainName.setText(company_name);
                                    } else {
                                        txtMainName.setText("Company Name");
                                    }
                                    if (industryname.equals("null") || industryname.trim().equals("")) {
                                        txtSubName.setText("Industry Name");
                                    } else {
                                        txtSubName.setText(industryname);
                                    }

                                    if (!address.equals("null"))
                                    {
                                        getLocationFromAddress(getContext(),address);
                                        Log.d(TAG, "run:Current Address " + address);
                                    }

                                    ChangeColorImageView(imgDetails);
                                    ChangeColorTextView(txtDetails);
                                    ChangeColorLinear(linDetails);
                                    linDesGallery.setVisibility(View.GONE);
                                    linDesReviews.setVisibility(View.GONE);
                                    linDesFollowers.setVisibility(View.GONE);
                                    lin_Location.setVisibility(View.GONE);
                                    textContent.setVisibility(View.VISIBLE);
                                    textLocation.setVisibility(View.GONE);

                                    try {

                                      /*  LatLng sydney = new LatLng(Float.parseFloat(company_lat), Float.parseFloat(company_lon));
                                        mMap.addMarker(new MarkerOptions().position(sydney).title(txtMainName.getText().toString() + ""));
                                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 6.0f));*/
                                        // mMap.setMinZoomPreference(6.0f);
                                        //   mMap.setMaxZoomPreference(14.0f);
                                    } catch (Exception e) {

                                    }

                                }
                            }
                        });


                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == 1) {
                handleBigCameraPhoto();
            } else if (requestCode == 2) {
                moveToDetails=true;
                try {
                    // Get the Image from data

                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    imagesEncodedList = new ArrayList<String>();
                    if (data.getData() != null) {
                        Log.d(TAG, "onActivityResult"+"Normal data");

                        Uri mImageUri = data.getData();
                        Picasso.with(getActivity()).load(mImageUri.toString()).into(imgSave);
                        addMore.setVisibility(View.GONE);
                        linSaveCancel.setVisibility(View.VISIBLE);
                        String selectedFilePath = FilePath.getPath(getActivity(), mImageUri);
                        InputStream inputStream = new FileInputStream(selectedFilePath);//You can get an inputStream using any IO API
                        byte[] bytes;
                        byte[] buffer = new byte[8192];
                        int bytesRead;
                        ByteArrayOutputStream output = new ByteArrayOutputStream();
                        try {
                            while ((bytesRead = inputStream.read(buffer)) != -1) {
                                output.write(buffer, 0, bytesRead);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        bytes = output.toByteArray();
                        encodedString = encodeToString(bytes, android.util.Base64.DEFAULT);

                    } else {
                        if (data.getClipData() != null) {
                            Log.d(TAG, "onActivityResult"+"Clip data");

                            ClipData mClipData = data.getClipData();
                            ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                            for (int i = 0; i < mClipData.getItemCount(); i++) {

                                ClipData.Item item = mClipData.getItemAt(i);
                                Uri uri = item.getUri();
                                String selectedFilePath = FilePath.getPath(getActivity(), uri);
                                InputStream inputStream = new FileInputStream(selectedFilePath);//You can get an inputStream using any IO API
                                byte[] bytes;
                                byte[] buffer = new byte[8192];
                                int bytesRead;
                                ByteArrayOutputStream output = new ByteArrayOutputStream();
                                try {
                                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                                        output.write(buffer, 0, bytesRead);
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                bytes = output.toByteArray();
                                String encodedString = encodeToString(bytes, android.util.Base64.DEFAULT);
                                imagesEncodedList.add(encodedString);
                            }
                            Log.e("LOG_TAG", "imagesEncodedList>>" + imagesEncodedList.size());
                            String lla = "";
                            for (int a = 0; a < imagesEncodedList.size(); a++) {
                                lla = lla + String.valueOf(imagesEncodedList.get(a));
                            }
                            Log.e("LOG_lla2", lla);
                            ImageUploaderDirect();
                        }
                    }
                    /*Uri selectedFileUri = data.getData();
                    Picasso.with(getActivity()).load(selectedFileUri.toString()).into(imgSave);
                    addMore.setVisibility(View.GONE);
                    linSaveCancel.setVisibility(View.VISIBLE);
                    String selectedFilePath = FilePath.getPath(getActivity(), selectedFileUri);
                    File file = new File(selectedFilePath);
                    bin = new FileBody(file);
                    InputStream inputStream = new FileInputStream(selectedFilePath);//You can get an inputStream using any IO API
                    byte[] bytes;
                    byte[] buffer = new byte[8192];
                    int bytesRead;
                    ByteArrayOutputStream output = new ByteArrayOutputStream();
                    try {
                        while ((bytesRead = inputStream.read(buffer)) != -1) {
                            output.write(buffer, 0, bytesRead);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    bytes = output.toByteArray();
                    encodedString = encodeToString(bytes, android.util.Base64.DEFAULT);
                    Log.e("encodedString", encodedString);*/

                } catch (Exception e) {
                    Log.e("eroore", e.toString());
                    e.printStackTrace();
                }
            }
        }
    }

    public void showPopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater) ((Activity) getActivity()).getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.add_review, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        RelativeLayout linearLayout = (RelativeLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        TextView txtname = (TextView) popupView.findViewById(R.id.txtname);
        final MaterialRatingBar ratingbar = (MaterialRatingBar) popupView.findViewById(R.id.ratingbar);
        final EditText textText = (EditText) popupView.findViewById(R.id.txtText);
        ShapesImage ivProfileUser = (ShapesImage) popupView.findViewById(R.id.ivProfileUser);
        Glide.with(getActivity()).load(WebApis.userProfileImage + profile_pic)
                /*.placeholder(R.drawable.image).error(R.drawable.image)*/.into(ivProfileUser);
        TextView btnCancel = (TextView) popupView.findViewById(R.id.btnCancel);
        TextView btnSumbmit = (TextView) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(((Activity) getActivity()).getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(((Activity) getActivity()).getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(((Activity) getActivity()).getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        txtname.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = textText.getText().toString();
                if (text.trim().length() > 0) {
                    float rating = ratingbar.getRating();
                    //Share(id, text);
                    addReview(text, rating);
                    popupWindow.dismiss();
                } else {
                    Toast.makeText(getActivity(), "Please Write Something", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public String convertPreImagetBitmapToString(Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream); //compress to which format you want.
        byte[] byte_arr = stream.toByteArray();
        String attachmentStr = Base64.encodeBytes(byte_arr);
        Log.e("imageStr", attachmentStr);
        // showPopup(ChatActivity.this, attachmentStr, bmp);
        return attachmentStr;
       /* int bytes = bmp.getByteCount();
//or we can calculate bytes this way. Use a different value than 4 if you don't use 32bit images.
//int bytes = b.getWidth()*b.getHeight()*4;

        ByteBuffer buffer = ByteBuffer.allocate(bytes); //Create a new buffer
        bmp.copyPixelsToBuffer(buffer); //Move the byte data to the buffer

        byte[] array = buffer.array();
        return encodeToString(array, android.util.Base64.DEFAULT);*/
    }


    public void addReview(String Comment, Float rating) {
        if (lin_Location.getVisibility() == View.VISIBLE) {
            lin_Location.setVisibility(View.GONE);
        }
        final Context context = getActivity();
        final LoaderDiloag loaderDiloag = new LoaderDiloag(context);
        if (ApplicationGlobles.isConnectingToInternet((Activity) context)) {
            loaderDiloag.displayDiloag();

            MediaType mediaType = MediaType.parse("application/json");

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("comment", Comment);
                jsonObject.put("rating", rating);
            } catch (Exception e) {

            }
            Log.e("jsonObject", jsonObject.toString() + "");
            RequestBody body = RequestBody.create(mediaType, jsonObject.toString() + "");
            Request request = new Request.Builder()
                    .url(WebApis.AddCompanyReviews + Constant.user_id + "/")
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .build();
            OkHttpClient client = new OkHttpClient();
            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (context == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {

                        final String jsonData = response.body().string();
                        Log.e("respons", jsonData + "");
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(jsonData);
                                    String success = jsonObject.getString("status");
                                    if (success.equals("success")) {
                                        Toast.makeText(context, jsonObject.getString("message") + "", Toast.LENGTH_SHORT).show();
                                        getReviews();
                                        // postList.get(pos).setComments_count(jsonObject.getInt("comment_count") + " Comments");
                                    }
                                } catch (Exception e) {

                                }
                                loaderDiloag.dismissDiloag();
                            }
                        });
                    }
                });

            } catch (Exception e) {
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(context, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void getGallery() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            gridBeanList.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.GalleryAPI", WebApis.GalleryAPI + Constant.user_id);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.GalleryAPI + Constant.user_id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getDetails", jsonData + "");
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {
                                JSONObject jsonObject = array.getJSONObject(a);
                                gridBean = new GridBean();
                                gridBean.setName(jsonObject.getString("created_at"));
                                gridBean.setImage(jsonObject.getString("image"));
                                gridBeanList.add(gridBean);
                            }

                        } catch (Exception e) {
                            loaderDiloag.dismissDiloag();
                        }


                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                                if (gridBeanList.size() > 0) {
                                    Log.d(TAG, "Gallery");
                                    gridView.setExpanded(true);
                                    gridViewAdapter = new GalleryGridAdapter(getActivity(), gridBeanList, height, Profile.this);
                                    gridView.setAdapter(gridViewAdapter);
                                } else if (gridBeanList.size() == 0) {
                                    Log.d(TAG, "GalleryNo");
                                    if (galleryTxt.getVisibility() == View.GONE)
                                    {
                                        galleryTxt.setVisibility(View.VISIBLE);
                                        galleryTxt.setText(" Oops, No Gallery Found!");

                                    }
                                }
                            }
                        });


                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    public void getReviews()
    {
        txtGallery.setVisibility(View.GONE);
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            companyReviewsArrayList.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.CompanyReviews", WebApis.CompanyReviews + Constant.user_id);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.CompanyReviews + Constant.user_id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getDetails", jsonData + "");
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {
                                JSONObject jsonObject = array.getJSONObject(a);
                                companyReviewsBean = new CompanyReviewsBean();
                                companyReviewsBean.setFullname(jsonObject.getString("fullname"));
                                companyReviewsBean.setProfile_pic(jsonObject.getString("profile_pic"));
                                companyReviewsBean.setUser(jsonObject.getString("user"));
                                companyReviewsBean.setComment(jsonObject.getString("comment"));
                                companyReviewsBean.setRating(jsonObject.getString("rating"));
                                companyReviewsBean.setCreated_at(parseDate(jsonObject.getString("created_at")));
                                companyReviewsArrayList.add(companyReviewsBean);
                            }

                        } catch (Exception e) {
                            loaderDiloag.dismissDiloag();
                        }


                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run()
                            {
                                loaderDiloag.dismissDiloag();
                                if (companyReviewsArrayList.size() > 0) {
                                    Log.d(TAG, "Reviewss");
                                    expReviewsList.setExpanded(true);
                                    companyReviewAdapter = new CompanyReviewAdapter(getActivity(), companyReviewsArrayList, height);
                                    expReviewsList.setAdapter(companyReviewAdapter);
                                } else if (companyReviewsArrayList.size() == 0) {
                                    if (lin_Location.getVisibility() == View.GONE)
                                    {
                                        lin_Location.setVisibility(View.VISIBLE);
                                        textLocation.setText(" Oops, No Review Found!");
                                        Log.d(TAG, "Review");
                                    }

                                }
                            }
                        });


                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    public void getFollowers() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            companyFollowersBeanList.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.Followers", WebApis.Followers + Constant.user_id);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.Followers + Constant.user_id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getDetails", jsonData + "");
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {
                                JSONObject jsonObject = array.getJSONObject(a);
                                companyFollowersBean = new CompanyFollowersBean();
                                companyFollowersBean.setFullName(jsonObject.getString("fullname"));
                                companyFollowersBean.setProfilePic(jsonObject.getString("profile_pic"));
                                companyFollowersBean.setUserid(jsonObject.getString("userid"));
                                companyFollowersBean.setDescription("No Parameter in API");
                                companyFollowersBeanList.add(companyFollowersBean);
                            }

                        } catch (Exception e) {
                            loaderDiloag.dismissDiloag();
                        }


                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                                if (companyFollowersBeanList.size() > 0) {
                                    expFollowersList.setExpanded(true);
                                    companyFollowersAdapter = new CompanyFollowersAdapter(getActivity(), companyFollowersBeanList, height, Profile.this);
                                    expFollowersList.setAdapter(companyFollowersAdapter);
                                } else if (companyFollowersBeanList.size() == 0) {
                                    if (expFollowersList.getVisibility() == View.VISIBLE) {
                                        expFollowersList.setVisibility(View.GONE);
                                        followertxt.setVisibility(View.VISIBLE);
                                        followertxt.setText("Oops, No Follower Found!");
                                    }
                                }
                            }
                        });


                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    public String parseDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//2016-01-24T16:00:00.000Z
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd,yyyy hh:mm a");
        String newFormat = formatter.format(testDate);
        return newFormat;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (!moveToDetails)
        getDetails();

        //getDeviceLocation();


        if (Constant.update) {
            Constant.update = false;
           // getDetails();
        }
    }

    @Override
    public void unfollow(String id, int pos) {

        showPopup(id, pos);
    }

    public void showPopup(final String id, final int pos) {
        LayoutInflater layoutInflater
                = (LayoutInflater) getActivity()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_group_join_leave, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        final TextView textText = (TextView) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        textTitle.setText("Remove Follower");
        textText.setText("Are you sure, you want to remove this follower?");
        // textText.setText("Are you sure, you want to leave " + '"' + text + '"' + " Group?");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeFollowers(id, pos);
                popupWindow.dismiss();

            }
        });
    }

    public void removeFollowers(final String id, final int pos) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());
        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.Followers", WebApis.RemoveFollowers + id);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.RemoveFollowers + id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                String message = "";
                                if (status.equals("success")) {
                                    //dbHelper.deletePostByUserId(Integer.parseInt(id));

                                    message = jsonObject.getString("message");
                                    final String finalMessage = message;
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                companyFollowersBeanList.remove(pos);
                                                companyFollowersAdapter.notifyDataSetChanged();
                                            } catch (Exception e) {
                                            }
                                            Toast.makeText(getActivity(), finalMessage + "", Toast.LENGTH_SHORT).show();
                                            //  finish();
                                        }
                                    });
                                } else {
                                    message = jsonObject.getString("message");
                                    final String finalMessage1 = message;
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(), finalMessage1 + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    @Override
    public void image(final String pos) {
        final int p = Integer.parseInt(pos);
        Log.e("here", "here");
        LayoutInflater layoutInflater
                = (LayoutInflater) getActivity().getBaseContext()
                .getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.popup_image_show_viewpager, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        ImageView ic_download = (ImageView) popupView.findViewById(R.id.ic_download);
        ImageView ic_close = (ImageView) popupView.findViewById(R.id.ic_close);
        // ImageViewTouch imageViewTouch;
        // imageViewTouch = (ImageViewTouch) popupView.findViewById(R.id.myImage);
        // Glide.with(getActivity()).load(path).error(R.drawable.noimage).into(imageViewTouch);
        ic_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        final ViewPager viewPager;
        viewPager = (ViewPager) popupView.findViewById(R.id.pager);

        ViewPagerAdapterOk adapter;
        adapter = new ViewPagerAdapterOk(getActivity());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(1);
        viewPager.setCurrentItem(p);
        ic_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                file_download(viewPager.getCurrentItem());
            }
        });
        //  WebApis.CmpGalleryImage + data.get(position).getImage()
    }

    public void file_download(int p) {
        final String uRl = WebApis.CmpGalleryImage + gridBeanList.get(p).getImage();
        Log.e("uRl", uRl + "");
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/lynkpal");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        DownloadManager mgr = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);

        Uri downloadUri = Uri.parse(uRl);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("Lynkpal")
                .setDescription("Image Downloading...")
                .setDestinationInExternalPublicDir("/lynkpal", "image.jpg");

        mgr.enqueue(request);

        Toast.makeText(getActivity(), "Successfully Downloaded", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onAttach(Context context) 
    {
        super.onAttach(context);
        Log.d(TAG, "onAttach: Profile Attach");
        if (LoginActivity.isNetworkConnected(getContext()))
        {
            getDetails();


        }
        else
        {
            Toast.makeText(getContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
            return;
        }



      //  getLocationFromAddress(getContext(),adrs);

    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.d(TAG, "onDestroy: Profile Fragment");
    }

    public void getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null)
            {
                return;
            }
            else
            {
                try {
                    Address location = address.get(0);
                    mylocationValue = new LatLng(location.getLatitude(), location.getLongitude());
                    Log.d(TAG, "getLocationFromAddress: " + mylocationValue);
                    getDeviceLocation(mylocationValue);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }





        } catch (IOException ex) {

            ex.printStackTrace();
        }


    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;
        if (mLocationpermissionGranted)
        {
          //  getDetails();
            Log.d(TAG,"check"  + mylocationValue);
            if (LoginActivity.isNetworkConnected(getContext()))
            {
               if(!address.isEmpty())
               {
                   Log.d(TAG, "onMapReady: AddressValue" + address);

               }
            }
            else
            {
                //Toast.makeText(getContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                return;
            }


        }
    }
/*
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }*/

    private class ViewPagerAdapterOk extends PagerAdapter {
        Context context;
        LayoutInflater inflater;
        int NumberOfPages;
        ImageViewTouch imgflag;
        Matrix matrix = new Matrix();
        float scale = 1f;
        ScaleGestureDetector scaleGestureDetector;
        // private ArrayList<gettersetter> mGridData = new ArrayList<gettersetter>();

        public ViewPagerAdapterOk(Context context) {
            this.context = context;
            NumberOfPages = gridBeanList.size();
        }

        @Override
        public int getCount() {
            return NumberOfPages;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((RelativeLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View itemView = inflater.inflate(R.layout.viewpager_gallery_item, container,
                    false);
            imgflag = (ImageViewTouch) itemView.findViewById(R.id.imageViewForGallery);
            //Log.e("Adapter", String.valueOf((gridBeanList.get(position).getImage())));
            Glide.with(context).load(WebApis.CmpGalleryImage + gridBeanList.get(position).getImage())/*.error(R.drawable.noimage)*/.into(imgflag);
            ((ViewPager) container).addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((RelativeLayout) object);

        }


    }


}
