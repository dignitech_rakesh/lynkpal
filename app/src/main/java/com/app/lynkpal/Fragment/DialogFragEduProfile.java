package com.app.lynkpal.Fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Adapter.EducationAdapter;
import com.app.lynkpal.Adapter.ExperienceAdapter;
import com.app.lynkpal.Bean.ProfileEducationBean;
import com.app.lynkpal.Bean.ProfileExperienceBean;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.ProfileActivity;
import com.app.lynkpal.R;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class DialogFragEduProfile extends DialogFragment
{

    private EditText medtUni,medtClass,medtSub;
    TextView mtxtStartDate,mtxtEndDate,txtMain;
    Button mbtnCancel,mbtnOK;
    Calendar myCalendar, myCalSTREDU, myCalENDEDU, myCalSTREXP, myCalENDEXP;
    public static String TAG = DialogFragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_frag_edu_profile, container,
                false);
       // getDialog().setTitle("DialogFragment Tutorial");
        // Do something else

        final String eduOrExp=getArguments().getString("openFrag");
        Log.d(TAG, "onCreateView: " +eduOrExp);
        myCalENDEDU = Calendar.getInstance();
        myCalSTREDU = Calendar.getInstance();

        medtUni=(EditText)rootView.findViewById(R.id.edtUni);
        medtClass=(EditText)rootView.findViewById(R.id.edtClass);
        medtSub=(EditText)rootView.findViewById(R.id.edtSub);
        mtxtStartDate=(TextView)rootView.findViewById(R.id.txtStartDate);
        mtxtEndDate=(TextView)rootView.findViewById(R.id.txtEndDate);
        txtMain=(TextView)rootView.findViewById(R.id.txtMain);
        mbtnCancel=(Button)rootView.findViewById(R.id.btnCancel);
        mbtnOK=(Button)rootView.findViewById(R.id.btnOK);


        if (eduOrExp.equalsIgnoreCase("educationFrag")){
            txtMain.setText("Add New Education Detail");
            medtUni.setHint("School or University");
            medtClass.setHint("Class or Degree");
            medtSub.setHint("Subject or Course");
            mtxtStartDate.setHint("Start Date");
            mtxtEndDate.setHint("End Date");
        }else{
            txtMain.setText("Add New Experience Detail");
            medtUni.setHint("Job Title");
            medtClass.setHint("Company Name");
            medtSub.setHint("Job Location");
            mtxtStartDate.setHint("Start Date");
            mtxtEndDate.setHint("End Date");
        }

        final DatePickerDialog.OnDateSetListener dateStartEdu = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalSTREDU.set(Calendar.YEAR, year);
                myCalSTREDU.set(Calendar.MONTH, monthOfYear);
                myCalSTREDU.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                mtxtStartDate.setText(sdf.format(myCalSTREDU.getTime()));
            }

        };
        final DatePickerDialog.OnDateSetListener dateEndEdu = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalENDEDU.set(Calendar.YEAR, year);
                myCalENDEDU.set(Calendar.MONTH, monthOfYear);
                myCalENDEDU.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                mtxtEndDate.setText(sdf.format(myCalENDEDU.getTime()));
            }

        };
        mtxtStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(medtSub.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);
                } catch (Exception e) {
                }
                new DatePickerDialog(getActivity(), dateStartEdu, myCalSTREDU
                        .get(Calendar.YEAR), myCalSTREDU.get(Calendar.MONTH),
                        myCalSTREDU.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
        mtxtEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(medtSub.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);
                } catch (Exception e) {
                }
                new DatePickerDialog(getActivity(), dateEndEdu, myCalENDEDU
                        .get(Calendar.YEAR), myCalENDEDU.get(Calendar.MONTH),
                        myCalENDEDU.get(Calendar.DAY_OF_MONTH)).show();

            }
        });


        mbtnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ApplicationGlobles.isNullOrEmpty(medtUni.getText().toString())) {
                    if (!ApplicationGlobles.isNullOrEmpty(medtClass.getText().toString())) {
                        if (!ApplicationGlobles.isNullOrEmpty(medtSub.getText().toString())) {
                            if (!mtxtStartDate.getText().toString().equals("Start Date")) {
                                if (!mtxtEndDate.getText().toString().equals("End Date")) {

                                    if (eduOrExp.equalsIgnoreCase("educationFrag")) {
                                        AddEducation(medtUni.getText().toString(), medtClass.getText().toString(),
                                                medtSub.getText().toString(), mtxtStartDate.getText().toString(),
                                                mtxtEndDate.getText().toString());
                                    }else{
                                        AddExperience(medtUni.getText().toString(), medtClass.getText().toString(),
                                                medtSub.getText().toString(), mtxtStartDate.getText().toString(),
                                                mtxtEndDate.getText().toString());
                                    }
                                } else {
                                    Toast.makeText(getActivity(), "Please Select End Date", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), "Please Select Start Date", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Please Enter Subject or Course Name", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Please Enter Class or Degree Name", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Please Enter University or School Name", Toast.LENGTH_SHORT).show();
                }

            }
        });
        mbtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //popupWindow.dismiss();
                getDialog().dismiss();
            }
        });

        return rootView;
    }

    public void AddEducation(String sc, String cl, String sub, String SD, String ED) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());

        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            JSONObject params = new JSONObject();
            try {
                params.put("board_university", sc);
                params.put("class_degree", cl);
                params.put("end_date", ED);
                params.put("start_date", SD);
                params.put("subject_course", sub);

                Log.e("param", params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(mediaType, params + "");
            Request request = new Request.Builder()
                    .url(WebApis.ADDEDUCATION)
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");
                        final String message;

                        if (jsonData.length() > 0) {
                            try {
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                   message = jsonObject.getString("message");
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();

                                             getDialog().dismiss();
                                        }
                                    });
                                } else {
                                    message = jsonObject.getString("message");
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }


    public void AddExperience(String sc, String cl, String sub, String SD, String ED) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(getActivity());

        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            JSONObject params = new JSONObject();
            try {
                params.put("title", sc);
                params.put("location", sub);
                params.put("end_date", ED);
                params.put("start_date", SD);
                params.put("company", cl);

                Log.e("param", params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(mediaType, params + "");
            Request request = new Request.Builder()
                    .url(WebApis.ADDEXPERIENCE)
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (getActivity() == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        final String message;
                        if (jsonData.length() > 0) {
                            try {
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    message = jsonObject.getString("message");
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();
                                          getDialog().dismiss();
                                            //  finish();
                                        }
                                    });
                                } else {
                                    message = jsonObject.getString("message");
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
       getActivity().finish();
        Intent intent = new Intent(getActivity(),ProfileActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
