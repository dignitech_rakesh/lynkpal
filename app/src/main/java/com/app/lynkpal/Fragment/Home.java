package com.app.lynkpal.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;


import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.app.lynkpal.Adapter.FriendAdapter;
import com.app.lynkpal.Adapter.HomeAdapter;
import com.app.lynkpal.AddTrainingActivity;
import com.app.lynkpal.Application.Utils;
import com.app.lynkpal.Bean.FriendSuggestion;
import com.app.lynkpal.Bean.PostBean;

//import com.app.lynkpal.BuildConfig;

import com.app.lynkpal.CardDetailActivity;
import com.app.lynkpal.Helper.AlbumStorageDirFactory;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Base64;
import com.app.lynkpal.Helper.BaseAlbumDirFactory;
import com.app.lynkpal.Helper.CircleImageView;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.FilePath;
import com.app.lynkpal.Helper.FroyoAlbumDirFactory;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.OnCloseListener;
import com.app.lynkpal.Interface.OpenFragment;
import com.app.lynkpal.Interface.ShareListener;
import com.app.lynkpal.Interface.deletePost;
import com.app.lynkpal.Interface.showImagePopup;
import com.app.lynkpal.Interface.showVideoPopup;
import com.app.lynkpal.LoginActivity;
import com.app.lynkpal.PostActivity;
import com.app.lynkpal.R;
import com.app.lynkpal.SearchActivity;
import com.app.lynkpal.VideoActivity;
import com.app.lynkpal.storage.DbHelper;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.JsonObject;
import com.onesignal.OneSignal;
import com.yqritc.recyclerviewflexibledivider.VerticalDividerItemDecoration;

import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import static android.content.Context.MODE_PRIVATE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;
//import static com.facebook.accountkit.internal.AccountKitController.getApplicationContext;

public class Home extends Fragment implements deletePost,showImagePopup,showVideoPopup,OpenFragment,OnCloseListener,ShareListener,LocationListener {
    private static final int REQUEST_CAMERA_ACCESS_PERMISSION = 5674;
    public String postFile_path;
    LocationManager locationManager;


    //FileBody bin;
    private static final String JPEG_FILE_PREFIX = "IMG_";
    private static final String JPEG_FILE_SUFFIX = ".jpg";
    private static final String TAG = Home.class.getSimpleName();
    private static final int CAMERA_REQUEST = 222;
    // adapted from post by Phil Haack and modified to match better
    public final static String tagStart=
            "\\<\\w+((\\s+\\w+(\\s*\\=\\s*(?:\".*?\"|'.*?'|[^'\"\\>\\s]+))?)+\\s*|\\s*)\\>";
    public final static String tagEnd=
            "\\</\\w+\\>";
    public final static String tagSelfClosing=
            "\\<\\w+((\\s+\\w+(\\s*\\=\\s*(?:\".*?\"|'.*?'|[^'\"\\>\\s]+))?)+\\s*|\\s*)/\\>";
    public final static String htmlEntity=
            "&[a-zA-Z][a-zA-Z0-9]+;";
    public final static Pattern htmlPattern=Pattern.compile(
            "("+tagStart+".*"+tagEnd+")|("+tagSelfClosing+")|("+htmlEntity+")",
            Pattern.DOTALL
    );
    DbHelper dbHelper;
    RecyclerView recyclerView;
    String message;
    TextView head;
    EditText edtSEND;
    TextView edtSearch;
    String extension;
    CircleImageView imgUserImage;
    List<PostBean> homeBeanList = new ArrayList<>();
    List<PostBean> homeBeanList_adv = new ArrayList<>();
    List<PostBean> final_homeBeanList = new ArrayList<>();
    List<PostBean> homeBeanList_m = new ArrayList<>();
    List<PostBean> homeBeanList_un = new ArrayList<>();
    ArrayList matched_homeBeanList = new ArrayList<>();
    ArrayList matched_homeBeanList_adv = new ArrayList<>();
    ArrayList unmatched_homeBeanList = new ArrayList<>();
    ArrayList online_homeBeanList = new ArrayList<>();
    ArrayList offline_homeBeanList = new ArrayList<>();
    HomeAdapter homeAdapter;
    ImageView imgImage, imgUploadimg, imgRemove, imgSend;
    Intent pickPhoto;
    AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    String encodedString = "";
    FileBody filePath;
    String postText = ""/*, extension = ""*/;
    RelativeLayout layImage;
    LoaderDiloag loaderDiloag;
    ProgressDialog progress;
    PostBean postBean;
    SwipeRefreshLayout swipeRefreshLayout;
    SharedPreferences settings;
    String profile_pic,shrdPref_userEmail,shrdPref_userId;
    LinearLayoutManager mLayoutManager;
    Boolean loading;
    int page = 0;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private String mCurrentPhotoPath;
    public static String profileUserImage = " ";
    private String profileUserImagePath = "";
    EditText editPost;
    ImageView show_image;
    AlertDialog.Builder dialogBuilder;
    LayoutInflater linflater;
    ImageView profileImage,displayImage;
    boolean wrapInScrollView = true;
    View view;
    MaterialDialog dialog;
    AppCompatButton postdata;
    EditText edit_post;
    private RecyclerView mFriendRecycler;
    Uri camera_photo;
    FriendSuggestion friendSuggestion;
    List<FriendSuggestion> mFriendList = new ArrayList<>() ;
    FriendAdapter mFriendAdapter;
    TextView mKnowText,mHideText;
    NestedScrollView mScrollview;
    LinearLayout searchlayout;
    DisplayMetrics displayMetrics;
    Uri photoURI;
    private String path_download,path_video,path_image;

    VerticalDividerItemDecoration mItemDecoration;

    Timer mTimer;
    private String thumbnail_path;
    private RelativeLayout rl_hide;
    private boolean chkScrollStatus;
    private TextView tv_refresh_ad;
    private LinearLayout weather_ll;
    private TextView tv_weather,tv_city,tv_temperature;
    private ImageView iv_weather;
    private LinearLayout llUploading;
    private RelativeLayout rlUploading;
    private TextView refreshUploading;
    private ProgressBar progressUploading;
    private String typeIntent="";
    private Uri testUri;
    private BroadcastReceiver mBroadcastReceiver;


    public Home() {
        // Required empty public constructor
    }

   /* public Home(FileBody intentFilePath,String intentPostData,String intentExtension) {
        // Required empty public constructor
    }*/
    Loader mImageloader;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceAsColor")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        displayMetrics = new DisplayMetrics();
         getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        dbHelper = new DbHelper(getContext());
        mImageloader = new Loader(getContext());
        head = (TextView) rootView.findViewById(R.id.head);
        edtSearch = (TextView) rootView.findViewById(R.id.edtSearch);
        CoordinatorLayout frame_layout = rootView.findViewById(R.id.fragment_home_container);
        mFriendRecycler = rootView.findViewById(R.id.friend_recycler_view);
        mKnowText = rootView.findViewById(R.id.know_text);
        mHideText = rootView.findViewById(R.id.hide_text);
        rl_hide = (RelativeLayout) rootView.findViewById(R.id.rl_hide);
        mScrollview = rootView.findViewById(R.id.scroll_view);
        searchlayout = rootView.findViewById(R.id.search_layout);


        tv_city = (TextView) rootView.findViewById(R.id.city);
        tv_temperature = (TextView) rootView.findViewById(R.id.temp);
        tv_weather = (TextView) rootView.findViewById(R.id.weather);
        weather_ll = (LinearLayout) rootView.findViewById(R.id.weather_ll);
        iv_weather = (ImageView) rootView.findViewById(R.id.weather_img);

        llUploading = (LinearLayout) rootView.findViewById(R.id.llUploading);
        rlUploading = (RelativeLayout) rootView.findViewById(R.id.rlUploading);
        refreshUploading = (TextView) rootView.findViewById(R.id.refreshUploading);

        tv_refresh_ad = (TextView) rootView.findViewById(R.id.refresh_ad);
        tv_refresh_ad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page=0;
                homeBeanList.clear();
                homeBeanList_adv.clear();
                final_homeBeanList.clear();
                homeAdapter = new HomeAdapter(getActivity(), homeBeanList, Home.this, Home.this,Home.this, profile_pic,Home.this,Home.this);
                loaderDiloag.dismissDiloag();
                recyclerView.setAdapter(homeAdapter);
                recyclerView.setNestedScrollingEnabled(false);
                getAllPost();
            }
        });



        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }

        String charSet= String.valueOf(Charset.defaultCharset());
        Log.d("charSet", charSet);

        settings = (getActivity()).getSharedPreferences(Constant.PREFS_NAME, 0);
        shrdPref_userEmail = settings.getString("email", "");
         shrdPref_userId = settings.getString("userid", "");

        Log.d("sharedDetails",shrdPref_userEmail+"--"+shrdPref_userId);


        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), SearchActivity.class)/*.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)*/);

            }
        });


//Folder Creating Into Phone Storage
        path_download = Environment.getExternalStorageDirectory() + "/Test";
        path_video = Environment.getExternalStorageDirectory() + "/TestVideo";
        path_image = Environment.getExternalStorageDirectory() + "/TestImage";



        MaterialDialog.Builder builder = new MaterialDialog.Builder(getContext());
        builder.customView(R.layout.image_show,wrapInScrollView);
        builder.title("Posting");
        dialog = builder.build();
        dialog.setCanceledOnTouchOutside(true);
        view = dialog.getCustomView();
        show_image = view.findViewById(R.id.posting_image);
        postdata = view.findViewById(R.id.posting_button);
        edit_post = view.findViewById(R.id.edit_post_data);




        edtSEND = (EditText) rootView.findViewById(R.id.edtSEND);
        imgUserImage = rootView.findViewById(R.id.imgUserImage);
        settings = (getActivity()).getSharedPreferences(Constant.PREFS_NAME, 0);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        // recyclerView.setNestedScrollingEnabled(false);
        imgImage = (ImageView) rootView.findViewById(R.id.imgImage);
        imgUploadimg = (ImageView) rootView.findViewById(R.id.imgUploadimg);
        loaderDiloag = new LoaderDiloag(getActivity());
        loaderDiloag.setCancelable(false);
        imgRemove = (ImageView) rootView.findViewById(R.id.imgRemove);
        imgSend = (ImageView) rootView.findViewById(R.id.imgSend);
        layImage = (RelativeLayout) rootView.findViewById(R.id.relative_home);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swpPost);
        Log.e("userid", Constant.user_id);
        swipeRefreshLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        dialogBuilder = new AlertDialog.Builder(getContext());

        mHideText.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (mHideText.getText().equals("Show"))
                {
                    mFriendRecycler.setVisibility(View.VISIBLE);
                    mHideText.setText("Hide");
                }
                else
                {
                    mFriendRecycler.setVisibility(View.GONE);
                    mHideText.setText("Show");
                }


            }
        });



       /* sendImageData.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (editPost.getText().toString()!=null)
                {
                    dialogBuilder.setCancelable(true);
                    postText = editPost.getText().toString().trim();
                }
                else if (edtSEND.getText().toString()!=null)
                {
                    postText = edtSEND.getText().toString();
                }


                if (postText.trim().length() <= 0 || filePath == null)
                {
                    Log.d(TAG,"validation " + postText + "filepath " + filePath);
                    Toast.makeText(getActivity(), "Please write something or select an image", Toast.LENGTH_SHORT).show();
                } else {
                    if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
                        edtSEND.setText("");
                        new VideoUploader().execute();
                    } else {
                        Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });*/

        imgSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                postText = edtSEND.getText().toString().trim();
                Log.d(TAG,"validation " + postText + filePath);
                if (postText.trim().length() <= 0 && filePath == null)
                {

                    Toast.makeText(getActivity(), "Please write something or select an image", Toast.LENGTH_SHORT).show();
                } else {
                    if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
                        edtSEND.setText("");
                        new VideoUploader().execute();
                    } else {
                        Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });

        postdata.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                postText = edit_post.getText().toString().trim();
                new VideoUploader().execute();
            }
        });

        imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layImage.setVisibility(View.GONE);
                filePath = null;
            }
        });
        Typeface tf_reg = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Bold.ttf");
        head.setTypeface(tf_bold);
        edtSearch.setTypeface(tf_bold);
        edtSEND.setTypeface(tf_med);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
                    swipeRefreshLayout.setRefreshing(true);
                    page = 0;
                    visibleItemCount = 0;
                    totalItemCount = 0;
                    pastVisiblesItems = 0;
                    homeBeanList.clear();
                    homeBeanList_adv.clear();
                    final_homeBeanList.clear();
                    getAllPost();
                    suggestionApi(getContext(), settings.getString("token",""));

                } else {
                    Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
                    swipeRefreshLayout.setRefreshing(false);
                }

            }
        });

        try {

            page=0;
            homeBeanList.clear();
            homeBeanList_adv.clear();
            final_homeBeanList.clear();
            homeAdapter = new HomeAdapter(getActivity(), homeBeanList, Home.this, Home.this, Home.this,profile_pic,Home.this,Home.this);
            loaderDiloag.dismissDiloag();
            recyclerView.setAdapter(homeAdapter);
            recyclerView.setNestedScrollingEnabled(false);
            getAllPost();

            if (LoginActivity.isNetworkConnected(getContext()))
                suggestionApi(getContext(),settings.getString("token",""));
               // suggestionApi(getContext(),Constant.token);

        }catch (Exception e){

        }
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);



        //((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);


       /* recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    Log.e("loading", loading + "");
                   *//* Log.e("visibleItemCount", visibleItemCount + "");
                    Log.e("pastVisiblesItems", pastVisiblesItems + "");
                    Log.e("pastVisiblesItems", pastVisiblesItems + "");
                    Log.e("totalItemCount", totalItemCount + "");*//*
                    Log.e("loading_", ((visibleItemCount + pastVisiblesItems) > 10 && (visibleItemCount + pastVisiblesItems) >= (totalItemCount - 5)) + "");
                    if (!loading) {
                        if ((visibleItemCount + pastVisiblesItems) > 9 && (visibleItemCount + pastVisiblesItems) >= (totalItemCount - 5)) {
                            loading = true;
                            Thread thread = new Thread(null, new Runnable() {
                                @Override
                                public void run() {
                                    getAllPost();
                                }
                            });
                            thread.start();
                        }
                    }
                }
            }
        });
*/

       /* mScrollview.setOnScrollChangeListener(new View.OnScrollChangeListener()
        {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY)
            {
                if (scrollY > oldScrollY)
                {
                    Log.i(TAG, "Scroll DOWN");
                  //  mHideText.setVisibility(View.GONE);
                  //  mFriendRecycler.setVisibility(View.GONE);
                  //  mKnowText.setVisibility(View.GONE);
                  //  searchlayout.setVisibility(View.GONE);
                }
                if (scrollY < oldScrollY)
                {
                    Log.i(TAG, "Scroll UP");

                }

                if (scrollY == 0) {
                    Log.i(TAG, "TOP SCROLL");
                    if (mHideText.getText().toString()=="Show")
                        mHideText.setText("Hide");
                    mHideText.setVisibility(View.VISIBLE);
                    mKnowText.setVisibility(View.VISIBLE);
                    mFriendRecycler.setVisibility(View.VISIBLE);
                    searchlayout.setVisibility(View.VISIBLE);
                }


            }
        });*/





      /*  if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            // only for gingerbread and newer versions
            mScrollview.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
                @Override
                public void onScrollChanged()
                {
                    View view = (View)mScrollview.getChildAt(mScrollview.getChildCount() - 1);

                    int diff = (view.getBottom() - (mScrollview.getHeight() + mScrollview
                            .getScrollY()));

                    if (diff == 0)
                    {
                        Log.d(TAG, "onScrollChanged: ScrBottom");
                        getAllPost();
                        // your pagination code
                    }
                   int scrollX = mScrollview.getScrollX();
                   int scrollY = mScrollview.getScrollY();
                   if (scrollX<scrollY)
                   {
                       Log.d(TAG, "onScrollChanged: Scrup");
                       //searchlayout.setVisibility(View.GONE);

                   }
                   else
                   {
                       Log.d(TAG, "onScrollChanged: Scrdown");
                       if (mHideText.getText().toString()=="Show")
                           mHideText.setText("Hide");
                       mHideText.setVisibility(View.VISIBLE);
                       mKnowText.setVisibility(View.VISIBLE);
                       mFriendRecycler.setVisibility(View.VISIBLE);
                       searchlayout.setVisibility(View.VISIBLE);
                   }
                }
            });

        }*/


        mScrollview.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                visibleItemCount = mLayoutManager.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

              /*  Log.d("scrollCount",visibleItemCount+"--"+totalItemCount+"--"+pastVisiblesItems);
                Log.d("mScrollviewCount",mScrollview.getMinimumHeight()+"--"+mScrollview.getNestedScrollAxes()
                        +"--"+mScrollview.getMaxScrollAmount());*/

                if (scrollY > oldScrollY) {
                    //Log.d(TAG, "onScrollChanged: Scroll DOWN");
                    chkScrollStatus=true;
                }
                if (scrollY < oldScrollY) {
                   // Log.d(TAG, "onScrollChanged: Scroll UP");
                    chkScrollStatus=false;

                }

                if (scrollY == 0) {
                   // Log.d(TAG, "onScrollChanged: TOP SCROLL");
                }

                if (scrollY == ( v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight() )) {
                   // Log.d(TAG, "onScrollChanged: BOTTOM SCROLL"+"--"+scrollY+"--"+oldScrollY+"--"+v.getChildAt(0).getMeasuredHeight() +"--"+v.getMeasuredHeight());
                   // View view = (View) mScrollview.getChildAt(mScrollview.getChildCount() - 1);
                    //mScrollview.scrollTo(0,view.getTop());
                    if (chkScrollStatus) {

                      /*  // for api calling
                        loaderDiloag.displayDiloag();
                        if (homeAdapter != null && page>1) {
                            //recyclerView.getRecycledViewPool().clear();
                            try {
                                //loaderDiloag.dismissDiloag();
                                //homeAdapter.notifyDataSetChanged();
                                homeAdapter.notifyItemRangeChanged(0,recyclerView.getChildCount());
                                //CargarCandidatos cargarCandidatos=new CargarCandidatos();
                                 //cargarCandidatos.execute();
                            } catch (Exception e) {
                                Log.e("error", e.toString() + "");
                            }
                        } else {
                            try {
                                homeAdapter = new HomeAdapter(getActivity(), homeBeanList, Home.this, Home.this,Home.this, profile_pic,Home.this,Home.this);
                                loaderDiloag.dismissDiloag();
                                recyclerView.setAdapter(homeAdapter);
                                recyclerView.setNestedScrollingEnabled(false);
                                homeAdapter.notifyDataSetChanged();
                            } catch (Exception e) {
                                Log.e("error", e.toString() + "");
                            }

                        }*/


                        //old

                        Log.d("Api", "getAllPost");
                        getAllPost();

                    }
                    chkScrollStatus = false;
                   // Log.d(TAG, "onScrollChanged: BOTTOM"+"--"+scrollY+"--"+oldScrollY+"--"+v.getChildAt(0).getMeasuredHeight()
                     //       +"--"+v.getMeasuredHeight());
                   /* Timer buttonTimer = new Timer();
                    buttonTimer.schedule(new TimerTask() {

                        @Override
                        public void run() {
                            getActivity().runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    //myButton.setEnabled(true);
                                    getAllPost();
                                }
                            });
                        }
                    }, 2000);*/
                }

               /* View view = (View) mScrollview.getChildAt(mScrollview.getChildCount() - 1);
                int diff = (view.getBottom() - (mScrollview.getHeight() + mScrollview.getScrollY()));

                // if diff is zero, then the bottom has been reached
                if (diff <= 10) {
                    // do stuff
                    Log.d(TAG, "onScrollChanged: BOTTOM SCROLL new");

                }*/


            }
        });



        imgUploadimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();

            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }


       /* if (LoginActivity.isNetworkConnected(getContext())) {
            new Timer().scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    if (mFriendList.size() <= 0) {
                        try {
                            if (LoginActivity.isNetworkConnected(getContext()))
                                suggestionApi(getContext(), Constant.token);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, 0, 4000);


        }*/
        //if (ApplicationGlobles.isConnectingToInternet(getActivity()))
            getProfileInformaition();


      /*  OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                Log.d("debug", "User:" + userId);
                if (registrationId != null)
                    Log.d("debug", "registrationId:" + registrationId);

            }
        });*/



//      typeIntent=this.getArguments().getString("type");

        if (Constant.postType.equalsIgnoreCase("postFile")) {
        // if (typeIntent.equalsIgnoreCase("post")) {
             //  FileBody fileBody = this.getArguments().getParcelable("filepath");
            Constant.postType="";
             Uri uriIntent =  Uri.parse(this.getArguments().getString("uriIntent"));
            // Uri uriIntent =  this.getArguments().getParcelable("uriIntent");
            Log.d("uriIntent","Frag--"+uriIntent);

            String postData = this.getArguments().getString("postText");
             String ext = "";
             ext = this.getArguments().getString("extension");

             if (!ext.equalsIgnoreCase("")) {
                     if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
                         edtSEND.setText("");
                         // filePath = fileBody;
                        // convertImage(getActivity(),uriIntent);
                         convertImage(getActivity(),Constant.staticUri);
                         postText = postData;
                         extension = ext;
                        // new VideoUploader().execute();
                         llUploading.setVisibility(View.VISIBLE);
                         rlUploading.setVisibility(View.VISIBLE);

                     } else {
                         Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();

                     }

             }

        // }  //typeIntent
     }
        return rootView;
    }

    private void convertImage(Context context, Uri uri)
    {

        try {
            String selectedFilePath = FilePath.getPath(context,uri);
            File file = new File(selectedFilePath);
            filePath = new FileBody(file);
            //extension = "png";
        }catch (Exception e){
            Log.d("ecxeption",e+"");
        }

    }

    private void convertVideo(Context context, Uri uri)
    {
        String selectedFilePath = FilePath.getPath(context,uri);
        File file = new File(selectedFilePath);
        filePath = new FileBody(file);
        //extension = "mp4";
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.d(TAG, "onActivityCreated: ");
    }



    private void suggestionApi(final Context context, final String auth)
    {
       loaderDiloag.displayDiloag();
        StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.GET, WebApis.SUGGESTION_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                loaderDiloag.dismissDiloag();
                Log.d(TAG, "Suggestion " + response.toString());
                mFriendList.clear();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i=0;i<jsonArray.length();i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String user_name = jsonObject.getString("username");
                        String frnd_name = jsonObject.getString("first_name");
                        String last_name = jsonObject.getString("last_name");
                        String frnd_email = jsonObject.getString("email");
                        String profile_pic = jsonObject.getString("profile_pic");

                        friendSuggestion = new FriendSuggestion();
                        friendSuggestion.setId(id);
                        friendSuggestion.setFirstname(frnd_name);
                        friendSuggestion.setUsername(user_name);
                        friendSuggestion.setLastname(last_name);
                        friendSuggestion.setEmail(frnd_email);
                        friendSuggestion.setProfilepic(profile_pic);

                        mFriendList.add(friendSuggestion);
                    }
                    Log.d(TAG,"frndList " + mFriendList.size());
                    mFriendAdapter = new FriendAdapter(getContext(),mFriendList,Home.this);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
                   /* linearLayoutManager.setReverseLayout(true);
                    linearLayoutManager.setStackFromEnd(true);*/
                    mFriendRecycler.setLayoutManager(linearLayoutManager);
                    mFriendRecycler.setAdapter(mFriendAdapter);
                    if (mItemDecoration!=null)
                    {
                        Log.d(TAG, "RecyclerFriend: " + "not null");
                      //  mFriendRecycler.removeItemDecoration(mItemDecoration);
                    }
                    else
                    {
                        Log.d(TAG, "RecyclerFriend: " + "null");
                        mItemDecoration = new VerticalDividerItemDecoration.Builder(getContext()).size(10).build();
                        mFriendRecycler.addItemDecoration(mItemDecoration);
                    }




                } catch (JSONException e) {
                    loaderDiloag.dismissDiloag();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
               loaderDiloag.dismissDiloag();
                Log.d(TAG, "Error " + error.toString());
               // if (error.toString().contains(Constant.timeout_error_string));
               // Toast.makeText(context,getString(R.string.slow_internet), Toast.LENGTH_SHORT).show();

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(context, context.getString(R.string.slow_internet), Toast.LENGTH_LONG).show();
                }
            }
        }
        ) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("authorization",auth);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.VOLLEY_TIMEOUT_MS,
                Constant.VOLLEY_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        Volley.newRequestQueue(context).add(stringRequest);
    }

    public void getProfileInformaition() {

        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            //OkHttpClient client = new OkHttpClient();
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(Constant.VOLLEY_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                    .writeTimeout(Constant.WRITE_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                    .readTimeout(Constant.READ_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                    .build();

            Log.e("Constant.token", Constant.token);
            Request request = new Request.Builder()
                    .url(WebApis.USERPROFILE + Constant.user_id)
                    .get()
                    //.addHeader("authorization", Constant.token)
                    .addHeader("authorization", settings.getString("token",""))
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        e.printStackTrace();
                        if (e.toString().contains(Constant.timeout_error_string)) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(), getString(R.string.slow_internet), Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                        if (getActivity() == null)
                            return;

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("responseprofile", jsonData + "");

                        try {
                            JSONObject object = new JSONObject(jsonData);
                            String status = object.getString("status");
                            if (status.contains("error")) {
                                SharedPreferences pref = getActivity().getSharedPreferences(Constant.PREFS_NAME, 0);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.clear();
                                editor.apply();
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        Log.d(TAG, "onResponse: No data");
                                        new MaterialDialog.Builder(getContext())
                                                .title("Session Expired")
                                                .content(message)
                                                .positiveText("OK")
                                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                    @Override
                                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                        getActivity().finish();
                                                        startActivity(new Intent(getContext(), LoginActivity.class));
                                                    }
                                                })
                                                .show();
                                        return;
                                    }
                                });


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        if (jsonData.length() > 0) {
                            try {
                                JSONObject jsonObject = new JSONObject(jsonData);
                                final String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("personal_profile");
                                    //     profile_slug = jsonObject1.getString("profile_slug");
                                    profile_pic = jsonObject1.getString("profile_pic");
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            SharedPreferences.Editor editor = settings.edit();


                                            profileUserImage = WebApis.userProfileImage + profile_pic;
                                            Log.d(TAG, "Profilee " + profileUserImage);
                                            if (profile_pic != "null") {
                                                downloadImage(profileUserImage, path_download, "profileUserImage_" + Constant.user_id);
                                                profileUserImagePath = path_download + "/" + "profileUserImage_" + Constant.user_id;

                                                editor.putString("profile_pic", profile_pic);
                                                editor.putString("profileUserImagePath", profileUserImagePath);
                                                editor.commit();


                                                Glide.with(getContext()).load(profileUserImage).diskCacheStrategy(DiskCacheStrategy.SOURCE).error(R.drawable.image).into(imgUserImage);
                                            } else {
                                                Log.d(TAG, "run: noimage");
                                                Glide.with(getContext()).load(R.drawable.image).into(imgUserImage);

                                            }
                                           /* Glide.with(getActivity()).load(WebApis.userProfileImage + profile_pic).asBitmap().placeholder(R.drawable.image).error(R.drawable.image).centerCrop().into(new BitmapImageViewTarget(imgUserImage) {
                                                @Override
                                                protected void setResource(Bitmap resource) {
                                                    RoundedBitmapDrawable circularBitmapDrawable =
                                                            RoundedBitmapDrawableFactory.create(getActivity().getResources(), resource);
                                                    circularBitmapDrawable.setCircular(true);
                                                    imgUserImage.setImageDrawable(circularBitmapDrawable);
                                                }
                                            });*/
                                        }
                                    });
                                    //  imgUserImage.se
                                }


                            } catch (Exception je) {
                                je.printStackTrace();
                            }
                        }
                    }
                });

            }catch (Exception e) {
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
           // Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
            String userImgPath=settings.getString("profileUserImagePath","");
            Glide.with(getContext()).load(userImgPath).error(R.drawable.image).into(imgUserImage);


        }

        return;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == getActivity().RESULT_OK)
        {
            if (requestCode == 1)
            {
                //  handleBigCameraPhoto();


                Bitmap photo = (Bitmap) data.getExtras().get("data");

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();


                if (profileUserImage!=null)
                {
                    getActivity().finish();
                    startActivity(new Intent(getActivity(), PostActivity.class).putExtra("image",byteArray).putExtra("photo",profileUserImage));
                }
                else
                {
                    getActivity().finish();
                    startActivity(new Intent(getActivity(), PostActivity.class).putExtra("image",byteArray));
                }

              /*  getImageUri(getContext(),photo);

                Picasso.with(getContext()).load(camera_photo).into(show_image);
                dialog.show();

                String selectedFilePath = FilePath.getPath(getActivity(),camera_photo);
                File file = new File(selectedFilePath);




                filePath = new FileBody(file);
                extension = "png";*/


            } else if (requestCode == 2) {
                try {
                    Uri selectedFileUri = data.getData();

                    Constant.staticUri=selectedFileUri;

                      testUri=selectedFileUri;
                    convertImage(getActivity(),Constant.staticUri);

                    if (profileUserImage!=null)
                    {
                        getActivity().finish();
                        startActivity(new Intent(getActivity(), PostActivity.class).putExtra("image",selectedFileUri).putExtra("photo",profileUserImage));
                    }
                    else
                    {
                        getActivity().finish();
                        startActivity(new Intent(getActivity(), PostActivity.class).putExtra("image",selectedFileUri));
                    }



                   /* Picasso.with(getActivity()).load(selectedFileUri.toString()).into(show_image);
                    dialog.show();



                    String selectedFilePath = FilePath.getPath(getActivity(), selectedFileUri);
                    File file = new File(selectedFilePath);




                    filePath = new FileBody(file);
                    extension = "png";*/




                } catch (Exception e) {
                    Log.e("eroore", e.toString());
                    e.printStackTrace();
                }

            } else if (requestCode == 3)
            {
                try {
                    Uri selectedFileUri = data.getData();
                    String selectedFilePath = FilePath.getPath(getActivity(), selectedFileUri);
                    Bitmap bmThumbnail;
                    bmThumbnail = ThumbnailUtils.createVideoThumbnail(selectedFilePath,
                            MediaStore.Video.Thumbnails.MINI_KIND);
                   /* imgImage.setImageBitmap(bmThumbnail);
                    layImage.setVisibility(View.VISIBLE);
                    File file = new File(selectedFilePath);
                    filePath = new FileBody(file);
                    extension = "mp4";*/
                    //  new VideoUploader().execute();
                    if (bmThumbnail!=null)
                    {
                        try {


                            Log.d(TAG, "onActivityResult: Thumbnail " + bmThumbnail + selectedFilePath);
                            startActivity(new Intent(getContext(), PostActivity.class).putExtra("thumbnail", bmThumbnail)
                                    .putExtra("videoUri", selectedFilePath).putExtra("photo", profileUserImage)
                                    .putExtra("pathUri", selectedFileUri));
                        }catch (Exception e) {
                            //    imgImageView.setImageResource(R.drawable.placeholder);
                            Log.d("thumbnailExpOne",e +"");

                        }
                    }


                } catch (Exception e) {
                    //    imgImageView.setImageResource(R.drawable.placeholder);
                    Log.d("thumbnailExp",e +"");

                }
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        camera_photo = Uri.parse(path);
        return camera_photo;
    }


    private void saveFile(Bitmap sourceUri, File destination) {
        if (destination.exists()) destination.delete();
        try {
            FileOutputStream out = new FileOutputStream(destination);
            sourceUri.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String convertPreImagetBitmapToString(Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream); //compress to which format you want.
        byte[] byte_arr = stream.toByteArray();
        String attachmentStr = Base64.encodeBytes(byte_arr);
        Log.e("imageStr", attachmentStr);
        // showPopup(ChatActivity.this, attachmentStr, bmp);
        return attachmentStr;

    }


    public void getAllPost() {


        if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
            rl_hide.setVisibility(View.VISIBLE);


            //homeBeanList.clear();
            matched_homeBeanList.clear();
            matched_homeBeanList_adv.clear();
            loaderDiloag.displayDiloag();
            page++;

            homeBeanList.clear();
            homeBeanList_adv.clear();
            //dbHelper.deleteAdvByPageNo(page,-1);
            dbHelper.deleteDataPageNo(page);

           /* try {
                Cursor cursor_sug1_ = dbHelper.getAllAddData(page,shrdPref_userId,-1);
                Cursor cursor1_ = dbHelper.getAllData(page,shrdPref_userId,-1);


                Log.d("deleteAdvByPageNo", final_homeBeanList.size()+"--"+cursor1_.getCount()
                        +"--"+ cursor_sug1_.getCount());
            }catch (Exception e){

            }*/

              // For Timeout
            OkHttpClient client;
            client = new OkHttpClient.Builder()
                    .connectTimeout(4, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(10, TimeUnit.SECONDS)
                    .build();

           // OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.allPOST", WebApis.allPOST + page);
            Request request = new Request.Builder()

                    .url(WebApis.allPOST +page)
                    .get()
                    .addHeader("authorization", settings.getString("token",""))
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {

                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (e.toString().contains(Constant.timeout_error_string)||e.getMessage().contains("java.net.SocketTimeoutException: connect timed out")) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //Toast.makeText(getActivity(), getString(R.string.slow_internet), Toast.LENGTH_SHORT).show();
                                   // recyclerView.setVisibility(View.GONE);
                               // tv_refresh_ad.setVisibility(View.VISIBLE);

                                }
                            });

                        }

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), getString(R.string.api_error), Toast.LENGTH_SHORT).show();
                                recyclerView.setVisibility(View.GONE);
                                tv_refresh_ad.setVisibility(View.VISIBLE);

                            }
                        });

                        if (getActivity() == null)
                            return;
                        loaderDiloag.dismissDiloag();
                        swipeRefreshLayout.setRefreshing(false);


                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");

                        loading = false;
                        //swipeRefreshLayout.setRefreshing(false);

                        if (jsonData.length() > 0) {
                            try {


                               //demo(jsonData);
                               // JSONArray array = new JSONArray(jsonData);

                                JSONObject jsonObject = new JSONObject(jsonData);

                                JSONArray postArray = jsonObject.getJSONArray("posts");
                                JSONArray sponseredArray = jsonObject.getJSONArray("sponsored");
                                JSONArray mainArray=new JSONArray();

                               /* for (int i=0;i<postArray.length();i++){
                                    mainArray.put(postArray.getJSONObject(i));
                                    if (i%(sponseredArray.length()+1)==0 && i>0){
                                        int quotient=i/(sponseredArray.length()+1);
                                        JSONObject jsonObject1=sponseredArray.getJSONObject(quotient-1);
                                        mainArray.put(jsonObject1);
                                    }
                                }*/

                               // Log.d("mainArray",mainArray.length()+"");

                                if (postArray.length() > 0) {
                                    /* for (int i = 0; i < mainArray.length(); i++) {
                                        JSONObject jsonObject1 = mainArray.getJSONObject(i);
                                        //   String id=jsonObject1.getString("id");

                                       if(jsonObject1.has("ad_image")){


                                            Integer adv_id=jsonObject1.getInt("id");
                                            String adv_name=jsonObject1.getString("name");
                                            String adv_tags=jsonObject1.getString("tags");
                                            String adv_link=jsonObject1.getString("link");
                                            String adv_startDate=jsonObject1.getString("startdate");
                                            String adv_endDate=jsonObject1.getString("enddate");
                                            Integer adv_amount=jsonObject1.getInt("amount");
                                            Integer adv_paid=jsonObject1.getInt("paid");
                                            String adv_image=jsonObject1.getString("ad_image");
                                            String adv_readCount=jsonObject1.getString("read_count");

                                            Integer id = -1;
                                            Integer user = -1;

                                            String desc="";
                                            String created = "2018-07-31T11:48:05.061288Z";
                                            String file = "";
                                            String parent_post = "";
                                            String sharer_msg = "";
                                            String fullname = "";
                                            String profile_pic = "";

                                            String profile_pic_path="";
                                            if (!profile_pic.equals("null")) {
                                                //downloadImage(WebApis.userProfileImage+profile_pic,path_download,"profileImage_"+id);
                                                // profile_pic_path = path_download +"/" + "profileImage_"+id;
                                            }

                                            String likes_count = "";
                                            String comments_count = "";
                                            String owner_fullname = "";
                                            String owner_profile_pic = "";

                                            String owner_profile_pic_path="";
                                            if (!owner_profile_pic.equals("null")) {
                                                // downloadImage(WebApis.userProfileImage + owner_profile_pic, path_download, "profileImgShare_" + id);

                                                //owner_profile_pic_path = path_download +"/" + "profileImgShare_" + id;
                                            }
                                            String likestatus = "";
                                            String file_type = "";
                                            String frame_name = "";

                                            postFile_path="";
                                            thumbnail_path="";
                                            if (file_type.equalsIgnoreCase("video")){
                                                if (!file.equalsIgnoreCase("null")) {
                                                    //downloadImage(WebApis.postImage + file, path_video, "Video_" + id);
                                                    // downloadImage(WebApis.postImage + frame_name, path_video, "Thumbnail_" + id);
                                                    //postFile_path = path_video + "/" + "Video_" + id;
                                                    // thumbnail_path = path_video + "/" + "Thumbnail_" + id;
                                                }


                                            }else if (file_type.equalsIgnoreCase("image")){
                                                if (!file.equalsIgnoreCase("null")) {
                                                    //downloadImage(WebApis.postImage + file, path_image, "postImage_" + id);
                                                    //postFile_path = path_image + "/" + "postImage_" + id;
                                                }

                                            }

                                            Log.d("filePath",postFile_path);


                                            if (dbHelper.chkAdvData(adv_id)) {
                                                boolean abc=  dbHelper.updateDataAdv(likestatus, id, desc, postFile_path, created, user, parent_post,
                                                        sharer_msg,fullname, profile_pic_path, likes_count, comments_count, owner_fullname,
                                                        owner_profile_pic_path, thumbnail_path, file_type,page,shrdPref_userId,profile_pic,file,owner_profile_pic,
                                                        frame_name,adv_id,adv_name,adv_tags,adv_link,adv_startDate,adv_endDate,adv_amount,
                                                        adv_paid,adv_image,adv_readCount);
                                                matched_homeBeanList.add(adv_id);

                                                if (abc)
                                                    Log.d("abc", "true");
                                            }else{
                                                boolean insert = dbHelper.insertData(likestatus, id, desc, postFile_path, created, user, parent_post, sharer_msg,
                                                        fullname, profile_pic_path, likes_count, comments_count, owner_fullname,
                                                        owner_profile_pic_path, thumbnail_path, file_type,page,shrdPref_userId,profile_pic,file,owner_profile_pic,
                                                        frame_name,adv_id,adv_name,adv_tags,adv_link,adv_startDate,adv_endDate,adv_amount,
                                                        adv_paid,adv_image,adv_readCount);
                                                matched_homeBeanList.add(adv_id);

                                                if (insert)
                                                    Log.d("abc", "false");
                                            }
                                        }


                                        else {*/

                                    for(int i=0;i<postArray.length();i++){
                                        JSONObject jsonObject1 = postArray.getJSONObject(i);

                                            Integer adv_id=-1;
                                            String adv_name="";
                                            String adv_tags="";
                                            String adv_link="";
                                            String adv_startDate="";
                                            String adv_endDate="";
                                            Integer adv_amount=-1;
                                            Integer adv_paid=-1;
                                            String adv_image="";
                                            String adv_readCount="";



                                            // old post data
                                            Integer id = jsonObject1.getInt("id");
                                            Integer user = jsonObject1.getInt("user");


                                            Log.d("html", jsonObject1.getString("description"));

                                            //String bodyStr=StringUtils.substringBetween(jsonObject1.getString("description"),"<body>","</body>");

                                            //int imgIndex=StringUtils.indexOf(bodyStr,"<img");

                                            String desc = "";

                                            if (isHtml(jsonObject1.getString("description"))) {

                                                String tmpHtml = jsonObject1.getString("description");
                                                //  Spanned temp = Html.fromHtml(tmpHtml);
                                                //String tmpHtml= "<html><head></head><body><p>test </p><img alt='\\U263a\\Ufe0f' class='emojioneemoji' src='https://cdnjs.cloudflare.com/ajax/libs/emojione/2.1.4/assets/png/1f60a.png'><p>testtt</P></body></html>";

                                                Document doc = Jsoup.parse(tmpHtml);
                                                //      String nameL = doc.select("img[src]").attr("value");

                                                //   Elements elements = doc.body().select("*");

//                                            for (Element img : doc.select("img:not([alt])")){
//
//                                                desc+= img.ownText();
//
//                                            }

                                                String tag = null;

                                                Elements elements = doc.body().select("*");

                                                for (Element element : elements) {

                                                    tag = element.tagName();
                                                    Log.d("MyTag", tag);

                                                    if ("img".equalsIgnoreCase(tag)) {

                                                        // Get all img tags
                                                        //    Elements img = doc.getElementsByTag("img");

                                                        //  int counter = 0;

                                                        String temp;

                                                        // Loop through img tags
//                                                    for (Element el : img) {
//                                                        // If alt is empty or null, add one to counter
//                                                        if(el.attr("alt") == null || el.attr("alt").equals("")) {
//                                                            counter++;
//                                                        }
                                                        Log.d("imageTag", element.attr("src") + " Alt: " + element.attr("alt") + "");
                                                        System.out.println("image tag: " + element.attr("src") + " Alt: " + element.attr("alt"));

                                                        temp = "";
                                                        temp = Utils.decodeMessage(element.attr("alt"));
                                                        desc += temp;

                                                        //  }


                                                    } else if ("p".equalsIgnoreCase(tag)) {

                                                        //desc+= doc.body().ownText();

                                                        desc += element.text();
                                                    } else {
                                                        desc += element.text();
                                                    }


                                                }


//                                            for (Element element : elements) {
//                                                System.out.println(element.ownText());
//                                                Log.d("element",element.ownText());
//                                            }


                                            } else {

                                                desc = Utils.decodeMessage(jsonObject1.getString("description"));

                                            }


                                            //html                            String title = StringUtils.substringBetween(testHtml, "<title>", "</title>");
                                            // String desc = jsonObject1.getString("description");
                                            //String desc = Utils.decodeMessage(jsonObject1.getString("description"));
                                            // String desc = URLDecoder.decode(jsonObject1.getString("description"),"UTF-8");
                                            String created = jsonObject1.getString("created_at");
                                            String file = jsonObject1.getString("file");
                                            String parent_post = jsonObject1.getString("parent_post");
                                            String sharer_msg = jsonObject1.getString("sharer_msg");
                                            String fullname = jsonObject1.getString("fullname");
                                            String profile_pic = jsonObject1.getString("profile_pic");

                                            String profile_pic_path = "";
                                            if (!profile_pic.equals("null")) {
                                                //downloadImage(WebApis.userProfileImage+profile_pic,path_download,"profileImage_"+id);
                                                // profile_pic_path = path_download +"/" + "profileImage_"+id;
                                            }

                                            String likes_count = jsonObject1.getString("likes_count");
                                            String comments_count = jsonObject1.getString("comments_count");
                                            String owner_fullname = jsonObject1.getString("owner_fullname");
                                            String owner_profile_pic = jsonObject1.getString("owner_profile_pic");

                                            String owner_profile_pic_path = "";
                                            if (!owner_profile_pic.equals("null")) {
                                                // downloadImage(WebApis.userProfileImage + owner_profile_pic, path_download, "profileImgShare_" + id);

                                                //owner_profile_pic_path = path_download +"/" + "profileImgShare_" + id;
                                            }
                                            String likestatus = jsonObject1.getString("likestatus");
                                            String file_type = jsonObject1.getString("file_type");
                                            String frame_name = jsonObject1.getString("frame_name");

                                            postFile_path = "";
                                            thumbnail_path = "";
                                            if (file_type.equalsIgnoreCase("video")) {
                                                if (!file.equalsIgnoreCase("null")) {
                                                    //downloadImage(WebApis.postImage + file, path_video, "Video_" + id);
                                                    // downloadImage(WebApis.postImage + frame_name, path_video, "Thumbnail_" + id);
                                                    //postFile_path = path_video + "/" + "Video_" + id;
                                                    // thumbnail_path = path_video + "/" + "Thumbnail_" + id;
                                                }


                                            } else if (file_type.equalsIgnoreCase("image")) {
                                                if (!file.equalsIgnoreCase("null")) {
                                                    //downloadImage(WebApis.postImage + file, path_image, "postImage_" + id);
                                                    //postFile_path = path_image + "/" + "postImage_" + id;
                                                }

                                            }

                                            Log.d("filePath", postFile_path);


                                            if (dbHelper.chkData(id)) {
                                                boolean abc = dbHelper.updateData(likestatus, id, desc, postFile_path, created, user, parent_post,
                                                        sharer_msg, fullname, profile_pic_path, likes_count, comments_count, owner_fullname,
                                                        owner_profile_pic_path, thumbnail_path, file_type, page, shrdPref_userId, profile_pic, file, owner_profile_pic,
                                                        frame_name,adv_id,adv_name,adv_tags,adv_link,adv_startDate,adv_endDate,adv_amount,
                                                        adv_paid,adv_image,adv_readCount);
                                                matched_homeBeanList.add(id);

                                                if (abc)
                                                    Log.d("abc", "true");
                                            } else {
                                                boolean insert = dbHelper.insertData(likestatus, id, desc, postFile_path, created, user, parent_post, sharer_msg,
                                                        fullname, profile_pic_path, likes_count, comments_count, owner_fullname,
                                                        owner_profile_pic_path, thumbnail_path, file_type, page, shrdPref_userId, profile_pic, file, owner_profile_pic,
                                                        frame_name,adv_id,adv_name,adv_tags,adv_link,adv_startDate,adv_endDate,adv_amount,
                                                        adv_paid,adv_image,adv_readCount);
                                                matched_homeBeanList.add(id);

                                                if (insert)
                                                    Log.d("abc", "false");
                                            }
                                       /* }//else*/




                                    }// uncmnt

                                    int count=(page-1)*5;
                                    int lmt=5;
                                    try {
                                        //Cursor cursor = dbHelper.getAllData(String.valueOf(count),String.valueOf(lmt));
                                        Cursor cursor = dbHelper.getAllData(page,shrdPref_userId,-1);
                                        Log.d("resCount", cursor.getCount() + "--" + dbHelper.getCount()+
                                        "--"+matched_homeBeanList.size());

                                        if (cursor.getCount() == 0) {
                                            // show message
                                            // showMessage("Error","Nothing found");
                                            return;
                                        }

                                        StringBuffer buffer = new StringBuffer();
                                        while (cursor.moveToNext()) {
//                                                buffer.append("Id :"+ cursor.getString(2)+"\n");
//                                                buffer.append("File :"+ cursor.getString(4)+"\n");
//                                                buffer.append("profile_pic :"+ cursor.getString(10)+"\n");
//                                                buffer.append("owner_profile_pic :"+ cursor.getString(14)+"\n\n");

                                            Log.d("IndexCursor",cursor.getString(2)+"--"+cursor.getString(23));
                                            for (int i=0;i<matched_homeBeanList.size();i++){
                                                if ((int)matched_homeBeanList.get(i) == Integer.parseInt(cursor.getString(2))/*|| (int)matched_homeBeanList.get(i) == Integer.parseInt(cursor.getString(23))*/){
                                                    Log.d("matched_homeBeanList",(int)matched_homeBeanList.get(i)+"--"+Integer.parseInt(cursor.getString(2))
                                                    +"--"+ Integer.parseInt(cursor.getString(23)) );
                                                    Log.d("matchecon",matched_homeBeanList.get(i)+"");
                                                    postBean = new PostBean();
                                                    postBean.setId(Integer.parseInt(cursor.getString(2)));
                                                    postBean.setUser(Integer.parseInt(cursor.getString(6)));
                                                    postBean.setDesc(cursor.getString(3));
                                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//2016-01-24T16:00:00.000Z
                                                    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                                                    String created = String.valueOf(cursor.getString(5));
                                                    long time = sdf.parse(created).getTime();
                                                    long now = System.currentTimeMillis();
                                                    CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
                                                    postBean.setCreated(String.valueOf(ago));
                                                    postBean.setFile(cursor.getString(4));
                                                    postBean.setParent_post(cursor.getString(7));
                                                    postBean.setSharer_msg(cursor.getString(8));
                                                    postBean.setFullname(cursor.getString(9));
                                                    postBean.setProfile_pic(cursor.getString(10));
                                                    postBean.setLikes_count(cursor.getString(11));
                                                    postBean.setType(cursor.getString(16));
                                                    postBean.setFrame_name(cursor.getString(15));
                                                    if (cursor.getString(1).equals("1")) {
                                                        postBean.setLike(true);
                                                    } else {
                                                        postBean.setLike(false);

                                                    }
                                                    postBean.setComment(false);
                                                    postBean.setDelete(false);
                                                    postBean.setComments_count(cursor.getString(12) + " Comments");
                                                    postBean.setOwner_fullname(cursor.getString(13));
                                                    postBean.setOwner_profile_pic(cursor.getString(14));

                                                    postBean.setOrg_profile_pic(cursor.getString(19));
                                                    postBean.setOrg_file(cursor.getString(20));
                                                    postBean.setOrg_owner_profile_pic(cursor.getString(21));
                                                    postBean.setOrg_frame_name(cursor.getString(22));

                                                    postBean.setAdv_id(Integer.parseInt(cursor.getString(23)));
                                                    postBean.setAdv_name(cursor.getString(24));
                                                    postBean.setAdv_tags(cursor.getString(25));
                                                    postBean.setAdv_link(cursor.getString(26));
                                                    postBean.setAdv_startDate(cursor.getString(27));
                                                    postBean.setAdv_endDate(cursor.getString(28));
                                                    postBean.setAdv_amount(cursor.getInt(29));
                                                    postBean.setAdv_paid(cursor.getInt(30));
                                                    postBean.setAdv_image(cursor.getString(31));
                                                    postBean.setAdv_readCount(cursor.getString(32));

                                                    Log.d("IDHomebeenlist",Integer.parseInt(cursor.getString(2))+"--"+cursor.getInt(23));
                                                    homeBeanList.add(postBean);
                                                }else{
                                                    if (matched_homeBeanList.contains(Integer.parseInt(cursor.getString(2))) /*|| matched_homeBeanList.contains(Integer.parseInt(cursor.getString(23)))*/)
                                                    {
                                                        Log.d("dontdeletePost",(int)matched_homeBeanList.get(i)+"");

                                                    }else {
                                                        Log.d("delPostById",Integer.parseInt(cursor.getString(2))+"--"+
                                                                Integer.parseInt(cursor.getString(23))+"--"+ matched_homeBeanList.get(i));
                                                       // dbHelper.deletePostByPostId((int)matched_homeBeanList.get(i));
                                                        /*dbHelper.deletePostByAdvId((int)matched_homeBeanList.get(i));*/
                                                    }
                                                    // dbHelper.deletePostByPostId((int)matched_homeBeanList.get(i));
                                                }
                                            }
                                        }

                                        for (int i=0;i<sponseredArray.length();i++){
                                            JSONObject jsonObject1 = sponseredArray.getJSONObject(i);
                                            //   String id=jsonObject1.getString("id");

                                            if(jsonObject1.has("ad_image")) {


                                                Integer adv_id = jsonObject1.getInt("id");
                                                String adv_name = jsonObject1.getString("name");
                                                String adv_tags = jsonObject1.getString("tags");
                                                String adv_link = jsonObject1.getString("link");
                                                String adv_startDate = jsonObject1.getString("startdate");
                                                String adv_endDate = jsonObject1.getString("enddate");
                                                Integer adv_amount = jsonObject1.getInt("amount");
                                                Integer adv_paid = jsonObject1.getInt("paid");
                                                String adv_image = jsonObject1.getString("ad_image");
                                                String adv_readCount = jsonObject1.getString("read_count");

                                                Integer id = -1;
                                                Integer user = -1;

                                                String desc = "";
                                                String created = "2018-07-31T11:48:05.061288Z";
                                                String file = "";
                                                String parent_post = "";
                                                String sharer_msg = "";
                                                String fullname = "";
                                                String profile_pic = "";

                                                String profile_pic_path = "";
                                                if (!profile_pic.equals("null")) {
                                                    //downloadImage(WebApis.userProfileImage+profile_pic,path_download,"profileImage_"+id);
                                                    // profile_pic_path = path_download +"/" + "profileImage_"+id;
                                                }

                                                String likes_count = "";
                                                String comments_count = "";
                                                String owner_fullname = "";
                                                String owner_profile_pic = "";

                                                String owner_profile_pic_path = "";
                                                if (!owner_profile_pic.equals("null")) {
                                                    // downloadImage(WebApis.userProfileImage + owner_profile_pic, path_download, "profileImgShare_" + id);

                                                    //owner_profile_pic_path = path_download +"/" + "profileImgShare_" + id;
                                                }
                                                String likestatus = "";
                                                String file_type = "";
                                                String frame_name = "";

                                                postFile_path = "";
                                                thumbnail_path = "";
                                                if (file_type.equalsIgnoreCase("video")) {
                                                    if (!file.equalsIgnoreCase("null")) {
                                                        //downloadImage(WebApis.postImage + file, path_video, "Video_" + id);
                                                        // downloadImage(WebApis.postImage + frame_name, path_video, "Thumbnail_" + id);
                                                        //postFile_path = path_video + "/" + "Video_" + id;
                                                        // thumbnail_path = path_video + "/" + "Thumbnail_" + id;
                                                    }


                                                } else if (file_type.equalsIgnoreCase("image")) {
                                                    if (!file.equalsIgnoreCase("null")) {
                                                        //downloadImage(WebApis.postImage + file, path_image, "postImage_" + id);
                                                        //postFile_path = path_image + "/" + "postImage_" + id;
                                                    }

                                                }

                                                Log.d("filePath", postFile_path);


                                                if (dbHelper.chkAdvData(adv_id,page)) {
                                                    boolean abc = dbHelper.updateDataAdv(likestatus, id, desc, postFile_path, created, user, parent_post,
                                                            sharer_msg, fullname, profile_pic_path, likes_count, comments_count, owner_fullname,
                                                            owner_profile_pic_path, thumbnail_path, file_type, page, shrdPref_userId, profile_pic, file, owner_profile_pic,
                                                            frame_name, adv_id, adv_name, adv_tags, adv_link, adv_startDate, adv_endDate, adv_amount,
                                                            adv_paid, adv_image, adv_readCount);
                                                    matched_homeBeanList_adv.add(adv_id);

                                                    if (abc)
                                                        Log.d("abc", "true");
                                                } else {
                                                    boolean insert = dbHelper.insertData(likestatus, id, desc, postFile_path, created, user, parent_post, sharer_msg,
                                                            fullname, profile_pic_path, likes_count, comments_count, owner_fullname,
                                                            owner_profile_pic_path, thumbnail_path, file_type, page, shrdPref_userId, profile_pic, file, owner_profile_pic,
                                                            frame_name, adv_id, adv_name, adv_tags, adv_link, adv_startDate, adv_endDate, adv_amount,
                                                            adv_paid, adv_image, adv_readCount);
                                                    matched_homeBeanList_adv.add(adv_id);

                                                    if (insert)
                                                        Log.d("abc", "false");
                                                }
                                            }
                                        }

                                       /* Cursor cursor_sug1 = dbHelper.getAllAddData(page,shrdPref_userId,-1);
                                        Cursor cursor1 = dbHelper.getAllData(page,shrdPref_userId,-1);


                                        Log.d("final_homeBeanListOne", final_homeBeanList.size()+"--"+cursor1.getCount()
                                                +"--"+ cursor_sug1.getCount());*/



                                        Cursor cursor_sug = dbHelper.getAllAddData(page,shrdPref_userId,-1);

                                        while (cursor_sug.moveToNext()) {
//                                                buffer.append("Id :"+ cursor.getString(2)+"\n");
//                                                buffer.append("File :"+ cursor.getString(4)+"\n");
//                                                buffer.append("profile_pic :"+ cursor.getString(10)+"\n");
//                                                buffer.append("owner_profile_pic :"+ cursor.getString(14)+"\n\n");

                                            Log.d("IndexCursorAdv",cursor_sug.getString(2)+"--"+cursor_sug.getString(23));
                                            for (int i=0;i<matched_homeBeanList_adv.size();i++){
                                                if ((int)matched_homeBeanList_adv.get(i) == Integer.parseInt(cursor_sug.getString(23)) /*|| (int)matched_homeBeanList.get(i) == Integer.parseInt(cursor.getString(23))*/){
                                                    Log.d("homeBeanAdv",(int)matched_homeBeanList_adv.get(i)+"--"+Integer.parseInt(cursor_sug.getString(2))
                                                            +"--"+ Integer.parseInt(cursor_sug.getString(23)) );
                                                    Log.d("matchecon",matched_homeBeanList_adv.get(i)+"");
                                                    postBean = new PostBean();
                                                    postBean.setId(Integer.parseInt(cursor_sug.getString(2)));
                                                    postBean.setUser(Integer.parseInt(cursor_sug.getString(6)));
                                                    postBean.setDesc(cursor_sug.getString(3));
                                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//2016-01-24T16:00:00.000Z
                                                    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                                                    String created = String.valueOf(cursor_sug.getString(5));
                                                    long time = sdf.parse(created).getTime();
                                                    long now = System.currentTimeMillis();
                                                    CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
                                                    postBean.setCreated(String.valueOf(ago));
                                                    postBean.setFile(cursor_sug.getString(4));
                                                    postBean.setParent_post(cursor_sug.getString(7));
                                                    postBean.setSharer_msg(cursor_sug.getString(8));
                                                    postBean.setFullname(cursor_sug.getString(9));
                                                    postBean.setProfile_pic(cursor_sug.getString(10));
                                                    postBean.setLikes_count(cursor_sug.getString(11));
                                                    postBean.setType(cursor_sug.getString(16));
                                                    postBean.setFrame_name(cursor_sug.getString(15));
                                                    if (cursor_sug.getString(1).equals("1")) {
                                                        postBean.setLike(true);
                                                    } else {
                                                        postBean.setLike(false);

                                                    }
                                                    postBean.setComment(false);
                                                    postBean.setDelete(false);
                                                    postBean.setComments_count(cursor_sug.getString(12) + " Comments");
                                                    postBean.setOwner_fullname(cursor_sug.getString(13));
                                                    postBean.setOwner_profile_pic(cursor_sug.getString(14));

                                                    postBean.setOrg_profile_pic(cursor_sug.getString(19));
                                                    postBean.setOrg_file(cursor_sug.getString(20));
                                                    postBean.setOrg_owner_profile_pic(cursor_sug.getString(21));
                                                    postBean.setOrg_frame_name(cursor_sug.getString(22));

                                                    postBean.setAdv_id(Integer.parseInt(cursor_sug.getString(23)));
                                                    postBean.setAdv_name(cursor_sug.getString(24));
                                                    postBean.setAdv_tags(cursor_sug.getString(25));
                                                    postBean.setAdv_link(cursor_sug.getString(26));
                                                    postBean.setAdv_startDate(cursor_sug.getString(27));
                                                    postBean.setAdv_endDate(cursor_sug.getString(28));
                                                    postBean.setAdv_amount(cursor_sug.getInt(29));
                                                    postBean.setAdv_paid(cursor_sug.getInt(30));
                                                    postBean.setAdv_image(cursor_sug.getString(31));
                                                    postBean.setAdv_readCount(cursor_sug.getString(32));

                                                    Log.d("IDHomebeenlistAdv",Integer.parseInt(cursor_sug.getString(2))+"--"+cursor_sug.getInt(23));
                                                    homeBeanList_adv.add(postBean);
                                                }else{
                                                    if (matched_homeBeanList_adv.contains(Integer.parseInt(cursor_sug.getString(23))) /*|| matched_homeBeanList.contains(Integer.parseInt(cursor.getString(23)))*/)
                                                    {
                                                        Log.d("dontdeletePost",(int)matched_homeBeanList_adv.get(i)+"");

                                                    }else {
                                                        Log.d("delPostByAdvId",Integer.parseInt(cursor_sug.getString(2))+"--"+
                                                                Integer.parseInt(cursor_sug.getString(23)));
                                                       // dbHelper.deletePostByPostId((int)matched_homeBeanList.get(i));
                                                        dbHelper.deletePostByAdvId((int)matched_homeBeanList_adv.get(i));
                                                    }
                                                    // dbHelper.deletePostByPostId((int)matched_homeBeanList.get(i));
                                                }
                                            }
                                        }
                                        Log.d("finalSixe",homeBeanList.size()+"--"+ homeBeanList_adv.size());

                                      /*  int div=homeBeanList.size()/homeBeanList_adv.size();
                                        //div=div-1;
                                        Log.d("div", div+"");*/

                                        //int finalCount=homeBeanList.size()+homeBeanList_adv.size();

                                           if (matched_homeBeanList.size()>10 && matched_homeBeanList_adv.size()>1) {
                                               for (int i = 1; i <= matched_homeBeanList_adv.size(); i++) {

                                                   int in = (matched_homeBeanList.size() + matched_homeBeanList_adv.size()) / matched_homeBeanList_adv.size();
                                                   int index = i * in;
                                                   Log.d("index", index + "--" + matched_homeBeanList.size() + "--" + matched_homeBeanList_adv.size());
                                                   // if (i<homeBeanList.size())
                                                   homeBeanList.add(index - 1, homeBeanList_adv.get(i - 1));
                                               /* else
                                                    homeBeanList.add(index-1,homeBeanList_adv.get(i-1));*/

                                               }
                                           }else{

                                           }


                                        final_homeBeanList.addAll(homeBeanList);
                                        loaderDiloag.displayDiloag();


                                     /*   for (int i=0;i<homeBeanList.size();i++){

                                            final_homeBeanList.add(homeBeanList.get(i));

                                        }*/


                                      /*  if (div>=matched_homeBeanList_adv.size()){

                                            for (int i=0;i<=homeBeanList.size();i++){
                                                if (i%div==0 && i>0){
                                                    int quotient = i/div;
                                                    // JSONObject jsonObject1=sponseredArray.getJSONObject(quotient-1);
                                                    // mainArray.put(jsonObject1);
                                                    final_homeBeanList.add(homeBeanList_adv.get(quotient-1));

                                                }

                                                if (i<homeBeanList.size())
                                                    final_homeBeanList.add(homeBeanList.get(i));

                                            }
                                        }else{

                                            for (int i=0;i<matched_homeBeanList.size();i++){
                                                final_homeBeanList.add(homeBeanList.get(i));

                                            }

                                            for (int j=0;j<matched_homeBeanList.size();j++){
                                                final_homeBeanList.add(homeBeanList_adv.get(j));

                                            }

                                        }*/


                                       /* for (int i=0;i<=homeBeanList.size();i++){
                                            if (i%div==0 && i>0){
                                                int quotient = i/div;
                                               // JSONObject jsonObject1=sponseredArray.getJSONObject(quotient-1);
                                               // mainArray.put(jsonObject1);
                                                final_homeBeanList.add(homeBeanList_adv.get(quotient-1));

                                            }

                                            if (i<homeBeanList.size())
                                            final_homeBeanList.add(homeBeanList.get(i));

                                        }*/

                                       /* Cursor cursor_sug2 = dbHelper.getAllAddData(page,shrdPref_userId,-1);
                                        Cursor cursor2 = dbHelper.getAllData(page,shrdPref_userId,-1);


                                        Log.d("final_homeBeanList", final_homeBeanList.size()+"--"+cursor2.getCount()
                                        +"--"+ cursor_sug2.getCount());*/

                                       /* for (int i=0;i<postArray.length();i++){
                                            mainArray.put(postArray.getJSONObject(i));
                                            if (i%(sponseredArray.length()+1)==0 && i>0){
                                                int quotient=i/(sponseredArray.length()+1);
                                                JSONObject jsonObject1=sponseredArray.getJSONObject(quotient-1);
                                                mainArray.put(jsonObject1);
                                            }
                                        }*/

                                    }catch (Exception e){
                                        Log.d("exception",e+"");

                                    }


                                    //Log.d("allImageUrlData",buffer.toString());
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                           // loaderDiloag.dismissDiloag();
                                            //swipeRefreshLayout.setRefreshing(false);
                                            Log.d("listSixe", homeBeanList.size()+"");

                                            recyclerView.setVisibility(View.VISIBLE);

                                            tv_refresh_ad.setVisibility(View.GONE);


                                            loaderDiloag.dismissDiloag();
                                            swipeRefreshLayout.setRefreshing(false);
                                            // homeAdapter = new HomeAdapter(getActivity(), homeBeanList, Home.this, profile_pic);
                                            // recyclerView.setAdapter(homeAdapter);
                                            if (page == 1) {
                                                homeAdapter = new HomeAdapter(getActivity(), final_homeBeanList, Home.this, Home.this,Home.this, profile_pic,Home.this,Home.this);
                                                loaderDiloag.dismissDiloag();
                                                recyclerView.setAdapter(homeAdapter);
                                                recyclerView.setNestedScrollingEnabled(false);

                                               // getAllPost();    // called only for page 1
                                            } else {
                                                if (homeAdapter != null) {
                                                    //recyclerView.getRecycledViewPool().clear();
                                                    try {
                                                        loaderDiloag.dismissDiloag();
                                                        //homeAdapter.notifyDataSetChanged();

                                                        homeAdapter.notifyItemRangeChanged(0,recyclerView.getChildCount());
                                                    } catch (Exception e) {
                                                        Log.e("error", e.toString() + "");
                                                    }
                                                } else {
                                                    try {
                                                        homeAdapter = new HomeAdapter(getActivity(), final_homeBeanList, Home.this, Home.this,Home.this, profile_pic,Home.this,Home.this);
                                                        loaderDiloag.dismissDiloag();
                                                        recyclerView.setAdapter(homeAdapter);
                                                        recyclerView.setNestedScrollingEnabled(false);
                                                        homeAdapter.notifyDataSetChanged();
                                                    } catch (Exception e) {
                                                        Log.e("error", e.toString() + "");
                                                    }

                                                }
                                            }

                                        }
                                    });

                                }
                            } catch (Exception je) {
                                swipeRefreshLayout.setRefreshing(false);
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
           // Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
            rl_hide.setVisibility(View.GONE);


            loaderDiloag.displayDiloag();


            page++;
            homeBeanList.clear();
            homeBeanList_adv.clear();
            int count=(page-1)*5;
            int lmt=5;
            try {
                //Cursor cursor = dbHelper.getAllData(String.valueOf(count),String.valueOf(lmt));
                Cursor cursor = dbHelper.getAllData(page,shrdPref_userId,-1);
                Log.d("resCountOff", cursor.getCount() + "--" + dbHelper.getCount());

                if (cursor.getCount() == 0) {
                    // show message
                    // showMessage("Error","Nothing found");
                     //Toast.makeText(getActivity(), "Need to visit the page with internet for the first time", Toast.LENGTH_SHORT).show();
                    loaderDiloag.dismissDiloag();

                    return;
                }

                StringBuffer buffer = new StringBuffer();
                while (cursor.moveToNext()) {
//                                                buffer.append("Id :"+ cursor.getString(2)+"\n");
//                                                buffer.append("File :"+ cursor.getString(4)+"\n");
//                                                buffer.append("profile_pic :"+ cursor.getString(10)+"\n");
//                                                buffer.append("owner_profile_pic :"+ cursor.getString(14)+"\n\n");

                    postBean = new PostBean();
                    postBean.setId(Integer.parseInt(cursor.getString(2)));
                    postBean.setUser(Integer.parseInt(cursor.getString(6)));
                    postBean.setDesc(cursor.getString(3));
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//2016-01-24T16:00:00.000Z
                    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String created = String.valueOf(cursor.getString(5));
                    long time = sdf.parse(created).getTime();
                    long now = System.currentTimeMillis();
                    CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
                    postBean.setCreated(String.valueOf(ago));
                    postBean.setFile(cursor.getString(4));
                    postBean.setParent_post(cursor.getString(7));
                    postBean.setSharer_msg(cursor.getString(8));
                    postBean.setFullname(cursor.getString(9));
                    postBean.setProfile_pic(cursor.getString(10));
                    postBean.setLikes_count(cursor.getString(11));
                    postBean.setType(cursor.getString(16));
                    postBean.setFrame_name(cursor.getString(15));
                    if (cursor.getString(1).equals("1")) {
                        postBean.setLike(true);
                    } else {
                        postBean.setLike(false);

                    }
                    postBean.setComment(false);
                    postBean.setDelete(false);
                    postBean.setComments_count(cursor.getString(12) + " Comments");
                    postBean.setOwner_fullname(cursor.getString(13));
                    postBean.setOwner_profile_pic(cursor.getString(14));

                    postBean.setOrg_profile_pic(cursor.getString(19));
                    postBean.setOrg_file(cursor.getString(20));
                    postBean.setOrg_owner_profile_pic(cursor.getString(21));
                    postBean.setOrg_frame_name(cursor.getString(22));

                    postBean.setAdv_id(Integer.parseInt(cursor.getString(23)));
                    postBean.setAdv_name(cursor.getString(24));
                    postBean.setAdv_tags(cursor.getString(25));
                    postBean.setAdv_link(cursor.getString(26));
                    postBean.setAdv_startDate(cursor.getString(27));
                    postBean.setAdv_endDate(cursor.getString(28));
                    postBean.setAdv_amount(cursor.getInt(29));
                    postBean.setAdv_paid(cursor.getInt(30));
                    postBean.setAdv_image(cursor.getString(31));
                    postBean.setAdv_readCount(cursor.getString(32));
                    homeBeanList.add(postBean);
                }

                Cursor cursor_sug = dbHelper.getAllAddData(page,shrdPref_userId,-1);
                Log.d("resCountAdvOff", cursor.getCount() + "--" + cursor_sug.getCount());

                while (cursor_sug.moveToNext()) {
//                                                buffer.append("Id :"+ cursor.getString(2)+"\n");
//                                                buffer.append("File :"+ cursor.getString(4)+"\n");
//                                                buffer.append("profile_pic :"+ cursor.getString(10)+"\n");
//                                                buffer.append("owner_profile_pic :"+ cursor.getString(14)+"\n\n");


                            postBean = new PostBean();
                            postBean.setId(Integer.parseInt(cursor_sug.getString(2)));
                            postBean.setUser(Integer.parseInt(cursor_sug.getString(6)));
                            postBean.setDesc(cursor_sug.getString(3));
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//2016-01-24T16:00:00.000Z
                            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                            String created = String.valueOf(cursor_sug.getString(5));
                            long time = sdf.parse(created).getTime();
                            long now = System.currentTimeMillis();
                            CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
                            postBean.setCreated(String.valueOf(ago));
                            postBean.setFile(cursor_sug.getString(4));
                            postBean.setParent_post(cursor_sug.getString(7));
                            postBean.setSharer_msg(cursor_sug.getString(8));
                            postBean.setFullname(cursor_sug.getString(9));
                            postBean.setProfile_pic(cursor_sug.getString(10));
                            postBean.setLikes_count(cursor_sug.getString(11));
                            postBean.setType(cursor_sug.getString(16));
                            postBean.setFrame_name(cursor_sug.getString(15));
                            if (cursor_sug.getString(1).equals("1")) {
                                postBean.setLike(true);
                            } else {
                                postBean.setLike(false);

                            }
                            postBean.setComment(false);
                            postBean.setDelete(false);
                            postBean.setComments_count(cursor_sug.getString(12) + " Comments");
                            postBean.setOwner_fullname(cursor_sug.getString(13));
                            postBean.setOwner_profile_pic(cursor_sug.getString(14));

                            postBean.setOrg_profile_pic(cursor_sug.getString(19));
                            postBean.setOrg_file(cursor_sug.getString(20));
                            postBean.setOrg_owner_profile_pic(cursor_sug.getString(21));
                            postBean.setOrg_frame_name(cursor_sug.getString(22));

                            postBean.setAdv_id(Integer.parseInt(cursor_sug.getString(23)));
                            postBean.setAdv_name(cursor_sug.getString(24));
                            postBean.setAdv_tags(cursor_sug.getString(25));
                            postBean.setAdv_link(cursor_sug.getString(26));
                            postBean.setAdv_startDate(cursor_sug.getString(27));
                            postBean.setAdv_endDate(cursor_sug.getString(28));
                            postBean.setAdv_amount(cursor_sug.getInt(29));
                            postBean.setAdv_paid(cursor_sug.getInt(30));
                            postBean.setAdv_image(cursor_sug.getString(31));
                            postBean.setAdv_readCount(cursor_sug.getString(32));

                            Log.d("IDHomebeenlistAdv",Integer.parseInt(cursor_sug.getString(2))+"--"+cursor_sug.getInt(23));
                            homeBeanList_adv.add(postBean);


                }

                Log.d("home",homeBeanList.size()+"--+"+homeBeanList_adv.size());

                if (homeBeanList.size()>10 && homeBeanList_adv.size()>1) {
                    for (int i = 1; i <= homeBeanList_adv.size(); i++) {

                        int in = (homeBeanList.size() + homeBeanList_adv.size()) / homeBeanList_adv.size();
                        //in=in-1;
                        int index = i * in;
                        Log.d("index", index + "--" + homeBeanList.size() + "--" + homeBeanList_adv.size());
                        // if (i<homeBeanList.size())
                        homeBeanList.add(index - 1, homeBeanList_adv.get(i - 1));
                                               /* else
                                                    homeBeanList.add(index-1,homeBeanList_adv.get(i-1));*/

                    }
                }else{

                }


                final_homeBeanList.addAll(homeBeanList);
                Log.d("final_homeBeanListOff",final_homeBeanList.size()+"--"+ homeBeanList.size());


            }catch (Exception e){
                Log.d("exceptionOff",e+"");

            }


            //Log.d("allImageUrlData",buffer.toString());
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // loaderDiloag.dismissDiloag();
                    //swipeRefreshLayout.setRefreshing(false);

                    recyclerView.setVisibility(View.VISIBLE);

                    tv_refresh_ad.setVisibility(View.GONE);


                    loaderDiloag.dismissDiloag();
                    swipeRefreshLayout.setRefreshing(false);
                    // homeAdapter = new HomeAdapter(getActivity(), homeBeanList, Home.this, profile_pic);
                    // recyclerView.setAdapter(homeAdapter);
                    if (page == 1) {
                        homeAdapter = new HomeAdapter(getActivity(), final_homeBeanList, Home.this, Home.this,Home.this, profile_pic,Home.this,Home.this);
                        loaderDiloag.dismissDiloag();
                        recyclerView.setAdapter(homeAdapter);
                        recyclerView.setNestedScrollingEnabled(false);
                       // getAllPost();                                 //called only for page 1
                    } else {
                        if (homeAdapter != null) {
                            //recyclerView.getRecycledViewPool().clear();
                            try {
                                loaderDiloag.dismissDiloag();
                               // homeAdapter.notifyDataSetChanged();
                                homeAdapter.notifyItemRangeChanged(0,recyclerView.getChildCount());
                            } catch (Exception e) {
                                Log.e("error", e.toString() + "");
                            }
                        } else {
                            try {
                                homeAdapter = new HomeAdapter(getActivity(), final_homeBeanList, Home.this, Home.this,Home.this, profile_pic,Home.this,Home.this);
                                loaderDiloag.dismissDiloag();
                                recyclerView.setAdapter(homeAdapter);
                                recyclerView.setNestedScrollingEnabled(false);
                                homeAdapter.notifyDataSetChanged();
                            } catch (Exception e) {
                                Log.e("error", e.toString() + "");
                            }

                        }
                    }

                }
            });
        }

        return;
    }

  /*  private void demo(String jsonData) throws JSONException {

        try {
            JSONArray arr = new JSONArray(jsonData);

            for(int i = 0; i < arr.length();i++){

                JSONObject tes = arr.getJSONObject(i);

                Integer temp = tes.getInt("id");

                if(dbHelper.matchData(temp) >= 0)
                {
                    Log.d("matchData",dbHelper.matchData(temp)+"");
              matched_homeBeanList.add(dbHelper.matchData(temp));

                }
                else{
                    Log.d("unmatchData",temp+"");

                    unmatched_homeBeanList.add(temp);

                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (matched_homeBeanList.size()>0)
            demo_match(matched_homeBeanList,jsonData);

    }

    private void demo_match(ArrayList matched_homeBeanList, String jsonData) throws JSONException {

        JSONArray arr = new JSONArray(jsonData);

        for(int i = 0; i < arr.length();i++){

            JSONObject ter = arr.getJSONObject(i);

           // if(ter.getInt("id") == Integer.valueOf((String) matched_homeBeanList.get(i))) {
            if(ter.getInt("id") ==  (int)matched_homeBeanList.get(i)) {

                postFile_path = "";
                thumbnail_path = "";
                if (ter.getString("file_type").equalsIgnoreCase("video")) {
                    if (!ter.getString("file").equalsIgnoreCase("null")) {
                        downloadImage(WebApis.postImage + ter.getString("file"), path_video, "Video_" + ter.getString("id"));
                        downloadImage(WebApis.postImage + ter.getString("frame_name"), path_video, "Thumbnail_" + ter.getString("id"));
                        postFile_path = path_video + "/" + "Video_" + ter.getString("id");
                        thumbnail_path = path_video + "/" + "Thumbnail_" + ter.getString("id");
                    }


                } else if (ter.getString("file_type").equalsIgnoreCase("image")) {
                    if (!ter.getString("file").equalsIgnoreCase("null")) {
                        downloadImage(WebApis.postImage + ter.getString("file"), path_image, "postImage_" + ter.getString("id"));
                        postFile_path = path_image + "/" + "postImage_" + ter.getString("file");
                    }

                }

                String profile_pic = ter.getString("profile_pic");

                String profile_pic_path = "";
                if (!profile_pic.equals("null")) {
                    downloadImage(WebApis.userProfileImage + profile_pic, path_download, "profileImage_" + ter.getString("id"));
                    profile_pic_path = path_download + "/" + "profileImage_" + ter.getString("id");
                }
                String owner_profile_pic_path = "";
                if (!ter.getString("owner_profile_pic").equals("null")) {
                    downloadImage(WebApis.userProfileImage + ter.getString("owner_profile_pic"), path_download, "profileImgShare_" + ter.getString("id"));

                    owner_profile_pic_path = path_download + "/" + "profileImgShare_" + ter.getString("id");
                }


                dbHelper.updateData(ter.getString("likestatus"), ter.getInt("id"), ter.getString("description"), postFile_path, ter.getString("created_at"),
                        ter.getInt("user"), ter.getString("parent_post"),
                        ter.getString("sharer_msg"), ter.getString("fullname"), profile_pic_path,
                        ter.getString("likes_count"), ter.getString("comments_count"), ter.getString("owner_fullname"),
                        owner_profile_pic_path, thumbnail_path, ter.getString("file_type"), page, shrdPref_userId,
                        profile_pic, ter.getString("file"), ter.getString("owner_profile_pic"), ter.getString("frame_name"));


            }

        }


        if (unmatched_homeBeanList.size()>0)
        demo_unmatch(unmatched_homeBeanList,jsonData);


    }

    private void demo_unmatch(ArrayList unmatched_homeBeanList, String jsonData) throws JSONException {

        JSONArray arr = new JSONArray(jsonData);

        for(int i = 0; i < arr.length();i++) {

            JSONObject ter = arr.getJSONObject(i);

          //  if (ter.getInt("id") == Integer.valueOf((String) unmatched_homeBeanList.get(i))) {
            if (ter.getInt("id") ==  (int)unmatched_homeBeanList.get(i)) {


                dbHelper.deletePostByPostId(ter.getInt("id"));

            }
            else
                {

                    postFile_path="";
                    thumbnail_path="";
                    if (ter.getString("file_type").equalsIgnoreCase("video")){
                        if (!ter.getString("file").equalsIgnoreCase("null")) {
                            downloadImage(WebApis.postImage + ter.getString("file"), path_video, "Video_" + ter.getString("id"));
                            downloadImage(WebApis.postImage + ter.getString("frame_name") , path_video, "Thumbnail_" + ter.getString("id"));
                            postFile_path = path_video + "/" + "Video_" + ter.getString("id");
                            thumbnail_path = path_video + "/" + "Thumbnail_" + ter.getString("id");
                        }


                    }else if (ter.getString("file_type").equalsIgnoreCase("image")){
                        if (!ter.getString("file").equalsIgnoreCase("null")) {
                            downloadImage(WebApis.postImage + ter.getString("file"), path_image, "postImage_" + ter.getString("id"));
                            postFile_path = path_image + "/" + "postImage_" + ter.getString("file");
                        }

                    }

                    String profile_pic = ter.getString("profile_pic");

                    String profile_pic_path="";
                    if (!profile_pic.equals("null")) {
                        downloadImage(WebApis.userProfileImage+profile_pic,path_download,"profileImage_"+ter.getString("id"));
                        profile_pic_path = path_download +"/" + "profileImage_"+ter.getString("id");
                    }
                    String owner_profile_pic_path="";
                    if (!ter.getString("owner_profile_pic").equals("null")) {
                        downloadImage(WebApis.userProfileImage + ter.getString("owner_profile_pic"), path_download, "profileImgShare_" + ter.getString("id"));

                        owner_profile_pic_path = path_download +"/" + "profileImgShare_" + ter.getString("id");
                    }


                    dbHelper.insertData(ter.getString("likestatus"), ter.getInt("id"), ter.getString("description"), postFile_path, ter.getString("created_at"),
                            ter.getInt("user"), ter.getString("parent_post"),
                            ter.getString("sharer_msg"), ter.getString("fullname"), profile_pic_path,
                            ter.getString("likes_count"), ter.getString("comments_count"), ter.getString("owner_fullname"),
                            owner_profile_pic_path, thumbnail_path, ter.getString("file_type"), page, shrdPref_userId,
                            profile_pic, ter.getString("file"), ter.getString("owner_profile_pic"), ter.getString("frame_name"));


                }

        }


            }



    private void demo2(String jsonData) throws JSONException, ParseException {

        try {
            JSONArray arr = new JSONArray(jsonData);

            for(int i = 0; i < arr.length();i++){

                JSONObject jsonObject1 = arr.getJSONObject(i);
                //   String id=jsonObject1.getString("id");
                Integer id = jsonObject1.getInt("id");
                Integer user = jsonObject1.getInt("user");


                Log.d("html",jsonObject1.getString("description"));

                String desc="";

                if(isHtml(jsonObject1.getString("description"))){

                    String tmpHtml = jsonObject1.getString("description");
                    //  Spanned temp = Html.fromHtml(tmpHtml);
                    //String tmpHtml= "<html><head></head><body><p>test </p><img alt='\\U263a\\Ufe0f' class='emojioneemoji' src='https://cdnjs.cloudflare.com/ajax/libs/emojione/2.1.4/assets/png/1f60a.png'><p>testtt</P></body></html>";

                    Document doc = Jsoup.parse(tmpHtml);


                    String tag = null;

                    Elements elements = doc.body().select("*");

                    for (Element element : elements) {

                        tag = element.tagName();
                        Log.d("MyTag",tag);

                        if ( "img".equalsIgnoreCase(tag) ) {



                            String temp;

                            Log.d("imageTag",element.attr("src") + " Alt: " + element.attr("alt")+"");
                            System.out.println("image tag: " + element.attr("src") + " Alt: " + element.attr("alt"));

                            temp = "";
                            temp = Utils.decodeMessage(element.attr("alt"));
                            desc+= temp;




                        }
                        else if("p".equalsIgnoreCase(tag)){

                            //desc+= doc.body().ownText();

                            desc+= element.text();
                        }
                        else{
                            desc+= element.text();
                        }



                    }


                }
                else{

                    desc = Utils.decodeMessage(jsonObject1.getString("description"));

                }





                String created = jsonObject1.getString("created_at");
                String file = jsonObject1.getString("file");
                //String str_result1=new
                //DownloadImageTask().execute(WebApis.postImage+file,"postImage_"+id).get();
                String parent_post = jsonObject1.getString("parent_post");
                String sharer_msg = jsonObject1.getString("sharer_msg");
                String fullname = jsonObject1.getString("fullname");
                String profile_pic = jsonObject1.getString("profile_pic");

                String profile_pic_path="";
                if (!profile_pic.equals("null")) {
                    downloadImage(WebApis.userProfileImage+profile_pic,path_download,"profileImage_"+id);
                    profile_pic_path = path_download +"/" + "profileImage_"+id;
                }

                // String str_result2=new
                // DownloadImageTask().execute(WebApis.userProfileImage+profile_pic,"profileImage_"+id).get();
                String likes_count = jsonObject1.getString("likes_count");
                String comments_count = jsonObject1.getString("comments_count");
                String owner_fullname = jsonObject1.getString("owner_fullname");
                String owner_profile_pic = jsonObject1.getString("owner_profile_pic");

                String owner_profile_pic_path="";
                if (!owner_profile_pic.equals("null")) {
                    downloadImage(WebApis.userProfileImage + owner_profile_pic, path_download, "profileImgShare_" + id);

                    owner_profile_pic_path = path_download +"/" + "profileImgShare_" + id;
                }
                //String str_result3=new
                // DownloadImageTask().execute(WebApis.userProfileImage+profile_pic,"shareProfileImage_"+id).get();
                String likestatus = jsonObject1.getString("likestatus");
                String file_type = jsonObject1.getString("file_type");
                String frame_name = jsonObject1.getString("frame_name");

                postFile_path="";
                thumbnail_path="";
                if (file_type.equalsIgnoreCase("video")){
                    if (!file.equalsIgnoreCase("null")) {
                        downloadImage(WebApis.postImage + file, path_video, "Video_" + id);
                        downloadImage(WebApis.postImage + frame_name, path_video, "Thumbnail_" + id);
                        postFile_path = path_video + "/" + "Video_" + id;
                        thumbnail_path = path_video + "/" + "Thumbnail_" + id;
                    }


                }else if (file_type.equalsIgnoreCase("image")){
                    if (!file.equalsIgnoreCase("null")) {
                        downloadImage(WebApis.postImage + file, path_image, "postImage_" + id);
                        postFile_path = path_image + "/" + "postImage_" + id;
                    }

                }

                Log.d("filePath",postFile_path);




               // JSONObject ter = arr.getJSONObject(i);

                Integer temp = jsonObject1.getInt("id");
                Cursor cursor=dbHelper.matchData2(temp);

                if(cursor.getCount() > 0)
                {
                    //Log.d("matchData",dbHelper.matchData(temp)+"");
                  *//*  boolean abc=  dbHelper.updateData(likestatus, id, desc, postFile_path, created, user, parent_post,
                            sharer_msg,fullname, profile_pic_path, likes_count, comments_count, owner_fullname,
                            owner_profile_pic_path, thumbnail_path, file_type,page,shrdPref_userId,profile_pic,file,owner_profile_pic,
                            frame_name);*//*

                    postBean = new PostBean();
                    postBean.setId(id);
                    postBean.setUser(user);
                    postBean.setDesc(desc);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//2016-01-24T16:00:00.000Z
                    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String created_at = String.valueOf(created);
                    long time = sdf.parse(created_at).getTime();
                    long now = System.currentTimeMillis();
                    CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
                    postBean.setCreated(String.valueOf(ago));
                    postBean.setFile(postFile_path);
                    postBean.setParent_post(parent_post);
                    postBean.setSharer_msg(sharer_msg);
                    postBean.setFullname(fullname);
                    postBean.setProfile_pic(profile_pic_path);
                    postBean.setLikes_count(likes_count);
                    postBean.setType(file_type);
                    postBean.setFrame_name(thumbnail_path);
                    if (likestatus.equals("1")) {
                        postBean.setLike(true);
                    } else {
                        postBean.setLike(false);

                    }
                    postBean.setComment(false);
                    postBean.setDelete(false);
                    postBean.setComments_count(comments_count + " Comments");
                    postBean.setOwner_fullname(owner_fullname);
                    postBean.setOwner_profile_pic(owner_profile_pic_path);

                    postBean.setOrg_profile_pic(profile_pic);
                    postBean.setOrg_file(file);
                    postBean.setOrg_owner_profile_pic(owner_profile_pic);
                    postBean.setOrg_frame_name(frame_name);

                    //homeBeanList.add(postBean);
                    homeBeanList_m.add(postBean);

                }
                else{
                    Log.d("unmatchData",temp+"");

                   // unmatched_homeBeanList.add(temp);

                    postBean = new PostBean();
                    postBean.setId(id);
                    postBean.setUser(user);
                    postBean.setDesc(desc);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//2016-01-24T16:00:00.000Z
                    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String created_at = String.valueOf(created);
                    long time = sdf.parse(created_at).getTime();
                    long now = System.currentTimeMillis();
                    CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
                    postBean.setCreated(String.valueOf(ago));
                    postBean.setFile(postFile_path);
                    postBean.setParent_post(parent_post);
                    postBean.setSharer_msg(sharer_msg);
                    postBean.setFullname(fullname);
                    postBean.setProfile_pic(profile_pic_path);
                    postBean.setLikes_count(likes_count);
                    postBean.setType(file_type);
                    postBean.setFrame_name(thumbnail_path);
                    if (likestatus.equals("1")) {
                        postBean.setLike(true);
                    } else {
                        postBean.setLike(false);

                    }
                    postBean.setComment(false);
                    postBean.setDelete(false);
                    postBean.setComments_count(comments_count + " Comments");
                    postBean.setOwner_fullname(owner_fullname);
                    postBean.setOwner_profile_pic(owner_profile_pic_path);

                    postBean.setOrg_profile_pic(profile_pic);
                    postBean.setOrg_file(file);
                    postBean.setOrg_owner_profile_pic(owner_profile_pic);
                    postBean.setOrg_frame_name(frame_name);

                    //homeBeanList.add(postBean);
                    homeBeanList_un.add(postBean);

                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        //if (homeBeanList_m.size()>0)
           // demo_match2(homeBeanList_m,jsonData);

    }*/

/*
    private void demo_match2(List<PostBean> homeBeanList_m, String jsonData) throws JSONException {

        JSONArray arr = new JSONArray(jsonData);

        for(int i = 0; i < arr.length();i++){

            JSONObject ter = arr.getJSONObject(i);

            // if(ter.getInt("id") == Integer.valueOf((String) matched_homeBeanList.get(i))) {
            if(ter.getInt("id") ==  (int)matched_homeBeanList.get(i)) {



            }

        }


        if (unmatched_homeBeanList.size()>0)
            demo_unmatch(unmatched_homeBeanList,jsonData);

        Cursor cursor = dbHelper.getAllData(page,shrdPref_userId);
        Log.d("resCount", cursor.getCount() + "--" + dbHelper.getCount());

        if (cursor.getCount() == 0) {
            // show message
            // showMessage("Error","Nothing found");
            //Toast.makeText(getActivity(), "Need to visit the page with internet for the first time", Toast.LENGTH_SHORT).show();

            return;
        }


        while (cursor.moveToNext()) {
            for (int i=0;i<homeBeanList_m.size();i++) {
                if (Integer.parseInt(cursor.getString(2))==(int) homeBeanList_m.get(i).getId()){

                    postFile_path = "";
                    thumbnail_path = "";
                    if (ter.getString("file_type").equalsIgnoreCase("video")) {
                        if (!ter.getString("file").equalsIgnoreCase("null")) {
                            downloadImage(WebApis.postImage + ter.getString("file"), path_video, "Video_" + ter.getString("id"));
                            downloadImage(WebApis.postImage + ter.getString("frame_name"), path_video, "Thumbnail_" + ter.getString("id"));
                            postFile_path = path_video + "/" + "Video_" + ter.getString("id");
                            thumbnail_path = path_video + "/" + "Thumbnail_" + ter.getString("id");
                        }


                    } else if (ter.getString("file_type").equalsIgnoreCase("image")) {
                        if (!ter.getString("file").equalsIgnoreCase("null")) {
                            downloadImage(WebApis.postImage + ter.getString("file"), path_image, "postImage_" + ter.getString("id"));
                            postFile_path = path_image + "/" + "postImage_" + ter.getString("file");
                        }

                    }

                    String profile_pic = ter.getString("profile_pic");

                    String profile_pic_path = "";
                    if (!profile_pic.equals("null")) {
                        downloadImage(WebApis.userProfileImage + profile_pic, path_download, "profileImage_" + ter.getString("id"));
                        profile_pic_path = path_download + "/" + "profileImage_" + ter.getString("id");
                    }
                    String owner_profile_pic_path = "";
                    if (!ter.getString("owner_profile_pic").equals("null")) {
                        downloadImage(WebApis.userProfileImage + ter.getString("owner_profile_pic"), path_download, "profileImgShare_" + ter.getString("id"));

                        owner_profile_pic_path = path_download + "/" + "profileImgShare_" + ter.getString("id");
                    }


                    dbHelper.updateData(ter.getString("likestatus"), ter.getInt("id"), ter.getString("description"), postFile_path, ter.getString("created_at"),
                            ter.getInt("user"), ter.getString("parent_post"),
                            ter.getString("sharer_msg"), ter.getString("fullname"), profile_pic_path,
                            ter.getString("likes_count"), ter.getString("comments_count"), ter.getString("owner_fullname"),
                            owner_profile_pic_path, thumbnail_path, ter.getString("file_type"), page, shrdPref_userId,
                            profile_pic, ter.getString("file"), ter.getString("owner_profile_pic"), ter.getString("frame_name"));

                }else{

                }

            }
        }


    }
*/


    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose Image", "Choose Video", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo or Video!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item)
            {

                if (options[item].equals("Take Photo"))
                {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                            && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED)
                    {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                REQUEST_CAMERA_ACCESS_PERMISSION);
                    }
                    else
                    {
                        dispatchTakePictureIntent();
                    }

                }
                else if (options[item].equals("Choose Image"))
                {
                    if (Build.VERSION.SDK_INT >=23)
                    {
                        Log.d(TAG, "onClick: This is Marshmallow");
                        if (getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED || getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                        {
                            Log.v("TAG", "Permission is granted");
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            //  Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(photoPickerIntent, 2);//one can be replaced with any action code

                        }
                        else
                            {

                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                        }
                    }

                    else
                    {
                        Log.v("TAG", "This is not Marshmallow");
                        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
                        photoPickerIntent.setType("image/*");
                        //  Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(photoPickerIntent, 2);//one can be replaced with any action code

                    }

                } else if (options[item].equals("Choose Video"))
                {
                    if (Build.VERSION.SDK_INT >= 23)
                    {
                        if (getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                                == PackageManager.PERMISSION_GRANTED || getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                        {
                            Log.v("TAG", "Permission is granted");
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("video/*");
                            //  Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(photoPickerIntent, 3);//one can be replaced with any action code

                        } else
                            {

                            Log.v("TAG", "Permission is revoked");
                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                        }
                    } else {

                        Log.v("TAG", "Permission is granted");
                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                        photoPickerIntent.setType("image/*");
                        //  Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(photoPickerIntent, 4);//one can be replaced with any action code
                    }

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void dispatchTakePictureIntent() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);


      /*  File f = null;
        try {
            photoURI = FileProvider.getUriForFile(getActivity(),
                    BuildConfig.APPLICATION_ID + ".provider",
                    setUpPhotoFile());
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        } catch (IOException e) {
            e.printStackTrace();
        }*/


        startActivityForResult(takePictureIntent, 1);
    }

    private File setUpPhotoFile() throws IOException {

        File f = createImageFile();
        mCurrentPhotoPath = f.getAbsolutePath();
        //    Log.d(TAG,"CurrentPath " + mCurrentPhotoPath);

        return f;
    }

    private void handleBigCameraPhoto()
    {
        // Log.d(TAG,"Photo " + mCurrentPhotoPath.toString());
        if (mCurrentPhotoPath != null)
        {
            setPic();
            galleryAddPic();
            mCurrentPhotoPath = null;
        }

    }

    private void setPic() {

        /* There isn't enough memory to open up more than a camera photos */
        /* So pre-scale the target bitmap into which the file is decoded */

        /* Get the size of the ImageView */
        int targetW = 100;//.getWidth();
        int targetH = 100;////mImageView.getHeight();

        /* Get the size of the image */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        /* Figure out which way needs to be reduced less */
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        }

        /* Set bitmap options to scale the image decode target */
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        /* Decode the JPEG file into a Bitmap */
        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);


        //  imgImage.setImageBitmap(bitmap);
        layImage.setVisibility(View.VISIBLE);
        // Uri selectedFileUri = data.getData();
        // String selectedFilePath = FilePath.getPath(getActivity(), selectedFileUri);
        // File file = new File(selectedFilePath);
        //bin = new FileBody(file);
        File file = new File(mCurrentPhotoPath);
        //bin = new FileBody(file);
        filePath = new FileBody(file);
        extension = "png";
        /* Associate the Bitmap to the ImageView */
        /*mImageView.setImageBitmap(bitmap);
        mVideoUri = null;
        mImageView.setVisibility(View.VISIBLE);
        mVideoView.setVisibility(View.INVISIBLE);*/
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }



    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
        return imageF;
    }

    private File getAlbumDir() {
        File storageDir = null;

        storageDir = new File(Environment.getExternalStorageDirectory()
                + "/lynkpal");
      /*  if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir("CameraSample");

            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }*/

        return storageDir;
    }


    @Override
    public void onStart() {
        super.onStart();
        //Log.d("pressed","onStart");
       // Toast.makeText(getActivity(),"onStart",Toast.LENGTH_SHORT).show();
       /* try {


            page=0;
            homeBeanList.clear();
            homeAdapter = new HomeAdapter(getActivity(), homeBeanList, Home.this, Home.this, Home.this,profile_pic,Home.this,Home.this);
            loaderDiloag.dismissDiloag();
            recyclerView.setAdapter(homeAdapter);
            recyclerView.setNestedScrollingEnabled(false);
            getAllPost();

            if (LoginActivity.isNetworkConnected(getContext()))
                suggestionApi(getContext(),Constant.token);

        }catch (Exception e){

        }*/
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //Log.d("pressed","isVisibleToUser");


    }

    @Override
    public void onPause() {
        super.onPause();
        //Log.d("pressed","OnPause");
        //Toast.makeText(getActivity(),"onPause",Toast.LENGTH_SHORT).show();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                   /* if (LoginActivity.isNetworkConnected(getContext()))
                        suggestionApi(getContext(),Constant.token);

                    page=0;
                    homeBeanList.clear();
                    getAllPost();*/

                }catch (Exception e){

                }
            }
        });

    }

    @Override
    public void onStop() {
       // getActivity().unregisterReceiver(mBroadcastReceiver);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
         getActivity().unregisterReceiver(mBroadcastReceiver);

    }

    @Override
    public void onResume()
    {
        super.onResume();
      //  Toast.makeText(getActivity(),"onResume",Toast.LENGTH_SHORT).show();
       /* if (LoginActivity.isNetworkConnected(getContext()))
        {*/
       try {
           //new GetAllPost().execute();
         /*  //GetAllPost(getContext(),Constant.token);
           if (LoginActivity.isNetworkConnected(getContext()))
           suggestionApi(getContext(),Constant.token);
           page=0;
           homeBeanList.clear();
           //homeBeanList.removeAll(homeBeanList);
           homeAdapter = new HomeAdapter(getActivity(), homeBeanList, Home.this, Home.this, profile_pic,Home.this,Home.this);
           loaderDiloag.dismissDiloag();
           recyclerView.setAdapter(homeAdapter);
           recyclerView.setNestedScrollingEnabled(false);
           getAllPost();*/

           getLocation();

       }catch (Exception e){

       }

       /* }
        else
        {

        }*/

       // Log.d("pressed","OnResume");
        Log.d(TAG,"checksize " + mFriendList.size()+  " " +homeBeanList.size());
        if (Constant.update_home)
        {
           /* Constant.update_home = false;
            page = 0;
            visibleItemCount = 0;
            totalItemCount = 0;
            pastVisiblesItems = 0;
            homeBeanList.clear();
            Log.d(TAG, "onResume: Bracket ");

            getAllPost();*/


        }

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                String action = intent.getAction();
                switch (action)
                {
                    case Constant.UI_AUTHENTICATED:
                        Log.d("BroadCast","Got a broadcast to show the main app window");
                        page=0;
                        homeBeanList.clear();
                        homeBeanList_adv.clear();
                        final_homeBeanList.clear();
                        homeAdapter = new HomeAdapter(getActivity(), homeBeanList, Home.this, Home.this,Home.this, profile_pic,Home.this,Home.this);
                        loaderDiloag.dismissDiloag();
                        recyclerView.setAdapter(homeAdapter);
                        recyclerView.setNestedScrollingEnabled(false);
                        getAllPost();
                        llUploading.setVisibility(View.GONE);
                        rlUploading.setVisibility(View.GONE);
                        new MaterialDialog.Builder(getContext())
                                .title("Post Successful")
                                .content("Your post has been succeccfully uploaded")
                                .iconRes(R.drawable.ic_check_circle)
                                .positiveText("OK")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                    }
                                })
                                .show();
                        //Show the main app window
//                            showProgress(false);
//                            Intent i2 = new Intent(mContext,ContactListActivity.class);
//                            startActivity(i2);
//                            finish();
                        break;
                }

            }
        };
        IntentFilter filter = new IntentFilter(Constant.UI_AUTHENTICATED);
        getActivity().registerReceiver(mBroadcastReceiver, filter);
    }

    void getLocation() {
        try {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 15000, 15, this);
        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        homeBeanList.remove(id);
        homeAdapter.notifyDataSetChanged();
    }

    @Override
    public void image(final String path)
    {
        Log.d(TAG, "image: " + path);
        LayoutInflater layoutInflater
                = (LayoutInflater) getActivity().getBaseContext()
                .getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.popup_image_show, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.NO_GRAVITY, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        ImageView ic_download = (ImageView) popupView.findViewById(R.id.ic_download);
        ImageView ic_close = (ImageView) popupView.findViewById(R.id.ic_close);

        ImageView imageViewTouch;
        imageViewTouch = (ImageView) popupView.findViewById(R.id.myImage);
        Glide.with(getActivity()).load(path).into(imageViewTouch);
        ic_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        ic_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                //file_download(path);
               if (LoginActivity.isNetworkConnected(getContext()))
               {
                   mImageloader.startLoading();
                   imageDownloading(getContext(),path);
               }
               else
               {
                   Toast.makeText(getContext(),"Please Connect to Internet",Toast.LENGTH_SHORT).show();
               }
            }
        });


    }


    @Override
    public void videoPopup(final String path) {
        Log.d(TAG, "video: " + path);
        LayoutInflater layoutInflater
                = (LayoutInflater) getActivity().getBaseContext()
                .getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.popup_video_show, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.NO_GRAVITY, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        ImageView ic_download = (ImageView) popupView.findViewById(R.id.ic_download);
        ImageView ic_close = (ImageView) popupView.findViewById(R.id.ic_close);

        //ImageView imageViewTouch;
        //imageViewTouch = (ImageView) popupView.findViewById(R.id.myImage);
        // Glide.with(getActivity()).load(path).into(imageViewTouch);

        VideoView vidView = (VideoView) popupView.findViewById(R.id.myVideo);

        loaderDiloag = new LoaderDiloag(getActivity());
        loaderDiloag.setCancelable(true);
        loaderDiloag.displayDiloag();
        vidView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                if (loaderDiloag.isShowing())
                    loaderDiloag.dismissDiloag();
            }
        });
        MediaController vidControl = new MediaController(getActivity());
        vidControl.setAnchorView(vidView);
        vidView.setMediaController(vidControl);
        vidView.setVideoPath(path);
        vidView.start();
        ic_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        ic_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                //file_download(path);
                if (LoginActivity.isNetworkConnected(getContext()))
                {
                    mImageloader.startLoading();
                    imageDownloading(getContext(),path);
                }
                else
                {
                    Toast.makeText(getContext(),"Please Connect to Internet",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void imageDownloading(final Context context, String url)
    {
        com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(context);

        // Initialize a new ImageRequest
        ImageRequest imageRequest = new ImageRequest(
                url, // Image URL
                new Response.Listener<Bitmap>() { // Bitmap listener
                    @Override
                    public void onResponse(Bitmap response)
                    {
                        mImageloader.stopLoading();
                        Log.d(TAG, "onResponse: Successful" + response);
                        Toast.makeText(context,"Image Successfully downloaded",Toast.LENGTH_SHORT).show();
                      saveToInternalStorage(response);
                    }
                },
                0, // Image width
                0, // Image height
                ImageView.ScaleType.CENTER_CROP, // Image scale type
                Bitmap.Config.RGB_565, //Image decode configuration
                new Response.ErrorListener() { // Error listener
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        mImageloader.stopLoading();
                        error.printStackTrace();
                        Log.d(TAG, "onErrorResponse: failed");
                        Toast.makeText(context,"Fail to downloaded",Toast.LENGTH_SHORT).show();
                      //  Snackbar.make(mCLayout,"Error",Snackbar.LENGTH_LONG).show();
                    }
                }
        );

        // Add ImageRequest to the RequestQueue
        requestQueue.add(imageRequest);
}

    private String saveToInternalStorage(Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,"profile.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Log.d(TAG, "saveToInternalStorage: " + directory.getAbsolutePath());

        return directory.getAbsolutePath();
    }




    public void file_download(String uRl) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/lynkpal");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        DownloadManager mgr = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);

        Uri downloadUri = Uri.parse(uRl);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("Lynkpal")
                .setDescription("Image Downloading...")
                .setDestinationInExternalPublicDir("/lynkpal", "image.jpg");

        mgr.enqueue(request);

        Toast.makeText(getActivity(), "Successfully Downloaded", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void clicktoopen(int id,int user)
    {
        Log.d(TAG,"Post_Id "  + id + user);
        ReportFragment fragment = new ReportFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putInt("post_id",id);
        bundle.putInt("user",user);
        fragment.setArguments(bundle);
        /*
         * When this container fragment is created, we fill it with our first
         * "real" fragment
         */
        transaction.replace(R.id.fragment_home_container,fragment);

        transaction.commit();
    }

    @Override
    public void clicktodelete(int id)
    {
        DeleteFragment deleteFragment = new DeleteFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putInt("post_id",id);

        deleteFragment.setArguments(bundle);
        /*
         * When this container fragment is created, we fill it with our first
         * "real" fragment
         */
        transaction.replace(R.id.fragment_home_container,deleteFragment);

        transaction.commit();
    }

    @Override
    public void onCloseClick(int id)
    {
        // mFriendList = new ArrayList<>();

        try {
            mFriendList.remove(id);
            Log.d(TAG, "frnidSize " + "--" + mFriendList.size() + "--" + id + "--" + friendSuggestion.getId());
            mFriendAdapter.notifyDataSetChanged();
            if (mFriendList.size()==0){
                if (LoginActivity.isNetworkConnected(getContext()))
                suggestionApi(getContext(), settings.getString("token",""));
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onFriendAdd(String id)
    {
        Log.d(TAG,"Frndid " + id);
        if (LoginActivity.isNetworkConnected(getContext())) {
            addFriend(getContext(), id);
            loaderDiloag.displayDiloag();

        } else {
            Toast.makeText(getActivity(), "Please connect to internet", Toast.LENGTH_SHORT).show();
        }
    }

    private void addFriend(final Context context, String user) {


        com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(context);
        Map<String, String> postParam = new HashMap<String, String>();
        postParam.put("toFollow", user);

        Log.d(TAG, "frndParam " + postParam.toString());


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.POST,
                WebApis.FollowUnfollow, new JSONObject(postParam),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response)
                    {
                        loaderDiloag.dismissDiloag();
                        Log.d(TAG, "FrndResponse " + response.toString());
                        try {
                            String status = response.getString("status");
                            String message = response.getString("message");
                            if (status.contains("success"))
                            {
                              mFriendAdapter.notifyDataSetChanged();
                               suggestionApi(context, settings.getString("token",""));
                                Toast.makeText(context, "Followed successfully ", Toast.LENGTH_LONG).show();
                            } else
                                {

                                Log.d(TAG, "frndError " + response.toString());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener()
        {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                loaderDiloag.dismissDiloag();
                error.printStackTrace();
                if (error.toString().contains(Constant.timeout_error_string));
                Toast.makeText(context,getString(R.string.slow_internet), Toast.LENGTH_SHORT).show();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String,String> data = new HashMap<String, String>();
                data.put("Authorization",settings.getString("token",""));
                data.put("Content-Type","application/json");
                Log.d(TAG,"header " + data.toString());
                return data;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                Constant.VOLLEY_TIMEOUT_MS,
                Constant.VOLLEY_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        jsonObjReq.setTag(TAG);
        // Adding request to request queue
        requestQueue.add(jsonObjReq);
    }



    public void share(final Context context, int postID, String Comment) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(context);
        if (ApplicationGlobles.isConnectingToInternet((Activity) context)) {
            loaderDiloag.displayDiloag();

            MediaType mediaType = MediaType.parse("application/json");

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sharerMsg", Comment);
                jsonObject.put("postId", postID);
            } catch (Exception e) {

            }
            Log.e("jsonObject", jsonObject.toString());
            RequestBody body = RequestBody.create(mediaType, jsonObject.toString() + "");
            Request request = new Request.Builder()
                    .url(WebApis.SharePOST)
                    .post(body)
                    .addHeader("authorization", settings.getString("token",""))
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();
            //OkHttpClient client = new OkHttpClient();
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(Constant.VOLLEY_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                    .writeTimeout(Constant.WRITE_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                    .readTimeout(Constant.READ_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                    .build();
            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (e.toString().contains(Constant.timeout_error_string)) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(), getString(R.string.slow_internet), Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                        if (context == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException
                    {
                        loaderDiloag.dismissDiloag();
                        final String jsonData = response.body().string();
                        Log.e("respons", jsonData + "");
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(jsonData);
                                    String success = jsonObject.getString("status");
                                    if (success.equals("success"))
                                    {
                                        Constant.update_home = true;
                                        Toast.makeText(context, jsonObject.getString("message") + "", Toast.LENGTH_SHORT).show();

                                        page = 0;
                                        visibleItemCount = 0;
                                        totalItemCount = 0;
                                        pastVisiblesItems = 0;
                                        homeBeanList.clear();
                                        homeBeanList_adv.clear();
                                        final_homeBeanList.clear();

                                        getAllPost();
                                        // postList.get(pos).setComments_count(jsonObject.getInt("comment_count") + " Comments");

                                    }
                                } catch (Exception e) {

                                }

                            }
                        });
                    }
                });

            } catch (Exception e) {
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(context, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    @Override
    public void clicktoShare(int id, String text)
    {
        share(getContext(),id,text);
    }

    @Override
    public void onLocationChanged(Location location) {

        Log.d("LatLong", location.getLatitude()+"--"+ location.getLongitude());
        if (location.getLatitude()!=0 && location.getLongitude()!=0 && getActivity()!=null){
            if (ApplicationGlobles.isConnectingToInternet(getActivity())) {
                getWeatherDetail(getActivity(),String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()));
            } else {
                Toast.makeText(getActivity(), "Please connect to internet", Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void getWeatherDetail(final Context context, String lat,String lon) {


        com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        Map<String, String> postParam = new HashMap<String, String>();
        postParam.put("lat", lat);
        postParam.put("lon", lon);
        postParam.put("APPID", "0e85dd958eee7e250441b2605470eadc");

        Log.d(TAG, "frndParam " + postParam.toString());


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.GET,
                "https://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lon+"&APPID=0e85dd958eee7e250441b2605470eadc", null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response)
                    {
                        loaderDiloag.dismissDiloag();
                        Log.d(TAG, "FrndResponse " + response.toString());
                        try {
                            JSONObject json = response.getJSONObject("coord");
                            String res_lon = json.getString("lon");
                            String res_lat = json.getString("lat");

                            JSONArray weat = response.getJSONArray("weather");

                            String res_weather_id = weat.getJSONObject(0).getString("id");

                            String res_main = weat.getJSONObject(0).getString("main");

                            String res_description = weat.getJSONObject(0).getString("description");
                            String res_icon = weat.getJSONObject(0).getString("icon");

                            JSONObject mainJson = response.getJSONObject("main");
                            String temp = mainJson.getString("temp");   //TEMP in kelvin  subtract 273.15.
                            double abc= Double.parseDouble(temp)-273.15;
                            int i = (int)abc;
                            temp=i+"\u00b0"+"C";
                            String temp_min = mainJson.getString("temp_min");
                            String temp_max = mainJson.getString("temp_max");

                            //https://stackoverflow.com/questions/19477324/how-do-i-calculate-the-temperature-in-celsius-returned-in-openweathermap-org-jso



                            JSONObject sysJson = response.getJSONObject("sys");
                            String sunrise = sysJson.getString("sunrise");
                            String sunset = sysJson.getString("sunset");

                          /*  Date date = new Date(sunrise);
                            DateFormat formatter = new SimpleDateFormat("HH:mm:ss.SSS");
                            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                            String dateFormatted = formatter.format(date);*/

                          /*long millis= Long.parseLong(sunrise);
                            String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                           // System.out.println(hms);*/



                            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                            long milliSeconds= Long.parseLong(sunrise);
                            //System.out.println(milliSeconds);

                            Calendar calendar = Calendar.getInstance();
                            calendar.setTimeInMillis(milliSeconds);
                            //System.out.println(formatter.format(calendar.getTime()));

                            String city= response.getString("name");

                            Log.d("resWeat",res_main+"--"+ temp_max +"--"+ sunrise+"--"+city+"--"+formatter.format(calendar.getTime()));

                            String url="http://openweathermap.org/img/w/"+res_icon+".png";
                            weather_ll.setVisibility(View.VISIBLE);
                            Glide.with(getActivity().getApplicationContext()).load(url).into(iv_weather);
                            tv_city.setText(city);
                            tv_weather.setText(res_main);
                            tv_temperature.setText(temp);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener()
        {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                loaderDiloag.dismissDiloag();
                error.printStackTrace();
                if (error.toString().contains(Constant.timeout_error_string));
               // Toast.makeText(context,getString(R.string.slow_internet), Toast.LENGTH_SHORT).show();
            }
        });



        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                Constant.VOLLEY_TIMEOUT_MS,
                Constant.VOLLEY_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        jsonObjReq.setTag(TAG);
        // Adding request to request queue
        requestQueue.add(jsonObjReq);
    }



    private class VideoUploader extends AsyncTask<Void, Void, Void>
    {
        String url;


        @Override
        protected void onPreExecute()
        {
            dialog.dismiss();
            loaderDiloag.displayDiloag();
            url = WebApis.postURLNew;
            url = url.replace(" ", "%20");
            Log.e("extension", extension + "");
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            // TODO Auto-generated method stub

            // Client-side HTTP transport library
            HttpClient httpClient = new DefaultHttpClient();
            // using POST method
            HttpPost httpPostRequest = new HttpPost(url);
           // httpPostRequest.setHeader("authorization", Constant.token);
            httpPostRequest.setHeader("authorization", settings.getString("token",""));
            try {
                MultipartEntityBuilder multiPartEntityBuilder = MultipartEntityBuilder.create();

                if (filePath != null)
                {
                    multiPartEntityBuilder.addPart("postFile", filePath);

                } else
                    {
                    extension = "text";
                }

              /*  String preparedData=replaceMsg(URLEncoder.encode(postText,"UTF-8"));
                Log.d("preparedData",preparedData);
                String abc=htmlEncode(postText);
                Log.d("preparedData",abc);

                String s=Utils.encodeMessage(postText);
                String postDecimalDesc= s.replaceAll("\\\\u(....)", "&#x$1;");*/

                multiPartEntityBuilder.addTextBody("extension", extension.trim());
                //multiPartEntityBuilder.addTextBody("description",postText);
                multiPartEntityBuilder.addTextBody("description", Utils.encodeMessage(postText));
                //multiPartEntityBuilder.addTextBody("description", htmlEncode(postText));
                //multiPartEntityBuilder.addTextBody("description", URLEncoder.encode(postText,"UTF-8"));

                httpPostRequest.setEntity(multiPartEntityBuilder.build());
                Log.e("response", multiPartEntityBuilder.toString());
                HttpResponse httpResponse = null;
                httpResponse = httpClient.execute(httpPostRequest);
                HttpEntity httpEntity = httpResponse.getEntity();
                String jsonStr = EntityUtils.toString(httpEntity);
                Log.e("response", jsonStr);
                if (jsonStr != null)
                {

                    try {

                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String status = jsonObj.getString("status");
                        if (status.equals("error"))
                        {
                            message = jsonObj.getString("message");// message
                            Log.e("message", message);
                        }
                        else
                            {
                            message = "Post Successfully";
                        }
                    } catch (Exception e) {
                        Log.e("Error", e.toString());
                    }

                } else {
                    Log.e("ServiceHandler", "Couldn't get any data from the url");
                }
            } catch (Exception e) {
                Log.e("Error", e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            page = 0;
            visibleItemCount = 0;
            totalItemCount = 0;
            pastVisiblesItems = 0;
            filePath = null;
            extension = "";
            postText = "";
            layImage.setVisibility(View.GONE);
            loaderDiloag.dismissDiloag();

            homeBeanList.clear();
            homeBeanList_adv.clear();
            final_homeBeanList.clear();
            getAllPost();
            Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();
            super.onPostExecute(aVoid);
            //  }
        }
    }


    private class GetAllPost extends AsyncTask<Void,Void,String>{

        LoaderDiloag  mLoader=new LoaderDiloag(getActivity());
        //String url="";
        @Override
        protected void onPreExecute() {
            dialog.dismiss();
            //mLoader.displayDiloag();
            //url = WebApis.allPOST + page;
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            page++;
            // These two need to be declared outside the try/catch
            // so that they can be closed in the finally block.
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            // Will contain the raw JSON response as a string.
            String forecastJsonStr = null;

            try {

                URL url = new URL(WebApis.allPOST + page);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setRequestProperty("authorization", Constant.token);
                urlConnection.setRequestProperty("cache-control", "no-cache");
                urlConnection.connect();
                int lengthOfFile = urlConnection.getContentLength();
                // Read the input stream into a String
                InputStream inputStream = urlConnection.getErrorStream();
                //InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    return null;
                }
                forecastJsonStr = buffer.toString();
                Log.d("Json1", forecastJsonStr);

                JSONArray jsonArray = new JSONArray(forecastJsonStr);
                /*for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    ContactModel contactModel = new ContactModel();
                    contactModel.setID(jsonObject.getString("id"));
                    contactModel.setName(jsonObject.getString("name"));
                    contactModel.setPhone(jsonObject.getString("mobile"));
                    contactModel.setImageURL(jsonObject.getString("profile_imge"));
                    contactModels.add(contactModel);
                }*/

                return forecastJsonStr;
            } catch (IOException e) {
                Log.e("PlaceholderFragment", "Error ", e);
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e("PlaceholderFragment", "Error closing stream", e);
                    }
                }
            }
            return forecastJsonStr;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            Log.d("aVoid",aVoid);
            super.onPostExecute(aVoid);
        }
    }

    private void GetAllPost(final Context context, final String auth)
    {
        //loaderDiloag.displayDiloag();
        StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.GET, WebApis.allPOST + page, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                loaderDiloag.dismissDiloag();
                Log.d(TAG, "GetAllPost " + response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                loaderDiloag.dismissDiloag();
                Log.d(TAG, "Error " + error.toString());
                //Toast.makeText(context, "Please refresh the page", Toast.LENGTH_LONG).show();
            }
        }
        ) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("authorization",auth);
                params.put("cache-control", "no-cache");
                return params;
            }
        };

        Volley.newRequestQueue(context).add(stringRequest);
    }

    public Uri getOutputMediaFileUri(int type)
    {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"name");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + "name" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

   /* private String encodeMessage(String message) {
        message = message.replaceAll("&", ":and:");
        message = message.replaceAll("\\+", ":plus:");
        return StringEscapeUtils.escapeJava(message);
    }

    private String decodeMessage(String message) {
        message = message.replaceAll(":and:", "&");
        message = message.replaceAll(":plus:", "+");
        return StringEscapeUtils.unescapeJava(message);
    }*/

   private String replaceMsg(String message){
       String newMsg=message.replaceAll("%", "0x");
       return newMsg;
   }

    /**
     * Takes UTF-8 strings and encodes non-ASCII as
     * ampersand-octothorpe-digits-semicolon
     * HTML-encoded characters
     *
     * @param string
     * @return HTML-encoded String
     * https://objectpartners.com/2013/04/24/html-encoding-utf-8-characters/
     */
    private String htmlEncode(final String string) {
        final StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < string.length(); i++) {
            final Character character = string.charAt(i);
            if (CharUtils.isAscii(character)) {
                // Encode common HTML equivalent characters
                stringBuffer.append(
                        StringEscapeUtils.escapeHtml4(character.toString()));
            } else {
                // Why isn't this done in escapeHtml4()?
                stringBuffer.append(
                        String.format("&#x%x;",
                                Character.codePointAt(string, i)));
            }
        }
        return stringBuffer.toString();
    }

    private String htmlEncodeNew(final String s){
        StringBuilder sb = new StringBuilder();
        int n = s.length();
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            if (Character.UnicodeBlock.of(c) != Character.UnicodeBlock.BASIC_LATIN) {
                sb.append("&#");
                sb.append((int)c);
                sb.append(';');
            } else {
                sb.append(c);
            }

        }
        return sb.toString();
    }



    public void PatternMatcherGroupHtml() {


            String stringToSearch = "<p>Yada yada yada <code>StringBuffer</code> yada yada ...</p>";

            // the pattern we want to search for
            Pattern p = Pattern.compile("<code>(\\S+)</code>");
        Matcher m = p.matcher(stringToSearch);



            // if we find a match, get the group
            if (m.find()) {

                // get the matching group
                String codeGroup = m.group(1);

                // print the group
                System.out.format("'%s'\n", codeGroup);

            }


    }


    public static boolean isHtml(String s) {
        boolean ret=false;
        if (s != null) {
            ret=htmlPattern.matcher(s).find();
        }
        return ret;

    }

    public static void findIndexes(){
        String searchableString = "don’t be evil.being evil is bad";
        String keyword = "be";

        int index = searchableString.indexOf(keyword);
        while (index >=0){
            System.out.println("Index : "+index);
            index = searchableString.indexOf(keyword, index+keyword.length());
        }

    }

    private void downloadImage(String url, final String path, String name){

        AndroidNetworking.download(url,path,name).build().startDownload(new DownloadListener() {
            @Override
            public void onDownloadComplete() {


            }

            @Override
            public void onError(ANError anError) {

            }
        });
    }

    private class DownloadImageTask extends AsyncTask<String,Void,String>{
        // Before the tasks execution

        protected void onPreExecute(){
            // Display the progress dialog on async task start
            //mProgressDialog.show();
        }

        // Do the task in background/non UI thread
        protected String doInBackground(String...params){
            //URL url = params[0];
           /* URI uri = null;
            URL url = null;
            String uriString = params[0];
            try {
                uri = new URI(uriString);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            // Convert the absolute URI to a URL object
            try {
                url = uri.toURL();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }*/

                URL url= stringToURL(params[0]);
            String UniqueFileName=params[1];

            //HttpURLConnection connection = null;

            try{

                HttpGet httpRequest = new HttpGet(URI.create(params[0]) );
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse response = (HttpResponse) httpclient.execute(httpRequest);
                HttpEntity entity = response.getEntity();
                BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(entity);
               Bitmap bmp = BitmapFactory.decodeStream(bufHttpEntity.getContent());
                httpRequest.abort();
/*
                // Initialize a new http url connection
                connection = (HttpURLConnection) url.openConnection();

                // Connect the http url connection
                connection.connect();

                // Get the input stream from http url connection
                InputStream inputStream = connection.getInputStream();


                // Initialize a new BufferedInputStream from InputStream
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);


                // Convert BufferedInputStream to Bitmap object
                Bitmap bmp = BitmapFactory.decodeStream(bufferedInputStream);*/

                // Return the downloaded bitmap
                //return bmp;
                Uri imageInternalUri;
                if (bmp!=null) {
                     imageInternalUri = saveImageToInternalStorage(bmp, UniqueFileName);
                    return imageInternalUri.toString();
                }else
                return "null";

            }catch(IOException e){
                e.printStackTrace();
            }finally{
                // Disconnect the http url connection
               // connection.disconnect();
            }
            return null;
        }

        // When all async task done
        protected void onPostExecute(Bitmap result){
            // Hide the progress dialog
            //mProgressDialog.dismiss();

           /* if(result!=null){
                // Display the downloaded image into ImageView
                //mImageView.setImageBitmap(result);

                // Save bitmap to internal storage
                //Uri imageInternalUri = saveImageToInternalStorage(result);
                // Set the ImageView image from internal storage
                //mImageViewInternal.setImageURI(imageInternalUri);
                //return imageInternalUri.toString();
            }else {
                // Notify user that an error occurred while downloading image
                //Snackbar.make(mCLayout,"Error",Snackbar.LENGTH_LONG).show();
              //  Toast.makeText(getActivity(),"error",Toast.LENGTH_SHORT).show();
            }*/
        }
    }

    // Custom method to convert string to url
    protected URL stringToURL(String urlString){
        try{
            URL url = new URL(urlString);
            return url;
        }catch(MalformedURLException e){
            e.printStackTrace();
        }
        return null;
    }

    // Custom method to save a bitmap into internal storage
    protected Uri saveImageToInternalStorage(Bitmap bitmap,String UniqueFileName){
        // Initialize ContextWrapper
        ContextWrapper wrapper = new ContextWrapper(getActivity());

        // Initializing a new file
        // The bellow line return a directory in internal storage
        File file = wrapper.getDir("Images",MODE_PRIVATE);

        // Create a file to save the image
        file = new File(file, UniqueFileName+".jpg");

        try{
            // Initialize a new OutputStream
            OutputStream stream = null;

            // If the output file exists, it can be replaced or appended to it
            stream = new FileOutputStream(file);

            // Compress the bitmap
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);

            // Flushes the stream
            stream.flush();

            // Closes the stream
            stream.close();

        }catch (IOException e) // Catch the exception
        {
            e.printStackTrace();
        }

        // Parse the gallery image url to uri
        Uri savedImageURI = Uri.parse(file.getAbsolutePath());

        // Return the saved image Uri
        return savedImageURI;
    }



    class CargarCandidatos extends AsyncTask<Void,Void, String> {
         ProgressDialog pDialog;
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           // loaderDiloag.displayDiloag();
           pDialog= new ProgressDialog(getActivity());
            pDialog.setMessage("Loading ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting All products from url
         * */
        protected String doInBackground(Void... args) {
            homeAdapter.notifyItemRangeChanged(0,recyclerView.getChildCount());

       return "hdjs" ;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String d) {
            pDialog.dismiss();
           // loaderDiloag.dismissDiloag();
           /* runOnUiThread(new Runnable() {
                public void run() {

                }
            });*/

        }

    }
}





