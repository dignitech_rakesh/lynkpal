package com.app.lynkpal.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.AddTrainingActivity;
import com.app.lynkpal.R;


public class JobSilverFrag extends Fragment {

    private TextView tv_one,tv_two,tv_three,tv_four,tv_five,tv_six,tv_seven,tvBtn;
    ImageView ivMostPopular;


    public JobSilverFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       // return inflater.inflate(R.layout.fragment_job_gold, container, false);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_job_gold, container, false);
        tv_one = view.findViewById(R.id.headingOne);
        tv_two = view.findViewById(R.id.headingTwo);
        tv_three = view.findViewById(R.id.headingThree);
        tv_four = view.findViewById(R.id.headingFour);
        tv_five = view.findViewById(R.id.headingFive);
        tv_six= view.findViewById(R.id.headingSix);
        tv_seven = view.findViewById(R.id.headingSeven);
        tvBtn = view.findViewById(R.id.tvBtn);

        ivMostPopular = view.findViewById(R.id.ivMostPopular);
        ivMostPopular.setVisibility(View.VISIBLE);


        tv_one.setText(Html.fromHtml("<b>" + "# 18,900 or 189.00 ¢" + "</b> " ));
        tv_two.setText(Html.fromHtml("<b>" + "Professionally written - " + "</b> " + "A top-standard CV that showcases your strengths & achievements"));
        tv_three.setText(Html.fromHtml("<b>" + "Fine-tuned to suit your industry - " + "</b> " + "Your CV will be well-tailored to suit the specific industry you are in, or aspire to be in."));
        tv_four.setText(Html.fromHtml("<b>" + "Cover Letter - " + "</b> " + "Employers are more likely to read a CV with a cover letter. We will help you with a perfectly tailored one"));
        tv_five.setText(Html.fromHtml("<b>" + "Turnaround Time : " + "</b> " + "5 – 7"));
        tv_six.setText(Html.fromHtml("<b>" + "Working Days -" + "</b> " + "The first draft of your CV will be sent to you within 5 – 7 working days"));
        tv_seven.setText(Html.fromHtml("<b>" + "Up to 2 Revisions on your order - " + "</b> " + "You can give us feedback on your first draft, and we’ll make the needed adjustments. Up to 2 revisions."));

        tvBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(getActivity(),"Silver",Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(getActivity(), AddTrainingActivity.class);
                intent.putExtra("key","silver");
                getActivity().startActivity(intent);
                getActivity().finish();

            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

}
