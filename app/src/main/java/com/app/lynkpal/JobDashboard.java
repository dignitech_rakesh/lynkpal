package com.app.lynkpal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.lynkpal.Adapter.JobAppliedListAdapter;
import com.app.lynkpal.Adapter.JobCreatedListAdapter;
import com.app.lynkpal.Adapter.JobInterestedListAdapter;
import com.app.lynkpal.Bean.JobAppliedBean;
import com.app.lynkpal.Bean.JobCreatedListBean;
import com.app.lynkpal.Bean.JobInterestedBean;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.ExpandedHightListview;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.ApplyJob;
import com.app.lynkpal.Interface.DeleteAppliedJob;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

//import static com.google.api.client.googleapis.testing.auth.oauth2.MockGoogleCredential.ACCESS_TOKEN;

public class JobDashboard extends AppCompatActivity implements ApplyJob, DeleteAppliedJob {
    private static final String TAG = JobDashboard.class.getSimpleName();
    TextView txtCreated, txtApplied;
    TextView head, txtJobInter;
    FloatingActionButton floatAdd;
    LinearLayout linCreated, linApplied;
    List<JobCreatedListBean> jobCreatedListBeen = new ArrayList<JobCreatedListBean>();
    JobCreatedListBean createBean;
    JobInterestedBean interestBean;
    JobAppliedBean jobAppliedBean;
    JobCreatedListAdapter jobCreatedListAdapter;
    List<JobInterestedBean> jobInterestedBeen = new ArrayList<JobInterestedBean>();
    JobInterestedListAdapter jobInterestedListAdapter;
    List<JobAppliedBean> jobAppliedBeen = new ArrayList<JobAppliedBean>();
    JobAppliedListAdapter jobAppliedListAdapter;
    ExpandedHightListview list_Applied, list_Created, list_Interested;
    ImageView ic_back;
    TextView tv_upgradeToPremium;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_dashboard);
        sharedPreferences = JobDashboard.this.getSharedPreferences(Constant.PREFS_NAME, 0);

        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        ic_back = (ImageView) findViewById(R.id.ic_back);
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        head = (TextView) findViewById(R.id.head);
        tv_upgradeToPremium = (TextView) findViewById(R.id.upgradeToPremium);
        txtJobInter = (TextView) findViewById(R.id.txtJobInter);
        txtCreated = (TextView) findViewById(R.id.txtCreated);
        txtApplied = (TextView) findViewById(R.id.txtApplied);
        linCreated = (LinearLayout) findViewById(R.id.linCreated);
        linApplied = (LinearLayout) findViewById(R.id.linApplied);
        list_Created = (ExpandedHightListview) findViewById(R.id.created);
        list_Interested = (ExpandedHightListview) findViewById(R.id.interested);
        list_Applied = (ExpandedHightListview) findViewById(R.id.applied);

        list_Applied.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //   startActivity(new Intent(JobDashboard.this, JobDetail.class));
            }
        });

        floatAdd = (FloatingActionButton) findViewById(R.id.floatAdd);
        txtApplied.setTypeface(tf_bold);
        txtCreated.setTypeface(tf_bold);
        head.setTypeface(tf_bold);
        txtJobInter.setTypeface(tf_bold);


        Log.d("premium",sharedPreferences.getString("premiumMember", ""));
        if (sharedPreferences.getString("premiumMember", "").equalsIgnoreCase("yes")) {
            tv_upgradeToPremium.setVisibility(View.GONE);
        } else{
            tv_upgradeToPremium.setVisibility(View.VISIBLE);

        }

        list_Interested.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                String job_id = jobInterestedBeen.get(i).getId();
               Log.d(TAG,"clicked " + job_id);
              startActivity(new Intent(JobDashboard.this,JobDetail.class).putExtra("job_id",job_id));
            }
        });

        floatAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(JobDashboard.this, PostAJob.class));
            }
        });
        txtCreated.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view)
            {
                Log.d(TAG, "onClick: Created");
                txtCreated.setBackground(getResources().getDrawable(R.drawable.left_dashboard));
                txtApplied.setBackground(null);
                floatAdd.setVisibility(View.VISIBLE);
                txtCreated.setTextColor(Color.WHITE);
                txtApplied.setTextColor(Color.parseColor("#375cc8"));
                linCreated.setVisibility(View.VISIBLE);
                linApplied.setVisibility(View.GONE);
            }
        });

        txtApplied.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view)
            {
                Log.d(TAG, "onClick: Applied");
                txtApplied.setBackground(getResources().getDrawable(R.drawable.right_dashboard));
                txtCreated.setBackground(null);
                floatAdd.setVisibility(View.GONE);
                txtApplied.setTextColor(Color.WHITE);
                txtCreated.setTextColor(Color.parseColor("#375cc8"));
                linCreated.setVisibility(View.GONE);
                linApplied.setVisibility(View.VISIBLE);
                getApplied();
            }
        });
       /* new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {*/
        getChargeDetails();
        getInterested();

          /*  }
        }, 10);*/



          tv_upgradeToPremium.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  Intent intent=new Intent(JobDashboard.this,JobUpgardePremium.class);
                  startActivity(intent);
                  finish();
              }
          });

    }

    public void getChargeDetails() {

        if (ApplicationGlobles.isConnectingToInternet(JobDashboard.this)) {
            final LoaderDiloag loaderDiloag = new LoaderDiloag(JobDashboard.this);
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //  Log.e("Constant.token", Constant.token);
            //  Log.e("WebApis.ALLMESSAGE", WebApis.ALLMESSAGE + id);
            Request request = new Request.Builder()
                    .url(WebApis.GETPLANS)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (JobDashboard.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response", jsonData + "");
                        if (jsonData.length() > 0) {
                            try {
                                JSONArray array = new JSONArray(jsonData);
                                if (array.length() > 0) {
                                   /* for (int i = 0; i < array.length(); i++) {*/
                                    JSONObject jsonObject1 = array.getJSONObject(0);
                                    final String amount = jsonObject1.getString("amount");
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            loaderDiloag.dismissDiloag();
                                            getCreated(amount);
                                            //  txtHotJob.setText("Mark as hot job(Charged " + amount + "$)");
                                        }
                                    });
                                    //   String id=jsonObject1.getString("id");
                                    // }

                                }

                            } catch (Exception je) {
                                je.printStackTrace();
                            }
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //swipeRefreshLayout.setRefreshing(false);
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                // loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(JobDashboard.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void getCreated(final String amount) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(JobDashboard.this);
        if (ApplicationGlobles.isConnectingToInternet(JobDashboard.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            jobCreatedListBeen.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.JOBCREATED", WebApis.JOBCREATED);
            Request request = new Request.Builder()
                    .url(WebApis.JOBCREATED)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (JobDashboard.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getDetails", jsonData + "");
                        loaderDiloag.dismissDiloag();
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {

                                JSONObject jsonObject = array.getJSONObject(a);
                                createBean = new JobCreatedListBean();
                                createBean.setId(jsonObject.getString("id"));
                                createBean.setName(jsonObject.getString("title"));
                                createBean.setExperience(jsonObject.getString("experience"));
                                createBean.setEmployment_type(jsonObject.getString("employment_type"));
                                createBean.setDescription(jsonObject.getString("description"));
                                createBean.setLocation(jsonObject.getString("location"));
                                createBean.setHot(jsonObject.getString("hot"));
                                Log.e("hot", jsonObject.getString("hot"));
                                createBean.setFrom("From:" + parseDateToddMMyyyy(jsonObject.getString("added")));
                                createBean.setExtra(jsonObject.getString("applications_count") + " Applications");
                                createBean.setImage(jsonObject.getString("image"));
                                createBean.setPaid(jsonObject.getString("paid"));
                                jobCreatedListBeen.add(createBean);
                            }


                        } catch (Exception e) {

                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                                if (jobCreatedListBeen.size() > 0) {
                                    list_Created.setExpanded(true);
                                    jobCreatedListAdapter = new JobCreatedListAdapter(JobDashboard.this, jobCreatedListBeen, amount,JobDashboard.this);
                                    list_Created.setAdapter(jobCreatedListAdapter);
                                }
                            }
                        });


                    }
                });

            } catch (Exception e) {
                JobDashboard.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(JobDashboard.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        String outputPattern = "MMM dd,yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public void getInterested() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(JobDashboard.this);
        if (ApplicationGlobles.isConnectingToInternet(JobDashboard.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            jobInterestedBeen.clear();
            Log.e("Constant.token", Constant.token);
            Log.d("WebApis.JOBINTERESTED", WebApis.JOBINTERESTED);
            Request request = new Request.Builder()
                    .url(WebApis.JOBINTERESTED)
                    .get()
                    .addHeader("Authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (JobDashboard.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.d(TAG,"IntrestedData " + jsonData);

                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {

                                JSONObject jsonObject = array.getJSONObject(a);
                                interestBean = new JobInterestedBean();
                                interestBean.setId(jsonObject.getString("id"));
                                interestBean.setName(jsonObject.getString("title"));
                                interestBean.setLocation(jsonObject.getString("location"));
                                interestBean.setFrom("From:" + parseDateToddMMyyyy(jsonObject.getString("added")));
                                interestBean.setExtra("Apply");
                                interestBean.setImage(jsonObject.getString("image"));
                                interestBean.setHot_interested(jsonObject.getString("hot"));
                                jobInterestedBeen.add(interestBean);
                            }


                        } catch (Exception e) {

                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                                if (jobInterestedBeen.size() > 0) {
                                    list_Interested.setExpanded(true);
                                    jobInterestedListAdapter = new JobInterestedListAdapter(JobDashboard.this, jobInterestedBeen, JobDashboard.this);
                                    list_Interested.setAdapter(jobInterestedListAdapter);
                                }
                            }
                        });


                    }
                });

            } catch (Exception e) {
                JobDashboard.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(JobDashboard.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void getApplied() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(JobDashboard.this);
        if (ApplicationGlobles.isConnectingToInternet(JobDashboard.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            jobAppliedBeen.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.JOBAPPLIED", WebApis.JOBAPPLIED);
            Request request = new Request.Builder()
                    .url(WebApis.JOBAPPLIED)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (JobDashboard.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getDetails", jsonData + "");

                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {

                                JSONObject jsonObject = array.getJSONObject(a);
                                jobAppliedBean = new JobAppliedBean();
                                jobAppliedBean.setId(jsonObject.getString("id"));
                                jobAppliedBean.setName(jsonObject.getString("title"));
                                jobAppliedBean.setLocation(jsonObject.getString("location"));
                                jobAppliedBean.setFrom("From:" + parseDateToddMMyyyy(jsonObject.getString("added")));
                                jobAppliedBean.setImage(jsonObject.getString("image"));
                                jobAppliedBean.setExtra("Delete Application");
                                jobAppliedBean.setHotApplied(jsonObject.getString("hot"));
                                jobAppliedBeen.add(jobAppliedBean);
                            }


                        } catch (Exception e) {

                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                                if (jobAppliedBeen.size() > 0) {
                                    list_Applied.setExpanded(true);
                                    jobAppliedListAdapter = new JobAppliedListAdapter(JobDashboard.this, jobAppliedBeen, JobDashboard.this);
                                    list_Applied.setAdapter(jobAppliedListAdapter);
                                }
                            }
                        });


                    }
                });

            } catch (Exception e) {
                JobDashboard.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(JobDashboard.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }

        return;
    }

    @Override
    protected void onResume() {
        super.onResume();

        getInterested();
        getApplied();
        if (Constant.update_created) {
            Constant.update_created = false;
            getChargeDetails();
        }
    }

    @Override
    public void Apply(String id, int pos) {
        setApply(id, pos);
    }

    @Override
    public void Delete(String id, int pos) {
        deleteJobCreated(id,pos);
    }

    public void setApply(String id, final int pos) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(JobDashboard.this);
        if (ApplicationGlobles.isConnectingToInternet(JobDashboard.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.JOBINTERESTED", WebApis.APPLYJOB + id);
            Request request = new Request.Builder()
                    .url(WebApis.APPLYJOB + id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (JobDashboard.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("Apply", jsonData + "");

                        try {
                            final JSONObject jsonObject = new JSONObject(jsonData);
                            final String status = jsonObject.getString("status");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (status.equals("success")) {
                                        jobInterestedBeen.remove(pos);
                                        jobInterestedListAdapter.notifyDataSetChanged();
                                        getApplied();
                                        try {
                                            Toast.makeText(JobDashboard.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });


                        } catch (Exception e) {

                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                            }
                        });


                    }
                });

            } catch (Exception e) {
                JobDashboard.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(JobDashboard.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }


    public void deleteJobCreated(final String id, final int pos) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(JobDashboard.this);
        if (ApplicationGlobles.isConnectingToInternet(JobDashboard.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            JSONObject jsonObject = new JSONObject();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, jsonObject + "");
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.JOBDELETE", WebApis.JOBDELETE + id + "/");
            Request request = new Request.Builder()
                    .url(WebApis.JOBDELETE + id + "/")
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (JobDashboard.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.d("DeleteJobDetails", jsonData + "");

                        try {
                            if (jsonData.length() > 0) {
                                try {
                                    final JSONObject jsonObject = new JSONObject(jsonData);
                                    String status = jsonObject.getString("status");
                                    if (status.equals("success")) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                //finish();

                                                jobCreatedListBeen.remove(pos);
                                                jobCreatedListAdapter.notifyDataSetChanged();
                                                    //getChargeDetails();

                                                try {
                                                    Toast.makeText(JobDashboard.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                                } catch (Exception e) {
                                                    Toast.makeText(JobDashboard.this, "Something went wrong try later...", Toast.LENGTH_SHORT).show();
                                                }
                                                //Constant.update_created = true;
                                                //finish();
                                            }
                                        });
                                    } else {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(JobDashboard.this, "Something went wrong try later...", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                    Log.e("json", jsonObject.toString());
                                } catch (Exception je) {
                                    loaderDiloag.dismissDiloag();
                                    je.printStackTrace();
                                }
                                loaderDiloag.dismissDiloag();
                            } else {
                                loaderDiloag.dismissDiloag();
                            }


                        } catch (Exception e) {

                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();

                            }
                        });


                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(JobDashboard.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }

        return;
    }




    @Override
    public void deleteApply(String id, int pos) {
        showPopup(id, pos);
    }

    public void showPopup(final String id, final int pos) {
        LayoutInflater layoutInflater
                = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_group_join_leave, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        final TextView textText = (TextView) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        textTitle.setText("Delete Application");
        textText.setText("Are you sure, you want to delete your Application from this Job?");
        // textText.setText("Are you sure, you want to leave " + '"' + text + '"' + " Group?");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteApplied(id, pos);
                popupWindow.dismiss();

            }
        });
    }

    public void deleteApplied(String id, final int pos) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(JobDashboard.this);
        if (ApplicationGlobles.isConnectingToInternet(JobDashboard.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.JOBINTERESTED", WebApis.DELETEAPPLICATION + id);
            Request request = new Request.Builder()
                    .url(WebApis.DELETEAPPLICATION + id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (JobDashboard.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("Apply", jsonData + "");

                        try {
                            final JSONObject jsonObject = new JSONObject(jsonData);
                            final String status = jsonObject.getString("status");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (status.equals("success")) {
                                        jobAppliedBeen.remove(pos);
                                        jobAppliedListAdapter.notifyDataSetChanged();
                                        getInterested();
                                        try {
                                            Toast.makeText(JobDashboard.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });


                        } catch (Exception e) {

                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                            }
                        });


                    }
                });

            } catch (Exception e) {
                JobDashboard.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(JobDashboard.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }




}
