package com.app.lynkpal;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
//import com.facebook.accountkit.AccessToken;
//import com.facebook.accountkit.Account;
//import com.facebook.accountkit.AccountKit;
//import com.facebook.accountkit.AccountKitCallback;
//import com.facebook.accountkit.AccountKitError;
//import com.facebook.accountkit.AccountKitLoginResult;
//import com.facebook.accountkit.PhoneNumber;
//import com.facebook.accountkit.ui.AccountKitActivity;
//import com.facebook.accountkit.ui.AccountKitConfiguration;
//import com.facebook.accountkit.ui.LoginType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class RegisterActivity extends AppCompatActivity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    EditText edtFname, edtLname, edtEmail, edtPass, edtCpass, edit_Company;
    Button btnSignUp;
    TextView txtLogin;
    String user_id, user_prof, user_name, user_email, token, message;
    SharedPreferences settings;
    public static int APP_REQUEST_CODE = 99;
    CheckBox mCheck;
    LoaderDiloag loaderDiloag;

    String status="",resend_otp="";
    public int userId;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        changeStatusBarColor();
        setContentView(R.layout.activity_register);
        loaderDiloag = new LoaderDiloag(this);
        settings = (this).getSharedPreferences(Constant.PREFS_NAME, 0);
        edtFname = (EditText) findViewById(R.id.edit_fname);
        edtLname = (EditText) findViewById(R.id.edit_lname);
        edtEmail = (EditText) findViewById(R.id.edit_email);
        edtPass = (EditText) findViewById(R.id.edit_pass);
        edtCpass = (EditText) findViewById(R.id.edit_con_pass);
        edit_Company = (EditText) findViewById(R.id.edit_Company);
        btnSignUp = (Button) findViewById(R.id.btnSignUp);
        txtLogin = (TextView) findViewById(R.id.txtAlrLogin);
        mCheck = findViewById(R.id.agree_check);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        edtFname.setTypeface(tf_reg);
        edtLname.setTypeface(tf_reg);
        edtEmail.setTypeface(tf_reg);
        edtPass.setTypeface(tf_reg);
        edtCpass.setTypeface(tf_reg);
        edit_Company.setTypeface(tf_reg);
        btnSignUp.setTypeface(tf_bold);
        txtLogin.setTypeface(tf_med);
        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
            /*   Intent intent=new Intent(RegisterActivity.this,OtpActivity.class);
               startActivity(intent);*/

                if (edtFname.getText().toString().trim().isEmpty())
                {
                    edtFname.setError("Required");
                }

                if (edtLname.getText().toString().trim().isEmpty())
                {
                    edtLname.setError("Required");
                }

                if (!Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText().toString()).matches())
                {
                    edtEmail.setError("Please enter valid email");
                }
                if (Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText().toString()).matches())
                {
                    if (edtPass.getText().toString().trim().isEmpty())
                    {
                        edtPass.setError("Enter password");
                    }

                    if (edtCpass.getText().toString().trim().isEmpty())
                    {
                        edtCpass.setError("Enter Confirm Password");
                    }

                }

                if (!edtCpass.getText().toString().isEmpty())
                {
                    if (edtCpass.getText().toString().contains(edtPass.getText().toString()))
                    {

                        if (!mCheck.isChecked())
                        {
                            Toast.makeText(RegisterActivity.this,"Please Check the Agreement",Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            if (LoginActivity.isNetworkConnected(RegisterActivity.this))
                            {
                               // loaderDiloag.displayDiloag();
                                if (Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText().toString()).matches())
                                {
                                    //checkRegistrationUrl(RegisterActivity.this,view);
                                    callRegistration(RegisterActivity.this);

                                }else{
                                    loaderDiloag.dismissDiloag();

                                    Toast.makeText(RegisterActivity.this,"Please enter valid email",Toast.LENGTH_LONG).show();

                                }

                            }
                            else
                            {
                                Toast.makeText(RegisterActivity.this,"No Internet Connection",Toast.LENGTH_LONG).show();
                                return;
                            }


                            //Toast.makeText(RegisterActivity.this,"Fine",Toast.LENGTH_LONG).show();
                        }

                    }
                    else
                    {
                        Toast.makeText(RegisterActivity.this,"Password does not match",Toast.LENGTH_LONG).show();
                    }
                }




            }
        });
    }


    private void checkRegistrationUrl(final Context context, final View view)
    {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("email", edtEmail.getText().toString());


            Log.d(TAG,"jsonLogin " + jsonBody.toString());
            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.POST,WebApis.CHECK_REGISTER_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    loaderDiloag.dismissDiloag();

                    Log.i("VOLLEYLogin", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.contains("success"))
                        {
                            new MaterialDialog.Builder(context)
                                    .title("Registration")
                                    .content(message)
                                    .iconRes(R.drawable.ic_check_circle)
                                    .positiveText("Ok")
                                    .onPositive(new MaterialDialog.SingleButtonCallback()
                                    {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                        {

                                            dialog.dismiss();
                                        }
                                    }).show();

                        }
                        else
                            {

                               emailLogin(view);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    loaderDiloag.dismissDiloag();
                    Log.e("VOLLEYOut", error.toString());
                }
            })

            {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    HashMap<String,String> param = new HashMap<String, String>();
                    param.put("Content-Type","application/json");
                    return param;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }


            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }








    public void emailLogin(final View view) {
      /* //commented for facebook
      final Intent intent = new Intent(RegisterActivity.this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.EMAIL,
                        AccountKitActivity.ResponseType.CODE); // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, APP_REQUEST_CODE);*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == APP_REQUEST_CODE)
        {/* //commented for facebook

        // confirm that this response matches your request
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            Log.d(TAG,"Result " + loginResult.toString());
            String toastMessage;
            if (loginResult.getError() != null)
            {
                Log.d(TAG,"Error");
            }
            else if (loginResult.wasCancelled())
            {
                toastMessage = "Login Cancelled";
                Log.d(TAG,"Cancelled");
            }
           else if (loginResult!= null)
            {
                // toastMessage = "Success:" + loginResult.getAccessToken().getAccountId();
                Toast.makeText(RegisterActivity.this,"Successfully Verified",Toast.LENGTH_LONG).show();
                Log.d(TAG,"Verified");
                callRegistration(RegisterActivity.this);
            }
            else if (loginResult.getAccessToken()==null)
            {
                Log.d(TAG,"Null Value");
            }

*/

            }





    }

    public void callRegistration(final Context context)
    {

       // final LoaderDiloag loaderDiloag = new LoaderDiloag(RegisterActivity.this);
        final ProgressDialog progressDialog = new ProgressDialog(RegisterActivity.this);
        progressDialog.setTitle("SignUp");
        progressDialog.setMessage("Processing...");
        progressDialog.show();

        final MaterialDialog.Builder dialog = new MaterialDialog.Builder(RegisterActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(RegisterActivity.this))
        {


            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, "{\r\n\"username\":\"" + edtEmail.getText().toString() + "\",\r\n\"firstname\" : \"" + edtFname.getText().toString() + "\",\r\n\"lastname\" : \"" + edtLname.getText().toString() + "\",\r\n\"email\": \"" + edtEmail.getText().toString() + "\",\r\n\"confirmEmail\" : \"" + edtEmail.getText().toString() /*+ "\",\r\n\"companyName\": \""+edit_Company.getText().toString()*/+"\",\r\n\"password\": \"" + edtPass.getText().toString() + "\"\r\n}");
            Request request = new Request.Builder()
                    .url(WebApis.registerURL)
                    //.url("http://staging.lynkpal.com/api_registration/")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("postman-token", "a95f208b-cf91-ad21-e6de-2e13a887d93e")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {

                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (RegisterActivity.this == null)
                            return;
                        progressDialog.dismiss();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException
                    {

                        JSONObject jsonObject;
                        progressDialog.dismiss();
                        loaderDiloag.dismissDiloag();

                        String jsonData = response.body().string();

                        // if user is already Registered and again try to register
                      if(jsonData.contains("username")){
                          runOnUiThread(new Runnable() {
                              @Override
                              public void run() {
                                  dialog.title("Alert");
                                  dialog.content("A username with this Username already exist");
                                  dialog.iconRes(R.drawable.ic_cancel);
                                  dialog.positiveText("OK");
                                  //dialog.backgroundColor(getResources().getColor(R.color.transparent_color_bg));
                                  dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                                      @Override
                                      public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                      {
                                          dialog.dismiss();

                                      }
                                  });
                                  dialog.show();



                                /*  AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RegisterActivity.this);
// ...Irrelevant code for customizing the buttons and title
                                  LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                  View dialogView = inflater.inflate(R.layout.custom_alert_dialog, null);
                                  dialogBuilder.setView(dialogView);

                                  Button btn_ad = (Button) dialogView.findViewById(R.id.btn_ad);


                                  AlertDialog alertDialog = dialogBuilder.create();
                                  alertDialog.setCancelable(false);
                                 // AlertDialog alertDialog = new AlertDialog.Builder(RegisterActivity.this, R.style.MyDialogTheme);
                                  alertDialog.show();*/

                              }
                          });

                      }


                        Log.e("register response ", jsonData + "");
                        try {
                             jsonObject=new JSONObject(jsonData);
                            userId=jsonObject.getInt("user_id");
                            status=jsonObject.optString("status");
                            message = jsonObject.getString("message");
                            if (status.equalsIgnoreCase("error"))
                            resend_otp=jsonObject.optString("resend_otp");

                        }catch (Exception e){
                            e.printStackTrace();
                            progressDialog.dismiss();
                            loaderDiloag.dismissDiloag();
                        }

                        if(jsonData.contains("status")){

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    if (status.equalsIgnoreCase("success")) {
                                        Log.d(TAG, "status");
                                        dialog.title("Success");
                                        dialog.content(message);
                                        dialog.canceledOnTouchOutside(false);
                                        dialog.iconRes(R.drawable.ic_check_circle);
                                        dialog.positiveText("OK");
                                        dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                finish();
                                                //startActivity(new Intent(context,LoginActivity.class));

                                                Intent intent = new Intent(RegisterActivity.this, OtpActivity.class);
                                                intent.putExtra("userId", userId);
                                                intent.putExtra("type", "RegisterActivity");
                                                startActivity(intent);

                                            }
                                        })
                                        ;
                                        dialog.show();

                                    }else if(status.equalsIgnoreCase("error")&&resend_otp.equalsIgnoreCase("1")) {

                                        resendOtpUrl(RegisterActivity.this);
                                   /*     Log.d(TAG, "status");
                                        dialog.title("Success");
                                        dialog.content(message);
                                        dialog.canceledOnTouchOutside(false);
                                        dialog.iconRes(R.drawable.ic_check_circle);
                                        dialog.positiveText("OK");
                                        dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                finish();
                                                //startActivity(new Intent(context,LoginActivity.class));

                                                Intent intent = new Intent(RegisterActivity.this, OtpActivity.class);
                                                intent.putExtra("userId", userId);
                                                intent.putExtra("type", "RegisterActivity");
                                                //intent.putExtra("resend_otp", resend_otp);

                                                startActivity(intent);

                                            }
                                        })
                                        ;
                                        dialog.show();*/
                                    }else{
                                        Toast.makeText(RegisterActivity.this,message, Toast.LENGTH_SHORT).show();


                                    }
                                }




                                });
                        }



                        /*
                        progressDialog.dismiss();
                        String jsonData = response.body().string();

                        Log.e("register response ", jsonData + "");
                        try {
                            JSONObject jsonObject=new JSONObject(jsonData);
                            userId=jsonObject.getInt("user_id");

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                          if(jsonData.contains("status"))
                          {

                              runOnUiThread(new Runnable()
                              {
                                  @Override
                                  public void run()
                                  {

                                      Log.d(TAG,"status");
                                      dialog.title("Success");
                                      dialog.content(message);
                                    dialog.canceledOnTouchOutside(false);
                                      dialog.iconRes(R.drawable.ic_check_circle);
                                      dialog.positiveText("OK");
                                      dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                                          @Override
                                          public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                          {
                                              finish();
                                              //startActivity(new Intent(context,LoginActivity.class));

                                              Intent intent=new Intent(RegisterActivity.this,OtpActivity.class);
                                              intent.putExtra("userId",userId);
                                              intent.putExtra("type","RegisterActivity");
                                              startActivity(intent);

                                          }
                                      })
                                      ;
                                      dialog.show();

                                  }
                              });


                              try
                              {
                                  JSONObject jsonObject = new JSONObject(jsonData);


                                  String status = jsonObject.getString("status");

                                    if (status.equals("success"))
                                  {
                                      message = jsonObject.getString("message");

                                  }

                                else if (jsonData.contains("username"))
                                    {
                                        Log.d(TAG,"username");
                                        dialog.title("Fail");
                                        dialog.content("A username with this Username already exist");
                                        dialog.iconRes(R.drawable.ic_cancel);
                                        dialog.positiveText("OK");
                                        dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                            {
                                               dialog.dismiss();

                                            }
                                        })
                                        ;
                                        dialog.show();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(RegisterActivity.this, "A user with that username already exists.", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            }
                            catch (Exception je)
                              {

                                je.printStackTrace();
                            }

                        }
                        else if (jsonData.contains("username"))

                          {
                              progressDialog.dismiss();
                              Log.d(TAG,"username");
                              runOnUiThread(new Runnable() {
                                  @Override
                                  public void run() {
                                      dialog.title("Fail")
                                              .content("A username with this Username already exist")
                                              .positiveText("OK")
                                              .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                  @Override
                                                  public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                                  {
                                                      dialog.dismiss();
                                                      loaderDiloag.dismissDiloag();

                                                  }
                                              }).show();
                                  }
                              });



                          }

                          */
                    }
                });

            }
            catch (Exception e)
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        progressDialog.dismiss();
                        loaderDiloag.dismissDiloag();

                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        }
        else
            {
            Toast.makeText(RegisterActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    private void changeStatusBarColor()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }


    private void resendOtpUrl(final Context context)
    {
        String resendOtpUrl=WebApis.resendOtpUrl + userId+"/";
        final LoaderDiloag loaderDiloag = new LoaderDiloag(RegisterActivity.this);
        loaderDiloag.displayDiloag();

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            /*JSONObject jsonBody = new JSONObject();
            jsonBody.put("userid", userId);
            jsonBody.put("otp", otpString);


            Log.d(TAG,"jsonLogin " + jsonBody.toString());
            final String requestBody = jsonBody.toString();*/

            StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.GET, resendOtpUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    loaderDiloag.dismissDiloag();

                    Log.d("resendOtpUrl", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.contains("success"))
                        {
                            new MaterialDialog.Builder(context)
                                    .title("OTP")
                                    .content(message)
                                    .iconRes(R.drawable.ic_check_circle)
                                    .canceledOnTouchOutside(false)
                                    .cancelable(false)
                                    .positiveText("Ok")
                                    .onPositive(new MaterialDialog.SingleButtonCallback()
                                    {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                        {

                                            Intent intent = new Intent(RegisterActivity.this, OtpActivity.class);
                                            intent.putExtra("userId", userId);
                                            intent.putExtra("type", "RegisterActivity");
                                            startActivity(intent);
                                            dialog.dismiss();
                                            //startActivity(new Intent(context,LoginActivity.class));
                                        }
                                    }).show();

                        }
                        else
                        {
                            loaderDiloag.dismissDiloag();
                            Toast.makeText(RegisterActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        loaderDiloag.dismissDiloag();
                        Toast.makeText(RegisterActivity.this,getResources().getString(R.string.try_again),Toast.LENGTH_SHORT).show();

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    loaderDiloag.dismissDiloag();
                    Toast.makeText(RegisterActivity.this,getResources().getString(R.string.try_again),Toast.LENGTH_SHORT).show();
                    Log.e("VOLLEYOut", error.toString());
                }
            })

            {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    HashMap<String,String> param = new HashMap<String, String>();
                    param.put("Content-Type","application/json");
                    return param;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

               /* @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }*/


            };

            requestQueue.add(stringRequest);
        } catch (Exception e) {
            loaderDiloag.dismissDiloag();
            e.printStackTrace();
        }
    }
}
