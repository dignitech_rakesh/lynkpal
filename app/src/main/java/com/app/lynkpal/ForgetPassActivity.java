package com.app.lynkpal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class ForgetPassActivity extends AppCompatActivity {

    private EditText etForgetEmail;
    private Button btnConfirm;
    private int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pass);

        etForgetEmail=(EditText)findViewById(R.id.email_forget);
        btnConfirm=(Button)findViewById(R.id.btn_forget);

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (LoginActivity.isNetworkConnected(ForgetPassActivity.this)) {

                    if (Patterns.EMAIL_ADDRESS.matcher(etForgetEmail.getText().toString().trim()).matches()) {
                        forgetPassUrl(ForgetPassActivity.this);
                    } else {
                        etForgetEmail.setError("Please enter valid email");
                        //Intent intent=new Intent(ForgetPassActivity.this,ChangePassActivity.class);
                        //startActivity(intent);
                    }
                }else{
                    Toast.makeText(ForgetPassActivity.this,"No Internet Connection",Toast.LENGTH_LONG).show();
                    return;
                }
            }
        });
    }


    private void forgetPassUrl(final Context context)
    {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(ForgetPassActivity.this);
        loaderDiloag.displayDiloag();
       /* final ProgressDialog progressDialog = new ProgressDialog(ForgetPassActivity.this);
        progressDialog.setTitle("SignUp");
        progressDialog.setMessage("Processing...");
        progressDialog.show();*/

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("useremail", etForgetEmail.getText().toString().trim());
            //jsonBody.put("otp", otpString);


            Log.d("jsonLogin " , jsonBody.toString());
            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.POST, WebApis.forgotPassURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {

                    loaderDiloag.dismissDiloag();

                    Log.d("forgetPassUrl", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                         userId =jsonObject.optInt("userid");
                        if (status.contains("success"))
                        {
                            new MaterialDialog.Builder(context)
                                    .title("OTP")
                                    .content(message)
                                    .canceledOnTouchOutside(false)
                                    .iconRes(R.drawable.ic_check_circle)
                                    .positiveText("Ok")
                                    .onPositive(new MaterialDialog.SingleButtonCallback()
                                    {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                        {

                                            dialog.dismiss();
                                            Intent intent=new Intent(ForgetPassActivity.this,OtpActivity.class);
                                            intent.putExtra("userId",userId);
                                            intent.putExtra("type","ForgetPassActivity");
                                            startActivity(intent);
                                        }
                                    }).show();

                        }
                        else
                        {
                            loaderDiloag.dismissDiloag();
                            Toast.makeText(ForgetPassActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        loaderDiloag.dismissDiloag();
                        Toast.makeText(ForgetPassActivity.this,getResources().getString(R.string.try_again),Toast.LENGTH_SHORT).show();

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    loaderDiloag.dismissDiloag();
                    Toast.makeText(ForgetPassActivity.this,getResources().getString(R.string.try_again),Toast.LENGTH_SHORT).show();

                    Log.e("VOLLEYOut", error.toString());
                }
            })

            {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    HashMap<String,String> param = new HashMap<String, String>();
                    param.put("Content-Type","application/json");
                    return param;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }


            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
