package com.app.lynkpal.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.lynkpal.Bean.FriendSuggestion;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.OnCloseListener;
import com.app.lynkpal.R;
import com.bumptech.glide.Glide;

import java.util.List;

public class FriendAdapter extends RecyclerView.Adapter<FriendAdapter.FriendHolder>
{
    Context mContext;
    List<FriendSuggestion> mFriendList;
    OnCloseListener closeListener;
    private String TAG = FriendAdapter.class.getSimpleName();

    public FriendAdapter(Context mContext, List<FriendSuggestion> mFriendList, OnCloseListener closeListener) {
        this.mContext = mContext;
        this.mFriendList = mFriendList;
        this.closeListener = closeListener;
    }

    @NonNull
    @Override
    public FriendHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.friend_suggestion_item, parent, false);

        return new FriendHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FriendHolder holder, final int position)
    {
       final FriendSuggestion suggestion = mFriendList.get(position);
       holder.frnd_name.setText(suggestion.getFirstname());
        String image = suggestion.getProfilepic();
       Log.d(TAG,"getvalue " + image);
       if (image=="null")
       {
           Log.d(TAG,"ftrue " + suggestion.getProfilepic());
           Glide.with(mContext).load(R.drawable.userlynkpal).into(holder.frnd_image);
       }
       else

           {
               Log.d(TAG,"ffalse");
               Glide.with(mContext).load(WebApis.userProfileImage+suggestion.getProfilepic()).into(holder.frnd_image);
       }

       holder.add_bttn.setOnClickListener(new View.OnClickListener()
       {
           @Override
           public void onClick(View view)
           {
              closeListener.onFriendAdd(suggestion.getId());
           }
       });

        holder.close_image.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                closeListener.onCloseClick(position);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        //Log.d(TAG,"totalsize " + mFriendList.size());
        return mFriendList.size();
    }

    class FriendHolder extends RecyclerView.ViewHolder
    {
        ImageView frnd_image,close_image;
        TextView frnd_name,mutualfriend;
        AppCompatButton add_bttn;

        public FriendHolder(View itemView)
        {
            super(itemView);
            frnd_image = itemView.findViewById(R.id.friend_image);
            close_image = itemView.findViewById(R.id.close_image);
            frnd_name  = itemView.findViewById(R.id.friend_name);
            mutualfriend = itemView.findViewById(R.id.mutual_friend);
            add_bttn = itemView.findViewById(R.id.add_frnd_bttn);

          /*  close_image.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    closeListener.onCloseClick(getAdapterPosition());
                }
            });*/


        }


    }
}
