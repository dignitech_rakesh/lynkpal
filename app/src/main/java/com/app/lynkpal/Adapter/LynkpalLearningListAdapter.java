package com.app.lynkpal.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Bean.JobCreatedListBean;
import com.app.lynkpal.Bean.LynkaplLearningModel;
import com.app.lynkpal.EditJob;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.ApplyJob;
import com.app.lynkpal.Interface.MyTrainingInterface;
import com.app.lynkpal.JobDetail;
import com.app.lynkpal.PayActivity;
import com.app.lynkpal.R;
import com.bumptech.glide.Glide;

import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by user on 9/13/2017.
 */

public class LynkpalLearningListAdapter extends RecyclerView.Adapter<LynkpalLearningListAdapter.MyViewHolder> {
    String type;
    private Context context; //context
    private List<LynkaplLearningModel> items; //data source of the list adapter
    ApplyJob applyJob;
    MyTrainingInterface myTrainingInterface;

    //public constructor
    public LynkpalLearningListAdapter(Context context, List<LynkaplLearningModel> items, MyTrainingInterface myTrainingInterface) {
        this.context = context;
        this.items = items;
        this.myTrainingInterface=myTrainingInterface;
        //this.type = type;
       // this.applyJob=applyJob;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_linkpallearning_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final LynkaplLearningModel currentItem = items.get(position);

        String imgUrl=currentItem.getTraining_image();

        if (imgUrl != null && !imgUrl.equals("null")) {
            /*imgLogo.setImageBitmap(image);*/
            // Log.e("WebApis.JobImages+image",WebApis.JobImages+image);
            Glide.with(context).load(WebApis.BaseURL + currentItem.getTraining_image()).centerCrop()
                    .placeholder(R.drawable.noimage).error(R.drawable.noimage)
                    .into( holder.ivLearning);
        } else {
            holder.ivLearning.setImageResource(R.drawable.noimage);
        }
        holder.tvTitleLearning.setText(currentItem.getName());
        holder.tvHours.setText(currentItem.getDuration()+" Hours");
        holder.tvLectures.setText(currentItem.getLectures()+" Lectures");
        holder.tvAmt.setText("$ "+currentItem.getAmount());
        holder.tvSpecializationName.setText(currentItem.getSpecialization_name());

        if (currentItem.getPurchase_status()==0){
            holder.tvPurchased.setVisibility(View.GONE);
            holder.viewPurchased.setVisibility(View.GONE);
        }else {
            holder.tvPurchased.setVisibility(View.VISIBLE);

        }
        holder.cardViewLearning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTrainingInterface.detailAllTraining(String.valueOf(currentItem.getId()),position,currentItem);
            }
        });


    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitleLearning, tvHours, tvLectures,tvAmt,tvSpecializationName,tvPurchased;
        public ImageView ivLearning;
        public CardView cardViewLearning;
        public View viewPurchased;
        public MyViewHolder(View view) {
            super(view);
            cardViewLearning = (CardView) view.findViewById(R.id.cardViewLearning);
             ivLearning = (ImageView) view.findViewById(R.id.ivLearning);
             tvTitleLearning = (TextView) view.findViewById(R.id.tvTitleLearning);
             tvHours = (TextView) view.findViewById(R.id.tvHours);
             tvLectures = (TextView) view.findViewById(R.id.tvLectures);
             tvAmt = (TextView) view.findViewById(R.id.tvAmt);
             tvSpecializationName = (TextView) view.findViewById(R.id.tvSpecializationName);


            tvPurchased = (TextView) view.findViewById(R.id.tvPurchased);
            viewPurchased = (View) view.findViewById(R.id.viewPurchased);



        }
    }

}

