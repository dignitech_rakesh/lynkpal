package com.app.lynkpal.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.lynkpal.Bean.JobAppliedBean;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.DeleteAppliedJob;
import com.app.lynkpal.R;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by user on 9/13/2017.
 */

public class JobAppliedListAdapter extends BaseAdapter {
    private Context context; //context
    private List<JobAppliedBean> items; //data source of the list adapter
    DeleteAppliedJob deleteAppliedJob;
    //public constructor
    public JobAppliedListAdapter(Context context, List<JobAppliedBean> items,DeleteAppliedJob deleteAppliedJob) {
        this.context = context;
        this.items = items;
        this.deleteAppliedJob = deleteAppliedJob;
    }

    @Override
    public int getCount() {
        return items.size(); //returns total of items in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position); //returns list item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // inflate the layout for each list row
        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.job_create_listview_item, parent, false);
        }

        // get current item to be displayed
        JobAppliedBean currentItem = items.get(position);

        // get the TextView for item name and item description
        TextView txt_title = (TextView) convertView.findViewById(R.id.txt_title);
        TextView txt_Loca = (TextView) convertView.findViewById(R.id.txt_Loca);
        TextView txt_From = (TextView) convertView.findViewById(R.id.txt_From);
        TextView txt_Extra = (TextView) convertView.findViewById(R.id.txt_Extra);
        TextView txtPremiumJobIntrstd = (TextView) convertView.findViewById(R.id.txtPremiumJobIntrstd);
        ImageView imgImage = (ImageView) convertView.findViewById(R.id.imgImage);
        Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(context.getAssets(), "Roboto-Bold.ttf");
        txt_Loca.setTypeface(tf_reg);
        txt_title.setTypeface(tf_bold);
        txt_From.setTypeface(tf_reg);
        txt_Extra.setTypeface(tf_bold);
        String image = currentItem.getImage();
        if (image != null&&!image.equals("null")) {
            /*imgLogo.setImageBitmap(image);*/
            // Log.e("WebApis.JobImages+image",WebApis.JobImages+image);
            Glide.with(context).load(WebApis.JobImages+image)/*.centerCrop().placeholder(R.drawable.noimage).error(R.drawable.noimage)*/
                    .into(imgImage);
        } else {
            imgImage.setImageResource(R.drawable.noimage);
        }
        txt_title.setText(currentItem.getName());
        txt_Loca.setText(currentItem.getLocation());
        txt_Extra.setText(currentItem.getExtra());
        txt_From.setText(currentItem.getFrom());
        txt_Extra.setTag(position);
        txt_Extra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos= (int) view.getTag();
                deleteAppliedJob.deleteApply(items.get(pos).getId(),pos);
            }
        });

        if (currentItem.getHotApplied().equals("1")){
            txtPremiumJobIntrstd.setVisibility(View.VISIBLE);
        }else {
            txtPremiumJobIntrstd.setVisibility(View.GONE);

        }
        // returns the view for the current row
        return convertView;
    }
}

