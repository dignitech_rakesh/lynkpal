package com.app.lynkpal.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.lynkpal.Bean.SkillListBean;
import com.app.lynkpal.Interface.SelectInterface;
import com.app.lynkpal.ProfileActivity;
import com.app.lynkpal.R;

import java.util.List;

/**
 * Created by user on 9/22/2017.
 */

public class MultiSelectionList extends BaseAdapter {
    Context context;
    LayoutInflater inflter;
    SelectInterface selectInterface;
    // List<SkillListBean> custom_spinner_items = new ArrayList<SkillListBean>();

    public MultiSelectionList(Context applicationContext, List<SkillListBean> country, SelectInterface selectInterface) {
        this.context = applicationContext;
        //custom_spinner_items = country;
        this.selectInterface = selectInterface;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return ProfileActivity.list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        view = inflter.inflate(R.layout.multi_selection_list_item, null);
        TextView names = (TextView) view.findViewById(R.id.textView);
        names.setText(ProfileActivity.list.get(position).getName());
        Typeface tf_regular = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
        names.setTypeface(tf_regular);
        final CheckBox checkBox = (CheckBox) view.findViewById(R.id.chkBox);
        RelativeLayout layout = (RelativeLayout) view.findViewById(R.id.layout);
        if (ProfileActivity.list.get(position).getSelected()) {
            checkBox.setChecked(true);
        } else {
            checkBox.setChecked(false);
        }
        layout.setTag(position);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                if (ProfileActivity.list.get(pos).getSelected()) {
                    selectInterface.select(pos + "", 0);
                    notifyDataSetChanged();
                } else {
                    selectInterface.select(pos + "", 1);
                    notifyDataSetChanged();
                }
            }
        });
        return view;
    }
}