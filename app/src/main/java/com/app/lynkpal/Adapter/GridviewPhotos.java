package com.app.lynkpal.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.app.lynkpal.Bean.GridBean;
import com.app.lynkpal.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 9/14/2017.
 */

public class GridviewPhotos extends BaseAdapter {

    public List<GridBean> data = new ArrayList<GridBean>();
    Context context;
    int Height;

    public GridviewPhotos(Context context, List<GridBean> tablelist, int height) {
        this.data = tablelist;
        this.context = context;
        this.Height = height;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.grid_photos_layout, parent, false);

        } else {
            row = (View) convertView;
        }


        ImageView imgLogo = (ImageView) row.findViewById(R.id.imgIcon);

        imgLogo.getLayoutParams().height = (int) (Height / 6);
        String image = data.get(position).getImage();
        if (image != null) {
            Glide.with(context).load(image).into(imgLogo);/*placeholder(R.drawable.back).error(R.drawable.back)
                    .into(imgLogo);*/
        }
        return row;
    }


}
