package com.app.lynkpal.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
/*import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;*/
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.lynkpal.Bean.GridBean;
import com.app.lynkpal.GroupActivity;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by user on 7/21/2017.
 */

public class GridViewAdapter extends BaseAdapter {

    public List<GridBean> data = new ArrayList<GridBean>();
    Context context;
    int Height;

    public GridViewAdapter(Context context, List<GridBean> tablelist, int height) {
        this.data = tablelist;
        this.context = context;
        this.Height = height;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.grid_layout, parent, false);

        } else {
            row = (View) convertView;
        }

        row.setTag(position);
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos= (int) view.getTag();

                    context.startActivity(new Intent(context,GroupActivity.class)
                            .putExtra("name",data.get(pos).getName()).putExtra("id",data.get(pos).getId()).putExtra("image",data.get(pos).getImage()).putExtra("type","me"));
            }
        });
        final ImageView imgLogo = (ImageView) row.findViewById(R.id.imgIcon);
        TextView txtName = (TextView) row.findViewById(R.id.txtName);
        LinearLayout linearLayout = (LinearLayout) row.findViewById(R.id.linMain);
        Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        txtName.setTypeface(tf_reg);
        linearLayout.getLayoutParams().height = (int) (Height / 6);
        String image = data.get(position).getImage();
        if (!image.equals("null"))
        {
            Picasso.with(context).load(WebApis.GroupImages + image).into(imgLogo);
            /*imgLogo.setImageBitmap(image);*/
        /*    Glide.with(context).load(WebApis.GroupImages + image).placeholder(R.drawable.noimage).error(R.drawable.noimage)
                    .into(imgLogo);*/
          /*  Glide.with(context).load(WebApis.GroupImages + image).asBitmap().placeholder(R.drawable.noimage).error(R.drawable.noimage).centerCrop().into(new BitmapImageViewTarget(imgLogo) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    imgLogo.setImageDrawable(circularBitmapDrawable);
                }
            });*/
        } else {

        }
        txtName.setText(data.get(position).getName());
        //  txtName.setSelected(true);
        return row;
    }


}
