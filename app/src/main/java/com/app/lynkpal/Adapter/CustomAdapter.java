package com.app.lynkpal.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.lynkpal.Bean.industriesBean;
import com.app.lynkpal.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by user on 7/13/2017.
 */
public class CustomAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflter;
    List<industriesBean> custom_spinner_items = new ArrayList<industriesBean>();

    public CustomAdapter(Context applicationContext, List<industriesBean> country) {
        this.context = applicationContext;
        custom_spinner_items = country;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return custom_spinner_items.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        view = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = (TextView) view.findViewById(R.id.textView);
        names.setText(custom_spinner_items.get(position).getName());
        Typeface tf_regular = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
        names.setTypeface(tf_regular);
        return view;
    }
}