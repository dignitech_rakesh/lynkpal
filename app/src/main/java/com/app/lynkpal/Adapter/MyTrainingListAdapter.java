package com.app.lynkpal.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.lynkpal.Bean.LynkaplLearningModel;
import com.app.lynkpal.Bean.MyTrainingModel;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.ApplyJob;
import com.app.lynkpal.Interface.MyTrainingInterface;
import com.app.lynkpal.R;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by user on 9/13/2017.
 */

public class MyTrainingListAdapter extends RecyclerView.Adapter<MyTrainingListAdapter.MyViewHolder> {
    String type;
    private Context context; //context
    private List<MyTrainingModel> items; //data source of the list adapter
    MyTrainingInterface myTrainingInterface;

    //public constructor
    public MyTrainingListAdapter(Context context, List<MyTrainingModel> items,MyTrainingInterface myTrainingInterface) {
        this.context = context;
        this.items = items;
        //this.type = type;
        this.myTrainingInterface=myTrainingInterface;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_mytraining_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final MyTrainingModel currentItem = items.get(position);

        String imgUrl=currentItem.getTraining_image();

        if (imgUrl != null && !imgUrl.equals("null")) {
            /*imgLogo.setImageBitmap(image);*/
            // Log.e("WebApis.JobImages+image",WebApis.JobImages+image);
            Glide.with(context).load(WebApis.BaseURL + currentItem.getTraining_image()).centerCrop()
                    .placeholder(R.drawable.noimage).error(R.drawable.noimage)
                    .into( holder.ivLearning);
        } else {
            holder.ivLearning.setImageResource(R.drawable.noimage);
        }
        holder.tvTitleLearning.setText(currentItem.getName());
        holder.tvHours.setText(currentItem.getDuration()+" Hours");
        holder.tvLectures.setText(currentItem.getLectures()+" Lectures");
        holder.tvAmt.setText("$ "+currentItem.getAmount());
        holder.tvSpecializationName.setText(currentItem.getSpecialization_name());

        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myTrainingInterface.editTraining(String.valueOf(currentItem.getId()),position,currentItem);
            }
        });

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myTrainingInterface.deleteTraining(String.valueOf(currentItem.getId()),position);
            }
        });

        holder.ivDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myTrainingInterface.detailTraining(String.valueOf(currentItem.getId()),position,currentItem);
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitleLearning, tvHours, tvLectures,tvAmt,tvSpecializationName;
        public ImageView ivLearning,ivEdit,ivDelete,ivDetail;

        public MyViewHolder(View view) {
            super(view);
             ivLearning = (ImageView) view.findViewById(R.id.ivLearning);
            ivEdit = (ImageView) view.findViewById(R.id.ivEditMyTraining);
            ivDelete = (ImageView) view.findViewById(R.id.ivDeleteMyTraining);
            ivDetail = (ImageView) view.findViewById(R.id.ivDetailMyTraining);
             tvTitleLearning = (TextView) view.findViewById(R.id.tvTitleLearning);
             tvHours = (TextView) view.findViewById(R.id.tvHours);
             tvLectures = (TextView) view.findViewById(R.id.tvLectures);
             tvAmt = (TextView) view.findViewById(R.id.tvAmt);
             tvSpecializationName = (TextView) view.findViewById(R.id.tvSpecializationName);

        }
    }

}

