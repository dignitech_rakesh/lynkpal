package com.app.lynkpal.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.lynkpal.Bean.ChatBean;
import com.app.lynkpal.Helper.CircleImageView;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Softgators on 8/19/2016.
 */
public class ChatAdapter extends BaseAdapter {

    private List<ChatBean> chatBeanList;
    private Context context;


    public ChatAdapter(ArrayList<ChatBean> i, Context context) {
        this.chatBeanList = i;
        this.context = context;
    }

    @Override
    public int getCount() {
        return chatBeanList.size();
    }

    @Override
    public Object getItem(int position) {
        return chatBeanList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView left_message, right_message;
        TextView right_textView_time, left_textView_time;
        CircleImageView leftProfilePic, rightProfilePic;
        ImageView leftAttachment, rightAttachment;
        RelativeLayout leftRelativelayout, rightRelativeLayout;
        View view = null;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.right, null);

        right_textView_time = (TextView) view.findViewById(R.id.right_time_chat_out);
        rightAttachment = (ImageView) view.findViewById(R.id.right_attachment);
        right_message = (TextView) view.findViewById(R.id.right_text_value_out);
        rightProfilePic = (CircleImageView) view.findViewById(R.id.right_profile_pic);
        rightRelativeLayout = (RelativeLayout) view.findViewById(R.id.right_relative_layout);

        left_textView_time = (TextView) view.findViewById(R.id.left_time_chat_out);
        leftAttachment = (ImageView) view.findViewById(R.id.left_attachment);
        left_message = (TextView) view.findViewById(R.id.left_text_value_out);
        leftProfilePic = (CircleImageView) view.findViewById(R.id.left_profile_pic);
        leftRelativelayout = (RelativeLayout) view.findViewById(R.id.left_relative_layout);

        ChatBean chatBean = chatBeanList.get(position);

        String userID = Constant.user_id;
        Log.e("Getting ID", userID + "");
        if (userID.equals(chatBean.getSender())) {
            if (chatBean.getMessage() != null) {
                right_message.setVisibility(View.VISIBLE);
                rightRelativeLayout.setVisibility(View.VISIBLE);
                leftRelativelayout.setVisibility(View.GONE);
                right_message.setText(chatBean.getMessage().trim());
            } else {
                right_message.setVisibility(View.GONE);
            }
            if (chatBean.getSender_pic() != null && !chatBean.getSender_pic().equals("null") && !chatBean.getSender_pic().trim().equals("")) {
                Glide.with(context).load(WebApis.userProfileImage + chatBean.getSender_pic()).into(rightProfilePic);//error(R.drawable.image).placeholder(R.drawable.image).into(rightProfilePic);
            } else {
                rightProfilePic.setImageDrawable(context.getResources().getDrawable(R.drawable.image));
            }
            String referrerAffiliateId = chatBean.getCreated_at();
            if (referrerAffiliateId != null) {
                if (referrerAffiliateId.toLowerCase().equals("0 minutes ago") || referrerAffiliateId.toLowerCase().equals("in 0 minutes")) {
                    referrerAffiliateId = "Just Now";
                }
                right_textView_time.setText(referrerAffiliateId);
            } else {
                right_textView_time.setText(chatBean.getCurrenttime());
            }
            if (chatBean.getAttachments() == null || chatBean.getAttachments().equals("") || chatBean.getAttachments().equals("null")) {
                rightAttachment.setVisibility(View.GONE);
            } else {
                rightAttachment.setVisibility(View.VISIBLE);
                leftAttachment.setVisibility(View.GONE);
                String attachMent = WebApis.MessageImages + chatBean.getAttachments();
                Glide.with(context).load(attachMent).into(rightAttachment);
            }
        } else {
            if (chatBean.getMessage() != null) {
                left_message.setVisibility(View.VISIBLE);
                leftRelativelayout.setVisibility(View.VISIBLE);
                rightRelativeLayout.setVisibility(View.GONE);
                left_message.setText(chatBean.getMessage().trim());
            } else {
                left_message.setVisibility(View.GONE);
            }
            if (chatBean.getSender_pic() != null && !chatBean.getSender_pic().equals("null") && !chatBean.getSender_pic().trim().equals("")) {
                Glide.with(context).load(WebApis.userProfileImage + chatBean.getSender_pic()).into(leftProfilePic);//error(R.drawable.image).placeholder(R.drawable.image).into(leftProfilePic);
            } else {
                leftProfilePic.setImageDrawable(context.getResources().getDrawable(R.drawable.image));
            }

            String referrerAffiliateId = chatBean.getCreated_at();
            if (referrerAffiliateId != null) {
                if (referrerAffiliateId.toLowerCase().equals("0 minutes ago")) {
                    referrerAffiliateId = "Just Now";
                }
                left_textView_time.setText(referrerAffiliateId);
            } else {
                left_textView_time.setText(chatBean.getCurrenttime());
            }
            if (chatBean.getAttachments() != null && !chatBean.getAttachments().equals("null")) {
                leftAttachment.setVisibility(View.VISIBLE);
                rightAttachment.setVisibility(View.GONE);
                String attachMent = WebApis.MessageImages + chatBean.getAttachments();
                Glide.with(context).load(attachMent).into(leftAttachment);
            } else {
                leftAttachment.setVisibility(View.GONE);
            }
        }


        return view;
    }
}
