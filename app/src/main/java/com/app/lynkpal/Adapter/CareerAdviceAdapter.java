package com.app.lynkpal.Adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.lynkpal.Bean.CareerAdviceModel;
import com.app.lynkpal.Bean.LynkaplLearningModel;
import com.app.lynkpal.CareerAdviceDetail;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.ApplyJob;
import com.app.lynkpal.Interface.CareerAdviceInterface;
import com.app.lynkpal.Interface.MyTrainingInterface;
import com.app.lynkpal.R;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by user on 9/13/2017.
 */

public class CareerAdviceAdapter extends RecyclerView.Adapter<CareerAdviceAdapter.MyViewHolder> {
    String type;
    private Context context; //context
    private List<CareerAdviceModel> items; //data source of the list adapter
    CareerAdviceInterface careerAdviceInterface;
    public boolean readStatus;

    //public constructor
    public CareerAdviceAdapter(Context context, List<CareerAdviceModel> items,CareerAdviceInterface careerAdviceInterface) {
        this.context = context;
        this.items = items;
        this.careerAdviceInterface=careerAdviceInterface;
        //this.type = type;
       // this.applyJob=applyJob;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_careeradvice_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final CareerAdviceModel currentItem = items.get(position);

        readStatus=false;
        String imgUrl=currentItem.getImage();

        if (imgUrl != null && !imgUrl.equals("null")) {
            /*imgLogo.setImageBitmap(image);*/
            // Log.e("WebApis.JobImages+image",WebApis.JobImages+image);
            Glide.with(context).load(WebApis.BaseURL + currentItem.getImage()).centerCrop()
                    .placeholder(R.drawable.noimage).error(R.drawable.noimage)
                    .into( holder.ivCareerAdvice);
        } else {
            holder.ivCareerAdvice.setImageResource(R.drawable.noimage);
        }
        holder.tvTitleCareerAdvice.setText(currentItem.getTitle());
        holder.tvSmallDescCareerAdvice.setText(currentItem.getDescription()+" ...");

     /*   if (readStatus){
            holder.tvReadMore.setText("Read Less");

        }else {
            holder.tvReadMore.setText("Read More");
        }*/

        holder.tvReadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, CareerAdviceDetail.class);
                intent.putExtra("detail",currentItem);
                context.startActivity(intent);

              /*  // read Status false means haven't read the full description
                if (readStatus){
                    holder.tvReadMore.setText("Read More...");
                    readStatus=false;

                    holder.tvSmallDescCareerAdvice.setVisibility(View.VISIBLE);
                    holder.tvFullDescCareerAdvice.setVisibility(View.GONE);

                    careerAdviceInterface.Shrink(position);

                }else {
                    holder.tvReadMore.setText("Read Less...");
                    readStatus=true;

                    holder.tvSmallDescCareerAdvice.setVisibility(View.GONE);
                    holder.tvFullDescCareerAdvice.setVisibility(View.VISIBLE);
                }*/


            }
        });



        // new

      /*  holder.tvReadMore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (readStatus) {
                    holder.tvReadMore.setText("Read More");
                    readStatus=false;

                    holder.tvSmallDescCareerAdvice.setVisibility(View.VISIBLE);
                    holder.tvFullDescCareerAdvice.setVisibility(View.GONE);

                    careerAdviceInterface.Shrink(position);

                    ObjectAnimator animation = ObjectAnimator.ofInt(holder.tvReadMore, "maxLines", 40);
                    animation.setDuration(100).start();
                  //  holder.tvReadMore.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_collapse));
                } else {
                    holder.tvReadMore.setText("Read Less");
                    readStatus=true;

                    holder.tvSmallDescCareerAdvice.setVisibility(View.GONE);
                    holder.tvFullDescCareerAdvice.setVisibility(View.VISIBLE);

                    ObjectAnimator animation = ObjectAnimator.ofInt(holder.tvReadMore, "maxLines", 4);
                    animation.setDuration(500).start();
                    //btnSeeMore.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.ic_expand));
                }

            }
        });*/
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitleCareerAdvice, tvSmallDescCareerAdvice, tvFullDescCareerAdvice,tvReadMore;
        public ImageView ivCareerAdvice;
       // public CardView cardViewLearning;
        //public View viewPurchased;
        public MyViewHolder(View view) {
            super(view);
           // cardViewLearning = (CardView) view.findViewById(R.id.cardViewLearning);
            ivCareerAdvice = (ImageView) view.findViewById(R.id.ivCareerAdvice);
            tvTitleCareerAdvice = (TextView) view.findViewById(R.id.tvTitleCareerAdvice);
            tvSmallDescCareerAdvice = (TextView) view.findViewById(R.id.tvSmallDescCareerAdvice);
            tvFullDescCareerAdvice = (TextView) view.findViewById(R.id.tvFullDescCareerAdvice);
            tvReadMore = (TextView) view.findViewById(R.id.tvReadMore);




        }
    }

}

