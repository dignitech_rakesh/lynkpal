package com.app.lynkpal.Adapter;



import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
/*import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;*/
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.lynkpal.Bean.CompanyFollowersBean;
import com.app.lynkpal.Bean.FriendSuggestion;
import com.app.lynkpal.Helper.CircleImageView;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.OnCloseListener;
import com.app.lynkpal.Interface.Searchit;
import com.app.lynkpal.R;
import com.app.lynkpal.UserAndCompanyPageDetails;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 10/24/2017.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.GroupHolder>
{

    Context mContext;
    List<CompanyFollowersBean> mFriendList;
    private String TAG = SearchAdapter.class.getSimpleName();
    Searchit searchit;

    public SearchAdapter(Context mContext, List<CompanyFollowersBean> mFriendList,Searchit searchit)
    {
        this.mContext = mContext;
        this.mFriendList = mFriendList;
        this.searchit = searchit;
    }

    @NonNull
    @Override
    public GroupHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_group_members, parent, false);

        return new GroupHolder(itemView);
    }

    @Override
    public void onBindViewHolder(GroupHolder holder, int position)
    {
        final CompanyFollowersBean companyFollowersBean = mFriendList.get(position);
        holder.frnd_name.setText(companyFollowersBean.getFullName());
        if (companyFollowersBean.getProfilePic()!="null")
        {
            Log.d(TAG, "onBindViewHolder: Image yes");
            Glide.with(mContext).load(WebApis.userProfileImage + companyFollowersBean.getProfilePic()).into(holder.frnd_image);
        }
        else
        {
            Log.d(TAG, "onBindViewHolder: Image No");
            holder.frnd_image.setImageResource(R.drawable.image);
        }

        holder.cardView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
               searchit.onSearch(companyFollowersBean.getUserid());
            }
        });

    }

    @Override
    public int getItemCount()
    {
        return mFriendList.size();
    }

    class GroupHolder extends RecyclerView.ViewHolder
    {
        CircleImageView frnd_image;
        TextView frnd_name,removetxt;
        CardView cardView;

        public GroupHolder(View itemView)
        {
            super(itemView);
            frnd_image = itemView.findViewById(R.id.searchimageuser);
            frnd_name  = itemView.findViewById(R.id.name);
            removetxt = itemView.findViewById(R.id.txtRemove);
            removetxt.setVisibility(View.GONE);
            cardView = itemView.findViewById(R.id.card_view);




        }


    }


}


