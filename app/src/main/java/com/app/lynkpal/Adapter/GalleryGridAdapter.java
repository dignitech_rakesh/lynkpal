package com.app.lynkpal.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.lynkpal.Bean.GridBean;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.showImagePopup;
import com.app.lynkpal.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 10/3/2017.
 */

public class GalleryGridAdapter extends BaseAdapter {

    public List<GridBean> data = new ArrayList<GridBean>();
    Context context;
    int Height;
    showImagePopup showImagePopup;

    public GalleryGridAdapter(Context context, List<GridBean> tablelist, int height, showImagePopup showImagePopup) {
        this.data = tablelist;
        this.context = context;
        this.Height = height;
        this.showImagePopup = showImagePopup;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.item_gallery_profile, parent, false);

        } else {
            row = (View) convertView;
        }


        final ImageView imgLogo = (ImageView) row.findViewById(R.id.imgIcon);
        //   TextView txtName = (TextView) row.findViewById(R.id.txtName);
        LinearLayout linearLayout = (LinearLayout) row.findViewById(R.id.linMain);
        Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        // txtName.setTypeface(tf_reg);
        linearLayout.getLayoutParams().height = (int) (Height / 8);
        String image = data.get(position).getImage();
        if (image != null && !image.equals("null")) {
            Glide.with(context).load(WebApis.CmpGalleryImage + image)//error(R.drawable.noimage)
                    .into(imgLogo);

        } else {
            imgLogo.setImageResource(R.drawable.noimage);
        }
       // imgLogo.setTag(position);
        imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showImagePopup.image(position+"");
            }
        });
        return row;
    }


}
