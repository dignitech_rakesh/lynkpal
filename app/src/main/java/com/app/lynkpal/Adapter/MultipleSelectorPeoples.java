package com.app.lynkpal.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.lynkpal.Bean.AllUsersBean;
import com.app.lynkpal.Interface.SelectInterface;
import com.app.lynkpal.R;

import java.util.List;

/**
 * Created by user on 10/10/2017.
 */

public class MultipleSelectorPeoples extends BaseAdapter {
    Context context;
    LayoutInflater inflter;
    SelectInterface selectInterface;
    List<AllUsersBean> allUsersBeenList;
    // List<SkillListBean> custom_spinner_items = new ArrayList<SkillListBean>();

    public MultipleSelectorPeoples(Context applicationContext, List<AllUsersBean> allUsersBeen, SelectInterface selectInterface) {
        this.context = applicationContext;
        //custom_spinner_items = country;
        this.selectInterface = selectInterface;
        inflter = (LayoutInflater.from(applicationContext));
        this.allUsersBeenList = allUsersBeen;
    }

    @Override
    public int getCount() {
        return allUsersBeenList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        view = inflter.inflate(R.layout.multiple_selector_user, null);
        TextView names = (TextView) view.findViewById(R.id.textView);
        names.setText(allUsersBeenList.get(position).getFirst_name()+" "+allUsersBeenList.get(position).getLast_name());
        Typeface tf_regular = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
        names.setTypeface(tf_regular);
       // ShapesImage shapesImage = view.findViewById(R.id.image);
        final CheckBox checkBox = (CheckBox) view.findViewById(R.id.chkBox);
        RelativeLayout layout = (RelativeLayout) view.findViewById(R.id.layout);
  /*      if (!allUsersBeenList.get(position).getProfile_pic().toLowerCase().equals("null")) {
            String imageURL=WebApis.userProfileImage + allUsersBeenList.get(position).getProfile_pic();
            imageURL=imageURL.replace(" ","%20");
            Log.e("imageURL",imageURL);
            Glide.with(context).load
                    (imageURL)
                    .error(R.drawable.image).placeholder(R.drawable.image).into(shapesImage);
        }*/

        if (allUsersBeenList.get(position).getSelected()) {
            checkBox.setChecked(true);
        } else {
            checkBox.setChecked(false);
        }
        layout.setTag(position);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                if (allUsersBeenList.get(pos).getSelected()) {
                    selectInterface.select(pos + "", 0);
                    notifyDataSetChanged();
                } else {
                    selectInterface.select(pos + "", 1);
                    notifyDataSetChanged();
                }
            }
        });
        return view;
    }
}
