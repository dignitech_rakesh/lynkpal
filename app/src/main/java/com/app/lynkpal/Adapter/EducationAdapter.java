package com.app.lynkpal.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.lynkpal.Bean.ProfileEducationBean;
import com.app.lynkpal.Interface.EditInterface;
import com.app.lynkpal.Interface.deleteEduExpInterface;
import com.app.lynkpal.R;

import java.util.List;

/**
 * Created by user on 9/23/2017.
 */

public class EducationAdapter extends BaseAdapter {
    EditInterface editInterface;
    deleteEduExpInterface expInterface;
    private Context context; //context
    private List<ProfileEducationBean> items; //data source of the list adapter

    public EducationAdapter(Context context, List<ProfileEducationBean> items, EditInterface editInterface, deleteEduExpInterface expInterface) {
        this.context = context;
        this.items = items;
        this.editInterface = editInterface;
        this.expInterface = expInterface;
    }

    @Override
    public int getCount() {
        return items.size(); //returns total of items in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position); //returns list item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // inflate the layout for each list row
        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.education_list_items, parent, false);
        }

        // get current item to be displayed
        ProfileEducationBean currentItem = items.get(position);

        // get the TextView for item name and item description
        TextView txt_title = (TextView) convertView.findViewById(R.id.txtName);
        TextView txt_Loca = (TextView) convertView.findViewById(R.id.cl_sub);
        TextView txt_From = (TextView) convertView.findViewById(R.id.time);

        ImageView imgImage = (ImageView) convertView.findViewById(R.id.imgEdit);
        ImageView imgDelete = (ImageView) convertView.findViewById(R.id.imgDelete);
        Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(context.getAssets(), "Roboto-Bold.ttf");
        txt_Loca.setTypeface(tf_reg);
        txt_title.setTypeface(tf_bold);
        txt_From.setTypeface(tf_reg);

        txt_title.setText(currentItem.getBoard_university());
        txt_Loca.setText(currentItem.getClass_degree() + "," + currentItem.getSubject_course());
        txt_From.setText(currentItem.getStart_date() + "-" + currentItem.getEnd_date());
        if (currentItem.isDeleteEdit())
        {
            imgImage.setVisibility(View.VISIBLE);
            imgDelete.setVisibility(View.VISIBLE);
        }
        else
        {
            imgImage.setVisibility(View.GONE);
            imgDelete.setVisibility(View.GONE);

        }
        imgImage.setTag(position);
        imgImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                editInterface.Edit(items.get(pos).getId(), "Edu");
            }
        });
        imgDelete.setTag(position);
        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                expInterface.delete(items.get(pos).getId(), pos, "Edu");
            }
        });
        // returns the view for the current row
        return convertView;
    }
}

