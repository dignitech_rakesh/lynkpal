package com.app.lynkpal.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.lynkpal.Bean.GridBean;
import com.app.lynkpal.R;
import com.bumptech.glide.Glide;

import net.karthikraj.shapesimage.ShapesImage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 9/13/2017.
 */

public class GridViewApplicationsAdapter  extends BaseAdapter {

    public List<GridBean> data = new ArrayList<GridBean>();
    Context context;
    int Height;

    public GridViewApplicationsAdapter(Context context, List<GridBean> tablelist, int height) {
        this.data = tablelist;
        this.context = context;
        this.Height = height;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.grid_layout, parent, false);

        } else {
            row = (View) convertView;
        }


        final ShapesImage imgLogo = (ShapesImage) row.findViewById(R.id.imgIcon);
        TextView txtName = (TextView) row.findViewById(R.id.txtName);
        LinearLayout linearLayout = (LinearLayout) row.findViewById(R.id.linMain);
        Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        txtName.setTypeface(tf_reg);
        linearLayout.getLayoutParams().height = (int) (Height / 6);
        String image = data.get(position).getImage();
        if (image != null) {
            /*imgLogo.setImageBitmap(image);*/
            Glide.with(context).load(image).into(imgLogo);/*placeholder(R.drawable.image).centerCrop().error(R.drawable.image)
                    .into(imgLogo);*/
           /* Glide.with(context).load(image).asBitmap().centerCrop().placeholder(R.drawable.image)into(new BitmapImageViewTarget(imgLogo) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    imgLogo.setImageDrawable(circularBitmapDrawable);
                }
            });*/
        }
        else
        {
            imgLogo.setImageResource(R.drawable.image);
        }
        txtName.setText(data.get(position).getName());
        //  txtName.setSelected(true);
        return row;
    }


}
