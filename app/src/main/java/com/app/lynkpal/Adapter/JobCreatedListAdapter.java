package com.app.lynkpal.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Bean.JobCreatedListBean;
import com.app.lynkpal.EditJob;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.ApplyJob;
import com.app.lynkpal.JobDetail;
import com.app.lynkpal.PayActivity;
import com.app.lynkpal.R;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by user on 9/13/2017.
 */

public class JobCreatedListAdapter extends BaseAdapter {
    String amount;
    private Context context; //context
    private List<JobCreatedListBean> items; //data source of the list adapter
    ApplyJob applyJob;

    //public constructor
    public JobCreatedListAdapter(Context context, List<JobCreatedListBean> items, String amount,  ApplyJob applyJob) {
        this.context = context;
        this.items = items;
        this.amount = amount;
        this.applyJob=applyJob;
    }

    @Override
    public int getCount() {
        return items.size(); //returns total of items in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position); //returns list item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // inflate the layout for each list row
        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.job_create_listview_item_main, parent, false);
        }

        // get current item to be displayed
        final JobCreatedListBean currentItem = items.get(position);

        // get the TextView for item name and item description
        TextView txt_title = (TextView) convertView.findViewById(R.id.txt_title);
        LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.lin_main);
        TextView txt_Loca = (TextView) convertView.findViewById(R.id.txt_Loca);
        TextView txt_From = (TextView) convertView.findViewById(R.id.txt_From);
        TextView txt_Extra = (TextView) convertView.findViewById(R.id.txt_Extra);
        TextView txtPremiumJob = (TextView) convertView.findViewById(R.id.txtPremiumJob);
        final Button buttonPay = convertView.findViewById(R.id.buttonPay);
        ImageView imgImage = (ImageView) convertView.findViewById(R.id.imgImage);
        ImageView imgDelete = (ImageView) convertView.findViewById(R.id.delete_job);
        ImageView imgEdit = (ImageView) convertView.findViewById(R.id.edit_job);
        Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(context.getAssets(), "Roboto-Bold.ttf");
        txt_Loca.setTypeface(tf_reg);
        txt_title.setTypeface(tf_bold);
        txt_From.setTypeface(tf_reg);
        txt_Extra.setTypeface(tf_bold);
        buttonPay.setTypeface(tf_bold);
        String image = currentItem.getImage();
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                context.startActivity(new Intent(context, JobDetail.class)
                        .putExtra("id", currentItem.getId())
                        .putExtra("name", currentItem.getName())
                        .putExtra("com_name", "Lynkpal inc.")
                        .putExtra("from", "0")
                        .putExtra("image", currentItem.getImage())
                        .putExtra("dec", currentItem.getDescription())
                        .putExtra("hot", currentItem.getHot())
                        .putExtra("exp", currentItem.getExperience())
                        .putExtra("empType", currentItem.getEmployment_type())
                        .putExtra("time", currentItem.getFrom())
                        .putExtra("location", currentItem.getLocation()));
            }
        });
        if (image != null && !image.equals("null")) {
            /*imgLogo.setImageBitmap(image);*/
            // Log.e("WebApis.JobImages+image",WebApis.JobImages+image);
            Glide.with(context).load(WebApis.JobImages + image)/*.centerCrop().placeholder(R.drawable.noimage).error(R.drawable.noimage)*/
                    .into(imgImage);
        } else {
            imgImage.setImageResource(R.drawable.noimage);
        }
        txt_title.setText(currentItem.getName());
        txt_Loca.setText(currentItem.getLocation());
        txt_Extra.setText(currentItem.getExtra());
        txt_From.setText(currentItem.getFrom());


        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"delete",Toast.LENGTH_SHORT).show();

                showPopup(currentItem.getId(),position);

            }
        });


        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Edit",Toast.LENGTH_SHORT).show();

                context.startActivity(new Intent(context, EditJob.class)
                 .putExtra("id", currentItem.getId())
                        .putExtra("name", currentItem.getName())
                        .putExtra("com_name", "Lynkpal inc.")
                        //.putExtra("from", "0")
                        .putExtra("image", currentItem.getImage())
                        .putExtra("dec", currentItem.getDescription())
                        .putExtra("hot", currentItem.getHot())
                        .putExtra("exp", currentItem.getExperience())
                        .putExtra("empType", currentItem.getEmployment_type())
                        .putExtra("time", currentItem.getFrom())
                        .putExtra("location", currentItem.getLocation()));
            }
        });



        if (currentItem.getHot().equals("1")) {
            txtPremiumJob.setVisibility(View.VISIBLE);
            buttonPay.setVisibility(View.VISIBLE);
            if (currentItem.getPaid().equals("0")) {
                buttonPay.setText("Pay :" + amount);
            } else {
                buttonPay.setText("Published");
            }
        } else {
            txtPremiumJob.setVisibility(View.GONE);
            buttonPay.setVisibility(View.GONE);
        }

        buttonPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentItem.getPaid().equals("0")) {
                    context.startActivity(new Intent(context, PayActivity.class).putExtra("pay", amount + "").putExtra("id", currentItem.getId() + "").putExtra("type", "job"));
                } else {
                    Toast.makeText(context, "Published", Toast.LENGTH_SHORT).show();
                }
            }
        });
        // returns the view for the current row
        return convertView;
    }




    // For delete Job

    public void showPopup(final String id,final int pos) {
        LayoutInflater layoutInflater
                = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_group_join_leave, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        final TextView textText = (TextView) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(context.getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        textTitle.setText("Delete Job");
        textText.setText("Are you sure, you want to delete this Job?");
        // textText.setText("Are you sure, you want to leave " + '"' + text + '"' + " Group?");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // deleteJob();
                applyJob.Delete(id,pos);
                popupWindow.dismiss();

            }
        });
    }

}

