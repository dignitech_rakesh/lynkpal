package com.app.lynkpal.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Bean.AdvertisementsBean;
import com.app.lynkpal.Interface.EditInterface;
import com.app.lynkpal.Interface.deleteEduExpInterface;
import com.app.lynkpal.PayActivity;
import com.app.lynkpal.R;

import java.util.List;

/**
 * Created by user on 10/10/2017.
 */

public class AdvertismentAdapter extends BaseAdapter {
    EditInterface editInterface;
    deleteEduExpInterface expInterface;
    private Context context; //context
    private List<AdvertisementsBean> items; //data source of the list adapter

    public AdvertismentAdapter(Context context, List<AdvertisementsBean> items, EditInterface editInterface, deleteEduExpInterface expInterface) {
        this.context = context;
        this.items = items;
        this.editInterface = editInterface;
        this.expInterface = expInterface;
    }

    @Override
    public int getCount() {
        return items.size(); //returns total of items in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position); //returns list item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // inflate the layout for each list row
        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.list_item_adv, parent, false);
        }

        // get current item to be displayed
        AdvertisementsBean currentItem = items.get(position);

        // get the TextView for item name and item description
        TextView txt_title = (TextView) convertView.findViewById(R.id.txtName);
        TextView txt_Loca = (TextView) convertView.findViewById(R.id.cl_sub);
        TextView txt_From = (TextView) convertView.findViewById(R.id.time);

        ImageView imgImage = (ImageView) convertView.findViewById(R.id.imgEdit);
        ImageView imgDelete = (ImageView) convertView.findViewById(R.id.imgDelete);
        Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(context.getAssets(), "Roboto-Bold.ttf");
        txt_Loca.setTypeface(tf_reg);
        txt_title.setTypeface(tf_bold);
        txt_From.setTypeface(tf_reg);
        Button buttonPay = (Button) convertView.findViewById(R.id.buttonPay);
        buttonPay.setTypeface(tf_bold);

        if (currentItem.getPaid().equals("0")) {
            buttonPay.setText("Pay :" + currentItem.getAmount());
        } else {
            buttonPay.setText("Published");
        }
        txt_title.setText(currentItem.getName());
        txt_Loca.setText("From:" + currentItem.getStartdate() + " | Till:" + currentItem.getEnddate());
        txt_From.setText("Status: Yet to Start | Reach :" + currentItem.getRead_count());
        imgImage.setTag(position);
        imgImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                editInterface.Edit(String.valueOf(pos), "");
            }
        });
        imgDelete.setTag(position);
        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                expInterface.delete(items.get(pos).getId(), pos, "");
            }
        });

        buttonPay.setTag(position);
        buttonPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                if (items.get(pos).getPaid().equals("0")) {
                    context.startActivity(new Intent(context, PayActivity.class).putExtra("pay", items.get(pos).getAmount() + "").putExtra("id", items.get(pos).getId() + "").putExtra("type", "adv"));
                } else {
                    Toast.makeText(context, "Published", Toast.LENGTH_SHORT).show();
                }
            }
        });
        // returns the view for the current row
        return convertView;
    }

}

