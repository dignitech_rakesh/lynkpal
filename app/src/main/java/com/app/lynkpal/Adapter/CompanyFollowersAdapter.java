package com.app.lynkpal.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
/*import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;*/
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.lynkpal.Bean.CompanyFollowersBean;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.deleteFollowers;
import com.app.lynkpal.R;
import com.app.lynkpal.UserAndCompanyPageDetails;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 10/10/2017.
 */

public class CompanyFollowersAdapter extends BaseAdapter {

    public List<CompanyFollowersBean> data = new ArrayList<CompanyFollowersBean>();
    Context context;
    int Height;
    deleteFollowers followers;

    public CompanyFollowersAdapter(Context context, List<CompanyFollowersBean> tablelist, int height, deleteFollowers followers) {
        this.data = tablelist;
        this.context = context;
        this.Height = height;
        this.followers = followers;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.item_company_followers, parent, false);

        } else {
            row = (View) convertView;
        }


        final ImageView imageuserAvtar = (ImageView) row.findViewById(R.id.imageuserAvtar);
        TextView txtName = (TextView) row.findViewById(R.id.name);
        TextView txtRemove = (TextView) row.findViewById(R.id.txtRemove);
        //  LinearLayout linearLayout = row.findViewById(R.id.linMain);
        Typeface tf_bold = Typeface.createFromAsset(context.getAssets(), "Roboto-Bold.ttf");
        Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        Typeface tf_Med = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
        txtName.setTypeface(tf_bold);
        txtRemove.setTypeface(tf_bold);
        txtName.setText(data.get(position).getFullName());
        txtName.setTag(position);
        txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                context.startActivity(new Intent(context, UserAndCompanyPageDetails.class).putExtra("User_id", data.get(pos).getUserid() + ""));

            }
        });
        txtRemove.setTag(position);
        txtRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                followers.unfollow(data.get(position).getUserid(), pos);
            }
        });
        //  imageuserAvtar.getLayoutParams().height = (int) (Height / 8);
        String image = data.get(position).getProfilePic();
        if (image != null && !image.equals("null"))
        {
            Glide.with(context).load(WebApis.userProfileImage + image).into(imageuserAvtar);
           /* Glide.with(context).load(WebApis.userProfileImage + image).asBitmap().placeholder(R.drawable.image).error(R.drawable.image).centerCrop().into(new BitmapImageViewTarget(imageuserAvtar) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    imageuserAvtar.setImageDrawable(circularBitmapDrawable);
                }
            });*/

        } else {
            imageuserAvtar.setImageResource(R.drawable.image);
        }
        return row;
    }


}
