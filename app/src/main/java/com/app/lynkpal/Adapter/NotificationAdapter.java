package com.app.lynkpal.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
/*import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;*/
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.lynkpal.Bean.NotificationBean;
import com.app.lynkpal.Helper.CircleImageView;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.deleteAndreadNotification;
import com.app.lynkpal.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    public View itemView;
    Context context;
    deleteAndreadNotification deleteAndreadNotification;
    private List<NotificationBean> notificationBeanList;

    public NotificationAdapter(Activity ctx, List<NotificationBean> notificationBeanList, deleteAndreadNotification deleteAndreadNotification) {
        this.notificationBeanList = notificationBeanList;
        this.context = ctx;
        this.deleteAndreadNotification = deleteAndreadNotification;

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final NotificationBean notificationBean = notificationBeanList.get(position);

        if (notificationBean.getProfile_photo() != null && !notificationBean.getProfile_photo().equals("null"))
        {
            Glide.with(context).load(WebApis.userProfileImage + notificationBean.getProfile_photo()).into(holder.userImage);
           /* Glide.with(context).load(WebApis.userProfileImage + notificationBean.getProfile_photo()).asBitmap().placeholder(R.drawable.image).error(R.drawable.image)*//*.centerCrop()*//*.into(new BitmapImageViewTarget(holder.userImage) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    holder.userImage.setImageDrawable(circularBitmapDrawable);
                }
            });*/
            // Glide.with(context).load(notificationBean.getProfile_photo()).placeholder(R.drawable.image).error(R.drawable.image).into(holder.userImage);
        } else {
            holder.userImage.setImageResource(R.drawable.image);
        }
        if (notificationBean.getStatus().equals("read")) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#ffffff"));

        } else {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#D3D3D3"));
        }
        if (notificationBean.getText() != null) {
            holder.text.setText(notificationBean.getText());
        }
        if (notificationBean.getTime() != null) {
            holder.time.setText(notificationBean.getTime());
        }

        holder.imgDelete.setTag(position);
        holder.imgDelete.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        int pos = (int) view.getTag();
                        deleteAndreadNotification.readANDdelete("D", pos);
                    }
                }
        );
        holder.relativeLayout.setTag(position);
        holder.relativeLayout.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        int pos = (int) view.getTag();
                        deleteAndreadNotification.readANDdelete("R", pos);
                    }
                }
        );

    }

    @Override
    public int getItemCount() {
        return notificationBeanList.size();
    }

    @Override
    public long getItemId(int position) {
        setHasStableIds(true);
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView text, time;
        public CircleImageView userImage;
        public RelativeLayout relativeLayout;
        ImageView imgDelete;


        public MyViewHolder(View view) {
            super(view);
            text = (TextView) view.findViewById(R.id.user_name);
            time = (TextView) view.findViewById(R.id.txtTime);
            userImage = (CircleImageView) view.findViewById(R.id.user_image);
            imgDelete = (ImageView) view.findViewById(R.id.imgDelete);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.relative_notification);
            Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
            Typeface tf_med = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
            Typeface tf_bold = Typeface.createFromAsset(context.getAssets(), "Roboto-Bold.ttf");
            time.setTypeface(tf_reg);
            text.setTypeface(tf_bold);
        }
    }
}
