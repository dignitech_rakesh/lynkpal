package com.app.lynkpal.Adapter;



import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
/*import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;*/
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.lynkpal.Bean.CompanyFollowersBean;
import com.app.lynkpal.Helper.CircleImageView;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.deleteFollowers;
import com.app.lynkpal.R;
import com.app.lynkpal.UserAndCompanyPageDetails;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 10/10/2017.
 */

public class UserFollowerAdapter extends BaseAdapter {

    public List<CompanyFollowersBean> data = new ArrayList<CompanyFollowersBean>();
    Context context;
    int Height;
    private static String TAG = UserFollowerAdapter.class.getSimpleName();

    public UserFollowerAdapter(Context context, List<CompanyFollowersBean> tablelist, int height){
        this.data = tablelist;
        this.context = context;
        this.Height = height;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.user_follower, parent, false);

        } else {
            row = (View) convertView;
        }


        final CircleImageView imageuserAvtar = row.findViewById(R.id.imageuserAvtar);
        TextView txtName = (TextView) row.findViewById(R.id.name);
        TextView txtRemove = (TextView) row.findViewById(R.id.txtRemove);
        //  LinearLayout linearLayout = row.findViewById(R.id.linMain);
        Typeface tf_bold = Typeface.createFromAsset(context.getAssets(), "Roboto-Bold.ttf");
        Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        Typeface tf_Med = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
        txtName.setTypeface(tf_bold);
        txtRemove.setTypeface(tf_bold);
        txtName.setText(data.get(position).getFullName());
        txtName.setTag(position);
        txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                context.startActivity(new Intent(context, UserAndCompanyPageDetails.class).putExtra("User_id", data.get(pos).getUserid() + ""));

            }
        });
        txtRemove.setTag(position);

        //  imageuserAvtar.getLayoutParams().height = (int) (Height / 8);
        String image = data.get(position).getProfilePic();
        CompanyFollowersBean companyFollowersBean = data.get(position);
        if (!image.equals("null"))
        {
            Log.d(TAG, "getView: Image");
            Picasso.with(context).load(WebApis.userProfileImage + companyFollowersBean.getProfilePic()).into(imageuserAvtar);

        } else
            {
                Log.d(TAG, "getView: No Image");
            imageuserAvtar.setImageResource(R.drawable.image);
        }
        return row;
    }


}
