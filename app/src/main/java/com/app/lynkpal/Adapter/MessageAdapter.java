package com.app.lynkpal.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.lynkpal.Bean.MessageBean;
import com.app.lynkpal.Helper.CircleImageView;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.MarkRead;
import com.app.lynkpal.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

/**
 * Created by user on 8/26/2017.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MyViewHolder> {

    public View itemView;
    Context context;
    private List<MessageBean> notificationBeanList;
    MarkRead markRead;
    public MessageAdapter(Activity ctx, List<MessageBean> notificationBeanList ,MarkRead markRead) {
        this.notificationBeanList = notificationBeanList;
        this.context = ctx;
        this.markRead = markRead;

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final MessageBean notificationBean = notificationBeanList.get(position);

        if (notificationBean.getImage() != null && !notificationBean.getImage().equals("null")) {
            Log.e("show", WebApis.userProfileImage+notificationBean.getImage());
            try {

                Glide.with(context).load(WebApis.userProfileImage+notificationBean.getImage())/*.diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.image)*/.into(holder.userImage); /* Picasso.with(context).load(WebApis.userProfileImage + notificationBean.getImage())

                        .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(holder.userImage);*/
            }
            catch (Exception e)
            {
                Log.e("show", e.toString());
            }
            //Glide.with(context).load(WebApis.userProfileImage+notificationBean.getImage()).into(holder.userImage);
        } else {
            holder.userImage.setImageResource(R.drawable.image);
        }
        if (notificationBean.getStatus().equals("read")) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#ffffff"));

        } else {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#D3D3D3"));
        }
        if (notificationBean.getText() != null) {
            holder.txtMsg.setText(notificationBean.getText());
        }
        if (notificationBean.getName() != null) {
            holder.name.setText(notificationBean.getName());
        }
        if (notificationBean.getTime() != null) {
            holder.time.setText(notificationBean.getTime());
        }
        holder.relativeLayout.setTag(position);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                markRead.Mark(pos);
            }
        });

    }

    @Override
    public int getItemCount() {
        return notificationBeanList.size();
    }

    @Override
    public long getItemId(int position) {
        setHasStableIds(true);
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, time, txtMsg;
        public CircleImageView userImage;
        public RelativeLayout relativeLayout;


        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.user_name);
            time = (TextView) view.findViewById(R.id.txtTime);
            txtMsg = (TextView) view.findViewById(R.id.txtMsg);
            userImage = (CircleImageView) view.findViewById(R.id.user_image);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.relative_notification);
            Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
            Typeface tf_med = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
            Typeface tf_bold = Typeface.createFromAsset(context.getAssets(), "Roboto-Bold.ttf");
            txtMsg.setTypeface(tf_reg);
            time.setTypeface(tf_med);
            name.setTypeface(tf_bold);
        }
    }
}
