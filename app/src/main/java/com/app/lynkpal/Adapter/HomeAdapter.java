package com.app.lynkpal.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Typeface;
/*import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;*/
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Bean.PostBean;
import com.app.lynkpal.CmntReplyActivity;
import com.app.lynkpal.CommentActivity;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.OpenFragment;
import com.app.lynkpal.Interface.ShareListener;
import com.app.lynkpal.Interface.deletePost;
import com.app.lynkpal.Interface.showImagePopup;
import com.app.lynkpal.Interface.showVideoPopup;
import com.app.lynkpal.LoginActivity;
import com.app.lynkpal.R;
import com.app.lynkpal.UserAndCompanyPageDetails;
import com.app.lynkpal.UserDetailsActivity;
import com.app.lynkpal.VideoActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;


import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by user on 8/26/2017.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> implements Html.ImageGetter {

    private static final String TAG = HomeAdapter.class.getSimpleName();
    String uid;
    deletePost deletePost;
    showImagePopup showImagePopup;
    showVideoPopup showVideoPopup;
    private List<PostBean> postList;
    private Context context;
    private String postId = "";
    private boolean status = true;
    private String postID = "";
    private String userImage = "";
    private OpenFragment openFragment;
    ShareListener mShareListener;
    SharedPreferences sharedPreference;

    MyViewHolder setTextHolder;
    int textPos;

    public HomeAdapter(Context ctx, List<PostBean> postBeen, deletePost deletePost, showImagePopup showImagePopup,showVideoPopup showVideoPopup, String userImage,OpenFragment openFragment,ShareListener shareListener) {
        this.postList = postBeen;
        this.context = ctx;
        this.deletePost = deletePost;
        this.userImage = userImage;
        this.showImagePopup = showImagePopup;
        this.showVideoPopup = showVideoPopup;
        this.openFragment = openFragment;
        this.mShareListener = shareListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.post_items, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
      try {
          final PostBean postBean = postList.get(position);
          //Log.d("catch",position+"");
         if (postBean.getAdv_id()==-1){

             holder.cardViewMain.setVisibility(View.VISIBLE);
             holder.cardViewAdv.setVisibility(View.GONE);

             final int suserid = postBean.getUser();
             //setTextHolder=new MyViewHolder();
             setTextHolder = holder;


             holder.txtCommentsCount.setTag(position);
             holder.txtCommentsCount.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     if (LoginActivity.isNetworkConnected(context)) {
                         int pos = (int) v.getTag();
                         PostBean postBean = postList.get(pos);
                         //Intent intent = new Intent(context, CommentActivity.class);
                         Intent intent = new Intent(context, CmntReplyActivity.class);
                         Log.e("postBean.getId()", String.valueOf(postBean.getId()));
                         intent.putExtra("id_", String.valueOf(postBean.getId()));
              /*  intent.putExtra("like", postBean.getLikes_count());
                intent.putExtra("islike", postBean.getLike());
                intent.putExtra("comment", postBean.getComments_count());
                intent.putExtra("created", postBean.getCreated());
                intent.putExtra("post_text", postBean.getDesc());
                intent.putExtra("file", postBean.getFile());
                intent.putExtra("user_name", postBean.getFullname());
                intent.putExtra("profile_pic", postBean.getProfile_pic());
                intent.putExtra("user_profile_pic", userImage);
                intent.putExtra("user_id", postBean.getUser().toString());*/
                         context.startActivity(intent);
                     } else {
                         Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                     }
                 }
             });

             holder.txtLikesCount.setText(postBean.getLikes_count());
             holder.txtCommentsCount.setText(postBean.getComments_count());
             holder.profileImage.setImageResource(R.drawable.image);
             if (postBean.getCreated().toLowerCase().equals("0 minutes ago") || postBean.getCreated().toLowerCase().equals("in 0 minutes")) {
                 holder.tvTimestamp.setText("Just Now");
             } else {
                 holder.tvTimestamp.setText(postBean.getCreated());

             }
             if (postBean.getUser().toString().equals(Constant.user_id)) {
                 holder.ic_more.setVisibility(View.VISIBLE);
                 holder.ic_more.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View view) {
                         openFragment.clicktodelete(postBean.getId());
                         Log.d(TAG, "deletePost " + postBean.getId());
                     }
                 });
             } else {

                 holder.ic_more.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View v) {
                         int pos = (int) v.getTag();
                        /*if (postList.get(pos).getDelete())
                        {
                            postList.get(pos).setDelete(false);





                            notifyDataSetChanged();
                        } else {
                            postList.get(pos).setDelete(true);
                            notifyDataSetChanged();
                        }*/


                         openFragment.clicktoopen(postBean.getId(), postBean.getUser());
                         Log.d(TAG, "AdapterListener " + postBean.getUser());


                     }
                 });

                 // holder.ic_more.setVisibility(View.GONE);

                 Log.d(TAG, "Hide dot " + postBean.getParent_post());

             }
             if (postBean.getDesc().equals("null") || postBean.getDesc().trim().equals("")) {
                 holder.postText.setVisibility(View.GONE);
             } else {

                 holder.postText.setVisibility(View.VISIBLE);
                 // holder.postText.setText(postBean.getDesc());
                 try {
                     // html image getter
                     // Spanned spanned = Html.fromHtml(postBean.getDesc(), this, null);
                     // holder.postText.setText(spanned);

               /* URLImageParser p = new URLImageParser(holder.postText, context);
                Spanned htmlSpan = Html.fromHtml(postBean.getDesc(), p, null);
                holder.postText.setText(htmlSpan);*/

                     holder.postText.setText(Html.fromHtml(postBean.getDesc()));
                 } catch (Exception e) {
                     holder.postText.setText(postBean.getDesc());
                 }
             }

             if (!postBean.getFullname().equals("null")) {
           /* if (!postBean.getOwner_fullname().equals("null")) {
                holder.userName.setText("<b>"+postBean.getFullname()+"</b> shared "+postBean.getOwner_fullname()+"'s post ");
            }
            else
            {*/
                 holder.userName.setText(postBean.getFullname());
                 /* }*/

             }


             holder.userName.setTag(position);
             holder.userName.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {
                     context.startActivity(new Intent(context, UserAndCompanyPageDetails.class).putExtra("User_id", postList.get(position).getUser() + ""));
                 }
             });
             // holder.profileImage.setTag(position);
             holder.profileImage.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     // int pos= (int) v.getTag();
                     Log.e("userid", postList.get(position).getUser() + "<<");
                     if (ApplicationGlobles.isConnectingToInternet((Activity) context)) {
                         context.startActivity(new Intent(context, UserDetailsActivity.class).putExtra("User_id", postList.get(position).getUser() + ""));

                     } else {
                         Toast.makeText(context, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
                     }

                /*   if (uid.equals(String.valueOf(suserid)))
                {
                  //  BaseActivity.viewPager.setCurrentItem(1);
                }
                else {
                   // context.startActivity(new Intent(context, BusinessActivity.class).putExtra("id", String.valueOf(suserid)).putExtra("authKey", HomeFragment.authKey));
                } */
                 }
             });

             holder.imgSendComment.setTag(position);
             holder.imgSendComment.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     int pos = (int) v.getTag();
                     String commentText = holder.edtComment.getText().toString();
                     if (commentText.trim().length() <= 0) {
                         Toast.makeText(context, "Please Write Something..", Toast.LENGTH_SHORT).show();
                     } else {
                         postList.get(pos).setComment(false);
                         holder.edtComment.setText("");
                         notifyDataSetChanged();
                         Comment(String.valueOf(postList.get(pos).getId()), holder.txtCommentsCount, commentText, pos);
                     }

                 }
             });

             // if (!postBean.getFile().equals("null")) {    //org
             if (!postBean.getOrg_file().equals("null")) {
                 if (postBean.getType().equals("image")) {
                     holder.linearLayout.setVisibility(View.VISIBLE);
                     holder.postimageView.setVisibility(View.VISIBLE);
                     holder.play_icon.setVisibility(View.GONE);
                     //Glide.with(context).load(WebApis.postImage + postBean.getFile()).into(holder.postimageView);  //org

                     Glide.with(context).load(WebApis.postImage + postBean.getOrg_file()).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(holder.postimageView);
                     // .placeholder(R.drawable.noimage).override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL) .into(holder.postimageView);
                     //Glide.with(context).load(postBean.getFile()).into(holder.postimageView);  //dwmloaded
                     //.centerCrop().error(R.drawable.noimage).into(holder.postimageView);
                 } else {
                     holder.linearLayout.setVisibility(View.VISIBLE);
                     holder.postimageView.setVisibility(View.VISIBLE);
                     holder.play_icon.setVisibility(View.VISIBLE);
                     //Glide.with(context).load(WebApis.postImage + postBean.getFrame_name()).into(holder.postimageView);//centerCrop().error(R.drawable.noimage).into(holder.postimageView);  //org

                     Glide.with(context).load(WebApis.postImage + postBean.getOrg_frame_name()).diskCacheStrategy(DiskCacheStrategy.SOURCE).error(R.drawable.noimage).into(holder.postimageView);//centerCrop().error(R.drawable.noimage).into(holder.postimageView);
                     // Glide.with(context).load(postBean.getFrame_name()).placeholder(R.drawable.video_placeholder).into(holder.postimageView);//centerCrop().error(R.drawable.noimage).into(holder.postimageView);  //dwmloaded

                 }
             } else {
                 holder.linearLayout.setVisibility(View.GONE);
                 holder.postimageView.setVisibility(View.GONE);
             }

             holder.postimageView.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {

                     Log.e("cLLL", "GGGG");

                     if (postBean.getType().equalsIgnoreCase("image")) {
                         Log.e("image", postBean.getFile());
                         Log.d(TAG, "onClick: posted image " + postBean.getFile());
                         //showImagePopup.image(WebApis.postImage + postBean.getFile());  //org


                         //showImagePopup.image(WebApis.postImage + postBean.getOrg_file());
                         //showImagePopup.image(postBean.getFile());   //dwmloaded

                         if (ApplicationGlobles.isConnectingToInternet((Activity) context)) {
                             showImagePopup.image(WebApis.postImage + postBean.getOrg_file());

                         } else {
                             Toast.makeText(context, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
                         }
                     } else {

                         Log.e("video", postBean.getFile());
                         //context.startActivity(new Intent(context, VideoActivity.class).putExtra("url", WebApis.postImage + postBean.getFile()));  //org


                         //context.startActivity(new Intent(context, VideoActivity.class).putExtra("url",WebApis.postImage +postBean.getOrg_file()));
                         // context.startActivity(new Intent(context, VideoActivity.class).putExtra("url", postBean.getFile()));  //dwmloaded
                         if (ApplicationGlobles.isConnectingToInternet((Activity) context)) {
                             context.startActivity(new Intent(context, VideoActivity.class).putExtra("url",WebApis.postImage +postBean.getOrg_file()));

                             //showVideoPopup.videoPopup(WebApis.postImage + postBean.getOrg_file());
                         } else {
                             Toast.makeText(context, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
                         }


                     }
                 }
             });
             holder.post_image_share.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {
                     // showImagePopup.image(WebApis.postImage + postBean.getFile());
                     if (postBean.getType().equals("image")) {
                         //showImagePopup.image(WebApis.postImage + postBean.getFile());   //org

                         //showImagePopup.image(postBean.getFile());

                         if (ApplicationGlobles.isConnectingToInternet((Activity) context)) {
                             showImagePopup.image(postBean.getOrg_file());

                         } else {
                             Toast.makeText(context, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
                         }
                     } else {
                         // context.startActivity(new Intent(context, VideoActivity.class).putExtra("url", WebApis.postImage + postBean.getFile()));   //org

                         //context.startActivity(new Intent(context, VideoActivity.class).putExtra("url", postBean.getFile()));//dwnloaded

                         if (ApplicationGlobles.isConnectingToInternet((Activity) context)) {
                             context.startActivity(new Intent(context, VideoActivity.class).putExtra("url",WebApis.postImage +postBean.getOrg_file()));

                         } else {
                             Toast.makeText(context, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
                         }

                     }
                 }
             });

             if (!postBean.getOwner_fullname().equals("null")) {
                 holder.linear_share_layout.setVisibility(View.VISIBLE);
                 holder.share.setVisibility(View.GONE);

                 if (!postBean.getFile().equals("null")) {
                     if (postBean.getType().equals("image")) {
                         holder.linear_mage_layout_share.setVisibility(View.VISIBLE);
                         holder.linearLayout.setVisibility(View.GONE);
                         holder.postimageView.setVisibility(View.GONE);
                         holder.play_icon_share.setVisibility(View.GONE);
                         //Glide.with(context).load(WebApis.postImage + postBean.getFile())./*centerCrop().error(R.drawable.noimage).*/into(holder.post_image_share);  //org

                         Glide.with(context).load(WebApis.postImage + postBean.getOrg_file()).diskCacheStrategy(DiskCacheStrategy.SOURCE)./*centerCrop().error(R.drawable.noimage).*/into(holder.post_image_share);
                         // Glide.with(context).load(postBean.getFile())./*centerCrop().error(R.drawable.noimage).*/into(holder.post_image_share);  //dwmloaded
                     } else {
                         holder.linear_mage_layout_share.setVisibility(View.VISIBLE);
                         holder.linearLayout.setVisibility(View.GONE);
                         holder.postimageView.setVisibility(View.GONE);
                         holder.play_icon_share.setVisibility(View.VISIBLE);
                         //Glide.with(context).load(WebApis.postImage + postBean.getFrame_name())./*centerCrop().error(R.drawable.noimage).*/into(holder.post_image_share);  //org

                         Glide.with(context).load(WebApis.postImage + postBean.getOrg_frame_name()).diskCacheStrategy(DiskCacheStrategy.SOURCE)./*centerCrop().error(R.drawable.noimage).*/into(holder.post_image_share);
                         //Glide.with(context).load(postBean.getFrame_name())./*centerCrop().error(R.drawable.noimage).*/into(holder.post_image_share);  //dwmloaded
                     }

                 } else {
                     holder.linear_mage_layout_share.setVisibility(View.GONE);
                     holder.post_image_share.setVisibility(View.GONE);
                 }
                 // holder.postimageView.setVisibility(View.VISIBLE);


             } else {
                 holder.linear_share_layout.setVisibility(View.GONE);
                 holder.share.setVisibility(View.VISIBLE);
                 //holder.postimageView.setVisibility(View.GONE);
             }

             // if (!postBean.getProfile_pic().equals("null")&& !postBean.getProfile_pic().equals("")) {          //org
             if (!postBean.getOrg_profile_pic().equals("null")&& !postBean.getOrg_profile_pic().equals("")) {
                 holder.profileImage.setFocusable(true);
                 Log.d(TAG, "getImage " + postBean.getProfile_pic());
                 //Glide.with(context).load(WebApis.userProfileImage + postBean.getProfile_pic()).into(holder.profileImage); //org


                 Glide.with(context).load(WebApis.userProfileImage + postBean.getOrg_profile_pic()).diskCacheStrategy(DiskCacheStrategy.SOURCE).error(R.drawable.image).into(holder.profileImage);
                 //Glide.with(context).load(postBean.getProfile_pic()).into(holder.profileImage);  //dwmloaded
          /*  Glide.with(context).load(WebApis.userProfileImage + postBean.getProfile_pic()).asBitmap().placeholder(R.drawable.image).error(R.drawable.image).centerCrop().into(new BitmapImageViewTarget(holder.profileImage) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    holder.profileImage.setImageDrawable(circularBitmapDrawable);
                }
            });
*//*
            Glide.with(context).load(WebApis.userProfileImage + userImage).asBitmap().placeholder(R.drawable.image).error(R.drawable.image).centerCrop().into(new BitmapImageViewTarget(holder.commentProfile) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    holder.commentProfile.setImageDrawable(circularBitmapDrawable);
                }
            });*/
                 // notifyDataSetChanged();
                 //  Glide.with(context).load(WebApis.userProfileImage+postBean.getProfile_pic()).placeholder(R.drawable.image).error(R.drawable.image).into(holder.profileImage);
                 if (userImage == null) {
                     holder.commentProfile.setImageResource(R.drawable.image);
                 } else {
                     // Glide.with(context).load(WebApis.userProfileImage + userImage).into(holder.commentProfile);  //org

                     Glide.with(context).load(userImage).into(holder.commentProfile);
                 }
                 Log.d(TAG, "onBindViewHolder: UserImage " + userImage);
             } else {

                 Log.d(TAG, "noImage " + postBean.getProfile_pic());
                 if (holder.comment_layout.getVisibility() == View.GONE) {
                     Log.d(TAG, "onBindViewHolder: fetching data");
                     holder.comment_layout.setVisibility(View.VISIBLE);
                     holder.commentProfile.setVisibility(View.VISIBLE);
                     holder.profileImage.setImageDrawable(context.getResources().getDrawable(R.drawable.image));
                     holder.commentProfile.setImageResource(R.drawable.image);
                 }

             }

             // if (!postBean.getOwner_profile_pic().equals("null")) {     //org
             if (!postBean.getOrg_owner_profile_pic().equals("null")) {
                 holder.profileImageShare.setFocusable(true);
                 //Glide.with(context).load(WebApis.userProfileImage + postBean.getOwner_profile_pic()).into(holder.profileImageShare); //org


                 Glide.with(context).load(WebApis.userProfileImage + postBean.getOrg_owner_profile_pic()).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(holder.profileImageShare);
                 //Glide.with(context).load(postBean.getOwner_profile_pic()).into(holder.profileImageShare);   //dwmloaded
           /* Glide.with(context).load(WebApis.userProfileImage + postBean.getOwner_profile_pic()).asBitmap().placeholder(R.drawable.image).error(R.drawable.image).centerCrop().into(new BitmapImageViewTarget(holder.profileImageShare) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    holder.profileImageShare.setImageDrawable(circularBitmapDrawable);
                }
            });*/
                 // notifyDataSetChanged();
                 //  Glide.with(context).load(WebApis.userProfileImage+postBean.getProfile_pic()).placeholder(R.drawable.image).error(R.drawable.image).into(holder.profileImage);
             } else {
                 holder.profileImageShare.setImageDrawable(context.getResources().getDrawable(R.drawable.image));
             }
             if (!postBean.getOwner_fullname().equals("null")) {
                 holder.username_share.setText(postBean.getOwner_fullname());
             }
             if (!postBean.getSharer_msg().trim().equals("")) {
                 if (postBean.getDesc().equals("null")) {
                     holder.post_text_share.setVisibility(View.GONE);
                 } else {
                     holder.post_text_share.setVisibility(View.VISIBLE);
                     // holder.post_text_share.setText(postBean.getDesc());
                     try {
                         holder.post_text_share.setText(Html.fromHtml(postBean.getDesc()));
                     } catch (Exception e) {
                         holder.post_text_share.setText(postBean.getDesc());
                     }
                 }
                 holder.postText.setVisibility(View.VISIBLE);
                 try {
                     holder.postText.setText(Html.fromHtml(postBean.getSharer_msg()));
                 } catch (Exception e) {
                     holder.postText.setText(postBean.getSharer_msg());
                 }
             }
             Log.d(TAG, "onBindViewHolder: " + postBean.getCreated());
             if (postBean.getCreated().toLowerCase().equals("0 minutes ago") || postBean.getCreated().toLowerCase().equals("in 0 minutes")) {
                 holder.time_stamp_share.setText("Just Now");
             } else {
                 holder.time_stamp_share.setText(postBean.getCreated());

             }
             if (postList.get(position).getLike()) {
                 holder.like.setImageResource(R.drawable.thumb_like);
             } else {
                 holder.like.setImageResource(R.drawable.thumb_unlike);
             }

             if (postList.get(position).getComment()) {
                 holder.comment_layout.setVisibility(View.VISIBLE);
             } else {
                 holder.comment_layout.setVisibility(View.GONE);
             }
             if (postList.get(position).getDelete()) {
                 holder.deletePopup.setVisibility(View.VISIBLE);
             } else {
                 holder.deletePopup.setVisibility(View.GONE);
             }
             holder.like.setTag(position);
             holder.like.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     if (LoginActivity.isNetworkConnected(context)) {
                         int pos = (int) v.getTag();
                         if (postList.get(pos).getLike()) {
                             postList.get(pos).setLike(false);
                             int count = Integer.parseInt(postList.get(pos).getLikes_count());
                             count = count - 1;
                             postList.get(pos).setLikes_count(String.valueOf(count));
                             Like(String.valueOf(postList.get(pos).getId()));
                             // notifyDataSetChanged();
                             notifyItemChanged(position);
                         } else {
                             postList.get(pos).setLike(true);
                             int count = Integer.parseInt(postList.get(pos).getLikes_count());
                             count = count + 1;
                             postList.get(pos).setLikes_count(String.valueOf(count));
                             Like(String.valueOf(postList.get(pos).getId()));
                             // notifyDataSetChanged();
                             notifyItemChanged(position);

                         }

                     } else {
                         Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                     }

                 }
             });
             holder.deletePopup.setTag(position);
             holder.deletePopup.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     int pos = (int) v.getTag();
                     postList.get(pos).setLike(false);
                     notifyDataSetChanged();
                     // delete();
                     showPopup(String.valueOf(postList.get(pos).getId()), pos);
                 }
             });
             holder.ic_more.setTag(position);

             holder.comment.setTag(position);
             holder.comment.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     int pos = (int) v.getTag();
                     if (LoginActivity.isNetworkConnected(context)) {
                         // Toast.makeText(context,"Comment Please",Toast.LENGTH_SHORT).show();

                         if (postList.get(pos).getComment()) {
                             postList.get(pos).setComment(false);
                             notifyDataSetChanged();
                             Log.d(TAG, "onClick: truec");
                         } else {
                             postList.get(pos).setComment(true);
                             notifyDataSetChanged();
                             Log.d(TAG, "onClick: falsec");
                         }
                     } else {
                         Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                     }

                 }
             });
             holder.cardViewMain.setTag(position);
             holder.cardViewMain.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     int pos = (int) v.getTag();
                     postList.get(pos).setDelete(false);
                     notifyDataSetChanged();
                 }
             });
             holder.share.setTag(position);
             holder.share.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     int pos = (int) v.getTag();
                     int id = postList.get(pos).getId();
                     showPopup(id);
/*
                if(sharedClickListener!=null)
                {
                    postId = String.valueOf(postList.get(position).getId());
                    String shareMessage=postBean.getSharer_msg();
                    if(!postBean.getParent_post().equals("null"))
                    {
                        postID =postList.get(position).getParent_post();
                    }
                    else {
                        postID = postId;
                    }
                    sharedClickListener.setSharedClickListener(postID,shareMessage,v);
                }*/
                 }
             });

         }else{

             sharedPreference = (context).getSharedPreferences(Constant.PREFS_NAME, 0);

             holder.cardViewMain.setVisibility(View.GONE);
             holder.cardViewAdv.setVisibility(View.VISIBLE);

             Glide.with(context).load(WebApis.BaseURL + postBean.getAdv_image()).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.noimage).into(holder.adv_img);
             holder.adv_tv.setText(postBean.getAdv_name());

             holder.cardViewAdv.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {

                     if (LoginActivity.isNetworkConnected(context)) {

                         try {
                            // String url = postBean.getAdv_link();
                             String url = "https://www.lynkpal.com/add_click/"+postBean.getAdv_id()+"?user="+sharedPreference.getString("userid","");
                             Log.d("AdvertisementUrl",url);
                             if (url.contains("https://") || url.contains("http://")) {
                                 Intent i = new Intent(Intent.ACTION_VIEW);
                                 i.setData(Uri.parse(url));
                                 context.startActivity(i);
                             }
                         }catch (Exception e){

                         }
                     }else{
                         Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                     }
                 }
             });

         }

      }catch (Exception e){
          e.printStackTrace();
      }

    }

    public void showPopup(final int id) {
        LayoutInflater layoutInflater
                = (LayoutInflater) ((Activity) context).getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_share, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        final EditText textText = (EditText) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(context.getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = textText.getText().toString();
                if (text.trim().length() > 0)
                {
                   mShareListener.clicktoShare(id,text);

                    popupWindow.dismiss();
                } else {
                    Toast.makeText(context, "Please Write Something...", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    @Override
    public long getItemId(int position) {
        setHasStableIds(true);
        return position;

        //PostBean postBean = postList.get(position);
        //return postBean.getId();
    }


    public void Like(String postID) {
        //final LoaderDiloag loaderDiloag = new LoaderDiloag(context);
        if (ApplicationGlobles.isConnectingToInternet((Activity) context)) {
            //loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", Constant.token + "");
            Request request = new Request.Builder()
                    .url(WebApis.LikePOST + postID)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (context == null)
                            return;
                        //  loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("respons", jsonData + "");
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //  loaderDiloag.dismissDiloag();
                            }
                        });
                    }
                });

            } catch (Exception e) {
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(context, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void Comment(String postID, final TextView textView, String Comment, final int pos) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(context);
        if (ApplicationGlobles.isConnectingToInternet((Activity) context)) {
            loaderDiloag.displayDiloag();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, "{\r\n\"message\" : \"" + Comment + "\"\r\n}");
            Request request = new Request.Builder()
                    .url(WebApis.CommentPOST + postID + "/")
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("postman-token", "ae0bf6c3-6ef6-bc06-6ca6-1475b65e1909")
                    .build();
            OkHttpClient client = new OkHttpClient();
            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (context == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        final String jsonData = response.body().string();
                        Log.e("respons", jsonData + "");
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(jsonData);
                                    String success = jsonObject.getString("status");
                                    final String message = jsonObject.getString("message");
                                    if (success.equals("success")) {
                                        postList.get(pos).setComments_count(jsonObject.getInt("comment_count") + " Comments");
                                        Toast.makeText(context, message + "", Toast.LENGTH_SHORT).show();
                                        notifyDataSetChanged();
                                    }
                                } catch (Exception e) {

                                }
                                loaderDiloag.dismissDiloag();
                            }
                        });
                    }
                });

            } catch (Exception e) {
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(context, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void showPopup(final String id, final int pos) {
        LayoutInflater layoutInflater
                = (LayoutInflater) context
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_group_join_leave, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        final TextView textText = (TextView) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(context.getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        textTitle.setText("Delete Post");
        textText.setText("Are you sure, you want to delete Post?");
        // textText.setText("Are you sure, you want to leave " + '"' + text + '"' + " Group?");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delete(id, pos);
                popupWindow.dismiss();

            }
        });
    }

    public void delete(final String postID, final int pos) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(context);
        if (ApplicationGlobles.isConnectingToInternet((Activity) context)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", Constant.token + "");
            Request request = new Request.Builder()
                    .url(WebApis.DeletePOST + postID)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (context == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("respons", jsonData + "");
                        try {
                            final JSONObject jsonObject = new JSONObject(jsonData);
                            final String success = jsonObject.getString("status");
                            final String message = jsonObject.getString("message");
                            ((Activity) context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (success.equals("success")) {
                                        deletePost.delete(pos);
                                        Toast.makeText(context, message + "", Toast.LENGTH_SHORT).show();

                                        notifyDataSetChanged();
                                    }
                                    loaderDiloag.dismissDiloag();
                                }
                            });
                        } catch (Exception e) {
                            ((Activity) context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loaderDiloag.dismissDiloag();
                                }
                            });
                        }
                    }
                });

            } catch (Exception e) {
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(context, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView postText, post_text_share, userName, username_share, txtLikesCount, txtCommentsCount, currentTimeStamp;
        public ImageView postimageView, play_icon, play_icon_share,adv_img;
        public ImageView profileImage, profileImageShare, commentProfile;
        public ImageView comment, like, share, imgSendComment, ic_more, post_image_share;
        public LinearLayout linearLayout, linear_share_layout, linear_mage_layout_share;
        public TextView tvTimestamp, time_stamp_share, deletePopup,adv_tv;
        RelativeLayout comment_layout;
        EditText edtComment;
        CardView cardViewMain;
        CardView cardViewAdv;
        RelativeLayout mParent;
        LinearLayout mReportChild;

        public MyViewHolder(View view) {
            super(view);


            mParent = view.findViewById(R.id.report_layout);
            mReportChild = view.findViewById(R.id.report_child_layout);
            txtLikesCount = (TextView) view.findViewById(R.id.like_count);
            txtCommentsCount = (TextView) view.findViewById(R.id.comment_count);
            postText = (TextView) view.findViewById(R.id.post_text);
            post_text_share = (TextView) view.findViewById(R.id.post_text_share);
            userName = (TextView) view.findViewById(R.id.username);
            username_share = (TextView) view.findViewById(R.id.username_share);
            deletePopup = (TextView) view.findViewById(R.id.deletePopup);
            profileImage = (ImageView) view.findViewById(R.id.profile_user_image);
            profileImageShare = (ImageView) view.findViewById(R.id.profile_user_image_share);
            post_image_share = (ImageView) view.findViewById(R.id.post_image_share);
            play_icon_share = (ImageView) view.findViewById(R.id.play_icon_share);
            //  profileImage.setImageResource(R.drawable.image);
            postimageView = (ImageView) view.findViewById(R.id.post_image);
            play_icon = (ImageView) view.findViewById(R.id.play_icon);
            commentProfile = (ImageView) view.findViewById(R.id.user_image_post);
            cardViewMain = (CardView) view.findViewById(R.id.cardViewMain);

            cardViewAdv = (CardView) view.findViewById(R.id.cardViewAdv);
            adv_img = (ImageView) view.findViewById(R.id.adv_img);
            adv_tv = (TextView) view.findViewById(R.id.adv_tv);

            //   commentProfile.setImageResource(R.drawable.image);
            comment = (ImageView) view.findViewById(R.id.chat);
            like = (ImageView) view.findViewById(R.id.like);
            share = (ImageView) view.findViewById(R.id.share);
            imgSendComment = (ImageView) view.findViewById(R.id.sendComment);
            ic_more = (ImageView) view.findViewById(R.id.ic_more);
            edtComment = (EditText) view.findViewById(R.id.user_Comment);
            linearLayout = (LinearLayout) view.findViewById(R.id.linear_mage_layout);
            linear_share_layout = (LinearLayout) view.findViewById(R.id.linear_share_layout);
            linear_mage_layout_share = (LinearLayout) view.findViewById(R.id.linear_mage_layout_share);
            comment_layout = (RelativeLayout) view.findViewById(R.id.comment_layout);
            currentTimeStamp = (TextView) view.findViewById(R.id.time_stamp);
            tvTimestamp = (TextView) view.findViewById(R.id.time_stamp);
            time_stamp_share = (TextView) view.findViewById(R.id.time_stamp_share);
            Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
            Typeface tf_med = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
            Typeface tf_bold = Typeface.createFromAsset(context.getAssets(), "Roboto-Bold.ttf");
            userName.setTypeface(tf_bold);
            username_share.setTypeface(tf_bold);
            txtLikesCount.setTypeface(tf_reg);
            txtCommentsCount.setTypeface(tf_reg);
            postText.setTypeface(tf_med);
            post_text_share.setTypeface(tf_med);
            tvTimestamp.setTypeface(tf_med);
            time_stamp_share.setTypeface(tf_med);
            edtComment.setTypeface(tf_med);
            deletePopup.setTypeface(tf_bold);
        }
    }

    // html image getter
    @Override
    public Drawable getDrawable(String source) {
        LevelListDrawable d = new LevelListDrawable();
        Drawable empty = context.getResources().getDrawable(R.drawable.clock_icon);
        d.addLevel(0, 0, empty);
        d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());

        new LoadImage().execute(source, d);

        return d;
    }

    // html image getter
    class LoadImage extends AsyncTask<Object, Void, Bitmap> {

        private LevelListDrawable mDrawable;

        @Override
        protected Bitmap doInBackground(Object... params) {
            String source = (String) params[0];
            mDrawable = (LevelListDrawable) params[1];
            Log.d(TAG, "doInBackground " + source);
            try {
                InputStream is = new URL(source).openStream();
                return BitmapFactory.decodeStream(is);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Log.d(TAG, "onPostExecute drawable " + mDrawable);
            Log.d(TAG, "onPostExecute bitmap " + bitmap);
            if (bitmap != null) {
                BitmapDrawable d = new BitmapDrawable(bitmap);
                mDrawable.addLevel(1, 1, d);
                mDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
                mDrawable.setLevel(1);
                // i don't know yet a better way to refresh TextView
                // mTv.invalidate() doesn't work as expected
               /* CharSequence t = hol.getText();
                mTv.setText(t);*/
            }
        }
    }

     /* public TextView GetTextView(TextView textView){
        View view= holder.postText
          view.findViewById(R.id.post_text);
      }
    private void updateListItem(int position) {
        View view = layoutManager.findViewByPosition(position);
        ImageView medicineSelected = (ImageView) view.findViewById(R.id.medicine_selected);
        medicineSelected.setVisibility(View.VISIBLE);
        TextView orderQuantity = (TextView) view.findViewById(R.id.order_quantity);
        orderQuantity.setVisibility(View.VISIBLE);
        orderQuantity.setText(quantity + " packet added!");

        medicinesArrayAdapter.notifyItemChanged(position);
    }*/


    public class URLImageParser implements Html.ImageGetter {
        Context c;
        View container;

        /***
         * Construct the URLImageParser which will execute AsyncTask and refresh the container
         * @param t
         * @param c
         */
        public URLImageParser(View t, Context c) {
            this.c = c;
            this.container = t;
        }

        public Drawable getDrawable(String source) {
            URLDrawable urlDrawable = new URLDrawable();

            // get the actual source
            ImageGetterAsyncTask asyncTask =
                    new ImageGetterAsyncTask( urlDrawable);

            asyncTask.execute(source);

            // return reference to URLDrawable where I will change with actual image from
            // the src tag
            return urlDrawable;
        }

        public class ImageGetterAsyncTask extends AsyncTask<String, Void, Drawable>  {
            URLDrawable urlDrawable;

            public ImageGetterAsyncTask(URLDrawable d) {
                this.urlDrawable = d;
            }

            @Override
            protected Drawable doInBackground(String... params) {
                String source = params[0];
                return fetchDrawable(source);
            }

            @Override
            protected void onPostExecute(Drawable result) {
                // set the correct bound according to the result from HTTP call
                urlDrawable.setBounds(0, 0, 0 + result.getIntrinsicWidth(), 0
                        + result.getIntrinsicHeight());

                // change the reference of the current drawable to the result
                // from the HTTP call
                urlDrawable.drawable = result;

                // redraw the image by invalidating the container
                URLImageParser.this.container.invalidate();
            }

            /***
             * Get the Drawable from URL
             * @param urlString
             * @return
             */
            public Drawable fetchDrawable(String urlString) {
                try {
                    InputStream is = fetch(urlString);
                    Drawable drawable = Drawable.createFromStream(is, "src");
                    drawable.setBounds(0, 0, 0 + drawable.getIntrinsicWidth(), 0
                            + drawable.getIntrinsicHeight());
                    return drawable;
                } catch (Exception e) {
                    return null;
                }
            }

            private InputStream fetch(String urlString) throws MalformedURLException, IOException {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpGet request = new HttpGet(urlString);
                HttpResponse response = httpClient.execute(request);
                return response.getEntity().getContent();
            }
        }
    }


    public class URLDrawable extends BitmapDrawable {
        // the drawable that you need to set, you could set the initial drawing
        // with the loading image if you need to
        protected Drawable drawable;

        @Override
        public void draw(Canvas canvas) {
            // override the draw to facilitate refresh function later
            if(drawable != null) {
                drawable.draw(canvas);
            }
        }
    }
}
