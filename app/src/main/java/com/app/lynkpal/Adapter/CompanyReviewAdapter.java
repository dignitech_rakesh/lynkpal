package com.app.lynkpal.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
/*import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;*/
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.app.lynkpal.Bean.CompanyReviewsBean;
import com.app.lynkpal.Helper.CircleImageView;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 10/4/2017.
 */

public class CompanyReviewAdapter extends BaseAdapter {

    public List<CompanyReviewsBean> data = new ArrayList<CompanyReviewsBean>();
    Context context;
    int Height;

    public CompanyReviewAdapter(Context context, List<CompanyReviewsBean> tablelist, int height) {
        this.data = tablelist;
        this.context = context;
        this.Height = height;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.item_company_reviews, parent, false);

        } else {
            row = (View) convertView;
        }


        final CircleImageView imageuserAvtar = row.findViewById(R.id.imageuserAvtar);
        TextView txtName = (TextView) row.findViewById(R.id.name);
        //  LinearLayout linearLayout = row.findViewById(R.id.linMain);
        Typeface tf_bold = Typeface.createFromAsset(context.getAssets(), "Roboto-Bold.ttf");
        Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        Typeface tf_Med = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
        txtName.setTypeface(tf_bold);
        txtName.setText(data.get(position).getFullname());

        TextView txtDate = (TextView) row.findViewById(R.id.txtDate);
        txtDate.setTypeface(tf_Med);
        txtDate.setText(data.get(position).getCreated_at());
        TextView txtDesc = (TextView) row.findViewById(R.id.txtDesc);
        txtDesc.setTypeface(tf_reg);
        txtDesc.setText(data.get(position).getComment());

        RatingBar ratingBar = (RatingBar) row.findViewById(R.id.ratingBar1);
        try {
            ratingBar.setRating(Float.parseFloat(data.get(position).getRating()));
        } catch (Exception e) {
        }
        //  imageuserAvtar.getLayoutParams().height = (int) (Height / 8);
        CompanyReviewsBean companyReviewsBean = data.get(position);



        if (companyReviewsBean.getProfile_pic()!="null")
        {
            Log.d("gettingData", "getView: " + "yes Image");
            Glide.with(context).load(WebApis.userProfileImage + companyReviewsBean.getProfile_pic()).into(imageuserAvtar);
           /* Glide.with(context).load(WebApis.userProfileImage + image).asBitmap().placeholder(R.drawable.image).error(R.drawable.image).centerCrop().into(new BitmapImageViewTarget(imageuserAvtar) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    imageuserAvtar.setImageDrawable(circularBitmapDrawable);
                }
            });*/

        } else {
            Log.d("gettingData", "getView: " + "no Image");
            imageuserAvtar.setImageResource(R.drawable.image);
        }
        return row;
    }


}
