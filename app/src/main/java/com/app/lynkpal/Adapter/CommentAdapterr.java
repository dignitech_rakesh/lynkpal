package com.app.lynkpal.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.lynkpal.Bean.CommentReplies;
import com.app.lynkpal.Bean.CommentsBean;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 9/18/2017.
 */

public class CommentAdapterr extends BaseAdapter {
    private Context context; //context
    private List<CommentsBean> items; //data source of the list adapter

    //public constructor
    public CommentAdapterr(Context context, List<CommentsBean> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size(); //returns total of items in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position); //returns list item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // inflate the layout for each list row
        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.comment_items, parent, false);
        }

        LinearLayout mainLinearLayout;
        //CardView cardViewReply;
        RelativeLayout rl_show_more;
        ImageView profile_user_image_rply,like;
        TextView username_rply,time_stamp_rply,post_text_rply,like_count,reply_count;

        // get current item to be displayed
        CommentsBean currentItem = items.get(position);


        // new Implementation
        mainLinearLayout = (LinearLayout) convertView.findViewById(R.id.mainLinearLayout);

        //cardViewReply = (CardView) convertView.findViewById(R.id.cardViewReply);
        rl_show_more = (RelativeLayout) convertView.findViewById(R.id.rl_show_more);
        //profile_user_image_rply  = (ImageView) convertView.findViewById(R.id.profile_user_image_rply);
        like  = (ImageView) convertView.findViewById(R.id.like);
         //username_rply = (TextView) convertView.findViewById(R.id.username_rply);
        //time_stamp_rply = (TextView) convertView.findViewById(R.id.time_stamp_rply);
       // post_text_rply = (TextView) convertView.findViewById(R.id.post_text_rply);
        like_count = (TextView) convertView.findViewById(R.id.like_count);
        reply_count = (TextView) convertView.findViewById(R.id.reply_count);

        // get the TextView for item name and item description
        TextView username = (TextView) convertView.findViewById(R.id.username);
        TextView time_stamp = (TextView) convertView.findViewById(R.id.time_stamp);
        TextView post_text = (TextView) convertView.findViewById(R.id.post_text);
        ImageView profile_user_image = (ImageView) convertView.findViewById(R.id.profile_user_image);
        Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(context.getAssets(), "Roboto-Bold.ttf");
        time_stamp.setTypeface(tf_med);
        username.setTypeface(tf_bold);
        post_text.setTypeface(tf_reg);
        String image = currentItem.getProfile_pic();
        if (image != null) {
            /*imgLogo.setImageBitmap(image);*/
            Glide.with(context).load(WebApis.userProfileImage+image).into(profile_user_image);/*placeholder(R.drawable.image).error(R.drawable.image)
                    .into(profile_user_image);*/
        } else {
            profile_user_image.setImageResource(R.drawable.image);
        }
        username.setText(currentItem.getFullname());
        time_stamp.setText(currentItem.getCreated_at());
        post_text.setText(currentItem.getMessage());


        //new implementation
        like_count.setText(Html.fromHtml(currentItem.getLikes_count()+" Likes"));
        reply_count.setText(Html.fromHtml(currentItem.getReplies_count()+ " Replies"));
        ArrayList<CommentReplies> arrayList=currentItem.getCommentRepliesArrayList();
        Log.d("arrayListSize", arrayList.size()+"");

       // if (currentItem.getCommentRepliesArrayList().size()>0){
           /* cardViewReply.setVisibility(View.VISIBLE);
            for (int i=0;i<currentItem.getCommentRepliesArrayList().size();i++) {
                username_rply.setText(currentItem.getCommentRepliesArrayList().get(i).getFullname_rply());
                post_text_rply.setText(currentItem.getCommentRepliesArrayList().get(i).getMessage_rply());
            }*/

            //dynamic CardView
          /*  for (int i=0;i<currentItem.getCommentRepliesArrayList().size();i++) {
                //CardView cardView = (CardView) createCardView();
                //mainLinearLayout.addView(cardView);

                final TextView rowTextView = new TextView(context);

                // set some properties of rowTextView or something
                rowTextView.setText("This is row #" + i);

                // add the textview to the linearlayout
                mainLinearLayout.addView(rowTextView);

            }

        }*/

        // returns the view for the current row
        return convertView;
    }

    public View createCardView(){
        // Initialize a new CardView
        CardView card = new CardView(context);

        // Set the CardView layoutParams
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        card.setLayoutParams(params);

        // Set CardView corner radius
        card.setRadius(9);

        // Set cardView content padding
        card.setContentPadding(15, 15, 15, 15);

        // Set a background color for CardView
        card.setCardBackgroundColor(Color.parseColor("#FFC6D6C3"));

        // Set the CardView maximum elevation
        card.setMaxCardElevation(15);

        // Set CardView elevation
        card.setCardElevation(9);

        // Initialize a new TextView to put in CardView
        TextView tv = new TextView(context);
        tv.setLayoutParams(params);
        tv.setText("CardView\nProgrammatically");
        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
        tv.setTextColor(Color.RED);

        // Put the TextView in CardView
        card.addView(tv);

        // Finally, add the CardView in root layout
        return card;
       // mainLinearLayout.addView(card);
    }


}