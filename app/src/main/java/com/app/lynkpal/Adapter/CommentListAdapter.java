package com.app.lynkpal.Adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Bean.CommentReplies;
import com.app.lynkpal.Bean.CommentsBean;
import com.app.lynkpal.Helper.CircleImageView;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.InterCmntRply;
import com.app.lynkpal.LoginActivity;
import com.app.lynkpal.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class CommentListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<CommentsBean> groups;
    InterCmntRply interCmntRply;
    boolean isLike=false;
    ImageView like;

    public CommentListAdapter(Context context, ArrayList<CommentsBean> groups) {
        this.context = context;
        this.groups = groups;
        //this.interLikeCmnt=interLikeCmnt;

        interCmntRply=(InterCmntRply)context;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<CommentReplies> chList = groups.get(groupPosition).getCommentRepliesArrayList();
        return chList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        CommentReplies child = (CommentReplies) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.child_item_comment, null);
        }
       /* TextView tv = (TextView) convertView.findViewById(R.id.country_name);
        ImageView iv = (ImageView) convertView.findViewById(R.id.flag);

        tv.setText(child.getMessage_rply().toString());
        //iv.setImageResource(child.getImage());

        return convertView;*/


        TextView username_rply,time_stamp_rply,post_text_rply;
        CircleImageView profile_user_image_rply;


       // RelativeLayout child_rl=(RelativeLayout)convertView.findViewById(R.id.child_rl);
        //child_rl.setBackgroundColor(context.getResources().getColor(R.color.colorLightGrey));
        username_rply = (TextView) convertView.findViewById(R.id.username_rply);
        time_stamp_rply = (TextView) convertView.findViewById(R.id.time_stamp_rply);
        post_text_rply = (TextView) convertView.findViewById(R.id.post_text_rply);
        profile_user_image_rply = (CircleImageView) convertView.findViewById(R.id.profile_user_image_rply);

        String image = child.getProfile_pic_rply();

        if (child.getProfile_pic_rply()==null||child.getProfile_pic_rply().equalsIgnoreCase("null")){
            profile_user_image_rply.setImageResource(R.drawable.image);

        }else{
            Glide.with(context).load(WebApis.userProfileImage+image).into(profile_user_image_rply);

        }
//        if (!image.equals("null")&&!image.equals(null)) {
//            /*imgLogo.setImageBitmap(image);*/
//            Glide.with(context).load("http://www.lynkpal.com/static/uploads/user_profile/"+image).into(profile_user_image_rply);/*placeholder(R.drawable.image).error(R.drawable.image)
//               //     .into(profile_user_image);*/
//           // profile_user_image_rply.setImageResource(R.mipmap.ic_launcher);
//        } else {
//            profile_user_image_rply.setImageResource(R.mipmap.ic_launcher);
//        }
        username_rply.setText(child.getFullname_rply());
        time_stamp_rply.setText(child.getCreated_at_rply());
        post_text_rply.setText(child.getMessage_rply());

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<CommentReplies> chList = groups.get(groupPosition).getCommentRepliesArrayList();
        return chList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        final CommentsBean group = (CommentsBean) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.group_item_comment, null);
        }
//        TextView tv = (TextView) convertView.findViewById(R.id.group_name);
//        tv.setText(group.getMessage());
//        return convertView;




        LinearLayout mainLinearLayout;
        //CardView cardViewReply;
        RelativeLayout rl_show_more;
        ImageView profile_user_image_rply;
        final TextView username_rply,time_stamp_rply,post_text_rply,like_count,reply_count,show_more;


         boolean show=false;

        RelativeLayout group_rl=(RelativeLayout)convertView.findViewById(R.id.group_rl);
        //group_rl.setBackgroundColor(context.getResources().getColor(R.color.cardview_shadow_start_color));
        // get current item to be displayed
        // CommentsBean currentItem = items.get(position);


        // new Implementation
        // mainLinearLayout = (LinearLayout) convertView.findViewById(R.id.mainLinearLayout);

        //cardViewReply = (CardView) convertView.findViewById(R.id.cardViewReply);
        rl_show_more = (RelativeLayout) convertView.findViewById(R.id.rl_show_more);
        //profile_user_image_rply  = (ImageView) convertView.findViewById(R.id.profile_user_image_rply);
        like  = (ImageView) convertView.findViewById(R.id.like);
        //username_rply = (TextView) convertView.findViewById(R.id.username_rply);
        //time_stamp_rply = (TextView) convertView.findViewById(R.id.time_stamp_rply);
        // post_text_rply = (TextView) convertView.findViewById(R.id.post_text_rply);
        like_count = (TextView) convertView.findViewById(R.id.like_count);
        reply_count = (TextView) convertView.findViewById(R.id.reply_count);
        show_more = (TextView) convertView.findViewById(R.id.show_more_);

        // get the TextView for item name and item description
        TextView username = (TextView) convertView.findViewById(R.id.username);
        TextView time_stamp = (TextView) convertView.findViewById(R.id.time_stamp);
        TextView post_text = (TextView) convertView.findViewById(R.id.post_text);
        CircleImageView profile_user_image = (CircleImageView) convertView.findViewById(R.id.profile_user_image);

        String image = group.getProfile_pic();
        if (!image.equals("null")&&!image.equals(null)) {
            /*imgLogo.setImageBitmap(image);*/
            Glide.with(context).load(WebApis.userProfileImage+image).into(profile_user_image);/*placeholder(R.drawable.image).error(R.drawable.image)
                    //.into(profile_user_image);*/
           // profile_user_image.setImageResource(R.mipmap.ic_launcher);
        } else {
            profile_user_image.setImageResource(R.drawable.image);
        }

        if (group.getCommentLikeStatus()==1){
            isLike=true;
            like.setImageResource(R.drawable.thumb_like);
        }else {
            isLike=false;
            like.setImageResource(R.drawable.thumb_unlike);
        }
        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {


               // interCmntRply.likeComment(Integer.parseInt(group.getId()));
                if (LoginActivity.isNetworkConnected(context)) {
                    //Constant.update_home = true;
                    if (isLike) {
                        isLike = false;
                        like.setImageResource(R.drawable.thumb_unlike);
                        int count = Integer.parseInt(group.getLikes_count());
                        count = count - 1;
                       String likeCount = String.valueOf(count);
                        like_count.setText(String.valueOf(count));
                        //Like();
                        interCmntRply.likeComment(Integer.parseInt(group.getId()));


                    }
                    else
                    {
                        isLike = true;
                        like.setImageResource(R.drawable.thumb_like);
                        int count = Integer.parseInt(group.getLikes_count());
                        count = count + 1;
                       String likeCount = String.valueOf(count);
                        like_count.setText(String.valueOf(count));
                        //Like();
                        interCmntRply.likeComment(Integer.parseInt(group.getId()));

                    }
                }
                else {
                    Toast.makeText(context,"No Internet Connection",Toast.LENGTH_SHORT).show();
                }
            }

        });

      /*  convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show_more.setText("Show Less....");

            }
        });*/
        username.setText(group.getFullname());
        time_stamp.setText(group.getCreated_at());
        post_text.setText(group.getMessage());


        //new implementation
        like_count.setText(Html.fromHtml(group.getLikes_count()+" Likes"));
        reply_count.setText(Html.fromHtml(group.getReplies_count()+ " Replies"));

        reply_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                interCmntRply.replyClick(Integer.parseInt(group.getId()),group);


            }
        });

      /*  group_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (show_more.getText().toString().equalsIgnoreCase("show more....")){
                    show_more.setText("show less....");
                }else{
                    show_more.setText("show more....");
                }

            }
        });*/

        return convertView;

    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

   /* public void refreshData() {

        notifyDataSetChanged();
    }*/
}
