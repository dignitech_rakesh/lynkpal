package com.app.lynkpal.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.app.lynkpal.Interface.SelectInterface;
import com.app.lynkpal.R;
import com.app.lynkpal.test.Contact;

import java.util.List;

/**
 * Created by user on 11/6/2017.
 */

public class InvitePeoplesAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflter;
    SelectInterface selectInterface;
    List<Contact> allUsersBeenList;
    // List<SkillListBean> custom_spinner_items = new ArrayList<SkillListBean>();

    public InvitePeoplesAdapter(Context applicationContext, List<Contact> allUsersBeen, SelectInterface selectInterface) {
        this.context = applicationContext;
        //custom_spinner_items = country;
        this.selectInterface = selectInterface;
        inflter = (LayoutInflater.from(applicationContext));
        this.allUsersBeenList = allUsersBeen;
    }

    @Override
    public int getCount() {
        return allUsersBeenList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        view = inflter.inflate(R.layout.custom_invites_items, null);
        TextView names = (TextView) view.findViewById(R.id.textView);
        TextView txtEmail = (TextView) view.findViewById(R.id.txtEmail);
        if (allUsersBeenList.get(position).getName().trim().length() < 1) {
            names.setText(allUsersBeenList.get(position).getEmail());
        } else {
            names.setText(allUsersBeenList.get(position).getName() + "");
        }
        txtEmail.setText(allUsersBeenList.get(position).getEmail() + "");
        Typeface tf_regular = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
        names.setTypeface(tf_regular);
        txtEmail.setTypeface(tf_regular);
        final CheckBox checkBox = (CheckBox) view.findViewById(R.id.chkBox);


        if (allUsersBeenList.get(position).getSelected()) {
            checkBox.setChecked(true);
        } else {
            checkBox.setChecked(false);
        }

        names.setTag(position);
        names.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                if (allUsersBeenList.get(pos).getSelected()) {
                    selectInterface.select("", pos);
                    notifyDataSetChanged();
                } else {
                    selectInterface.select("", pos);
                    notifyDataSetChanged();
                }
            }
        });
        txtEmail.setTag(position);
        txtEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (int) view.getTag();
                if (allUsersBeenList.get(pos).getSelected()) {
                    selectInterface.select("", pos);
                    notifyDataSetChanged();
                } else {
                    selectInterface.select("", pos);
                    notifyDataSetChanged();
                }
            }
        });
        return view;
    }
}