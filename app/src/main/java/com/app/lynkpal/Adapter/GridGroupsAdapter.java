package com.app.lynkpal.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
/*import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;*/
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Bean.GridBeanGroups;
import com.app.lynkpal.GroupActivity;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.JoinGroup;
import com.app.lynkpal.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 10/11/2017.
 */

public class GridGroupsAdapter extends BaseAdapter {

    public List<GridBeanGroups> data = new ArrayList<GridBeanGroups>();
    Context context;
    int Height;
    JoinGroup joinGroup;
    public GridGroupsAdapter(Context context, List<GridBeanGroups> tablelist, int height, JoinGroup joinGroup) {
        this.data = tablelist;
        this.context = context;
        this.Height = height;
        this.joinGroup = joinGroup;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.gridlayout_groups, parent, false);

        } else {
            row = (View) convertView;
        }
        row.setTag(position);
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos= (int) view.getTag();
                if (data.get(pos).getMembershipstatus().equals("0"))
                {
                    Toast.makeText(context, "Please!!! Join Group First", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    context.startActivity(new Intent(context,GroupActivity.class).putExtra("name",data.get(pos).getName()).putExtra("id",data.get(pos).getId()).putExtra("image",data.get(pos).getImage()).putExtra("type","other"));
                }
               // data.get(pos).getMembershipstatus()
            }
        });
        final ImageView imgLogo = (ImageView) row.findViewById(R.id.imgIcon);
        final ImageView imgCheck = (ImageView) row.findViewById(R.id.imgCheck);
        TextView txtName = (TextView) row.findViewById(R.id.txtName);
        LinearLayout linearLayout = (LinearLayout) row.findViewById(R.id.linMain);
        Typeface tf_reg = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        txtName.setTypeface(tf_reg);
        linearLayout.getLayoutParams().height = (int) (Height / 6);
        if (data.get(position).getMembershipstatus().equals("0"))
        {
            imgCheck.setImageDrawable(context.getResources().getDrawable(R.drawable.plus_icon));
        }
        else if (data.get(position).getMembershipstatus().equals("1"))
        {
            imgCheck.setImageDrawable(context.getResources().getDrawable(R.drawable.chek_icon));
        }
        else
        {
            imgCheck.setImageDrawable(context.getResources().getDrawable(R.drawable.clock_icon));

        }
        imgCheck.setTag(position);
        imgCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int po= (int) view.getTag();
                joinGroup.Join(po,"open");
            }
        });
        String image = data.get(position).getImage();
        if (!image.equals("null")) {
            Picasso.with(context).load(WebApis.GroupImages + image).into(imgLogo);

           /* Glide.with(context).load(WebApis.GroupImages + image).asBitmap().placeholder(R.drawable.noimage).error(R.drawable.noimage).centerCrop().into(new BitmapImageViewTarget(imgLogo) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    imgLogo.setImageDrawable(circularBitmapDrawable);
                }
            });*/
        } else {

        }
        txtName.setText(data.get(position).getName());
        //  txtName.setSelected(true);
        return row;
    }


}
