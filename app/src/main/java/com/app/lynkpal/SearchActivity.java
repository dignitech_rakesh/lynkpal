package com.app.lynkpal;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Adapter.GroupMembersAdapter;
import com.app.lynkpal.Adapter.SearchAdapter;
import com.app.lynkpal.Bean.CompanyFollowersBean;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.Searchit;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class SearchActivity extends AppCompatActivity implements Searchit {
    ImageView imgBack;
    ProgressBar progress;
    TextView txtNoResult;
    RecyclerView listSearch;
    SearchAdapter companyFollowersAdapter;
    EditText edtSearch;
    List<CompanyFollowersBean> companyFollowersBeanList = new ArrayList<>();
    public static String  TAG = SearchActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        progress = (ProgressBar) findViewById(R.id.progress);
        txtNoResult = (TextView) findViewById(R.id.txtNoResult);
        listSearch =  findViewById(R.id.listSearch);
        edtSearch = (EditText) findViewById(R.id.edtSearch);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        edtSearch.setTypeface(tf_bold);
        txtNoResult.setTypeface(tf_med);

        txtNoResult.setVisibility(View.INVISIBLE);
        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!edtSearch.getText().toString().trim().equals("")) {
                        String text = edtSearch.getText().toString().replace(" ", "%20");
                        getSearchResult(text);

                    } else {
                        try {
                            txtNoResult.setVisibility(View.INVISIBLE);
                            companyFollowersBeanList.clear();
                            companyFollowersAdapter = new SearchAdapter(SearchActivity.this, companyFollowersBeanList,SearchActivity.this);
                            listSearch.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
                            listSearch.setAdapter(companyFollowersAdapter);
                            companyFollowersAdapter.notifyDataSetChanged();
                        } catch (Exception e) {

                        }
                    }
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edtSearch.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    return true;
                }
                return false;
            }
        });


      /*  listSearch.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                String user_id = companyFollowersBeanList.get(position).getUserid();
                startActivity(new Intent(SearchActivity.this,UserDetailsActivity.class).putExtra("User_id",user_id)
                .putExtra("userImage",companyFollowersBeanList.get(position).getProfilePic()));
            }
        });
*/

      // Uncmnt for char wise search
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

              //  Log.d("textWatcher","beforeTextChanged");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                //Log.d("textWatcher","onTextChanged");

                //  String text = edtSearch.getText().toString().replace(" ", "%20");
                getSearchResult(edtSearch.getText().toString().replace(" ", "%20"));
            }

            @Override
            public void afterTextChanged(Editable s) {

              //  Log.d("textWatcher","afterTextChanged");

            }
        });
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                SearchActivity.this.overridePendingTransition(0, 0);

            }
        });
        try {
            edtSearch.setFocusable(true);
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(edtSearch, InputMethodManager.SHOW_IMPLICIT);
        } catch (Exception e) {

            Log.e("error", "r" + e.toString());
        }
    }

    public void getSearchResult(String search) {
        // final LoaderDiloag loaderDiloag = new LoaderDiloag(SearchActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(SearchActivity.this)) {
            //  loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            progress.setVisibility(View.VISIBLE);
            txtNoResult.setVisibility(View.INVISIBLE);
            companyFollowersBeanList.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.GROUPMEMBERS", WebApis.SearchUser + search);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.SearchUser + search)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (SearchActivity.this == null)
                            return;
                        progress.setVisibility(View.GONE);

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.d("getSearchDetails", jsonData + "");
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {
                                JSONObject jsonObject = array.getJSONObject(a);
                                CompanyFollowersBean companyFollowersBean = new CompanyFollowersBean();
                                companyFollowersBean.setFullName(jsonObject.getString("first_name") + " " + jsonObject.getString("last_name"));
                                companyFollowersBean.setProfilePic(jsonObject.getString("profile_pic"));

                                companyFollowersBean.setUserid(jsonObject.getString("id"));
                                companyFollowersBean.setDescription(jsonObject.getString("email"));
                                companyFollowersBeanList.add(companyFollowersBean);
                            }

                        } catch (Exception e) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress.setVisibility(View.GONE);
                                }
                            });
                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progress.setVisibility(View.GONE);
                                if (companyFollowersBeanList.size() > 0) {
                                    listSearch.setVisibility(View.VISIBLE);
                                    txtNoResult.setVisibility(View.GONE);

                                    companyFollowersAdapter = new SearchAdapter(SearchActivity.this, companyFollowersBeanList,SearchActivity.this);
                                    listSearch.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
                                     listSearch.setAdapter(companyFollowersAdapter);
                                } else {
                                    listSearch.setVisibility(View.GONE);
                                    txtNoResult.setVisibility(View.VISIBLE);
                                }
                            }
                        });


                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress.setVisibility(View.GONE);

                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    @Override
    public void onBackPressed() {
        finish();
        SearchActivity.this.overridePendingTransition(0, 0);
    }

    @Override
    public void onSearch(String id)
    {
        startActivity(new Intent(SearchActivity.this,UserDetailsActivity.class).putExtra("User_id",id));
    }
}
