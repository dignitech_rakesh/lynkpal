package com.app.lynkpal.oneSignal;

import android.util.Log;

import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import org.json.JSONObject;

public class MyNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {

    //This will be called when a notification is received while your app is running.

    @Override
    public void notificationReceived(OSNotification notification) {

        JSONObject data = notification.payload.additionalData;
        String data2 = notification.payload.title;
        String data3 = notification.payload.body;
        Log.d("payload",data2+"----"+data3);
        String customKey;

        if (data != null) {
            //While sending a Push notification from OneSignal dashboard
            // you can send an addtional data named "customkey" and retrieve the value of it and do necessary operation
            customKey = data.optString("customkey", null);
            if (customKey != null)
                Log.d("OneSignalExample", "customkey set with value: " + customKey);
        }
    }
}
