package com.app.lynkpal.oneSignal;

import android.content.Intent;
import android.util.Log;

import com.app.lynkpal.Application.LynkApplication;
import com.app.lynkpal.CommentActivity;
import com.app.lynkpal.PostActivity;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

public class MyNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        OSNotificationAction.ActionType actionType = result.action.type;
        // additionalData is basically additional parameters which can be used to determine which activity to be opened
        JSONObject data = result.notification.payload.additionalData;
        String title =result.notification.payload.title;
        String message =result.notification.payload.body;

        String activityToBeOpened;

        //While sending a Push notification from OneSignal dashboard
        // you can send an addtional data named "activityToBeOpened" and retrieve the value of it and do necessary operation
        //If key is "activityToBeOpened" and value is "AnotherActivity", then when a user clicks
        //on the notification, AnotherActivity will be opened.
        //Else, if we have not set any additional data MainActivity is opened.
        if (data != null) {
            String customKey = data.optString("post_id", null);

            Log.d("MyNotificationHandler", "customkey value: " + data.toString());
            Log.d("MyNotificationHandler", customKey  + "--"+ title + "--"+ message);

            activityToBeOpened = data.optString("activityToBeOpened", null);


            if (customKey!=null) {
                Intent intent = new Intent(LynkApplication.getContext(), CommentActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id_", customKey);
                LynkApplication.getContext().startActivity(intent);

            }

            /*if (activityToBeOpened != null && activityToBeOpened.equals("AnotherActivity")) {
                Log.d("OneSignalExample", "customkey set with value: " + activityToBeOpened);
                *//*Intent intent = new Intent(LynkApplication.getContext(), AnotherActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                LynkApplication.getContext().startActivity(intent);*//*
            } else if (activityToBeOpened != null && activityToBeOpened.equals("MainActivity")) {
                Log.d("OneSignalExample", "customkey set with value: " + activityToBeOpened);
                *//*Intent intent = new Intent(LynkApplication.getContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                LynkApplication.getContext().startActivity(intent);*//*
            } else {
                Log.d("OneSignalExample", "customkey set with value: " + activityToBeOpened);

                *//*Intent intent = new Intent(LynkApplication.getContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                LynkApplication.getContext().startActivity(intent);*//*
            }*/

        }

    }
}
