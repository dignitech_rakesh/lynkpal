package com.app.lynkpal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.lynkpal.Application.Utils;
import com.app.lynkpal.Fragment.Home;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.FilePath;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.bumptech.glide.Glide;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class PostActivity extends AppCompatActivity
{
    private static final String TAG = PostActivity.class.getSimpleName();
    EditText mEditPost;
   ImageView attach_image,posting_image,user_image;
   Uri gallery_image,videoUriPath;
   String video_uri;
    byte[] byteArray;
    Bitmap bmp,thumbnail;
    FileBody filepath;
    File file_intent;

    private String extension;
    String post_data = " ";
    TextView mPostText,mCancelText,userNameprofile;
    Uri camera_photo;
    LoaderDiloag mDialog;
    ImageView mPlayImage;
    VideoView mView;
    //LoaderDiloag mLoader;
    RelativeLayout mimageLayout;
    private Uri uriIntent;
    private String selectedFileuriIntent;
    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

       mEditPost = findViewById(R.id.edit_posting);
       attach_image = findViewById(R.id.image_send);
       user_image = findViewById(R.id.userImageprofile);
       posting_image = findViewById(R.id.postImage);
       mPostText = findViewById(R.id.postText);
       mCancelText = findViewById(R.id.cancel_text);
        userNameprofile = findViewById(R.id.userNameprofile);
       mDialog = new LoaderDiloag(this);
       mPlayImage = findViewById(R.id.play_image);
     //  mLoader = new LoaderDiloag(this);
       mimageLayout=findViewById(R.id.imageLayout);

        sharedPreferences = PostActivity.this.getSharedPreferences(Constant.PREFS_NAME, 0);

        userNameprofile.setText(sharedPreferences.getString("firstname",""));
        userNameprofile.append(" ");
        userNameprofile.append(sharedPreferences.getString("lastname",""));

        video_uri = getIntent().getStringExtra("videoUri");
       videoUriPath=getIntent().getParcelableExtra("pathUri");
        Log.d(TAG, "onCreate: VideoPath " + video_uri);
        thumbnail = getIntent().getParcelableExtra("thumbnail");

        if (video_uri!=null)
        {
           if (getIntent().getStringExtra("photo")!="null")
           {
               Glide.with(PostActivity.this).load(getIntent().getStringExtra("photo"))
                       .placeholder(R.drawable.userlynkpal).error(R.drawable.userlynkpal).into(user_image);
           } else
           {
               Log.d(TAG, "onCreate: NoUser");
               // Glide.with(PostActivity.this).load(R.drawable.userlynkpal).into(user_image);
               user_image.setImageResource(R.drawable.userlynkpal);
           }
          mPlayImage.setVisibility(View.VISIBLE);
           posting_image.setImageBitmap(thumbnail);

        }

        if (videoUriPath!=null){
            convertVideo(PostActivity.this,videoUriPath);
        }

        mPlayImage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                    startActivity(new Intent(PostActivity.this, PostVideoActivity.class).putExtra("videoUri",video_uri));

            }
        });

       // Log.d(TAG, "onCreate: " + video_uri+"--" + thumbnail);

        Log.d(TAG, "doInBackground: Post datafirst " + post_data);
       gallery_image = getIntent().getParcelableExtra("image");
        Log.d(TAG, "onCreate: " + gallery_image);

        if (getIntent().getStringExtra("photo")!=null)
        {
            Log.d(TAG, "onCreate: User");
            Glide.with(PostActivity.this).load(getIntent().getStringExtra("photo"))
                    .placeholder(R.drawable.userlynkpal).error(R.drawable.userlynkpal).into(user_image);
        }
        else
        {
            Log.d(TAG, "onCreate: NoUser");
           // Glide.with(PostActivity.this).load(R.drawable.userlynkpal).into(user_image);
            user_image.setImageResource(R.drawable.userlynkpal);
        }

       if (gallery_image!=null)
       {
           Glide.with(PostActivity.this).load(gallery_image).into(posting_image);
           Log.d("uriIntent","PostActivity--"+uriIntent);
           convertImage(PostActivity.this,gallery_image);
       }
       camera_photo = getIntent().getParcelableExtra("image");
        byteArray = getIntent().getByteArrayExtra("image");
        if (byteArray!=null)
        {
            bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            if (byteArray!=null)
            {
                posting_image.setImageBitmap(bmp);
                getImageUri(PostActivity.this,bmp);
                convertImage(PostActivity.this,camera_photo);
            }

        }

        attach_image.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

            }
        });

        mCancelText.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
                startActivity(new Intent(PostActivity.this,MainActivity.class));
            }
        });

        mPostText.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                post_data = mEditPost.getText().toString().trim();
               //postDataApi(PostActivity.this);
                if(post_data.isEmpty())
                {

                    Toast.makeText(PostActivity.this,"Please make some posting",Toast.LENGTH_SHORT).show();
                    return;
                }
                else {

                   if(LoginActivity.isNetworkConnected(PostActivity.this))
                   {
                      // mDialog.displayDiloag();
                       new VideoUploader().execute();

                       Constant.postType="postFile";

                       startActivity(new Intent(PostActivity.this,MainActivity.class)//.putExtra("filepath", (Parcelable) filepath)
                               .putExtra("uriIntent", uriIntent.toString())
                             //  .putExtra("uriIntent", selectedFileuriIntent)
                               .putExtra("postText",post_data).putExtra("extension",extension));//.putExtra("type","post"));
                   }
                   else
                   {
                       Toast.makeText(PostActivity.this,"Please Connect to Internet",Toast.LENGTH_SHORT).show();
                   }
                }

            }
        });

    }



    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
        startActivity(new Intent(PostActivity.this,MainActivity.class));
    }

    private void convertImage(Context context, Uri uri)
    {
       uriIntent=uri;
      // Constant.staticUri=uri;
        String selectedFilePath = FilePath.getPath(context,uri);
        selectedFileuriIntent=selectedFilePath;
        Constant.testImage=selectedFilePath;
        File file = new File(selectedFilePath);
        file_intent=file;
        filepath = new FileBody(file);
        extension = "png";
    }

    private void convertVideo(Context context, Uri uri)
    {
        uriIntent=uri;

        String selectedFilePath = FilePath.getPath(context,uri);
        File file = new File(selectedFilePath);
        file_intent=file;

        filepath = new FileBody(file);
        extension = "mp4";
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        camera_photo = Uri.parse(path);
        return camera_photo;
    }



    private class VideoUploader extends AsyncTask<Void, Void, Void>
    {
        String url;
        String status = " ";

        @Override
        protected void onPreExecute()
        {
            //dialog.dismiss();
           // loaderDiloag.displayDiloag();
            url = WebApis.postURLNew;
            url = url.replace(" ", "%20");
            Log.e("extension", extension + "");
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            // TODO Auto-generated method stub

            // Client-side HTTP transport library

            HttpClient httpClient = new DefaultHttpClient();
            // using POST method
            HttpPost httpPostRequest = new HttpPost(url);
            httpPostRequest.setHeader("authorization", Constant.token);
            try {
                MultipartEntityBuilder multiPartEntityBuilder = MultipartEntityBuilder.create();

                if (filepath != null)
                {
                    multiPartEntityBuilder.addPart("postFile", filepath);

                    multiPartEntityBuilder.addTextBody("description", Utils.encodeMessage(post_data));

                } else
                {
                    extension = "text";
                }
                multiPartEntityBuilder.addTextBody("extension", extension.trim());
                Log.d(TAG, "doInBackground: Post data " + post_data);
                multiPartEntityBuilder.addTextBody("description", Utils.encodeMessage(post_data));
                Log.d("description",Utils.encodeMessage(post_data) +"");

                httpPostRequest.setEntity(multiPartEntityBuilder.build());
                Log.e("response", multiPartEntityBuilder.toString());
                HttpResponse httpResponse = null;
                httpResponse = httpClient.execute(httpPostRequest);
                HttpEntity httpEntity = httpResponse.getEntity();
                String jsonStr = EntityUtils.toString(httpEntity);
                Log.e("response", jsonStr);
                if (jsonStr != null)
                {

                    try {

                        JSONObject jsonObj = new JSONObject(jsonStr);
                        Log.d(TAG, "doInBackground: Status " + status) ;
                        status = jsonObj.getString("status");
                        if (status.equals("error"))
                        {
                            mDialog.dismissDiloag();
                            Log.d(TAG, "Error");
                            Log.d(TAG, "doInBackground: Error");
                        }
                        else
                        {
                            mDialog.dismissDiloag();
                            Log.d(TAG, "doInBackground: Success");
                        }
                    } catch (Exception e)
                    {
                        Log.e("Error", e.toString());
                    }

                } else {
                    Log.e("ServiceHandler", "Couldn't get any data from the url");
                }
            } catch (Exception e) {
                Log.e("Error", e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
           /* page = 0;
            visibleItemCount = 0;
            totalItemCount = 0;
            pastVisiblesItems = 0;
            filePath = null;
            extension = "";
            postText = "";
            layImage.setVisibility(View.GONE);
            loaderDiloag.dismissDiloag();
            homeBeanList.clear();
            getAllPost();*/
           // Toast.makeText(PostActivity.this,"Post Successfully", Toast.LENGTH_SHORT).show();
            if (status.contains("success"))
            {

                try {
                    // startActivity(new Intent(PostActivity.this,MainActivity.class));
                    Log.d("BroadCast", "Send a broadcast to show the main app window");

                    Intent i = new Intent(Constant.UI_AUTHENTICATED);
                    //i.setPackage(mApplicationContext.getPackageName());
                    sendBroadcast(i);
                    finish();
                }catch (Exception e){
                    Log.d("broadExp",e +"");
                }
//https://mobikul.com/pass-data-activity-fragment-android/
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("params", (Serializable) filepath);
//// set MyFragment Arguments
//                Home myObj = new Home();
//                myObj.setArguments(bundle);
            }
            super.onPostExecute(aVoid);
            //  }
        }
    }


}
