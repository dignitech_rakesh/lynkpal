package com.app.lynkpal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.lynkpal.Bean.LynkaplLearningModel;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CardDetailActivity extends AppCompatActivity {

    ImageView ivCardDetail;
    String packageName,selectedMonth="",selectedYear="";
    EditText edCardNo;
    LoaderDiloag loaderDiloag;

    TextView tvProcessPayment,tvCancel,tvPkgName,tvAmt;
    Spinner spinnerMonth,spinnerYear;
    String[] spinnerMonthArray = {"01", "02", "03", "04","05","06","07","08","09","10","11","12"};
    String[] spinnerYearArray = {"2018", "2019", "2020", "2021","2022", "2023", "2024", "2025"};
    private SharedPreferences sharedPreferences;
    private LynkaplLearningModel allTrainingModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_detail);

        sharedPreferences = CardDetailActivity.this.getSharedPreferences(Constant.PREFS_NAME, 0);
        loaderDiloag = new LoaderDiloag(CardDetailActivity.this);
        loaderDiloag.setCancelable(false);

        Intent i = getIntent();
        packageName = i.getStringExtra("trainingKey");

        ivCardDetail=(ImageView)findViewById(R.id.ivCardDetail);
        edCardNo=(EditText) findViewById(R.id.edCardNo);
        tvProcessPayment=(TextView) findViewById(R.id.tvProcessPayment);
        tvCancel=(TextView) findViewById(R.id.tvCancel);
        tvPkgName=(TextView) findViewById(R.id.tvPkgName);
        tvAmt=(TextView) findViewById(R.id.tvAmt);

        if (packageName.equalsIgnoreCase("gold")) {
            ivCardDetail.setImageResource(R.drawable.gold);
            tvAmt.setText("Total Amount $ 30900");
            tvPkgName.setText("Gold Package Payment");
        } else  if (packageName.equalsIgnoreCase("bronze")) {
            ivCardDetail.setImageResource(R.drawable.bronze);
            tvAmt.setText("Total Amount $ 10900");
            tvPkgName.setText("Bronze Package Payment");
        }else  if (packageName.equalsIgnoreCase("silver")) {
            ivCardDetail.setImageResource(R.drawable.silver);
            tvAmt.setText("Total Amount $ 18,900");
            tvPkgName.setText("Silver Package Payment");
        }else  if (packageName.equalsIgnoreCase("TrainingDetail")) {
             allTrainingModel  = (LynkaplLearningModel) getIntent().getSerializableExtra("LynkaplLearningModel");
           // ivCardDetail.setImageResource(R.drawable.silver);
            Glide.with(CardDetailActivity.this).load(WebApis.BaseURL+allTrainingModel.getTraining_image()).centerCrop().into(ivCardDetail);

            tvAmt.setText("Toatal Amount $ " + String.valueOf(allTrainingModel.getAmount()));
            tvPkgName.setText("Training Payment");
        }


        tvProcessPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ApplicationGlobles.isConnectingToInternet(CardDetailActivity.this)) {

                    String card_no=edCardNo.getText().toString().trim();
                if (validation(card_no)) {
                    if (packageName.equalsIgnoreCase("TrainingDetail")){
                        TrainingPaymentCardDetailApi(card_no, String.valueOf(allTrainingModel.getId()));
                    }else {
                        cardDetailApi(card_no);


                    }
                }
                } else {
                    Toast.makeText(CardDetailActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

                }
            }
        });

        spinnerMonth =(Spinner)findViewById(R.id.spinnerMonth);
        spinnerYear =(Spinner)findViewById(R.id.spinnerYear);

        CardDetailActivity.spinnerAdapter adapter = new CardDetailActivity.spinnerAdapter(CardDetailActivity.this, android.R.layout.simple_list_item_1);
        adapter.addAll(spinnerMonthArray);
        adapter.add("Exp Month");
        spinnerMonth.setAdapter(adapter);
        spinnerMonth.setSelection(adapter.getCount());

        spinnerMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(spinnerMonth.getSelectedItem() == "Month")
                {
                    //Do nothing.
                }
                else{

                    selectedMonth=spinnerMonth.getSelectedItem().toString();
                   // Toast.makeText(CardDetailActivity.this, spinnerMonth.getSelectedItem().toString(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        //spinnerYear
        CardDetailActivity.spinnerAdapter adapterYear = new CardDetailActivity.spinnerAdapter(CardDetailActivity.this, android.R.layout.simple_list_item_1);
        adapterYear.addAll(spinnerYearArray);
        adapterYear.add(" Exp Year");
        spinnerYear.setAdapter(adapterYear);
        spinnerYear.setSelection(adapterYear.getCount());

        spinnerYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(spinnerYear.getSelectedItem() == "Year")
                {

                    //Do nothing.
                }
                else{

                    selectedYear=spinnerYear.getSelectedItem().toString();
                  //  Toast.makeText(CardDetailActivity.this, spinnerYear.getSelectedItem().toString(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public class spinnerAdapter extends ArrayAdapter<String> {

        public spinnerAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
            // TODO Auto-generated constructor stub

        }

        @Override
        public int getCount() {

            // TODO Auto-generated method stub
            int count = super.getCount();

            return count>0 ? count-1 : count ;


        }


    }


    private void TrainingPaymentCardDetailApi(String cardNo, String trainingId) {


            loaderDiloag.displayDiloag();
        com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(CardDetailActivity.this);
        Map<String, String> postParam = new HashMap<String, String>();
        //postParam.put("package", packageName);
        postParam.put("card_number", cardNo);
        postParam.put("mm_date", selectedMonth);
        postParam.put("yy_date", selectedYear);

        Log.d("TrainingPaymentCard", "CArdApi " + postParam.toString());


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.POST,
                WebApis.PAYMENTTRAINING + trainingId + "/", new JSONObject(postParam),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response)
                    {
                        loaderDiloag.dismissDiloag();
                        Log.d("cardDetailApi", "FrndResponse " + response.toString());
                        try {
                            String status = response.getString("status");
                            //String message = response.getString("message");
                            if (status.contains("success"))
                            {
                                showPopup();

                                new MaterialDialog.Builder(CardDetailActivity.this)
                                        .title("Payment Successful")
                                        .content("Your payment has been successfully submitted")
                                        .iconRes(R.drawable.ic_check_circle)
                                        .positiveText("OK")
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {


                                                Intent intent =new Intent(CardDetailActivity.this,LynkpalLearning.class);
                                                startActivity(intent);
                                                finish();

                                            }
                                        })
                                        .show();

                                // mFriendAdapter.notifyDataSetChanged();
                                // suggestionApi(context, settings.getString("token",""));
                               // Toast.makeText(CardDetailActivity.this, "Done successfully ", Toast.LENGTH_LONG).show();
                            } else
                            {

                                Log.d("cardDetailApi", "frndError " + response.toString());
                            }

                        } catch (JSONException e) {
                            loaderDiloag.dismissDiloag();

                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener()
        {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                loaderDiloag.dismissDiloag();
                error.printStackTrace();
               // if (error.toString().contains(Constant.timeout_error_string));
               // Toast.makeText(CardDetailActivity.this,getString(R.string.slow_internet), Toast.LENGTH_SHORT).show();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String,String> data = new HashMap<String, String>();
                data.put("Authorization",sharedPreferences.getString("token",""));
                data.put("Content-Type","application/json");
                Log.d("tag","header " + data.toString());
                return data;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                Constant.VOLLEY_TIMEOUT_MS,
                Constant.VOLLEY_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        jsonObjReq.setTag("CardDetailActivity");
        // Adding request to request queue
        requestQueue.add(jsonObjReq);
    }


    private void cardDetailApi(String cardNo) {
        loaderDiloag.displayDiloag();
        com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(CardDetailActivity.this);
        Map<String, String> postParam = new HashMap<String, String>();
        postParam.put("package", packageName);
        postParam.put("card_number", cardNo);
        postParam.put("mm_date", selectedMonth);
        postParam.put("yy_date", selectedYear);

        Log.d("cardDetailApi", "CArdApi " + postParam.toString());


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.POST,
                WebApis.CARDDETAILS, new JSONObject(postParam),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response)
                    {
                        loaderDiloag.dismissDiloag();
                        Log.d("cardDetailApi", "FrndResponse " + response.toString());
                        try {
                            String status = response.getString("status");
                            String message = response.getString("message");
                            if (status.contains("success"))
                            {
                                //showPopup();

                                new MaterialDialog.Builder(CardDetailActivity.this)
                                        .title("Payment Successful")
                                        .content("Your payment has been successfully submitted")
                                        .iconRes(R.drawable.ic_check_circle)
                                        .positiveText("OK")
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                //SharedPreferences settings;
                                               // settings = (this).getSharedPreferences(Constant.PREFS_NAME, 0);
                                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                                editor.putString("premiumMember","yes" );
                                                editor.commit();
                                                Intent intent =new Intent(CardDetailActivity.this,JobDashboard.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        })
                                        .show();

                               // mFriendAdapter.notifyDataSetChanged();
                               // suggestionApi(context, settings.getString("token",""));
                               // Toast.makeText(CardDetailActivity.this, "Done successfully ", Toast.LENGTH_LONG).show();
                            } else
                            {

                                Log.d("cardDetailApi", "frndError " + response.toString());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener()
        {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                loaderDiloag.dismissDiloag();
                error.printStackTrace();
               // if (error.toString().contains(Constant.timeout_error_string));
                //Toast.makeText(CardDetailActivity.this,getString(R.string.slow_internet), Toast.LENGTH_SHORT).show();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String,String> data = new HashMap<String, String>();
                data.put("Authorization",sharedPreferences.getString("token",""));
                data.put("Content-Type","application/json");
                Log.d("tag","header " + data.toString());
                return data;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                Constant.VOLLEY_TIMEOUT_MS,
                Constant.VOLLEY_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        jsonObjReq.setTag("CardDetailActivity");
        // Adding request to request queue
        requestQueue.add(jsonObjReq);
    }

    public boolean validation(String cardNo){
        if (cardNo.equalsIgnoreCase("")||cardNo.equals(null)||cardNo.length()<12){
            Toast.makeText(CardDetailActivity.this,"Please enter valid card Number",Toast.LENGTH_SHORT).show();
            return false;
        }else if (selectedMonth.equalsIgnoreCase("")||selectedMonth.equals(null)||selectedMonth.equalsIgnoreCase("Exp Month")){
            Toast.makeText(CardDetailActivity.this,"Please Select the Exp Month",Toast.LENGTH_SHORT).show();
            return false;
        } if (selectedYear.equalsIgnoreCase("")||selectedYear.equals(null)||selectedYear.equalsIgnoreCase(" Exp Year")){
            Toast.makeText(CardDetailActivity.this,"Please Select the Exp Year",Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;

    }

    public void showPopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_group_join_leave, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        final TextView textText = (TextView) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        textTitle.setText("Payment Successful");
        textText.setText("Your payment has been successfully submitted");
        // textText.setText("Are you sure, you want to leave " + '"' + text + '"' + " Group?");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //deleteTrainingApi(id, pos);
                finish();


                popupWindow.dismiss();

            }
        });
    }

}
