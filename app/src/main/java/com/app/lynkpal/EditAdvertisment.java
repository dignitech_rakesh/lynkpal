package com.app.lynkpal;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.FilePath;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class EditAdvertisment extends AppCompatActivity {
    TextView head;
    EditText edtName, edtAddTags, edtAddLink;
    TextView startLabel, startDate, endLabel, endDate, txtChrgPerDay, txtChrgPerDayLabel, txtTotalPayAmount, txtTotalPayAmountLabel;
    Button btnCreateAndPay, btnBrowse;
    DatePicker picker;
    ImageView imgImage, ic_back;
    Intent pickPhoto;
    String encodedString = null;
    String name = "", tag = "", link = "", image = "", s_date = "", e_date = "", charge = "", id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_advertisment);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        head = (TextView) findViewById(R.id.head);
        edtName = (EditText) findViewById(R.id.edtName);
        edtAddTags = (EditText) findViewById(R.id.edtAddTags);
        //edtBrowse = (EditText) findViewById(R.id.edtBrowse);
        edtAddLink = (EditText) findViewById(R.id.edtAddLink);
        startLabel = (TextView) findViewById(R.id.startLabel);
        startDate = (TextView) findViewById(R.id.startDate);
        endLabel = (TextView) findViewById(R.id.endLabel);
        endDate = (TextView) findViewById(R.id.endDate);
        imgImage = (ImageView) findViewById(R.id.imgImage);
        txtChrgPerDay = (TextView) findViewById(R.id.txtChrgPerDay);
        txtChrgPerDayLabel = (TextView) findViewById(R.id.txtChrgPerDayLabel);
        txtTotalPayAmount = (TextView) findViewById(R.id.txtTotalPayAmount);
        txtTotalPayAmountLabel = (TextView) findViewById(R.id.txtTotalPayAmountLabel);
        btnCreateAndPay = findViewById(R.id.btnCreateAndPay);
        btnBrowse = (Button) findViewById(R.id.btnBrowse);
        ic_back = (ImageView) findViewById(R.id.ic_back);
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        try {
            Intent intent = getIntent();
            name = intent.getStringExtra("name");
            edtName.setText(name);
            tag = intent.getStringExtra("tag");
            edtAddTags.setText(tag);
            link = intent.getStringExtra("link");
            edtAddLink.setText(link);
            image = intent.getStringExtra("image");
            Picasso.with(this).load(WebApis.AdvImages + image).error(R.drawable.noimage).placeholder(R.drawable.noimage).into(imgImage);
            s_date = intent.getStringExtra("s_date");
            startDate.setText(s_date);
            e_date = intent.getStringExtra("e_date");
            endDate.setText(e_date);
            charge = intent.getStringExtra("charge");
            txtTotalPayAmount.setText("$" + charge);
            id = intent.getStringExtra("id");
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong, try later...", Toast.LENGTH_SHORT).show();
        }
        btnBrowse.setTypeface(tf_bold);
        head.setTypeface(tf_bold);
        btnCreateAndPay.setTypeface(tf_bold);
        edtName.setTypeface(tf_reg);
        edtAddTags.setTypeface(tf_reg);
        edtAddLink.setTypeface(tf_reg);
        // edtBrowse.setTypeface(tf_reg);
        startLabel.setTypeface(tf_reg);
        startDate.setTypeface(tf_reg);
        endLabel.setTypeface(tf_reg);
        endDate.setTypeface(tf_reg);
        txtChrgPerDay.setTypeface(tf_reg);
        txtChrgPerDayLabel.setTypeface(tf_reg);
        txtTotalPayAmount.setTypeface(tf_reg);
        txtTotalPayAmountLabel.setTypeface(tf_reg);


        btnCreateAndPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = edtName.getText().toString();
                String tags = edtAddTags.getText().toString();
                String link = edtAddLink.getText().toString();
                String startdate = startDate.getText().toString();
                String enddate = endDate.getText().toString();

                if (name.trim().length() > 0) {
                    if (tags.trim().length() > 0) {
                        if (link.trim().length() > 0) {
                            if (!startdate.toLowerCase().contains("select")) {
                                if (!enddate.toLowerCase().contains("select")) {
                                    if (!startdate.toLowerCase().equals(enddate.toLowerCase())) {
                                        //if (encodedString != null) {
                                        UpdateAdvertisment(name, tags, link);
                                        // } else {
                                        //    Toast.makeText(EditAdvertisment.this, "Please Select a Image", Toast.LENGTH_SHORT).show();
                                        // }
                                    } else {
                                        Toast.makeText(EditAdvertisment.this, "Please Select at least 1 or more then 1 days", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(EditAdvertisment.this, "Please Select End Date", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(EditAdvertisment.this, "Please Select Start Date", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            edtAddLink.setError("Required");
                        }
                    } else {
                        edtAddTags.setError("Required");
                    }
                } else {
                    edtName.setError("Required");
                }
                //showPopup();
            }
        });


        btnBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED || checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.v("TAG", "Permission is granted");
                        pickPhoto = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, 0);//one can be replaced with any action code

                    } else {

                        Log.v("TAG", "Permission is revoked");
                        ActivityCompat.requestPermissions(EditAdvertisment.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                    }
                } else {

                    Log.v("TAG", "Permission is granted");
                    pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 0);//one can be replaced with any action code
                }
            }
        });
        getChargeDetails();
    }

    public void UpdateAdvertisment(String name, String tags, String link) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(EditAdvertisment.this);

        if (ApplicationGlobles.isConnectingToInternet(EditAdvertisment.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            //String params="{\r\n\"description\":\"Write text to post\",\r\n\"postFile\":\"sadc\",\r\n\"extension\":\"jpeg\"\r\n}";
            JSONObject params = new JSONObject();
            try {
                params.put("name", name);
                params.put("tags", tags);
                params.put("link", link);
                params.put("adPic", encodedString);
                Log.e("param", params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(mediaType, params + "");
            Request request = new Request.Builder()
                    .url(WebApis.UPDATE_ADVERTISMENT + id+"/")
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (EditAdvertisment.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                encodedString = null;
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            showPopup();
                                        }
                                    });
                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(EditAdvertisment.this, "Something went wrong try later...", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                        loaderDiloag.dismissDiloag();
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(EditAdvertisment.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "MM/dd/yy";
        String outputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public void getChargeDetails() {
        if (ApplicationGlobles.isConnectingToInternet(EditAdvertisment.this)) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(WebApis.GETPLANS)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (EditAdvertisment.this == null)
                            return;
                        // loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response", jsonData + "");
                        if (jsonData.length() > 0) {
                            try {
                                JSONArray array = new JSONArray(jsonData);
                                if (array.length() > 0) {
                                   /* for (int i = 0; i < array.length(); i++) {*/
                                    JSONObject jsonObject1 = array.getJSONObject(1);
                                    final String amount = jsonObject1.getString("amount");
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            txtChrgPerDay.setText("$" + amount);
                                            // txtTotalPayAmount.setText("$" + amount);
                                        }
                                    });
                                    //   String id=jsonObject1.getString("id");
                                    // }

                                }

                            } catch (Exception je) {
                                je.printStackTrace();
                            }
                        } else {
                            // loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //swipeRefreshLayout.setRefreshing(false);
                        //loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(EditAdvertisment.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            try {
                Uri selectedFileUri = data.getData();
                Picasso.with(EditAdvertisment.this).load(selectedFileUri.toString()).into(imgImage);
                imgImage.setVisibility(View.VISIBLE);
                String selectedFilePath = FilePath.getPath(EditAdvertisment.this, selectedFileUri);
                // File file = new File(selectedFilePath);
                //bin = new FileBody(file);
                InputStream inputStream = new FileInputStream(selectedFilePath);//You can get an inputStream using any IO API
                byte[] bytes;
                byte[] buffer = new byte[8192];
                int bytesRead;
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        output.write(buffer, 0, bytesRead);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                bytes = output.toByteArray();
                encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);
                Log.e("encodedString", encodedString);

            } catch (Exception e) {
                Log.e("eroore", e.toString());
                e.printStackTrace();
            }
        }

    }


    public void showPopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.custom_dialog_postjob, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        TextView textView = (TextView) popupView.findViewById(R.id.txtText);
        textView.setTypeface(tf_med);
        textView.setText("Advertisement has been updated successfully");
        Button button = (Button) popupView.findViewById(R.id.btnOk);
        button.setTypeface(tf_bold);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                finish();
            }
        });
    }


    public int getCountOfDays(String createdDateString, String expireDateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy", Locale.getDefault());

        Date createdConvertedDate = null, expireCovertedDate = null, todayWithZeroTime = null;
        try {
            createdConvertedDate = dateFormat.parse(createdDateString);
            expireCovertedDate = dateFormat.parse(expireDateString);

            Date today = new Date();

            todayWithZeroTime = dateFormat.parse(dateFormat.format(today));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int cYear = 0, cMonth = 0, cDay = 0;

        if (createdConvertedDate.after(todayWithZeroTime)) {
            Calendar cCal = Calendar.getInstance();
            cCal.setTime(createdConvertedDate);
            cYear = cCal.get(Calendar.YEAR);
            cMonth = cCal.get(Calendar.MONTH);
            cDay = cCal.get(Calendar.DAY_OF_MONTH);

        } else {
            Calendar cCal = Calendar.getInstance();
            cCal.setTime(todayWithZeroTime);
            cYear = cCal.get(Calendar.YEAR);
            cMonth = cCal.get(Calendar.MONTH);
            cDay = cCal.get(Calendar.DAY_OF_MONTH);
        }


    /*Calendar todayCal = Calendar.getInstance();
    int todayYear = todayCal.get(Calendar.YEAR);
    int today = todayCal.get(Calendar.MONTH);
    int todayDay = todayCal.get(Calendar.DAY_OF_MONTH);
    */

        Calendar eCal = Calendar.getInstance();
        eCal.setTime(expireCovertedDate);

        int eYear = eCal.get(Calendar.YEAR);
        int eMonth = eCal.get(Calendar.MONTH);
        int eDay = eCal.get(Calendar.DAY_OF_MONTH);

        Calendar date1 = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();

        date1.clear();
        date1.set(cYear, cMonth, cDay);
        date2.clear();
        date2.set(eYear, eMonth, eDay);

        long diff = date2.getTimeInMillis() - date1.getTimeInMillis();

        float dayCount = (float) diff / (24 * 60 * 60 * 1000);

        return ((int) dayCount);
    }
}
