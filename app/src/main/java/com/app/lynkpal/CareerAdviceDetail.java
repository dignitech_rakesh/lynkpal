package com.app.lynkpal;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.lynkpal.Bean.CareerAdviceModel;
import com.app.lynkpal.Helper.WebApis;
import com.bumptech.glide.Glide;

public class CareerAdviceDetail extends AppCompatActivity {

    private TextView tvDetailHeading,tvDetail;
    ImageView ivDetail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_career_advice_detail);
        tvDetailHeading=(TextView)findViewById(R.id.tvDetailHeading);
        tvDetail=(TextView)findViewById(R.id.tvDetail);
        ivDetail=(ImageView) findViewById(R.id.ivDetail);

        CareerAdviceModel careerAdviceModel= (CareerAdviceModel)getIntent().getSerializableExtra("detail");

        tvDetailHeading.setText(careerAdviceModel.getTitle());
        tvDetail.setText(careerAdviceModel.getDescription());

        String imgUrl=careerAdviceModel.getImage();

        if (imgUrl != null && !imgUrl.equals("null")) {
            /*imgLogo.setImageBitmap(image);*/
            // Log.e("WebApis.JobImages+image",WebApis.JobImages+image);
            Glide.with(CareerAdviceDetail.this).load(WebApis.BaseURL + careerAdviceModel.getImage()).centerCrop()
                    .placeholder(R.drawable.noimage).error(R.drawable.noimage)
                    .into( ivDetail);
        } else {
            ivDetail.setImageResource(R.drawable.noimage);
        }


    }
}
