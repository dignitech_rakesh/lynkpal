package com.app.lynkpal;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Fragment.DeleteFragment;
import com.app.lynkpal.Fragment.Groups;
import com.app.lynkpal.Fragment.Home;
import com.app.lynkpal.Fragment.Messaging;
import com.app.lynkpal.Fragment.Notification;
import com.app.lynkpal.Fragment.Profile;
import com.app.lynkpal.Fragment.ReportFragment;
import com.app.lynkpal.Fragment.Setting;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.FilePath;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.github.javiersantos.appupdater.AppUpdater;
import com.onesignal.OneSignal;

import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class MainActivity extends AppCompatActivity implements DeleteFragment.GetRefresh,ReportFragment.GetRefreshagain {

    private static final String TAG = MainActivity.class.getSimpleName();
    public static TextView tab_text_notification, tab_text_message;
    public static RelativeLayout show_counter_notification, show_counter_message;
    TabLayout tabLayout;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    ReportFragment reportFragment;
    private boolean doubleBackToExitPressedOnce=false;
    private String data,extension,type="";
    private Uri uriIntent;

    FileBody filepath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(6);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        // App Version Checking
        AppUpdater appUpdater = new AppUpdater(this);    //https://github.com/javiersantos/AppUpdater
          appUpdater.start();


      /*  new AppUpdater(this)
                .setTitleOnUpdateAvailable("Update available")
                .setContentOnUpdateAvailable("Check out the latest version available of my app!")
                .setTitleOnUpdateNotAvailable("Update not available")
                .setContentOnUpdateNotAvailable("No update available. Check for updates again later!")
                .setButtonUpdate("Update now?")
                .setCancelable(false)
                .setIcon(R.drawable.clock_icon)
        .start();*/

       /* //OneSignal

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                Log.d("debug", "User:" + userId);
                if (registrationId != null)
                    Log.d("debug", "registrationId:" + registrationId);

            }
        });

*/
        // Linking app

/*        List<String> path;
        if(getIntent().getData().getPathSegments()!=null){
            path=getIntent().getData().getPathSegments();
            Log.d("Linking", path.get(0)+"--"+ path.get(1));

        }*/

       // type = getIntent().getStringExtra("type");
       // if (type!=null) {
        //if (!type.equalsIgnoreCase("")) {
            if (Constant.postType.equalsIgnoreCase("postFile")) {
                // filepath = getIntent().getParcelableExtra("filepath");
                uriIntent = Uri.parse(getIntent().getStringExtra("uriIntent"));
                //uriIntent = getIntent().getParcelableExtra("uriIntent");
                Log.d("uriIntent","MainActivity--"+uriIntent);

                data = getIntent().getStringExtra("postText");
                extension = getIntent().getStringExtra("extension");

                //convertImage(MainActivity.this,uriIntent);

        }

        String profile = getIntent().getStringExtra("profile");
        Log.d(TAG, "onCreate: profile value " + profile);
        if (profile != null) {
            int tab = getIntent().getExtras().getInt("company");
            mViewPager.setCurrentItem(2);
        }
        // tabLayout = (TabLayout) findViewById(R.id.tabs);
     /*   for (int i = 0; i < 6; i++) {
            TabLayout.Tab tab = tabLayout.newTab()
                    .setText("tab name")
                    .setCustomView(R.layout.custom_tab);
            tabLayout.addTab(tab);
        }*/
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.drawable.home));
        tabLayout.getTabAt(0).setCustomView(R.layout.custom_tab_home);
        tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.drawable.office));
        tabLayout.getTabAt(1).setCustomView(R.layout.custom_tab_profile);
        tabLayout.getTabAt(2).setIcon(getResources().getDrawable(R.drawable.users));
        tabLayout.getTabAt(2).setCustomView(R.layout.custom_tab_group);
        tabLayout.getTabAt(3).setIcon(getResources().getDrawable(R.drawable.chat));
        tabLayout.getTabAt(3).setCustomView(R.layout.custom_tab_message);
        tab_text_message = (TextView) tabLayout.getTabAt(3).getCustomView().findViewById(R.id.txtCounter);
        show_counter_message = (RelativeLayout) tabLayout.getTabAt(3).getCustomView().findViewById(R.id.show_counter);
        tab_text_message.setText("0");
        tab_text_message.setTypeface(tf_bold);
        tabLayout.getTabAt(4).setIcon(getResources().getDrawable(R.drawable.notification));
        tabLayout.getTabAt(4).setCustomView(R.layout.custom_tab_notification);
        show_counter_notification = (RelativeLayout) tabLayout.getTabAt(4).getCustomView().findViewById(R.id.show_counter);
        tab_text_notification = (TextView) tabLayout.getTabAt(4).getCustomView().findViewById(R.id.txtCounter);
        tab_text_notification.setText("0");
        tab_text_notification.setTypeface(tf_bold);
        tabLayout.getTabAt(5).setIcon(getResources().getDrawable(R.drawable.setting));
        tabLayout.getTabAt(5).setCustomView(R.layout.custom_tab_setting);


        tabLayout.getTabAt(0).getIcon().setColorFilter(Color.parseColor("#4c69bf"), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(1).getIcon().setColorFilter(Color.parseColor("#939393"), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(2).getIcon().setColorFilter(Color.parseColor("#939393"), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(3).getIcon().setColorFilter(Color.parseColor("#939393"), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(4).getIcon().setColorFilter(Color.parseColor("#939393"), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(5).getIcon().setColorFilter(Color.parseColor("#939393"), PorterDuff.Mode.SRC_IN);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.parseColor("#4c69bf"), PorterDuff.Mode.SRC_IN);

              /*  if (tab.equals(1))
                {
                    Profile ni = (Profile) getSupportFragmentManager().findFragmentByTag("abc");
                    ni.refresh();
                }
*/

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.parseColor("#939393"), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        countNotification();
        countMessage();


        // ATTENTION: This was auto-generated to handle app links.
//        Intent appLinkIntent = getIntent();
//        String appLinkAction = appLinkIntent.getAction();
//        Uri appLinkData = appLinkIntent.getData();                       //http://lynkpal.com/Home/?name=Hitesh&roll=12

        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        String appLinkAction = intent.getAction();
        Uri appLinkData = intent.getData();
        if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null){
            String recipeId = appLinkData.getLastPathSegment();
            Uri appData = Uri.parse("http://lynkpal.com/Home/").buildUpon()
                    .appendPath(recipeId).build();
            //showRecipe(appData);
            Log.d("appData",appData.toString());


            String a=appLinkData.getQueryParameter("name");
            String b=appLinkData.getQueryParameter("roll");
            Log.d("appData",a+"--"+b);



        }
    }
    public void countNotification() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(MainActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(MainActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";

            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.COUNTNOTIFI", WebApis.COUNTNOTIFICATION);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.COUNTNOTIFICATION)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (MainActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getDetails", jsonData + "");
                        try {
                            final JSONObject jsonObject = new JSONObject(jsonData);
                            String status = jsonObject.getString("status");
                            if (status.equals("success")) {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            int count = Integer.parseInt(jsonObject.getString("unread_notifications_count"));
                                            Log.e("count", count + "");

                                            if (count > 99) {

                                                tab_text_notification.setText("99+");
                                            } else if (count == 0) {
                                                show_counter_notification.setVisibility(View.GONE);
                                            } else {
                                                tab_text_notification.setText(count + "");
                                            }

                                        } catch (Exception e) {

                                        }
                                    }
                                });
                            }

                        } catch (Exception e) {
                            loaderDiloag.dismissDiloag();
                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                            }
                        });


                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            //Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    public void countMessage() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(MainActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(MainActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";

            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.COUNTMESSAGE", WebApis.COUNTMESSAGE);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.COUNTMESSAGE)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (MainActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getDetails", jsonData + "");
                        try {
                            final JSONObject jsonObject = new JSONObject(jsonData);
                            String status = jsonObject.getString("status");
                            if (status.equals("success")) {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            int count = Integer.parseInt(jsonObject.getString("unread_messages_count"));
                                            Log.e("count", count + "");

                                            if (count > 99) {

                                                tab_text_message.setText("99+");
                                            } else if (count == 0) {
                                                show_counter_message.setVisibility(View.GONE);
                                            } else {
                                                tab_text_message.setText(count + "");
                                            }

                                        } catch (Exception e) {

                                        }
                                    }
                                });
                            }

                        } catch (Exception e) {
                            loaderDiloag.dismissDiloag();
                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                            }
                        });


                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            //Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    @Override
    public void clicktoRefresh()
    {
        /*Intent intent = getIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);*/
        finish();
        startActivity(getIntent());
    }

    @Override
    public void refreshOnce()
    {
        finish();
        startActivity(getIntent());
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0) {

                    if (Constant.postType.equalsIgnoreCase("postFile")) {
                        fragment = new Home();
                        Bundle bundle = new Bundle();
                        bundle.putString("uriIntent", uriIntent.toString());
                        bundle.putString("postText", data);
                        bundle.putString("extension", extension);
                       // bundle.putString("type", "post");


                        //MyFragment myObj = new MyFragment();
                        fragment.setArguments(bundle);
                    } else{
                    fragment = new Home();

                }

            } else if (position == 1){
                fragment = new Profile();
            } else if (position == 2) {
                fragment = new Groups();
            } else if (position == 3) {
                fragment = new Messaging();
            } else if (position == 4) {
                fragment = new Notification();
            } else if (position == 5) {
                fragment = new Setting();
            }
            return fragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 6;
        }


        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
             /*   case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";*/
            }
            return null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

       /* if (mViewPager.getCurrentItem() != 0) {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1,false);
        }else{
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                finish();
            }else{
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            }

        }
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);*/

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
       /* if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            mViewPager.setCurrentItem(0, true);
            Log.d(TAG,"Back Called");
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }*/
        return super.onKeyDown(keyCode, event);
    }


    private void convertImage(Context context, Uri uri)
    {
        uriIntent=uri;
        // Constant.staticUri=uri;
        String selectedFilePath = FilePath.getPath(context,uri);
        File file = new File(selectedFilePath);
        //file_intent=file;
        filepath = new FileBody(file);
        extension = "png";
    }
}