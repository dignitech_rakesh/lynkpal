package com.app.lynkpal;

import android.app.Application;
import android.util.Log;

import com.onesignal.OneSignal;

/**
 * Created by user on 10/31/2017.
 */

public class Lynkpal extends Application {
    public static String oneSignalID = "";

    @Override
    public void onCreate() {
        super.onCreate();
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
//a0d7a42f-c329-4000-9ff8-62d3b1d2d718
        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                Log.e("debug", "User:" + userId);
                oneSignalID = userId;
                if (registrationId != null) {
                    Log.e("debug", "registrationId:" + registrationId);

                }

            }
        });
    }
}
