package com.app.lynkpal;

import android.app.Activity;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.onesignal.OneSignal;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class SplashActivity extends Activity
{
    ImageView imageView;
    public SharedPreferences settings;
    public static String token;
    //private ProgressBar mProgress;
    public static String mplayerId;
    public String regId;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);


        // To check Application Version
      /*  if (ApplicationGlobles.isConnectingToInternet(SplashActivity.this)) {
            VersionChecker versionChecker = new VersionChecker();

            try {
                String mLatestVersionName = versionChecker.execute().get();
                // Log.d("versionChecker",mLatestVersionName);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

        }else{

        }*/
        //OneSignal

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                mplayerId=userId;
                regId=registrationId;
                //Log.d("debug", "User:" + userId);
                if (registrationId != null)
                    // Log.d("debug", "registrationId:" + registrationId);
                    Log.d("debug", "registrationId:"+ mplayerId +"-----"+ regId);

            }
        });

      // mProgress = findViewById(R.id.progress_bar);
        final Animation animation1 =
                AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.bounce);
       // animation1.setDuration(700);
        imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setVisibility(View.VISIBLE);
        imageView.startAnimation(animation1);

        imageView.startAnimation(animation1);
        settings = (this).getSharedPreferences(Constant.PREFS_NAME, 0);
        token = settings.getString("token", "");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (token.length()<=0)
                {
                    final String android_id = Settings.Secure.getString(SplashActivity.this.getContentResolver(),
                            Settings.Secure.ANDROID_ID);
                    Log.d("splash","idd " + android_id);
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    //intent.putExtra("playerId",mplayerId);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Constant.token=token;
                    Constant.user_id= settings.getString("userid", "");
                    Log.e("bnna",Constant.user_id);
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
//                finish();
//                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            }
        }, 1000);

    }

    public class VersionChecker extends AsyncTask<String, String, String> {

        private String newVersion;

        @Override
        protected String doInBackground(String... params) {

            try {
              /*  newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();*/

                String newVersion = null;

                try {

                    newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + SplashActivity.this.getPackageName()+ "&hl=en")
                            .timeout(60000)
                            .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                            .referrer("http://www.google.com")
                            .get()
                            .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                            .first()
                            .ownText();
                    Log.e("latestversion","---"+newVersion);

                    return newVersion;

                } catch (Exception e) {

                    return newVersion;

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return newVersion;
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            String currentVersion="";
            try {
                currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;

            }catch (Exception e){

            }

            if (onlineVersion != null && !onlineVersion.isEmpty()) {

                if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {

                    //show dialog
                    Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);

                }

            }
            //Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);


        }
    }
}
