package com.app.lynkpal;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Adapter.GroupMembersAdapter;
import com.app.lynkpal.Adapter.HomeAdapter;
import com.app.lynkpal.Bean.CompanyFollowersBean;
import com.app.lynkpal.Bean.PostBean;
import com.app.lynkpal.Fragment.Home;
import com.app.lynkpal.Helper.AlbumStorageDirFactory;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Base64;
import com.app.lynkpal.Helper.BaseAlbumDirFactory;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.ExpandedHightListview;
import com.app.lynkpal.Helper.FilePath;
import com.app.lynkpal.Helper.FroyoAlbumDirFactory;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.OpenFragment;
import com.app.lynkpal.Interface.ShareListener;
import com.app.lynkpal.Interface.deletePost;
import com.app.lynkpal.Interface.showImagePopup;
import com.app.lynkpal.Interface.showVideoPopup;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class GroupActivity extends AppCompatActivity implements deletePost, showImagePopup,showVideoPopup,OpenFragment,ShareListener {
    private static final String JPEG_FILE_PREFIX = "IMG_";
    private static final String JPEG_FILE_SUFFIX = ".jpg";
    private final int REQUEST_CAMERA_ACCESS_PERMISSION = 5674;
    TextView head;
    ImageView ic_back, ic_image;
    EditText edtPOst;
    TextView txtPost, txtMembers;
    RecyclerView recycler_view;
    PostBean postBean;
    List<PostBean> homeBeanList = new ArrayList<>();
    HomeAdapter homeAdapter;
    SharedPreferences settings;
    String name = "", id = "", image = "", type = "";
    ImageView back;
    ExpandedHightListview expFollowersList;
    List<CompanyFollowersBean> companyFollowersBeanList = new ArrayList<>();
    FloatingActionButton floatAdd;
    String encodedString = "";
    FileBody bin;
    String postText;
    String message;
    ImageView ic_edit, ic_delete;
    String members = "Add Peoples";
    ArrayList<String> membersID = new ArrayList<>();
    LoaderDiloag loaderDiloag;
    String extension;
    FileBody filePath;
    String p = "";
    AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    private String mCurrentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            name = getIntent().getStringExtra("name");
            id = getIntent().getStringExtra("id");
            image = getIntent().getStringExtra("image");
            type = getIntent().getStringExtra("type");

        } catch (Exception e) {

        }
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        changeStatusBarColor();


        setContentView(R.layout.activity_group);

        loaderDiloag = new LoaderDiloag(GroupActivity.this);
        expFollowersList = (ExpandedHightListview) findViewById(R.id.expFollowersList);
        settings = getSharedPreferences(Constant.PREFS_NAME, 0);
        back = (ImageView) findViewById(R.id.back);
        Log.e("WebApis.GroupImage", WebApis.GroupImages + image + "");
        Picasso.with(this).load(WebApis.GroupImages + image).placeholder(R.drawable.back).into(back);
        Animation bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.bottom_up);
        // ViewGroup hiddenPanel = (ViewGroup)findViewById(R.id.hidden_panel);
        back.startAnimation(bottomUp);
        back.setVisibility(View.VISIBLE);
        head = (TextView) findViewById(R.id.head);
        txtPost = (TextView) findViewById(R.id.txtPost);
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        floatAdd = (FloatingActionButton) findViewById(R.id.floatAdd);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setHasFixedSize(true);
        txtMembers = (TextView) findViewById(R.id.txtMembers);
        ic_back = (ImageView) findViewById(R.id.ic_back);
        ic_edit = (ImageView) findViewById(R.id.ic_edit);
        ic_delete = (ImageView) findViewById(R.id.ic_delete);
        if (type.equals("me")) {
            ic_edit.setVisibility(View.VISIBLE);
            ic_delete.setVisibility(View.VISIBLE);
        } else {
            ic_edit.setVisibility(View.GONE);
            ic_delete.setVisibility(View.GONE);
        }

        ic_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int a = 0; a < companyFollowersBeanList.size(); a++) {
                    membersID.add(companyFollowersBeanList.get(a).getUserid());
                    if (members.equals("Add Peoples")) {
                        members = companyFollowersBeanList.get(a).getFullName();
                    } else {
                        members = members + "," + companyFollowersBeanList.get(a).getFullName();
                    }
                }
                finish();
                startActivity(new Intent(GroupActivity.this, EditGroup.class).putExtra("id", id).putExtra("members", members).putStringArrayListExtra("membersID", membersID));
            }
        });
        ic_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(id, name);
            }
        });
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        head.setTypeface(tf_bold);
        head.setText(name);
        txtPost.setTypeface(tf_bold);
        txtMembers.setTypeface(tf_bold);
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txtPost.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                txtPost.setBackground(getResources().getDrawable(R.drawable.left_dashboard));
                txtMembers.setBackground(null);
                // floatAdd.setVisibility(View.VISIBLE);
                txtPost.setTextColor(Color.WHITE);
                txtMembers.setTextColor(Color.parseColor("#375cc8"));
                recycler_view.setVisibility(View.VISIBLE);
                expFollowersList.setVisibility(View.GONE);
            }
        });
        txtMembers.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                txtMembers.setBackground(getResources().getDrawable(R.drawable.right_dashboard));
                txtPost.setBackground(null);
                //   floatAdd.setVisibility(View.GONE);
                txtMembers.setTextColor(Color.WHITE);
                txtPost.setTextColor(Color.parseColor("#375cc8"));
                recycler_view.setVisibility(View.GONE);
                expFollowersList.setVisibility(View.VISIBLE);
            }
        });
        floatAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup();
            }
        });
        String p = settings.getString("profile_pic", "");
        Log.e("profilePic", p + ">>");
        try {
            getAllPost(p);
            getGroupMembers();
        } catch (Exception e) {
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }
    }

    public void showPopup(final String id, final String text) {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_group_join_leave, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        TextView textText = (TextView) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        textText.setText("Are you sure, you want to delete " + '"' + text + '"' + " Group?");
        textTitle.setText("Delete Group");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeleteGroup(id);
                popupWindow.dismiss();

            }
        });
    }

    public void DeleteGroup(String id) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(GroupActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(GroupActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            //  gridBeanList.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.DELETEGROUP", WebApis.DELETEGROUP + id);
            Request request = new Request.Builder()
                    .url(WebApis.DELETEGROUP + id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (GroupActivity.this == null)
                            return;
                        GroupActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                            }
                        });

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getMessage", jsonData + "");
                        try {
                            JSONObject jsonObject = new JSONObject(jsonData);
                            String status = jsonObject.getString("status");
                            if (status.equals("success")) {
                                final String message = jsonObject.getString("message");
                                GroupActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        loaderDiloag.dismissDiloag();
                                        Constant.update_myCreatedGroup = true;
                                        finish();
                                        Toast.makeText(GroupActivity.this, message, Toast.LENGTH_SHORT).show();
                                    }
                                });

                            } else {
                                Toast.makeText(GroupActivity.this, "Something went wrong ,Try later...", Toast.LENGTH_SHORT).show();
                            }


                            Log.e("json", jsonObject.toString());

                        } catch (Exception e) {
                            GroupActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loaderDiloag.dismissDiloag();
                                }
                            });
                        }


                    }
                });

            } catch (Exception e) {
                GroupActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(GroupActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public void showPopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_addpost, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        Typeface tf = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        TextView textView = (TextView) popupView.findViewById(R.id.txtMain);
        textView.setTypeface(tf);
        TextView btnBrowse = (TextView) popupView.findViewById(R.id.btnBrowse);
        btnBrowse.setTypeface(tf);
        edtPOst = (EditText) popupView.findViewById(R.id.edtPOst);
        edtPOst.setTypeface(tf);
        final TextView btnOK = (TextView) popupView.findViewById(R.id.btnOK);
        btnOK.setTypeface(tf);
        ImageView ic_Close = (ImageView) popupView.findViewById(R.id.ic_Close);
        ic_image = (ImageView) popupView.findViewById(R.id.imgImage);

        // customAdapter.notifyDataSetChanged();
    /*    btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((edtPOst.getText().toString().trim().length() <= 0) && (encodedString.length() <= 0)) {
                    Toast.makeText(GroupActivity.this, "Please Select a Image or Write Something...", Toast.LENGTH_SHORT).show();
                } else {
                    postText = edtPOst.getText().toString();
                    PostUploader();
                    popupWindow.dismiss();
                }


            }
        });*/

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postText = edtPOst.getText().toString();
                if (postText.trim().length() <= 0 && filePath == null) {
                    Toast.makeText(GroupActivity.this, "Please write something or select an image", Toast.LENGTH_SHORT).show();
                } else {
                    if (ApplicationGlobles.isConnectingToInternet(GroupActivity.this)) {
                        edtPOst.setText("");
                        new VideoUploader().execute();
                        popupWindow.dismiss();
                    } else {
                        Toast.makeText(GroupActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });


        btnBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();

            }
        });
        ic_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                encodedString = "";
                filePath = null;
                popupWindow.dismiss();
            }
        });
    }

  /*  public void PostUploader() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(GroupActivity.this);

        if (ApplicationGlobles.isConnectingToInternet(GroupActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            JSONObject params = new JSONObject();
            try {
                params.put("description", postText);
                params.put("postFile", encodedString);
                params.put("extension", "png");
                Log.e("param", params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("WebApis.POSTINGROUP", WebApis.POSTINGROUP + id + "/");
            RequestBody body = RequestBody.create(mediaType, params + "");
            Request request = new Request.Builder()
                    .url(WebApis.POSTINGROUP + id + "/")
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("postman-token", "0dc3effa-dfa8-5edb-7266-7b403f27f027")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (GroupActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    message = jsonObject.getString("message");
                                    GroupActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            ic_image.setImageDrawable(null);
                                            bin = null;
                                            postText = "";
                                            encodedString = "";
                                            edtPOst.setText("");
                                            Toast.makeText(GroupActivity.this, message + "", Toast.LENGTH_SHORT).show();
                                            homeBeanList.clear();
                                            String p = settings.getString("profile_pic", "");
                                            Log.e("profilePic", p + ">>");
                                            getAllPost(p);
                                        }
                                    });
                                } else {
                                    message = jsonObject.getString("message");
                                    GroupActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(GroupActivity.this, message + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                GroupActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(GroupActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }*/

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose Image", "Choose Video", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(GroupActivity.this);
        builder.setTitle("Add Photo or Video!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                            && ActivityCompat.checkSelfPermission(GroupActivity.this, Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                REQUEST_CAMERA_ACCESS_PERMISSION);
                    } else {
                       /* Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePicture.resolveActivity(GroupActivity.this.getPackageManager()) != null) {
                            startActivityForResult(takePicture, 1);
                        }*/
                        dispatchTakePictureIntent();
                    }

                } else if (options[item].equals("Choose Image")) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        if (GroupActivity.this.checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                                == PackageManager.PERMISSION_GRANTED || GroupActivity.this.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            Log.v("TAG", "Permission is granted");
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            //  Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(photoPickerIntent, 2);//one can be replaced with any action code

                        } else {

                            Log.v("TAG", "Permission is revoked");
                            ActivityCompat.requestPermissions(GroupActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                        }
                    }
                } else if (options[item].equals("Choose Video")) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        if (GroupActivity.this.checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                                == PackageManager.PERMISSION_GRANTED || GroupActivity.this.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            Log.v("TAG", "Permission is granted");
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("video/*");
                            //  Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(photoPickerIntent, 3);//one can be replaced with any action code

                        } else {

                            Log.v("TAG", "Permission is revoked");
                            ActivityCompat.requestPermissions(GroupActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                        }
                    } else {

                        Log.v("TAG", "Permission is granted");
                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                        photoPickerIntent.setType("image/*");
                        //  Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(photoPickerIntent, 4);//one can be replaced with any action code
                    }

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void dispatchTakePictureIntent() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);


        File f = null;
       /* try {
            Uri photoURI = FileProvider.getUriForFile(GroupActivity.this,
                    BuildConfig.APPLICATION_ID + ".provider",
                    setUpPhotoFile());
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        } catch (IOException e) {
            e.printStackTrace();
        }*/


        startActivityForResult(takePictureIntent, 1);
    }

    private File setUpPhotoFile() throws IOException {

        File f = createImageFile();
        mCurrentPhotoPath = f.getAbsolutePath();

        return f;
    }

    private void handleBigCameraPhoto() {

        if (mCurrentPhotoPath != null) {
            setPic();
            galleryAddPic();
            mCurrentPhotoPath = null;
        }

    }

    private void setPic() {

		/* There isn't enough memory to open up more than a camera photos */
        /* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView */
        int targetW = 100;//.getWidth();
        int targetH = 100;////mImageView.getHeight();

		/* Get the size of the image */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

		/* Figure out which way needs to be reduced less */
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        }

		/* Set bitmap options to scale the image decode target */
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

		/* Decode the JPEG file into a Bitmap */
        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

        ic_image.setImageBitmap(bitmap);
        ic_image.setVisibility(View.VISIBLE);
        File file = new File(mCurrentPhotoPath);
        filePath = new FileBody(file);
        extension = "png";
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
        return imageF;
    }

    private File getAlbumDir() {
        File storageDir = null;

        storageDir = new File(Environment.getExternalStorageDirectory()
                + "/lynkpal");
      /*  if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir("CameraSample");

            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }*/

        return storageDir;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                handleBigCameraPhoto();
            } else if (requestCode == 2) {
                try {
                    Uri selectedFileUri = data.getData();
                    Picasso.with(GroupActivity.this).load(selectedFileUri.toString()).into(ic_image);
                    ic_image.setVisibility(View.VISIBLE);
                    String selectedFilePath = FilePath.getPath(GroupActivity.this, selectedFileUri);
                    File file = new File(selectedFilePath);
                    //bin = new FileBody(file);
                    filePath = new FileBody(file);
                    extension = "png";
                   /* InputStream inputStream = new FileInputStream(selectedFilePath);//You can get an inputStream using any IO API
                    byte[] bytes;
                    byte[] buffer = new byte[8192];
                    int bytesRead;
                    ByteArrayOutputStream output = new ByteArrayOutputStream();
                    try {
                        while ((bytesRead = inputStream.read(buffer)) != -1) {
                            output.write(buffer, 0, bytesRead);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    bytes = output.toByteArray();
                    encodedString = encodeToString(bytes, android.util.Base64.DEFAULT);
                    Log.e("encodedString", encodedString);*/

                } catch (Exception e) {
                    Log.e("eroore", e.toString());
                    e.printStackTrace();
                }

            } else if (requestCode == 3) {
                try {
                    Uri selectedFileUri = data.getData();
                    String selectedFilePath = FilePath.getPath(GroupActivity.this, selectedFileUri);
                    Bitmap bmThumbnail;
                    bmThumbnail = ThumbnailUtils.createVideoThumbnail(selectedFilePath,
                            MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
                    ic_image.setImageBitmap(bmThumbnail);
                    ic_image.setVisibility(View.VISIBLE);
                    File file = new File(selectedFilePath);
                    filePath = new FileBody(file);
                    extension = "mp4";
                    //  new VideoUploader().execute();
                } catch (Exception e) {
                    //    imgImageView.setImageResource(R.drawable.placeholder);
                }
            }
        }
    }

    private void saveFile(Bitmap sourceUri, File destination) {
        if (destination.exists()) destination.delete();
        try {
            FileOutputStream out = new FileOutputStream(destination);
            sourceUri.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String convertPreImagetBitmapToString(Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 90, stream); //compress to which format you want.
        byte[] byte_arr = stream.toByteArray();
        String attachmentStr = Base64.encodeBytes(byte_arr);
        Log.e("imageStr", attachmentStr);
        // showPopup(ChatActivity.this, attachmentStr, bmp);
        return attachmentStr;

    }

    public void getAllPost(final String profilePic) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(GroupActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(GroupActivity.this)) {

            GroupActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loaderDiloag.displayDiloag();
                }
            });
            //homeBeanList.clear();
            //page++;
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.GROUPPOSTS", WebApis.GROUPPOSTS + id);
            Request request = new Request.Builder()

                    .url(WebApis.GROUPPOSTS + id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (GroupActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");
                        if (jsonData.length() > 0) {
                            try {
                               /* if (page == 1) {
                                    Thread thread = new Thread(null, new Runnable() {
                                        @Override
                                        public void run() {
                                            //Set flag so we cant load new items 2 at the same time
                                            loading = true;
                                            getAllPost();
                                        }
                                    });
                                    thread.start();
                                }*/
                                JSONArray array = new JSONArray(jsonData);

                                if (array.length() > 0) {
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject jsonObject1 = array.getJSONObject(i);
                                        //   String id=jsonObject1.getString("id");
                                        Integer id = jsonObject1.getInt("id");
                                        Integer user = jsonObject1.getInt("user");
                                        String desc = jsonObject1.getString("description");
                                        String created = jsonObject1.getString("created_at");
                                        String file = jsonObject1.getString("file");
                                        String parent_post = jsonObject1.getString("parent_post");
                                        String sharer_msg = jsonObject1.getString("sharer_msg");
                                        String fullname = jsonObject1.getString("fullname");
                                        String profile_pic = jsonObject1.getString("profile_pic");
                                        String likes_count = jsonObject1.getString("likes_count");
                                        String comments_count = jsonObject1.getString("comments_count");
                                        String owner_fullname = jsonObject1.getString("owner_fullname");
                                        String owner_profile_pic = jsonObject1.getString("owner_profile_pic");
                                        String likestatus = jsonObject1.getString("likestatus");
                                        String file_type = jsonObject1.getString("file_type");
                                        String frame_name = jsonObject1.getString("frame_name");

                                        postBean = new PostBean();
                                        postBean.setId(id);
                                        postBean.setUser(user);
                                        postBean.setDesc(desc);
                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//2016-01-24T16:00:00.000Z
                                        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                                        long time = sdf.parse(created).getTime();
                                        long now = System.currentTimeMillis();
                                        CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
                                        postBean.setCreated(String.valueOf(ago));
                                        postBean.setFile(file);
                                        postBean.setParent_post(parent_post);
                                        postBean.setSharer_msg(sharer_msg);
                                        postBean.setFullname(fullname);
                                        postBean.setProfile_pic(profile_pic);
                                        postBean.setLikes_count(likes_count);
                                        postBean.setType(file_type);
                                        postBean.setFrame_name(frame_name);
                                        if (likestatus.equals("1")) {
                                            postBean.setLike(true);
                                        } else {
                                            postBean.setLike(false);

                                        }
                                        postBean.setComment(false);
                                        postBean.setDelete(false);
                                        postBean.setComments_count(comments_count + " Comments");
                                        postBean.setOwner_fullname(owner_fullname);
                                        postBean.setOwner_profile_pic(owner_profile_pic);

                                        // new
                                        postBean.setOrg_profile_pic(profile_pic);
                                        postBean.setOrg_file(jsonObject1.getString("file"));
                                        postBean.setOrg_owner_profile_pic(jsonObject1.getString("owner_profile_pic"));
                                        postBean.setOrg_frame_name(jsonObject1.getString("frame_name"));

                                        Integer adv_id=-1;
                                        String adv_name="";
                                        String adv_tags="";
                                        String adv_link="";
                                        String adv_startDate="";
                                        String adv_endDate="";
                                        Integer adv_amount=-1;
                                        Integer adv_paid=-1;
                                        String adv_image="";
                                        String adv_readCount="";


                                        postBean.setAdv_id(adv_id);
                                        postBean.setAdv_name(adv_name);
                                        postBean.setAdv_tags(adv_tags);
                                        postBean.setAdv_link(adv_link);
                                        postBean.setAdv_startDate(adv_startDate);
                                        postBean.setAdv_endDate(adv_endDate);
                                        postBean.setAdv_amount(adv_amount);
                                        postBean.setAdv_paid(adv_paid);
                                        postBean.setAdv_image(adv_image);
                                        postBean.setAdv_readCount(adv_readCount);
                                        homeBeanList.add(postBean);

                                    }
                                    GroupActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            loaderDiloag.dismissDiloag();
                                            // swipeRefreshLayout.setRefreshing(false);
                                            // homeAdapter = new HomeAdapter(GroupActivity.this, homeBeanList, Home.this, profile_pic);
                                            // recyclerView.setAdapter(homeAdapter);
                                            //  if (page == 1) {

                                            homeAdapter = new HomeAdapter(GroupActivity.this, homeBeanList, GroupActivity.this, GroupActivity.this,GroupActivity.this, profilePic,GroupActivity.this,GroupActivity.this);
                                            recycler_view.setAdapter(homeAdapter);

                                            recycler_view.setNestedScrollingEnabled(false);
                                            homeAdapter.notifyDataSetChanged();
                                           // homeAdapter.notifyItemRangeChanged(0,recycler_view.getChildCount());
                                            /*} else {
                                                if (homeAdapter != null) {
                                                    //recyclerView.getRecycledViewPool().clear();
                                                    try {
                                                        homeAdapter.notifyDataSetChanged();
                                                    } catch (Exception e) {
                                                        Log.e("error", e.toString() + "");
                                                    }
                                                } else {
                                                    try {
                                                        homeAdapter = new HomeAdapter(GroupActivity.this, homeBeanList,Home.this, Home.this, profile_pic);
                                                        recyclerView.setAdapter(homeAdapter);
                                                        homeAdapter.notifyDataSetChanged();
                                                    } catch (Exception e) {
                                                        Log.e("error", e.toString() + "");
                                                    }

                                                }
                                            }*/


                                        }
                                    });

                                }
                            } catch (Exception je) {
                                //   swipeRefreshLayout.setRefreshing(false);
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                GroupActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // swipeRefreshLayout.setRefreshing(false);
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(GroupActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void getGroupMembers() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(GroupActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(GroupActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            companyFollowersBeanList.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.GROUPMEMBERS", WebApis.GROUPMEMBERS + id);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.GROUPMEMBERS + id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (GroupActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getDetails", jsonData + "");
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {
                                JSONObject jsonObject = array.getJSONObject(a);
                                CompanyFollowersBean companyFollowersBean = new CompanyFollowersBean();
                                companyFollowersBean.setFullName(jsonObject.getString("first_name") + " " + jsonObject.getString("last_name"));
                                companyFollowersBean.setProfilePic(jsonObject.getString("profile_pic"));
                                companyFollowersBean.setUserid(jsonObject.getString("id"));
                                companyFollowersBean.setDescription(jsonObject.getString("email"));
                                companyFollowersBeanList.add(companyFollowersBean);
                            }

                        } catch (Exception e) {
                            loaderDiloag.dismissDiloag();
                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                                if (companyFollowersBeanList.size() > 0) {
                                    GroupMembersAdapter companyFollowersAdapter = new GroupMembersAdapter(GroupActivity.this, companyFollowersBeanList);

                                    expFollowersList.setAdapter(companyFollowersAdapter);
                                }
                            }
                        });


                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    @Override
    public void delete(int id) {
        homeBeanList.remove(id);
        homeAdapter.notifyDataSetChanged();
    }

    @Override
    public void image(final String path) {
        Log.e("here", "here");
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.popup_image_show, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        ImageView ic_download = (ImageView) popupView.findViewById(R.id.ic_download);
        ImageView ic_close = (ImageView) popupView.findViewById(R.id.ic_close);
        ImageView imageViewTouch;
        imageViewTouch = (ImageView) popupView.findViewById(R.id.myImage);
        Glide.with(this).load(path)/*.error(R.drawable.noimage)*/.into(imageViewTouch);
        ic_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        ic_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                file_download(path);
            }
        });


    }

    public void file_download(String uRl) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/lynkpal");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        DownloadManager mgr = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        Uri downloadUri = Uri.parse(uRl);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("Lynkpal")
                .setDescription("Image Downloading...")
                .setDestinationInExternalPublicDir("/lynkpal", "image.jpg");

        mgr.enqueue(request);

        Toast.makeText(GroupActivity.this, "Successfully Downloaded", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void clicktoopen(int id,int user) {

    }

    @Override
    public void clicktodelete(int id) {

        Log.d("openFragInter","delete");
        //Toast.makeText(GroupActivity.this,"Gp Delete",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clicktoShare(int id, String text) {

    }

    @Override
    public void videoPopup(String path) {

    }

    private class VideoUploader extends AsyncTask<Void, Void, Void> {
        String url;


        @Override
        protected void onPreExecute() {
            loaderDiloag.displayDiloag();
            url = WebApis.POSTINGROUP + id + "/";
            url = url.replace(" ", "%20");
          /*  video_name = filePath.getFilename();
            int index = video_name.lastIndexOf('.');
            video_name = video_name.substring(index, video_name.length());
            video_name = video_name.replace(".", "");
            Log.e("video_name", video_name + "");*/
            Log.e("extension", extension + "");
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub

            // Client-side HTTP transport library
            HttpClient httpClient = new DefaultHttpClient();
            // using POST method
            HttpPost httpPostRequest = new HttpPost(url);
            // httpPostRequest.setHeader("Accept", "application/x-www-form-urlencoded");
            httpPostRequest.setHeader("authorization", Constant.token);
            try {
                MultipartEntityBuilder multiPartEntityBuilder = MultipartEntityBuilder.create();

                if (filePath != null) {
                    multiPartEntityBuilder.addPart("postFile", filePath);

                } else {
                    extension = "text";
                }
                multiPartEntityBuilder.addTextBody("extension", extension.trim());
                multiPartEntityBuilder.addTextBody("description", postText);

                httpPostRequest.setEntity(multiPartEntityBuilder.build());
                Log.e("response", multiPartEntityBuilder.toString());
                HttpResponse httpResponse = null;
                httpResponse = httpClient.execute(httpPostRequest);
                HttpEntity httpEntity = httpResponse.getEntity();
                String jsonStr = EntityUtils.toString(httpEntity);
                Log.e("response", jsonStr);
                if (jsonStr != null) {

                    try {

                        //{"status":"error","message":"Please write something to post."}
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        String status = jsonObj.getString("status");
                        if (status.equals("error")) {
                            message = jsonObj.getString("message");// message
                            Log.e("message", message);
                        } else {
                            message = "Success";
                        }
                    } catch (Exception e) {
                        Log.e("Error", e.toString());
                    }

                } else {
                    Log.e("ServiceHandler", "Couldn't get any data from the url");
                }
            } catch (Exception e) {
                Log.e("Error", e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
          /*  if (Constant.file_Type.equals("folder"))
            {
                Toast.makeText(context, "Folder Shared Successfully", Toast.LENGTH_SHORT).show();
               finish();
            }
            else {*/
            filePath = null;
            extension = "";
            postText = "";
            loaderDiloag.dismissDiloag();
            homeBeanList.clear();
            getAllPost(p);
            Toast.makeText(GroupActivity.this, message + "", Toast.LENGTH_SHORT).show();
            super.onPostExecute(aVoid);
            //  }
        }
    }
}
