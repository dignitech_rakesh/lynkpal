package com.app.lynkpal;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Adapter.CustomAdapter;
import com.app.lynkpal.Bean.industriesBean;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.FilePath;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class UpdateCompanyProfile extends AppCompatActivity {
    private static final String TAG = UpdateCompanyProfile.class.getSimpleName();
    EditText edtComName, edtAdd, edtWeb, edtPhone, edtDetails;
    Spinner spnr;
    ImageView imgImage;
    Button btnBrowse, btnUpdate;
    Intent pickPhoto;
    String encodedString;
    industriesBean industriesBean;
    CustomAdapter customAdapter;
    List<industriesBean> list = new ArrayList<>();
    TextView head;
    String selectedIndustry = "0", status;
    String company_link, company_description, mobile, company_name, address, industryname, company_banner;
    ImageView imgAddress;
    String lat, lon, add;
    String mark_address = " ";

    @Override
    protected void onResume() {
        if (Constant.update_address)
        {
            Constant.update_address = false;
            lat = String.valueOf(ChangeAddress.lat);
            lon = String.valueOf(ChangeAddress.lon);
            add = ChangeAddress.address;


        }
        super.onResume();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_company_profile);
        //Declare Fonts Typeface
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        edtComName = (EditText) findViewById(R.id.edtComName);
        head = (TextView) findViewById(R.id.head);
        edtAdd = (EditText) findViewById(R.id.edtAdd);
        edtWeb = (EditText) findViewById(R.id.edtWeb);
        edtPhone = (EditText) findViewById(R.id.edtPhone);
        edtDetails = (EditText) findViewById(R.id.edtDetails);
        spnr = (Spinner) findViewById(R.id.spnr);
        imgImage = (ImageView) findViewById(R.id.imgImage);
        imgAddress = findViewById(R.id.imgAddress);
       imgAddress.setOnClickListener(new View.OnClickListener()
       {
           @Override
           public void onClick(View view)
           {
               finish();
               startActivity(new Intent(UpdateCompanyProfile.this, MapActivity.class));
           }
       });
        btnBrowse = (Button) findViewById(R.id.btnBrowse);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        head.setTypeface(tf_bold);
        btnBrowse.setTypeface(tf_bold);
        btnUpdate.setTypeface(tf_bold);
        edtComName.setTypeface(tf_reg);
        edtAdd.setTypeface(tf_reg);
        edtWeb.setTypeface(tf_reg);
        edtPhone.setTypeface(tf_reg);
        edtDetails.setTypeface(tf_reg);
        ImageView ic_back = (ImageView) findViewById(R.id.ic_back);
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        spnr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedIndustry = list.get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mark_address = getIntent().getStringExtra("address");
        getDetails();



        btnBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED || checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.v("TAG", "Permission is granted");
                        pickPhoto = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, 0);//one can be replaced with any action code

                    } else {

                        Log.v("TAG", "Permission is revoked");
                        ActivityCompat.requestPermissions(UpdateCompanyProfile.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                    }
                } else {

                    Log.v("TAG", "Permission is granted");
                    pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 0);//one can be replaced with any action code
                }
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ApplicationGlobles.isNullOrEmpty(edtComName.getText().toString().trim())) {
                    if (!ApplicationGlobles.isNullOrEmpty(edtAdd.getText().toString().trim())) {
                        if (!ApplicationGlobles.isNullOrEmpty(edtWeb.getText().toString().trim())) {
                            if (!ApplicationGlobles.isNullOrEmpty(edtPhone.getText().toString().trim())) {
                                if (!ApplicationGlobles.isNullOrEmpty(edtDetails.getText().toString().trim())) {

                                    if (!selectedIndustry.equals("0")) {
                                        UpdateCompanyDetails(edtComName.getText().toString().trim(), edtAdd.getText().toString().trim(), edtWeb.getText().toString().trim(),
                                                edtPhone.getText().toString().trim(), edtDetails.getText().toString().trim());
                                    } else {
                                        Toast.makeText(UpdateCompanyProfile.this, "Please Select Industry Name", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    edtDetails.setError("Required");
                                }
                            } else {
                                edtPhone.setError("Required");
                            }
                        } else {
                            edtWeb.setError("Required");
                        }
                    } else {
                        edtAdd.setError("Required");
                    }
                } else {
                    edtComName.setError("Required");
                }
            }
        });

    }

    public void getDetails() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(UpdateCompanyProfile.this);
        if (ApplicationGlobles.isConnectingToInternet(UpdateCompanyProfile.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            status = "false";
            Log.e("Constant.token", Constant.token);
            Log.e("Constant.api", WebApis.CompanyDetails + Constant.user_id);
            Request request = new Request.Builder()
                    .url(WebApis.CompanyDetails + Constant.user_id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (UpdateCompanyProfile.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getDetails", jsonData + "");
                        loaderDiloag.dismissDiloag();
                        try {
                            JSONObject jsonObject = new JSONObject(jsonData);
                            status = jsonObject.getString("status");
                            if (status.equals("success")) {
                                company_link = jsonObject.getString("company_link");
                                company_description = jsonObject.getString("company_description");
                                mobile = jsonObject.getString("mobile");
                                //    company_lon = jsonObject.getString("company_lon");
                                // followers_count = jsonObject.getString("followers_count");
                                company_name = jsonObject.getString("company_name");
                                address = jsonObject.getString("address");
                                Log.d(TAG, "onResponse: Address " + address);

                                //   company_lat = jsonObject.getString("company_lat");
                                industryname = jsonObject.getString("industryname");
                                company_banner = jsonObject.getString("company_banner");


                                //   follow_status = jsonObject.getString("follow_status");
                            }
                        } catch (Exception e) {

                        }


                        UpdateCompanyProfile.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                                if (status.equals("success")) {
//                                    //    EditText edtComName, edtAdd, edtWeb, edtPhone, edtDetails;

                                    // Picasso.with(UpdateCompanyProfile.this).load(WebApis.CmpPageUserImage + profile_pic).placeholder(R.drawable.image).error(R.drawable.image).into(imgProfile);
                                    Picasso.with(UpdateCompanyProfile.this).load(WebApis.CmpBannerImage + company_banner).placeholder(R.drawable.noimage).error(R.drawable.noimage).into(imgImage);
                                    if (!company_description.equals("null"))
                                    {
                                        edtDetails.setText(company_description);
                                    }
                                    if (!company_name.equals("null"))
                                    {
                                        edtComName.setText(company_name);
                                    }
                                    Log.d(TAG, "run: Adrs " + mark_address + address);

                                    if (mark_address!=null)
                                    {
                                        edtAdd.setText(mark_address);
                                    }
                                    else if (address!="null")
                                    {
                                        edtAdd.setText(address);
                                        Log.d(TAG,"not null value " + mark_address);
                                    }
                                    else if (address=="null")
                                    {
                                        edtAdd.setText(" ");
                                    }


                                    if (!company_link.equals("null")) {
                                        edtWeb.setText(company_link);
                                    }
                                    if (!mobile.equals("null")) {
                                        edtPhone.setText(mobile);
                                    }
                                    getIndustry(industryname);
                                }
                            }
                        });


                    }
                });

            } catch (Exception e) {
                UpdateCompanyProfile.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(UpdateCompanyProfile.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void UpdateCompanyDetails(String edtComName, String edtAdd, String edtWeb, String edtPhone, String edtDetails)
    {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(UpdateCompanyProfile.this);
        mark_address = null;
        if (ApplicationGlobles.isConnectingToInternet(UpdateCompanyProfile.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            //String params="{\r\n\"description\":\"Write text to post\",\r\n\"postFile\":\"sadc\",\r\n\"extension\":\"jpeg\"\r\n}";
            JSONObject params = new JSONObject();
            try {
                params.put("companyName", edtComName);
                params.put("industry", selectedIndustry);
                params.put("companyDescription", edtDetails);
                params.put("companyAddress", edtAdd);
                params.put("companyMobile", edtPhone);
                params.put("companyLink", edtWeb);
                params.put("companyLat", lat);
                params.put("companyLon", lon);
                params.put("bannerPic", encodedString);
                Log.e("param", params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(mediaType, params + "");
            Request request = new Request.Builder()
                    .url(WebApis.UpdateCompanyDetails)
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("postman-token", "0dc3effa-dfa8-5edb-7266-7b403f27f027")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (UpdateCompanyProfile.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(UpdateCompanyProfile.this, "Company Profile Updated", Toast.LENGTH_SHORT).show();
                                            Constant.update = true;
                                            finish();
                                            startActivity(getIntent());
                                        }
                                    });
                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(UpdateCompanyProfile.this, "Something went wrong try later...", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                        loaderDiloag.dismissDiloag();
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(UpdateCompanyProfile.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            try {
                Uri selectedFileUri = data.getData();
                Picasso.with(UpdateCompanyProfile.this).load(selectedFileUri.toString()).into(imgImage);
                imgImage.setVisibility(View.VISIBLE);
                String selectedFilePath = FilePath.getPath(UpdateCompanyProfile.this, selectedFileUri);
                // File file = new File(selectedFilePath);
                //bin = new FileBody(file);
                InputStream inputStream = new FileInputStream(selectedFilePath);//You can get an inputStream using any IO API
                byte[] bytes;
                byte[] buffer = new byte[8192];
                int bytesRead;
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        output.write(buffer, 0, bytesRead);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                bytes = output.toByteArray();
                encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);
                Log.e("encodedString", encodedString);

            } catch (Exception e) {
                Log.e("eroore", e.toString());
                e.printStackTrace();
            }
        }

    }

    public void getIndustry(final String industryname) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(UpdateCompanyProfile.this);
        if (ApplicationGlobles.isConnectingToInternet(UpdateCompanyProfile.this)) {
            loaderDiloag.displayDiloag();
            list.clear();
            industriesBean = new industriesBean();
            industriesBean.setId("0");
            industriesBean.setName("---Select---");
            list.add(industriesBean);
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", Constant.token);
            Request request = new Request.Builder()
                    .url(WebApis.AllIndustry)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .addHeader("postman-token", "3f218766-a273-e0f2-84b8-a8a740a98c44")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (UpdateCompanyProfile.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                JSONArray array = new JSONArray(jsonData);
                                if (array.length() > 0) {
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject jsonObject1 = array.getJSONObject(i);
                                        //   String id=jsonObject1.getString("id");
                                        String id = jsonObject1.getString("id");
                                        String name = jsonObject1.getString("name");
                                        industriesBean = new industriesBean();
                                        industriesBean.setId(id);
                                        industriesBean.setName(name);
                                        list.add(industriesBean);

                                    }
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            loaderDiloag.dismissDiloag();
                                            customAdapter = new CustomAdapter(UpdateCompanyProfile.this, list);
                                            spnr.setAdapter(customAdapter);
                                            for (int a = 0; a < list.size(); a++) {
                                                if (industryname.equals(list.get(a).getName())) {
                                                    spnr.setSelection(a);
                                                }
                                            }
                                        }
                                    });

                                }
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(UpdateCompanyProfile.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

}
