package com.app.lynkpal;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
/*import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;*/
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Adapter.CommentAdapterr;
import com.app.lynkpal.Adapter.CommentListAdapter;
import com.app.lynkpal.Application.Utils;
import com.app.lynkpal.Bean.CommentReplies;
import com.app.lynkpal.Bean.CommentsBean;
import com.app.lynkpal.Bean.PostBean;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.CircleImageView;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.ExpandedHightListview;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.bumptech.glide.Glide;
//import com.bumptech.glide.request.RequestOptions;
//import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class CommentActivity extends AppCompatActivity  {
    public TextView postText, userName, txtLikesCount, txtCommentsCount, currentTimeStamp;
    public ImageView postimageView, profileImageShare, play_icon;
    public CircleImageView profileImage, commentProfile;
    public ImageView comment, like, share, imgSendComment, ic_more, post_image_share, play_icon_share;
    public LinearLayout linearLayout, linear_share_layout, linear_mage_layout_share;
    public TextView tvTimestamp, deletePopup, post_text_share, username_share, time_stamp_share;
    RelativeLayout comment_layout;
    EditText edtComment;
    CardView cardViewMain;
    String id, likeCount, commentCount, created, post_text, file, username, profile_pic, user_id, user_profile_pic;
    Boolean isLike;
    CommentsBean commentsBean;
    CommentAdapterr commentAdapterr;
    List<CommentsBean> commentsBeenList = new ArrayList<>();
    ExpandedHightListview list;

//    CommentsBean commentsBean;
//    CommentListAdapter commentAdapterr;
//    ArrayList<CommentsBean> commentsBeenList = new ArrayList<>();
//    ExpandableListView expandableListView;
    TextView head;
    PostBean postBean;
    SharedPreferences settings;
    public static String TAG = CommentActivity.class.getSimpleName();

    String token="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        settings = getSharedPreferences(Constant.PREFS_NAME, 0);
        profile_pic = settings.getString("profile_pic", "");

        list = (ExpandedHightListview) findViewById(R.id.list);
        //expandableListView = (ExpandableListView) findViewById(R.id.cmnt_exp_list);

        head = (TextView) findViewById(R.id.head);
        txtLikesCount = (TextView) findViewById(R.id.like_count);
        txtCommentsCount = (TextView) findViewById(R.id.comment_count);
        postText = (TextView) findViewById(R.id.post_text);
        profileImageShare = (ImageView) findViewById(R.id.profile_user_image_share);
        play_icon = (ImageView) findViewById(R.id.play_icon);
        userName = (TextView) findViewById(R.id.username);
        post_text_share = (TextView) findViewById(R.id.post_text_share);
        username_share = (TextView) findViewById(R.id.username_share);
        time_stamp_share = (TextView) findViewById(R.id.time_stamp_share);
        deletePopup = (TextView) findViewById(R.id.deletePopup);
        post_image_share = (ImageView) findViewById(R.id.post_image_share);
        play_icon_share = (ImageView) findViewById(R.id.play_icon_share);
        profileImage = (CircleImageView) findViewById(R.id.profile_user_image);
        profileImage.setImageResource(R.drawable.image);
        postimageView = (ImageView) findViewById(R.id.post_image);
        linear_share_layout = (LinearLayout) findViewById(R.id.linear_share_layout);
        commentProfile = (CircleImageView) findViewById(R.id.user_image_post);
        cardViewMain = (CardView) findViewById(R.id.cardViewMain);
        linear_mage_layout_share = (LinearLayout) findViewById(R.id.linear_mage_layout_share);
        commentProfile.setImageResource(R.drawable.image);
        comment = (ImageView) findViewById(R.id.chat);
        like = (ImageView) findViewById(R.id.like);
        share = (ImageView) findViewById(R.id.share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupShare();
            }
        });
        imgSendComment = (ImageView) findViewById(R.id.sendComment);
        ic_more = (ImageView) findViewById(R.id.ic_more);
        edtComment = (EditText) findViewById(R.id.user_Comment);
        linearLayout = (LinearLayout) findViewById(R.id.linear_mage_layout);
        comment_layout = (RelativeLayout) findViewById(R.id.comment_layout);
        currentTimeStamp = (TextView) findViewById(R.id.time_stamp);
        tvTimestamp = (TextView) findViewById(R.id.time_stamp);
        ImageView ic_back = (ImageView) findViewById(R.id.ic_back);
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        cardViewMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deletePopup.setVisibility(View.GONE);
            }
        });
        ic_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (deletePopup.getVisibility() == View.VISIBLE) {
                    deletePopup.setVisibility(View.GONE);

                } else {
                    deletePopup.setVisibility(View.VISIBLE);
                }
            }
        });
        deletePopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                deletePopup.setVisibility(View.GONE);
                showPopup();
            }
        });
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        userName.setTypeface(tf_bold);
        head.setTypeface(tf_bold);
        txtLikesCount.setTypeface(tf_reg);
        txtCommentsCount.setTypeface(tf_reg);
        postText.setTypeface(tf_med);
        post_text_share.setTypeface(tf_med);
        tvTimestamp.setTypeface(tf_med);
        username_share.setTypeface(tf_bold);
        edtComment.setTypeface(tf_med);
        deletePopup.setTypeface(tf_bold);
        time_stamp_share.setTypeface(tf_med);


        try {
            id = getIntent().getStringExtra("id_");

        } catch (Exception e) {
            Log.e("this", e + "");
            Toast.makeText(this, "Try Later", Toast.LENGTH_SHORT).show();
            finish();
        }
        SharedPreferences sharedPreferences = (this).getSharedPreferences(Constant.PREFS_NAME, 0);
        token= sharedPreferences.getString("token",token);
        if (token.equals("")){
            token=Constant.token;
        }

        if (!profile_pic.equals("null")&&!profile_pic.equals(null)&&!profile_pic.equals("")) {
            //Log.d("log3",postBean.getProfile_pic());

           Glide.with(this).load(WebApis.userProfileImage + profile_pic).into(commentProfile);
          //  commentProfile.setImageResource(R.drawable.image);

        } else {
            commentProfile.setImageResource(R.drawable.image);
        }
        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (LoginActivity.isNetworkConnected(CommentActivity.this))
                {
                    Constant.update_home = true;
                    if (isLike) {
                        isLike = false;
                        like.setImageResource(R.drawable.thumb_unlike);
                        int count = Integer.parseInt(likeCount);
                        count = count - 1;
                        likeCount = String.valueOf(count);
                        txtLikesCount.setText(String.valueOf(count));
                        Like();

                    }
                    else
                        {
                        isLike = true;
                        like.setImageResource(R.drawable.thumb_like);
                        int count = Integer.parseInt(likeCount);
                        count = count + 1;
                        likeCount = String.valueOf(count);
                        txtLikesCount.setText(String.valueOf(count));
                        Like();

                    }
                }
                else {
                    Toast.makeText(CommentActivity.this,"No Internet Connection",Toast.LENGTH_SHORT).show();
                }
            }

        });

        imgSendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.update_home = true;
                String commentText = Utils.encodeMessage(edtComment.getText().toString());
                if (commentText.trim().length() <= 0) {
                    Toast.makeText(CommentActivity.this, "Please Write Something..", Toast.LENGTH_SHORT).show();
                } else {
                    edtComment.setText("");
                    Comment(commentText);
                }

            }
        });
        GetPostDetail();
        getAllComments();


      /*  expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                setListViewHeight(parent, groupPosition);
                return false;
            }
        });
*/

    }


    private void setListViewHeight(ExpandableListView listView,
                                   int group) {
        ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group))
                    || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null,
                            listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                    totalHeight += listItem.getMeasuredHeight();

                }
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height;
        if (postimageView.getVisibility() == View.VISIBLE) {
            Log.d("image view","enter");
             height = totalHeight
                    + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1))+ postimageView.getLayoutParams().height;
            // Its visible
        } else {
            // Either gone or invisible

             height = totalHeight
                    + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1)) + 1000;

        }
//        int height = totalHeight
//                + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();

    }

    public void GetPostDetail() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(CommentActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(CommentActivity.this)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loaderDiloag.displayDiloag();
                }
            });
            //homeBeanList.clear();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", token);
            Log.e("WebApis.allPOST", WebApis.GetPostDetail + id);
            Request request = new Request.Builder()

                    .url(WebApis.GetPostDetail + id)
                    .get()
                    .addHeader("authorization", token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                            }
                        });

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");
                        if (jsonData.length() > 0) {
                            try {
                                JSONArray array = new JSONArray(jsonData);

                                if (array.length() > 0) {
                                    JSONObject jsonObject1 = array.getJSONObject(0);
                                    //   String id=jsonObject1.getString("id");
                                    Integer id = jsonObject1.getInt("id");
                                    Integer user = jsonObject1.getInt("user");
                                    String desc = Utils.decodeMessage(jsonObject1.getString("description"));
                                    String created = jsonObject1.getString("created_at");
                                    String file = jsonObject1.getString("file");
                                    String parent_post = jsonObject1.getString("parent_post");
                                    String sharer_msg = Utils.decodeMessage(jsonObject1.getString("sharer_msg"));
                                    String fullname = jsonObject1.getString("fullname");
                                    String profile_pic = jsonObject1.getString("profile_pic");
                                    likeCount = jsonObject1.getString("likes_count");
                                    String comments_count = jsonObject1.getString("comments_count");
                                    String owner_fullname = jsonObject1.getString("owner_fullname");
                                    String owner_profile_pic = jsonObject1.getString("owner_profile_pic");
                                    String likestatus = jsonObject1.getString("likestatus");
                                    String file_type = jsonObject1.getString("file_type");
                                    String frame_name = jsonObject1.getString("frame_name");
                                    postBean = new PostBean();
                                    postBean.setType(file_type);
                                    postBean.setFrame_name(frame_name);
                                    postBean.setId(id);
                                    postBean.setUser(user);
                                    postBean.setDesc(desc);
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//2016-01-24T16:00:00.000Z
                                    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                                    long time = sdf.parse(created).getTime();
                                    long now = System.currentTimeMillis();
                                    CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
                                    Log.d(TAG, "onResponse: Ago Value " + ago);
                                    postBean.setCreated(String.valueOf(ago));
                                    postBean.setFile(file);
                                    postBean.setParent_post(parent_post);
                                    postBean.setSharer_msg(sharer_msg);
                                    postBean.setFullname(fullname);
                                    postBean.setProfile_pic(profile_pic);
                                    postBean.setLikes_count(likeCount);
                                    Log.d("likesCount",likeCount);
                                    if (likestatus.equals("1")) {
                                        postBean.setLike(true);
                                    } else {
                                        postBean.setLike(false);

                                    }
                                    postBean.setComment(false);
                                    postBean.setDelete(false);
                                    postBean.setComments_count(comments_count + "");
                                    postBean.setOwner_fullname(owner_fullname);
                                    postBean.setOwner_profile_pic(owner_profile_pic);


                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            loaderDiloag.dismissDiloag();
                                            txtLikesCount.setText(postBean.getLikes_count());
                                            txtCommentsCount.setText(postBean.getComments_count());
                                            profileImage.setImageResource(R.drawable.image);
                                            if (postBean.getCreated().toLowerCase().equals("0 minutes ago") || postBean.getCreated().toLowerCase().equals("in 0 minutes")) {
                                                tvTimestamp.setText("Just Now");
                                            } else {
                                                tvTimestamp.setText(postBean.getCreated());

                                            }
                                            if (postBean.getUser().toString().equals(Constant.user_id)) {
                                                ic_more.setVisibility(View.VISIBLE);
                                            } else {
                                                ic_more.setVisibility(View.GONE);

                                            }
                                            if (postBean.getDesc().equals("null") || postBean.getDesc().trim().equals("")) {
                                                postText.setVisibility(View.GONE);
                                            } else {
                                                postText.setVisibility(View.VISIBLE);
                                                // postText.setText(postBean.getDesc());
                                                try {
                                                    postText.setText(Html.fromHtml(postBean.getDesc()));
                                                } catch (Exception e) {
                                                    postText.setText(postBean.getDesc());
                                                }
                                            }
                                            if (!postBean.getFullname().equals("null")) {
                                                userName.setText(postBean.getFullname());
                                            }
                                            userName.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    startActivity(new Intent(CommentActivity.this, UserAndCompanyPageDetails.class).putExtra("User_id", postBean.getUser() + ""));
                                                }
                                            });
                                            profileImage.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    startActivity(new Intent(CommentActivity.this, UserAndCompanyPageDetails.class).putExtra("User_id", postBean.getUser() + ""));

                                                }
                                            });
                                            if (!postBean.getFile().equals("null")) {
                                                if (postBean.getType().equals("image")) {
                                                    play_icon.setVisibility(View.GONE);
                                                    linearLayout.setVisibility(View.VISIBLE);
                                                    postimageView.setVisibility(View.VISIBLE);
                                                   /* RequestOptions options = new RequestOptions()
                                                            .fitCenter();*/
                                                    Glide.with(CommentActivity.this).load(WebApis.postImage + postBean.getFile()).into(postimageView);
                                                   // postimageView.setImageResource(R.drawable.image);
                                                } else {
                                                    linearLayout.setVisibility(View.VISIBLE);
                                                    postimageView.setVisibility(View.VISIBLE);
                                                    play_icon.setVisibility(View.VISIBLE);
                                                    Glide.with(CommentActivity.this).load(WebApis.postImage + postBean.getFrame_name())/*.centerCrop().error(R.drawable.noimage)*/.into(postimageView);
                                                   // postimageView.setImageResource(R.drawable.image);
                                                }


                                            } else {
                                                linearLayout.setVisibility(View.GONE);
                                                postimageView.setVisibility(View.GONE);
                                            }
                                            postimageView.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    if (postBean.getType().equals("image")) {
                                                        image(WebApis.postImage + postBean.getFile());
                                                    } else {
                                                        startActivity(new Intent(CommentActivity.this, VideoActivity.class).putExtra("url", WebApis.postImage + postBean.getFile()));
                                                    }
                                                }
                                            });
                                            if (!postBean.getOwner_fullname().equals("null")) {
                                                linear_share_layout.setVisibility(View.VISIBLE);

                                                if (!postBean.getFile().equals("null")) {
                                                    post_image_share.setVisibility(View.VISIBLE);
                                                    if (postBean.getType().equals("image")) {
                                                        linear_mage_layout_share.setVisibility(View.VISIBLE);
                                                        linearLayout.setVisibility(View.GONE);
                                                        postimageView.setVisibility(View.GONE);
                                                        play_icon_share.setVisibility(View.GONE);
                                                       Glide.with(CommentActivity.this).load(WebApis.postImage + postBean.getFile())/*.centerCrop().error(R.drawable.noimage)*/.into(post_image_share);
                                                      //  post_image_share.setImageResource(R.drawable.image);
                                                    } else {
                                                        linear_mage_layout_share.setVisibility(View.VISIBLE);
                                                        linearLayout.setVisibility(View.GONE);
                                                        postimageView.setVisibility(View.GONE);
                                                        play_icon_share.setVisibility(View.VISIBLE);
                                                       Glide.with(CommentActivity.this).load(WebApis.postImage + postBean.getFrame_name())/*.centerCrop().error(R.drawable.noimage)*/.into(post_image_share);
                                                    //    post_image_share.setImageResource(R.drawable.image);
                                                    }
                                                } else {
                                                    linear_mage_layout_share.setVisibility(View.GONE);
                                                    post_image_share.setVisibility(View.GONE);
                                                }
                                                // postimageView.setVisibility(View.VISIBLE);


                                            } else {
                                                linear_share_layout.setVisibility(View.GONE);
                                                //postimageView.setVisibility(View.GONE);
                                            }

                                            post_image_share.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    if (postBean.getType().equals("image")) {
                                                        image(WebApis.postImage + postBean.getFile());
                                                    } else {
                                                        startActivity(new Intent(CommentActivity.this, VideoActivity.class).putExtra("url", WebApis.postImage + postBean.getFile()));
                                                    }
                                                }
                                            });
                                            if (!postBean.getProfile_pic().equals("null"))
                                            {
                                                profileImage.setFocusable(true);
                                                Log.d("log1",postBean.getProfile_pic());
                                               // commentProfile.setImageResource(R.drawable.image);
                                                Glide.with(CommentActivity.this).load(WebApis.userProfileImage + postBean.getProfile_pic()).into(profileImage);
                                              /*  Glide.with(CommentActivity.this).load(WebApis.userProfileImage + postBean.getProfile_pic()).asBitmap().placeholder(R.drawable.image).error(R.drawable.image).centerCrop().into(new BitmapImageViewTarget(profileImage) {
                                                    @Override
                                                    protected void setResource(Bitmap resource) {
                                                        RoundedBitmapDrawable circularBitmapDrawable =
                                                                RoundedBitmapDrawableFactory.create(CommentActivity.this.getResources(), resource);
                                                        circularBitmapDrawable.setCircular(true);
                                                        profileImage.setImageDrawable(circularBitmapDrawable);
                                                    }
                                                });*/


                                                // notifyDataSetChanged();
                                                //  Glide.with(CommentActivity.this).load(WebApis.userProfileImage+postBean.getProfile_pic()).placeholder(R.drawable.image).error(R.drawable.image).into(profileImage);
                                            } else {
                                                profileImage.setImageDrawable(CommentActivity.this.getResources().getDrawable(R.drawable.image));
                                                commentProfile.setImageResource(R.drawable.image);
                                            }
                                            if (!postBean.getOwner_profile_pic().equals("null"))
                                            {
                                                profileImageShare.setFocusable(true);
                                                Log.d("log2",postBean.getProfile_pic());
                                               // commentProfile.setImageResource(R.drawable.image);
                                               Glide.with(CommentActivity.this).load(WebApis.userProfileImage + postBean.getOwner_profile_pic()).into(profileImageShare);
                                               /* Glide.with(CommentActivity.this).load(WebApis.userProfileImage + postBean.getOwner_profile_pic()).asBitmap().placeholder(R.drawable.image).error(R.drawable.image).centerCrop().into(new BitmapImageViewTarget(profileImageShare) {
                                                    @Override
                                                    protected void setResource(Bitmap resource) {
                                                        RoundedBitmapDrawable circularBitmapDrawable =
                                                                RoundedBitmapDrawableFactory.create(CommentActivity.this.getResources(), resource);
                                                        circularBitmapDrawable.setCircular(true);
                                                        profileImageShare.setImageDrawable(circularBitmapDrawable);
                                                    }
                                                });*/
                                                // notifyDataSetChanged();
                                                //  Glide.with(CommentActivity.this).load(WebApis.userProfileImage+postBean.getProfile_pic()).placeholder(R.drawable.image).error(R.drawable.image).into(profileImage);
                                            } else {
                                                profileImageShare.setImageDrawable(CommentActivity.this.getResources().getDrawable(R.drawable.image));
                                            }
                                            if (!postBean.getOwner_fullname().equals("null")) {
                                                username_share.setText(postBean.getOwner_fullname());
                                            }
                                            if (!postBean.getSharer_msg().trim().equals("")) {
                                                if (postBean.getDesc().equals("null")) {
                                                    post_text_share.setVisibility(View.GONE);
                                                } else {
                                                    post_text_share.setVisibility(View.VISIBLE);
                                                    post_text_share.setText(postBean.getDesc());
                                                }
                                                postText.setVisibility(View.VISIBLE);
                                                //postText.setText(postBean.getSharer_msg());
                                                try {
                                                    postText.setText(Html.fromHtml(postBean.getSharer_msg()));
                                                } catch (Exception e) {
                                                    postText.setText(postBean.getSharer_msg());
                                                }
                                            }
                                            if (postBean.getCreated().toLowerCase().equals("0 minutes ago") || postBean.getCreated().toLowerCase().equals("in 0 minutes")) {
                                                time_stamp_share.setText("Just Now");
                                            } else {
                                                time_stamp_share.setText(postBean.getCreated());

                                            }
                                            if (postBean.getLike()) {
                                                isLike = true;
                                                like.setImageResource(R.drawable.thumb_like);
                                            } else {
                                                isLike = false;
                                                like.setImageResource(R.drawable.thumb_unlike);
                                            }

                                            if (postBean.getDelete()) {
                                                deletePopup.setVisibility(View.VISIBLE);
                                            } else {
                                                deletePopup.setVisibility(View.GONE);
                                            }


                                        }
                                    });

                                }
                            } catch (Exception e) {
                                Log.e("error", "er>" + e.toString());
                            }


                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void image(final String path) {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.popup_image_show, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        ImageView ic_download = (ImageView) popupView.findViewById(R.id.ic_download);
        ImageView ic_close = (ImageView) popupView.findViewById(R.id.ic_close);
        ImageView imageViewTouch;
        imageViewTouch =  popupView.findViewById(R.id.myImage);
        Glide.with(this).load(path)/*.error(R.drawable.noimage).error(R.drawable.noimage)*/.into(imageViewTouch);
        ic_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        ic_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                file_download(path);
            }
        });
    }

    public void file_download(String uRl) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/lynkpal");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        DownloadManager mgr = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        Uri downloadUri = Uri.parse(uRl);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("Lynkpal")
                .setDescription("Image Downloading...")
                .setDestinationInExternalPublicDir("/lynkpal", "image.jpg");

        mgr.enqueue(request);

        Toast.makeText(this, "Successfully Downloaded", Toast.LENGTH_SHORT).show();

    }

    public void showPopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_group_join_leave, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        final TextView textText = (TextView) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        textTitle.setText("Delete Post");
        textText.setText("Are you sure, you want to delete Post?");
        // textText.setText("Are you sure, you want to leave " + '"' + text + '"' + " Group?");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delete();
                popupWindow.dismiss();

            }
        });
    }

    public void delete() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(CommentActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(CommentActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", token + "");
            Request request = new Request.Builder()
                    .url(WebApis.DeletePOST + id)
                    .get()
                    .addHeader("authorization", token)
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (CommentActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("respons", jsonData + "");
                        try {
                            JSONObject jsonObject = new JSONObject(jsonData);
                            final String success = jsonObject.getString("status");

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (success.equals("success")) {
                                        Toast.makeText(CommentActivity.this, "Post Successfully Deleted", Toast.LENGTH_SHORT).show();
                                        Constant.update_home = true;
                                        finish();
                                    }
                                    loaderDiloag.dismissDiloag();
                                }
                            });
                        } catch (Exception e) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loaderDiloag.dismissDiloag();
                                }
                            });
                        }
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(CommentActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    // old getAllComments
    public void getAllComments() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(CommentActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(CommentActivity.this)) {
            loaderDiloag.displayDiloag();
            commentsBeenList.clear();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", token);
            Request request = new Request.Builder()
                    .url(WebApis.AllCommentPOST + id + "/")
                    .get()
                    .addHeader("authorization", token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (CommentActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                JSONArray array = new JSONArray(jsonData);
                                if (array.length() > 0) {
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject jsonObject1 = array.getJSONObject(i);
                                        //   String id=jsonObject1.getString("id");
                                        Integer id = jsonObject1.getInt("id");
                                        Integer user = jsonObject1.getInt("user");
                                        String created = jsonObject1.getString("created_at");
                                        String message = Utils.decodeMessage(jsonObject1.getString("message"));
                                        String fullname = jsonObject1.getString("fullname");
                                        String profile_pic = jsonObject1.getString("profile_pic");
                                        commentsBean = new CommentsBean();
                                        commentsBean.setId(String.valueOf(id));
                                        commentsBean.setUser(String.valueOf(user));
                                        commentsBean.setProfile_pic(profile_pic);
                                        commentsBean.setFullname(fullname);
                                        commentsBean.setMessage(message);

                                        commentsBean.setLikes_count(jsonObject1.getString("likes_count"));
                                        commentsBean.setReplies_count(jsonObject1.getString("replies_count"));


                                        JSONArray jsonRplyArray=jsonObject1.getJSONArray("commentreplies");
                                        JSONObject jsonRplyObj;
                                        CommentReplies cmntRply ;
                                        ArrayList<CommentReplies> cmntRplyArrayList =new ArrayList<CommentReplies>();
                                        for (int j=0;j<jsonRplyArray.length();j++){
                                            cmntRply =new CommentReplies();
                                            jsonRplyObj=jsonRplyArray.getJSONObject(j);
                                            cmntRply.setId_rply(jsonRplyObj.getInt("id"));
                                            cmntRply.setLevel_rply(jsonRplyObj.getInt("level"));
                                            cmntRply.setFullname_rply(jsonRplyObj.getString("fullname"));
                                            cmntRply.setMessage_rply(jsonRplyObj.getString("message"));
                                            cmntRplyArrayList.add(cmntRply);
                                        }




                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//2016-01-24T16:00:00.000Z
                                        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                                        long time = sdf.parse(created).getTime();
                                        long now = System.currentTimeMillis();
                                        CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
                                        commentsBean.setCreated_at(String.valueOf(ago));
                                        commentsBean.setCommentRepliesArrayList(cmntRplyArrayList);
                                        commentsBeenList.add(commentsBean);

                                    }
                                    CommentActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            loaderDiloag.dismissDiloag();
                                            commentAdapterr = new CommentAdapterr(CommentActivity.this, commentsBeenList);
                                            list.setExpanded(true);
                                            list.setAdapter(commentAdapterr);
                                        }
                                    });

                                }
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                CommentActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(CommentActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

   /*    // New getAllComments
   public void getAllComments() {


        commentsBeenList.clear();
        OkHttpClient client = new OkHttpClient();
        // Log.e("Constant.token", "852fdcdbf8eccb01a7b0c5c52bf67834e4126f4e");
        Log.e("Constant.token", token);
        Request request = new Request.Builder()
                .url(WebApis.AllCommentPOST + id + "/")
                .get()
                .addHeader("authorization", token)
                .addHeader("cache-control", "no-cache")
                .build();


        try {
            okhttp3.Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(okhttp3.Call call, IOException e) {
                    if (CommentActivity.this == null)
                        return;


                }

                @Override
                public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                    String jsonData = response.body().string();
                    Log.e("response withme ", jsonData + "");


                    if (jsonData.length() > 0) {
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            if (array.length() > 0) {
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsonObject1 = array.getJSONObject(i);
                                    //   String id=jsonObject1.getString("id");
                                    Integer id = jsonObject1.getInt("id");
                                    Integer user = jsonObject1.getInt("user");
                                    String created = jsonObject1.getString("created_at");
                                    String message = jsonObject1.getString("message");
                                    // String message = Utils.decodeMessage(jsonObject1.getString("message"));
                                    String fullname = jsonObject1.getString("fullname");
                                    String profile_pic = jsonObject1.getString("profile_pic");
                                    commentsBean = new CommentsBean();
                                    commentsBean.setId(String.valueOf(id));
                                    commentsBean.setUser(String.valueOf(user));
                                    commentsBean.setProfile_pic(profile_pic);
                                    commentsBean.setFullname(fullname);
                                    commentsBean.setMessage(message);

                                    commentsBean.setLikes_count(jsonObject1.getString("likes_count"));
                                    commentsBean.setReplies_count(jsonObject1.getString("replies_count"));


                                    JSONArray jsonRplyArray=jsonObject1.getJSONArray("commentreplies");
                                    JSONObject jsonRplyObj;
                                    CommentReplies cmntRply ;
                                    ArrayList<CommentReplies> cmntRplyArrayList =new ArrayList<CommentReplies>();
                                    for (int j=0;j<jsonRplyArray.length();j++){
                                        cmntRply =new CommentReplies();
                                        jsonRplyObj=jsonRplyArray.getJSONObject(j);
                                        cmntRply.setId_rply(jsonRplyObj.getInt("id"));
                                        cmntRply.setLevel_rply(jsonRplyObj.getInt("level"));
                                        cmntRply.setFullname_rply(jsonRplyObj.getString("fullname"));
                                        cmntRply.setMessage_rply(jsonRplyObj.getString("message"));
                                        cmntRplyArrayList.add(cmntRply);
                                    }




                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//2016-01-24T16:00:00.000Z
                                    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                                    long time = sdf.parse(created).getTime();
                                    long now = System.currentTimeMillis();
                                    CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
                                    commentsBean.setCreated_at(String.valueOf(ago));
                                    commentsBean.setCommentRepliesArrayList(cmntRplyArrayList);
                                    commentsBeenList.add(commentsBean);

                                }
                                CommentActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        commentAdapterr = new CommentListAdapter(CommentActivity.this, commentsBeenList);
                                        //expandableListView.setExpanded(true);
                                        expandableListView.setAdapter(commentAdapterr);
                                    }
                                });

                            }
                        } catch (Exception je) {
                            //loaderDiloag.dismissDiloag();
                            je.printStackTrace();
                        }

                    } else {
                        //loaderDiloag.dismissDiloag();
                    }
                }
            });

        } catch (Exception e) {
            CommentActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    //loaderDiloag.dismissDiloag();
                }
            });
            e.printStackTrace();
            //  loaderDiloag.dismissDiloag();
        }


        return;
    }*/



    public void Like() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(CommentActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(CommentActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", token + "");
            Request request = new Request.Builder()
                    .url(WebApis.LikePOST + id)
                    .get()
                    .addHeader("authorization", token)
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (CommentActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("respons", jsonData + "");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                            }
                        });
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(CommentActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void showPopupShare() {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_share, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        final EditText textText = (EditText) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = textText.getText().toString();
                if (text.trim().length() > 0) {

                    Share(text);
                    popupWindow.dismiss();
                } else {
                    Toast.makeText(CommentActivity.this, "Please Write Something...", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void Share(String Comment) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(CommentActivity.this);
        if (ApplicationGlobles.isConnectingToInternet((Activity) CommentActivity.this)) {
            loaderDiloag.displayDiloag();

            MediaType mediaType = MediaType.parse("application/json");

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("sharerMsg", Comment);
                jsonObject.put("postId", id);
            } catch (Exception e) {

            }
            Log.e("jsonObject", jsonObject.toString());
            RequestBody body = RequestBody.create(mediaType, jsonObject.toString() + "");
            Request request = new Request.Builder()
                    .url(WebApis.SharePOST)
                    .post(body)
                    .addHeader("authorization", token)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();
            OkHttpClient client = new OkHttpClient();
            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (CommentActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        final String jsonData = response.body().string();
                        Log.e("respons", jsonData + "");
                        ((Activity) CommentActivity.this).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(jsonData);
                                    String success = jsonObject.getString("status");
                                    if (success.equals("success")) {
                                        Constant.update_home = true;
                                        Toast.makeText(CommentActivity.this, jsonObject.getString("message") + "", Toast.LENGTH_SHORT).show();
                                        // postList.get(pos).setComments_count(jsonObject.getInt("comment_count") + " Comments");
                                    }
                                } catch (Exception e) {

                                }
                                loaderDiloag.dismissDiloag();
                            }
                        });
                    }
                });

            } catch (Exception e) {
                ((Activity) CommentActivity.this).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(CommentActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void Comment(String Comment) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(CommentActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(CommentActivity.this)) {
            loaderDiloag.displayDiloag();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, "{\r\n\"message\" : \"" + Comment + "\"\r\n}");
            Request request = new Request.Builder()
                    .url(WebApis.CommentPOST + id + "/")
                    .post(body)
                    .addHeader("authorization", token)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("postman-token", "ae0bf6c3-6ef6-bc06-6ca6-1475b65e1909")
                    .build();
            OkHttpClient client = new OkHttpClient();
            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (CommentActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        final String jsonData = response.body().string();
                        Log.e("respons", jsonData + "");
                        CommentActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject(jsonData);
                                    String success = jsonObject.getString("status");
                                    if (success.equals("success")) {
                                        txtCommentsCount.setText(jsonObject.getInt("comment_count") + "");
                                        getAllComments();
                                    }
                                } catch (Exception e) {

                                }
                                loaderDiloag.dismissDiloag();
                            }
                        });
                    }
                });

            } catch (Exception e) {
                CommentActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(CommentActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }


}


