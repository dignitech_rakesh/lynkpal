package com.app.lynkpal.Helper;

import android.net.Uri;

/**
 * Created by user on 7/22/2017.
 */

public class Constant {
    public static final String PREFS_NAME = "";
    public static String token = "";
    public static String user_id = "";
    public static boolean update =false;
    public static boolean update_home =false;
    public static boolean update_created =false;
    public static boolean update_myCreatedGroup =false;
    public static boolean update_chat =false;
    public static boolean update_address =false;

    public static int VOLLEY_TIMEOUT_MS = 4000;
    public static int VOLLEY_MAX_RETRIES = 2;
    public static float VOLLEY_BACKOFF_MULT = 1f;

    public static int READ_TIMEOUT_MS = 4000;
    public static int WRITE_TIMEOUT_MS = 4000;


     public static String timeout_error_string = "Unable to resolve host \"www.lynkpal.com\": No address associated with hostname";
     public static String postType = "";
     public static String testImage = "";
     public static final String UI_AUTHENTICATED = "com.app.lynkpal";
     public static Uri staticUri=null;



}
