package com.app.lynkpal.Helper;

/**
 * Created by user on 9/14/2017.
 */

public class WebApis {
    //main pages
    public static String BaseURL = "https://www.lynkpal.com/";
   // public static String BaseURL = "http://staging.lynkpal.com/";


    public static String loginURL = BaseURL + "api_login/";
    public static String logoutURL = BaseURL + "api_logout/";
    public static String registerURL = BaseURL + "api_registration/";
    public static String postURL = BaseURL + "api_savepost/";
    public static String postURLNew = BaseURL + "api_save_post/";
    public static String allPOST = BaseURL + "api_allposts/";
    public static String LikePOST = BaseURL + "api_likepost/";
    public static String DeletePOST = BaseURL + "api_deletepost/";
    public static String CommentPOST = BaseURL + "api_savecomments/";
    public static String SharePOST = BaseURL + "api_sharepost/";
    public static String AllCommentPOST = BaseURL + "api_postcomments/";
    public static String GetPostDetail = BaseURL + "api_postdetail/";

    //company page
    public static String CompanyDetails = BaseURL + "api_companydetail/";
    public static String AllIndustry = BaseURL + "api_industries/";
    public static String UpdateCompanyDetails = BaseURL + "api_updatecompanyprofile/";
    public static String GalleryAPI = BaseURL + "api_companygallery/";
    public static String UploadGalleryAPI = BaseURL + "api_addgalleryimage/";
    public static String CompanyReviews = BaseURL + "api_companyreviews/";
    public static String AddCompanyReviews = BaseURL + "api_addreview/";
    public static String Followers = BaseURL + "api_followers/";
    public static String RemoveFollowers = BaseURL + "api_remove_follower/";
    public static String FollowUnfollow = BaseURL + "api_follow/";
    public static String UPLOAD_COMPANY_BANNER = "https://www.lynkpal.com/static/uploads/company_banners/";

    //job dashboard
    public static String POSTJOB = BaseURL + "api_addjob/";
    public static String UPDATEJOB = BaseURL + "api_updatejob/";
    public static String JOBCREATED = BaseURL + "api_jobscreated/";
    public static String JOBINTERESTED = BaseURL + "api_jobsinterest/";
    public static String JOBAPPLIED = BaseURL + "api_jobsapplied/";
    public static String JOBDELETE = BaseURL + "api_deletejob/";
    public static String APPLYJOB = BaseURL + "api_applyjob/";
    public static String DELETEAPPLICATION = BaseURL + "api_deleteapplication/";
    public static String APPLICANTS = BaseURL + "api_jobapplicants/";
    public static String JOBDETAIL = BaseURL + "api_jobdetail/";
    public static String CARDDETAILS = BaseURL + "api_package_payment/";
    public static String USERDETAILS = BaseURL + "api_user_details/";
    public static String AddTrainingUpdateCv = BaseURL + "api_confirm_package/";

    //LynkpalLearning
    public static String ALLTRAINING = BaseURL + "api_trainings/";
    public static String MYTRAINING = BaseURL + "api_mytrainings/";
    public static String MYTRAININGSTATUS = BaseURL + "api_agreementStatus/";

    public static String DELETETRAINING = BaseURL + "api_deleteTraining/";
    public static String UPDATETRAINING = BaseURL + "api_updateTraining/";
    public static String SPECIALIZATIONS = BaseURL + "api_specializations/";
    public static String ADDNEWTRAINING = BaseURL + "api_addTraining/";
    public static String PAYMENTTRAINING = BaseURL + "api_trainingPayment/";

    //Career Advice
    public static String CAREERADVICE = BaseURL + "api_jobBlogs/";

    //message
    public static String TALKED = BaseURL + "api_talkedto/";
    public static String ALLMESSAGE = BaseURL + "api_messages/";
    public static String SENDMESSAGE = BaseURL + "api_sendmessage/";
    public static String COUNTMESSAGE = BaseURL + "api_unreadMessagesCount/";
    public static String READMESSAGE = BaseURL + "api_markread/";

    //profile
    public static String GETSKILLS = BaseURL + "api_skills/";
    public static String ADDSKILLS = BaseURL + "api_addskills/";
    public static String USERPROFILE = BaseURL + "api_userprofile/";
    public static String UPDATEPERSONALINFO = BaseURL + "api_updatePersonalInfo/";
    public static String ADDEDUCATION = BaseURL + "api_addEducation/";
    public static String ADDEXPERIENCE = BaseURL + "api_addExperience/";
    public static String GETEXPERIENCE = BaseURL + "api_experienceDetail/";
    public static String GETEDUCATION = BaseURL + "api_educationDetail/";
    public static String UPDATEEDUCATION = BaseURL + "api_updateEducation/";
    public static String UPDATEEXPERIENCE = BaseURL + "api_updateExperience/";
    public static String DELETEEDUCATION = BaseURL + "api_deleteEducation/";
    public static String DELETEEXPERIENCE = BaseURL + "api_deleteExperience/";

    //Groups
    public static String GroupsCreatedByMe = BaseURL + "api_mygroups/";
    public static String ALLUSERS = BaseURL + "api_allusers";
    public static String CREATEGROUP = BaseURL + "api_addgroup/";
    public static String EDITGROUP = BaseURL + "api_updategroup/";
    public static String OTHERGROUP = BaseURL + "api_othersgroups/";
    public static String JOINLEAVEOPENGROUP = BaseURL + "api_join_leave_open_group/";
    public static String JOINLEAVECLOSEGROUP = BaseURL + "api_requesttojoin_closed_group/";
    public static String APPROVECLOSEGROUP = BaseURL + "api_approve_joining/";
    public static String GROUPPOSTS = BaseURL + "api_groupposts/";
    public static String GROUPMEMBERS = BaseURL + "api_groupmembers/";
    public static String POSTINGROUP = BaseURL + "api_savepost_group/";
    public static String DELETEGROUP = BaseURL + "api_deletegroup/";
    public static String GROUPDETAILS = BaseURL + "api_groupdetail/";

    //Notification
    public static String GETNOTIFICATION = BaseURL + "api_notifications/";
    public static String DELETENOTIFICATION = BaseURL + "api_delete_notification/";
    public static String READNOTIFICATION = BaseURL + "api_read_notification/";
    public static String COUNTNOTIFICATION = BaseURL + "api_unreadNotificationsCount/";


    //advertisement
    public static String GETPLANS = BaseURL + "api_adplans/";
    public static String ADDADV = BaseURL + "api_addAd/";
    public static String ALL_ADV = BaseURL + "api_ads/";
    public static String DELETE_ADVERTISMENT = BaseURL + "api_deleteAd/";
    public static String UPDATE_ADVERTISMENT = BaseURL + "api_updateAd/";


    //search
    public static String SearchUser = BaseURL + "api_searchedusers/";
    public static String SearchGroup = BaseURL + "api_searchedgroup/";


    //pay
    public static String AdvPay = BaseURL + "api_adpayment/";
    public static String JobPay = BaseURL + "api_jobpayment/";


    //BULK INVITATION
    public static String BulkInvite = BaseURL +"api_bulk_invitation/";

    public static String userProfileImage = "http://www.lynkpal.com/static/uploads/user_profile/";   // #define userProfileimg
    public static String postImage = "https://www.lynkpal.com/static/uploads/post_images/";           // #define PostImg
    public static String CmpPageUserImage = "https://www.lynkpal.com/static/img/";                   //#define CompanyPageUser @
    public static String CmpBannerImage = "https://www.lynkpal.com/static/uploads/company_banners/"; //#define CompanyBanner @
    public static String CmpGalleryImage = "https://www.lynkpal.com/static/uploads/company_gallery/"; //#define CompanyBanner @
    public static String MessageImages = "https://www.lynkpal.com/static/uploads/msg_images/"; //#define msg_images @
    public static String JobImages = "https://www.lynkpal.com/static/uploads/job_images/"; //#define msg_images @
    public static String GroupImages = "https://www.lynkpal.com/static/uploads/group_images/"; //#define msg_images @
    public static String AdvImages = "https://www.lynkpal.com/"; //#define msg_images @

    //REPORT

    public static String REPORT_URL = "https://www.lynkpal.com/api_reportpost/";
    public static String SUGGESTION_URL = BaseURL + "api_friend_suggestions/";

    //Reply Comment
    public static String LikeComment = BaseURL + "api_likecomment/";
    public static String ReplyComment = BaseURL + "api_savereply/";
    // check register

    //Forgot Pass
    public static String forgotPassURL = BaseURL + "api_set_forgotpass_otp/";
    public static String changePassURL = BaseURL + "api_set_new_password/";

    public static String chkOtpUrl_forgot = BaseURL + "api_check_forgotpass_otp/";
    public static String resendOtpUrl_forgot = BaseURL + "api_regenerate_forgotpass_otp/";

    public static String chkOtpUrl = BaseURL + "api_check_otp/";
    public static String resendOtpUrl = BaseURL + "api_regenerate_otp/";

    public static String CHECK_REGISTER_URL = "https://www.lynkpal.com/api_check_registered/";
}
