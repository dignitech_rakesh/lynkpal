package com.app.lynkpal.Helper;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ApplicationGlobles {

	public static String NoReadingError="No Reading Available";

	public static boolean isNullOrEmpty(String myString)
    {
         return myString == null || "".equals(myString);
    }

	public static void loadStrictModePolicies()
	{
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
		.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
	}

	public static boolean isConnectingToInternet(Activity CurrentActivity) {
		Boolean Connected=false;
		ConnectivityManager connectivity = (ConnectivityManager) CurrentActivity.getApplicationContext()
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						Log.e("Network is: ", "Connected");
						Connected= true;
					} 
					else
					{	
					}

		}
		else
		{
			Log.e("Network is: ", "Not Connected");
			
			//Toast.makeText(CurrentActivity.getApplicationContext(),"Please Connect to Internet",
					//Toast.LENGTH_LONG).show();
			Connected= false;
			
		}
		return Connected;
		
		
	}


	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}


	public static boolean isValidUrl(String url) {
		Pattern p = Patterns.WEB_URL;
		Matcher m = p.matcher(url);
		if(m.matches())
			return true;
		else
			return false;
	}





}
