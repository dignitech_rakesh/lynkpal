package com.app.lynkpal.Helper;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;

import com.app.lynkpal.R;


/**
 * Created by Lovekush on 9/25/2016.
 */
public class LoaderDiloag {
    Dialog ratingDialog;

    public LoaderDiloag(final Context mContext) {
        ratingDialog = new Dialog(mContext);
        ratingDialog.setCancelable(false);
        ratingDialog.setContentView(R.layout.loader_layout);
        ratingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }


    public void setCancelable(boolean value) {
        ratingDialog.setCancelable(value);
    }

    public void dismissDiloag() {
        try {
            if (ratingDialog != null && ratingDialog.isShowing())
                ratingDialog.dismiss();
        } catch (Exception e) {
            e.fillInStackTrace();
        }

    }

    public void displayDiloag() {
        try {
            ratingDialog.show();
        } catch (Exception e) {
            Log.e("error", e.toString());
        }
    }

    public boolean isShowing() {
        return ratingDialog.isShowing();
    }
}
