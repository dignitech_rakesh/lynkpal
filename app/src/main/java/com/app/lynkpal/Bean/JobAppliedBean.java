package com.app.lynkpal.Bean;

/**
 * Created by user on 9/20/2017.
 */

public class JobAppliedBean {
    String name;
    String id;
    String location;
    String hotApplied;

    public String getHotApplied() {
        return hotApplied;
    }

    public void setHotApplied(String hotApplied) {
        this.hotApplied = hotApplied;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    String image;
    String from;
    String extra;
}
