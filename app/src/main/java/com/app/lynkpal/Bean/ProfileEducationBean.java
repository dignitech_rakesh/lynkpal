package com.app.lynkpal.Bean;

/**
 * Created by user on 9/23/2017.
 */

public class ProfileEducationBean {
    String id;
    String board_university;
    String class_degree;
    String end_date;
    String start_date;

    public boolean isDeleteEdit() {
        return deleteEdit;
    }

    public void setDeleteEdit(boolean deleteEdit) {
        this.deleteEdit = deleteEdit;
    }

    boolean deleteEdit;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBoard_university() {
        return board_university;
    }

    public void setBoard_university(String board_university) {
        this.board_university = board_university;
    }

    public String getClass_degree() {
        return class_degree;
    }

    public void setClass_degree(String class_degree) {
        this.class_degree = class_degree;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getSubject_course() {
        return subject_course;
    }

    public void setSubject_course(String subject_course) {
        this.subject_course = subject_course;
    }

    String subject_course;
}
