package com.app.lynkpal.Bean;

import android.widget.ImageView;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by user on 9/18/2017.
 */

public class CommentsBean implements Serializable {
    String id;
    String message;
    String created_at;
    String user;
    String fullname;
    String profile_pic;
    String replies_count,likes_count;
    int commentLikeStatus;
    ArrayList<CommentReplies> commentRepliesArrayList=new ArrayList<CommentReplies>();

    public ArrayList<CommentReplies> getCommentRepliesArrayList() {
        return commentRepliesArrayList;
    }

    public void setCommentRepliesArrayList(ArrayList<CommentReplies> commentRepliesArrayList) {
        this.commentRepliesArrayList = commentRepliesArrayList;
    }

    public String getReplies_count() {
        return replies_count;
    }

    public void setReplies_count(String replies_count) {
        this.replies_count = replies_count;
    }

    public String getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(String likes_count) {
        this.likes_count = likes_count;
    }


    public int getCommentLikeStatus() {
        return commentLikeStatus;
    }

    public void setCommentLikeStatus(int commentLikeStatus) {
        this.commentLikeStatus = commentLikeStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }


    /*public static class CommentReplies{
        String message_rply,created_at_rply,user_rply,fullname_rply,profile_pic_rply;
        int id_rply,level_rply;

        public String getMessage_rply() {
            return message_rply;
        }

        public void setMessage_rply(String message_rply) {
            this.message_rply = message_rply;
        }

        public String getCreated_at_rply() {
            return created_at_rply;
        }

        public void setCreated_at_rply(String created_at_rply) {
            this.created_at_rply = created_at_rply;
        }

        public String getUser_rply() {
            return user_rply;
        }

        public void setUser_rply(String user_rply) {
            this.user_rply = user_rply;
        }

        public String getFullname_rply() {
            return fullname_rply;
        }

        public void setFullname_rply(String fullname_rply) {
            this.fullname_rply = fullname_rply;
        }

        public String getProfile_pic_rply() {
            return profile_pic_rply;
        }

        public void setProfile_pic_rply(String profile_pic_rply) {
            this.profile_pic_rply = profile_pic_rply;
        }

        public int getId_rply() {
            return id_rply;
        }

        public void setId_rply(int id_rply) {
            this.id_rply = id_rply;
        }

        public int getLevel_rply() {
            return level_rply;
        }

        public void setLevel_rply(int level_rply) {
            this.level_rply = level_rply;
        }
    }*/
}
