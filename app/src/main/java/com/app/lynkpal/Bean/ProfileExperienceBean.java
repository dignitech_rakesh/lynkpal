package com.app.lynkpal.Bean;

/**
 * Created by user on 9/23/2017.
 */

public class ProfileExperienceBean {
    String id;
    String title;
    String company;
    String location;
    String start_date;
    public boolean isDeleteEdit() {
        return deleteEdit;
    }

    public void setDeleteEdit(boolean deleteEdit) {
        this.deleteEdit = deleteEdit;
    }

    boolean deleteEdit;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    String end_date;
}
