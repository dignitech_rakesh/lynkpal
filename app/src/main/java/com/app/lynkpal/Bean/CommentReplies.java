package com.app.lynkpal.Bean;

import java.io.Serializable;

public class CommentReplies implements Serializable {
    String message_rply,created_at_rply,user_rply,fullname_rply,profile_pic_rply;
    int id_rply,level_rply;

    public String getMessage_rply() {
        return message_rply;
    }

    public void setMessage_rply(String message_rply) {
        this.message_rply = message_rply;
    }

    public String getCreated_at_rply() {
        return created_at_rply;
    }

    public void setCreated_at_rply(String created_at_rply) {
        this.created_at_rply = created_at_rply;
    }

    public String getUser_rply() {
        return user_rply;
    }

    public void setUser_rply(String user_rply) {
        this.user_rply = user_rply;
    }

    public String getFullname_rply() {
        return fullname_rply;
    }

    public void setFullname_rply(String fullname_rply) {
        this.fullname_rply = fullname_rply;
    }

    public String getProfile_pic_rply() {
        return profile_pic_rply;
    }

    public void setProfile_pic_rply(String profile_pic_rply) {
        this.profile_pic_rply = profile_pic_rply;
    }

    public int getId_rply() {
        return id_rply;
    }

    public void setId_rply(int id_rply) {
        this.id_rply = id_rply;
    }

    public int getLevel_rply() {
        return level_rply;
    }

    public void setLevel_rply(int level_rply) {
        this.level_rply = level_rply;
    }
}
