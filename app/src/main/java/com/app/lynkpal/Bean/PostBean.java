package com.app.lynkpal.Bean;

/**
 * Created by user on 9/15/2017.
 */

public class PostBean {
    Integer id;
    Integer user;
    String desc;
    String created;
    String file;
    String parent_post;
    String sharer_msg;
    String fullname;
    String profile_pic;
    Boolean isLike;
    String type;

    String org_profile_pic;
    String org_file;
    String org_owner_profile_pic;
    String org_frame_name;

    Integer adv_id;
    String adv_name;
    String adv_tags;
    String adv_link;
    String adv_startDate;
    String adv_endDate;
    Integer adv_amount;
    Integer adv_paid;
    String adv_image;
    String adv_readCount;

    public String getFrame_name() {
        return frame_name;
    }

    public void setFrame_name(String frame_name) {
        this.frame_name = frame_name;
    }

    String frame_name;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getDelete() {
        return isDelete;
    }

    public void setDelete(Boolean delete) {
        isDelete = delete;
    }

    Boolean isDelete;
    Boolean isComment;
    String likes_count;
    String comments_count;
    String owner_fullname;
    String owner_profile_pic;

    public Boolean getLike() {
        return isLike;
    }

    public void setLike(Boolean like) {
        isLike = like;
    }

    public Boolean getComment() {
        return isComment;
    }

    public void setComment(Boolean comment) {
        isComment = comment;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getParent_post() {
        return parent_post;
    }

    public void setParent_post(String parent_post) {
        this.parent_post = parent_post;
    }

    public String getSharer_msg() {
        return sharer_msg;
    }

    public void setSharer_msg(String sharer_msg) {
        this.sharer_msg = sharer_msg;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(String likes_count) {
        this.likes_count = likes_count;
    }

    public String getComments_count() {
        return comments_count;
    }

    public void setComments_count(String comments_count) {
        this.comments_count = comments_count;
    }

    public String getOwner_fullname() {
        return owner_fullname;
    }

    public void setOwner_fullname(String owner_fullname) {
        this.owner_fullname = owner_fullname;
    }

    public String getOwner_profile_pic() {
        return owner_profile_pic;
    }

    public void setOwner_profile_pic(String owner_profile_pic) {
        this.owner_profile_pic = owner_profile_pic;
    }


    public String getOrg_profile_pic() {
        return org_profile_pic;
    }

    public void setOrg_profile_pic(String org_profile_pic) {
        this.org_profile_pic = org_profile_pic;
    }

    public String getOrg_file() {
        return org_file;
    }

    public void setOrg_file(String org_file) {
        this.org_file = org_file;
    }

    public String getOrg_owner_profile_pic() {
        return org_owner_profile_pic;
    }

    public void setOrg_owner_profile_pic(String org_owner_profile_pic) {
        this.org_owner_profile_pic = org_owner_profile_pic;
    }

    public String getOrg_frame_name() {
        return org_frame_name;
    }

    public void setOrg_frame_name(String org_frame_name) {
        this.org_frame_name = org_frame_name;
    }





    //AdVERTISEMENT


    public Integer getAdv_id() {
        return adv_id;
    }

    public void setAdv_id(Integer adv_id) {
        this.adv_id = adv_id;
    }

    public String getAdv_name() {
        return adv_name;
    }

    public void setAdv_name(String adv_name) {
        this.adv_name = adv_name;
    }

    public String getAdv_tags() {
        return adv_tags;
    }

    public void setAdv_tags(String adv_tags) {
        this.adv_tags = adv_tags;
    }

    public String getAdv_link() {
        return adv_link;
    }

    public void setAdv_link(String adv_link) {
        this.adv_link = adv_link;
    }

    public String getAdv_startDate() {
        return adv_startDate;
    }

    public void setAdv_startDate(String adv_startDate) {
        this.adv_startDate = adv_startDate;
    }

    public String getAdv_endDate() {
        return adv_endDate;
    }

    public void setAdv_endDate(String adv_endDate) {
        this.adv_endDate = adv_endDate;
    }

    public Integer getAdv_amount() {
        return adv_amount;
    }

    public void setAdv_amount(Integer adv_amount) {
        this.adv_amount = adv_amount;
    }

    public Integer getAdv_paid() {
        return adv_paid;
    }

    public void setAdv_paid(Integer adv_paid) {
        this.adv_paid = adv_paid;
    }

    public String getAdv_image() {
        return adv_image;
    }

    public void setAdv_image(String adv_image) {
        this.adv_image = adv_image;
    }

    public String getAdv_readCount() {
        return adv_readCount;
    }

    public void setAdv_readCount(String adv_readCount) {
        this.adv_readCount = adv_readCount;
    }
}


