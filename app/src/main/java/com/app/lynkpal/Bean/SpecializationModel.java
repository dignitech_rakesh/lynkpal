package com.app.lynkpal.Bean;

import java.io.Serializable;

public class SpecializationModel implements Serializable {

    int specializationId;
    String specializationName;

    public int getSpecializationId() {
        return specializationId;
    }

    public void setSpecializationId(int specializationId) {
        this.specializationId = specializationId;
    }

    public String getSpecializationName() {
        return specializationName;
    }

    public void setSpecializationName(String specializationName) {
        this.specializationName = specializationName;
    }
}
