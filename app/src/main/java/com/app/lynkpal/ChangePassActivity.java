package com.app.lynkpal;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class ChangePassActivity extends AppCompatActivity {

    private EditText etPass,etPassConfirm;
    private Button btnChangePass;
    int userId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);

        userId= getIntent().getExtras().getInt("userId",0);

        etPass=(EditText)findViewById(R.id.pass);
        etPassConfirm=(EditText)findViewById(R.id.confirm_pass);
        btnChangePass=(Button)findViewById(R.id.btnChangePass);

        btnChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (LoginActivity.isNetworkConnected(ChangePassActivity.this)) {

                    if (etPass.getText().toString().trim().isEmpty()) {
                    etPass.setError("Enter password");

                } else if (etPassConfirm.getText().toString().trim().isEmpty()) {
                    etPassConfirm.setError("Enter confirm password");

                } else if (!etPassConfirm.getText().toString().trim().equals(etPass.getText().toString().trim())) {
                    etPassConfirm.setError("Password Mismatch");

                } else {
                    changePassUrl(ChangePassActivity.this);
                }
            }else{
                Toast.makeText(ChangePassActivity.this,"No Internet Connection",Toast.LENGTH_LONG).show();
                return;
            }
            }
        });
    }

    private void changePassUrl(final Context context)
    {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(ChangePassActivity.this);
        loaderDiloag.displayDiloag();
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("userid", String.valueOf(userId));
            jsonBody.put("password", etPass.getText().toString().trim());
            jsonBody.put("confirm_password", etPassConfirm.getText().toString().trim());


            Log.d("jsonLogin " , jsonBody.toString());
            final String requestBody = jsonBody.toString();

           // StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.POST, "http://staging.lynkpal.com/api_set_new_password/", new Response.Listener<String>() {
            StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.POST, WebApis.changePassURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    loaderDiloag.dismissDiloag();

                    Log.d("checkOtpUrl", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.contains("success"))
                        {
                            new MaterialDialog.Builder(context)
                                    .title("OTP")
                                    .content(message)
                                    .iconRes(R.drawable.ic_check_circle)
                                    .canceledOnTouchOutside(false)
                                    .positiveText("Ok")
                                    .onPositive(new MaterialDialog.SingleButtonCallback()
                                    {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                        {
                                            etPass.setText("");
                                            etPassConfirm.setText("");

                                            dialog.dismiss();
                                            startActivity(new Intent(context,LoginActivity.class));
                                        }
                                    }).show();

                        }
                        else
                        {
                            loaderDiloag.dismissDiloag();
                            Toast.makeText(ChangePassActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        loaderDiloag.dismissDiloag();
                        Toast.makeText(ChangePassActivity.this,getResources().getString(R.string.try_again),Toast.LENGTH_SHORT).show();

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    loaderDiloag.dismissDiloag();
                    Toast.makeText(ChangePassActivity.this,getResources().getString(R.string.try_again),Toast.LENGTH_SHORT).show();

                    Log.e("VOLLEYOut", error.toString());
                }
            })

            {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    HashMap<String,String> param = new HashMap<String, String>();
                    param.put("Content-Type","application/json");
                    return param;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }


            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
