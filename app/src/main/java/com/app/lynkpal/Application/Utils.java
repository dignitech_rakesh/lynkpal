package com.app.lynkpal.Application;

import org.apache.commons.lang3.StringEscapeUtils;

public class Utils {

    public static String encodeMessage(String message) {
        message = message.replaceAll("&", ":and:");
        message = message.replaceAll("\\+", ":plus:");
        return StringEscapeUtils.escapeJava(message);
    }

    public static String decodeMessage(String message) {
        message = message.replaceAll(":and:", "&");
        message = message.replaceAll(":plus:", "+");
        return StringEscapeUtils.unescapeJava(message);
    }
}
