package com.app.lynkpal.Application;

import android.app.Application;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.app.lynkpal.oneSignal.MyNotificationOpenedHandler;
import com.app.lynkpal.oneSignal.MyNotificationReceivedHandler;
import com.inscripts.orm.SugarContext;
import com.onesignal.OneSignal;

public class LynkApplication extends Application
{
    private static Context context;



    public static Context getContext() {
        return context;
    }

    protected RequestQueue queue;



    @Override
    public void onCreate() {
        super.onCreate();

        queue = Volley.newRequestQueue(this);

        SugarContext.init(this);


//        // OneSignal Initialization
 /*       OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();*/




        context = getApplicationContext();
        //MyNotificationOpenedHandler : This will be called when a notification is tapped on.
        //MyNotificationReceivedHandler : This will be called when a notification is received while your app is running.
        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new MyNotificationOpenedHandler())
                .setNotificationReceivedHandler( new MyNotificationReceivedHandler() )
                .init();


    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }
}
