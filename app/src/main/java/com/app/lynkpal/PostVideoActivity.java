package com.app.lynkpal;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

public class PostVideoActivity extends AppCompatActivity {

    String video_uri;
    VideoView mView;
    ImageView mCancel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_video);
        mView=findViewById(R.id.videoview);
        mCancel=findViewById(R.id.ic_cancel);

        video_uri=getIntent().getStringExtra("videoUri");

        MediaController vidControl = new MediaController(PostVideoActivity.this);
        vidControl.setAnchorView(mView);
        mView.setMediaController(vidControl);
        mView.setVideoPath(video_uri);
        mView.requestFocus();
        mView.start();

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
