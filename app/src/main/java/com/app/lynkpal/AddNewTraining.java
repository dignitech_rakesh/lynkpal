package com.app.lynkpal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.lynkpal.Adapter.LynkpalLearningListAdapter;
import com.app.lynkpal.Bean.LynkaplLearningModel;
import com.app.lynkpal.Bean.MyTrainingModel;
import com.app.lynkpal.Bean.SpecializationModel;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.FilePath;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.bumptech.glide.Glide;

import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddNewTraining extends AppCompatActivity {

    LoaderDiloag loaderDiloag;
    EditText edTrainingName,edDurationInHours,edNoOfLec,edAmtInDollars;

    TextView tvChooseTrainingImage,tvSubmitNewTraining,tvHeadAddNewTraining;
    ImageView ivChooseTrainingImage;

    FileBody filepath;
    private String extension,spinnerStr,type,url;
    private Spinner spinner;
    private int spinnerId = -1;
    String[] spinnerValue = {"1", "2", "3", "4","5","6", "7", "8", "9","10","11", "12", "13", "14","15","16", "17", "18", "19","20",
            "21", "22", "23", "24","25","26", "27", "28", "29","30","31", "32", "33", "34","35","36", "37", "38", "39","40",};
    ArrayList<SpecializationModel> specializationModelArrayList = new ArrayList<>();
    String[] spinnerValueName;
    SharedPreferences sharedPreferences;
    String strTrainingName, strDuration, strNoLectures, strAmtDollar;
    String imageBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_training);

        getSpecializationApi();
        loaderDiloag = new LoaderDiloag(AddNewTraining.this);
        sharedPreferences = AddNewTraining.this.getSharedPreferences(Constant.PREFS_NAME, 0);
        type=getIntent().getStringExtra("type");
        MyTrainingModel myTrainingModel = (MyTrainingModel) getIntent().getSerializableExtra("model");
        Log.d("type",type );


        edTrainingName = (EditText)findViewById(R.id.edTrainingName);
        edDurationInHours = (EditText)findViewById(R.id.edDurationInHours);
        edNoOfLec = (EditText)findViewById(R.id.edNoOfLec);
        edAmtInDollars = (EditText)findViewById(R.id.edAmtInDollars);
        spinner =(Spinner)findViewById(R.id.spinnerAddNewTraining);
        tvChooseTrainingImage = (TextView) findViewById(R.id.tvChooseTrainingImage);
        tvSubmitNewTraining = (TextView) findViewById(R.id.tvSubmitNewTraining);
        ivChooseTrainingImage = (ImageView) findViewById(R.id.ivChooseTrainingImage);

        tvHeadAddNewTraining = (TextView) findViewById(R.id.tvHeadAddNewTraining);
        if (type.equalsIgnoreCase("Add")){
            tvHeadAddNewTraining.setText("Add New Training");
            tvSubmitNewTraining.setText("Submit");
            url= WebApis.ADDNEWTRAINING;
        }else{
            tvHeadAddNewTraining.setText("Update Training");
            tvSubmitNewTraining.setText("SAVE");
            url= WebApis.UPDATETRAINING + myTrainingModel.getId()+"/";

            Log.d("typeModel",type +url+myTrainingModel.getId() );

            edTrainingName.setText(myTrainingModel.getName());
            edDurationInHours.setText(String.valueOf(myTrainingModel.getDuration()));
            edNoOfLec.setText(String.valueOf(myTrainingModel.getLectures()));
            edAmtInDollars.setText(String.valueOf(myTrainingModel.getAmount()));
            Glide.with(AddNewTraining.this).load(WebApis.BaseURL+myTrainingModel.getTraining_image()).centerCrop().error(R.drawable.noimage).into(ivChooseTrainingImage);
            //spinnerStr=myTrainingModel.getSpecialization_name();
//            spinner.setSelection(Integer.parseInt(myTrainingModel.getSpecialization_name()));

        }

        tvChooseTrainingImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 2);
            }
        });

        tvSubmitNewTraining.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ApplicationGlobles.isConnectingToInternet(AddNewTraining.this)) {

                    strTrainingName = edTrainingName.getText().toString().trim();
                strDuration = edDurationInHours.getText().toString().trim();
                strNoLectures = edNoOfLec.getText().toString().trim();
                strAmtDollar = edAmtInDollars.getText().toString().trim();

                if (validate() && spinnerId>=0){




                    //Toast.makeText(AddNewTraining.this,spinnerId,Toast.LENGTH_SHORT).show();
                    Log.d("spinnerIdTest",spinnerId+"--");

                    getSubmitApi();
                    //getApi();
                }else{
                    Toast.makeText(AddNewTraining.this,"Please fill all fields Properly",Toast.LENGTH_SHORT).show();

                }
                } else {
                    Toast.makeText(AddNewTraining.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

                }
            }
        });

       /* SpinnerAdapter adapter = new SpinnerAdapter(AddNewTraining.this, android.R.layout.simple_list_item_1);
        adapter.addAll(spinnerValueName);
        adapter.add("Specialization");
        spinner.setAdapter(adapter);
        spinner.setSelection(adapter.getCount());
*/
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(spinner.getSelectedItem() == "Specialization")
                {
                    spinnerStr="";
                    spinnerId=-1;
                    Log.d("spinnerIdTest",spinnerId+"--"+"null");

                    //Do nothing.
                }
                else{


                    spinnerStr=spinner.getSelectedItem().toString().trim();
                   // Toast.makeText(AddNewTraining.this, spinner.getSelectedItem().toString(), Toast.LENGTH_LONG).show();

                    for (int i=0; i<specializationModelArrayList.size();i++){
                        if (specializationModelArrayList.get(i).getSpecializationName().equalsIgnoreCase(spinnerStr)){
                            spinnerId = specializationModelArrayList.get(i).getSpecializationId();
                        }
                    }

                    Log.d("spinnerIdTest",spinnerId+"--");

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2) {
            try {
                final Uri imageUri = data.getData();
                Glide.with(AddNewTraining.this).load(imageUri).centerCrop().into(ivChooseTrainingImage);
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                //ivChooseTrainingImage.setImageBitmap(selectedImage);



                imageBase = bitmapToBase64(selectedImage);

               // Glide.with(AddNewTraining.this).load(imageUri).into(ivChooseTrainingImage);
               // convertImage(AddNewTraining.this,imageUri);
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(AddNewTraining.this, "Something went wrong", Toast.LENGTH_LONG).show();
            }

        }else {
            Toast.makeText(AddNewTraining.this, "You haven't picked Image",Toast.LENGTH_LONG).show();
        }
    }


    public boolean validate(){
        if (strTrainingName.equalsIgnoreCase("")||strTrainingName.isEmpty()){
            edTrainingName.setError("Please fill the field");
            return false;
        }else  if (strDuration.equalsIgnoreCase("")||strDuration.isEmpty()){
            edDurationInHours.setError("Please fill the field");
            return false;
        }else  if (strNoLectures.equalsIgnoreCase("")||strNoLectures.isEmpty()){
            edNoOfLec.setError("Please fill the field");
            return false;
        }else  if (strAmtDollar.equalsIgnoreCase("")||strAmtDollar.isEmpty()){
            edAmtInDollars.setError("Please fill the field");
            return false;
        }else  if (spinnerStr.equalsIgnoreCase("")||spinnerStr.isEmpty()||spinnerStr.equals(null)){
            //edDurationInHours.setError("Please fill the field");
            return false;
        }
        return true;
    }



    private void convertImage(Context context, Uri uri)
    {
        String selectedFilePath = FilePath.getPath(context,uri);
        File file = new File(selectedFilePath);
        filepath = new FileBody(file);
        extension = "png";
    }

    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }
    public class SpinnerAdapter extends ArrayAdapter<String> {

        public SpinnerAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
            // TODO Auto-generated constructor stub

        }

        @Override
        public int getCount() {

            // TODO Auto-generated method stub
            int count = super.getCount();

            return count>0 ? count-1 : count ;


        }


    }


    private void getSubmitApi() {
        if (ApplicationGlobles.isConnectingToInternet(AddNewTraining.this)) {

        loaderDiloag.displayDiloag();

        com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(AddNewTraining.this);

      /*  Map<String, String> postParam = new HashMap<String, String>();
        postParam.put("name", strTrainingName);
        postParam.put("duration", strDuration);
        postParam.put("lectures", strNoLectures);
        postParam.put("specialization",spinnerStr);
        postParam.put("amount", strAmtDollar);
        postParam.put("training_image", filepath.toString());

        Log.d("Training",    postParam.toString());*/

        JSONObject json = new JSONObject();;
        try {
            json.put("name", strTrainingName);
            json.put("duration", strDuration);
            json.put("lectures", strNoLectures);
            //json.put("specialization",spinnerStr);
            json.put("specialization",spinnerId);
            json.put("amount", strAmtDollar);
            json.put("training_image", imageBase);
            //json.put("training_image", "https://www.lynkpal.com/api_addTraining/");
            Log.d("Params",    json.toString());
        }catch (Exception e){

        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, json,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response)
                    {
                        loaderDiloag.dismissDiloag();
                        Log.d("AddNewTraining", "TrainingResponse " + response.toString());
                        try {
                            String status = response.getString("status");
                            // String message = response.getString("message");
                            if (status.contains("success"))
                            {

                                finish();

                            } else
                            {

                                Log.d("Training", "frndError " + response.toString());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener()
        {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                loaderDiloag.dismissDiloag();
                error.printStackTrace();
                if (error.toString().contains(Constant.timeout_error_string));
                // Toast.makeText(AddTrainingActivity.this,getString(R.string.slow_internet), Toast.LENGTH_SHORT).show();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String,String> data = new HashMap<String, String>();
                data.put("Authorization",sharedPreferences.getString("token",""));
                data.put("Content-Type","application/json");
                Log.d("Training","header " + data.toString());
                return data;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                Constant.VOLLEY_TIMEOUT_MS,
                Constant.VOLLEY_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        jsonObjReq.setTag("Training");
        // Adding request to request queue
        requestQueue.add(jsonObjReq);

        } else {
            Toast.makeText(AddNewTraining.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }
    }



    // SPECIALIZATION

    private void getSpecializationApi() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(AddNewTraining.this);

        if (ApplicationGlobles.isConnectingToInternet(AddNewTraining.this)) {

            loaderDiloag.displayDiloag();

        com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(AddNewTraining.this);
      /*  Map<String, String> postParam = new HashMap<String, String>();
        postParam.put("toFollow", user);

        Log.d("Training", "frndParam " + postParam.toString());*/


        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                WebApis.SPECIALIZATIONS, null,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response)
                    {
                        loaderDiloag.dismissDiloag();
                        Log.d("Training", "TrainingResponse " + response.toString());
                        try {
                            //String status = response.getString("status");
                            // String message = response.getString("message");
                            // if (status.contains("success"))
                            if (response.length()>0)
                            {
                                // Loop through the array elements
                                for(int i=0;i<response.length();i++){
                                    // Get current json object
                                    SpecializationModel specializationModel=new SpecializationModel();
                                    JSONObject jsonObject = response.getJSONObject(i);

                                    // Get the current student (json object) data
                                    specializationModel.setSpecializationId(jsonObject.getInt("id"));
                                    specializationModel.setSpecializationName(jsonObject.getString("name"));


                                    specializationModelArrayList.add(specializationModel);

                                    Log.d("lynkpalLearningListSize",specializationModelArrayList.size()+"");
                                }


                                if (specializationModelArrayList.size() > 0) {
                                 spinnerValueName = new String[specializationModelArrayList.size()];

                                 for (int i=0;i<specializationModelArrayList.size();i++){

                                     spinnerValueName[i]=specializationModelArrayList.get(i).getSpecializationName();

                                 }


                                    SpinnerAdapter adapter = new SpinnerAdapter(AddNewTraining.this, android.R.layout.simple_list_item_1);
                                    adapter.addAll(spinnerValueName);
                                    adapter.add("Specialization");
                                    spinner.setAdapter(adapter);
                                    spinner.setSelection(adapter.getCount());


                                   /* LynkpalLearningListAdapter mAdapter = new LynkpalLearningListAdapter(LynkpalLearning.this, lynkpalLearningList,LynkpalLearning.this);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                    rvAllTraining.setLayoutManager(mLayoutManager);
                                    rvAllTraining.setItemAnimator(new DefaultItemAnimator());
                                    rvAllTraining.setAdapter(mAdapter);*/
                                }

                            } else
                            {

                                Log.d("Training", "frndError " + response.toString());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener()
        {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                loaderDiloag.dismissDiloag();
                error.printStackTrace();
                // if (error.toString().contains(Constant.timeout_error_string));
                // Toast.makeText(AddTrainingActivity.this,getString(R.string.slow_internet), Toast.LENGTH_SHORT).show();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String,String> data = new HashMap<String, String>();
                data.put("Authorization",sharedPreferences.getString("token",""));
                data.put("Content-Type","application/json");
                Log.d("Training","header " + data.toString());
                return data;
            }

        };

        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.VOLLEY_TIMEOUT_MS,
                Constant.VOLLEY_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        jsonArrayRequest.setTag("Training");
        // Adding request to request queue
        requestQueue.add(jsonArrayRequest);

    } else {
        Toast.makeText(AddNewTraining.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

    }


    }



}
