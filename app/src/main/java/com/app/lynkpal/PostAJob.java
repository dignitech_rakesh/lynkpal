package com.app.lynkpal;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Adapter.CustomAdapter;
import com.app.lynkpal.Bean.industriesBean;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.FilePath;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class PostAJob extends AppCompatActivity {
    TextView head, txtHotJob;
    EditText edtTitle, edtLocation, edtDesc, edtEmploymentType;
    Button btnPostJob, btnBrowse;
    CheckBox chkBox;
    ImageView imgImage;
    industriesBean industriesBean;
    CustomAdapter customAdapter;
    Spinner spnr;
    List<industriesBean> list = new ArrayList<>();
    Intent pickPhoto;
    String encodedString, selectedExp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_ajob);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        chkBox = (CheckBox) findViewById(R.id.chkBox);
        head = (TextView) findViewById(R.id.head);
        head.setTypeface(tf_bold);
        txtHotJob = (TextView) findViewById(R.id.txtHotJob);
        txtHotJob.setTypeface(tf_med);
        edtTitle = (EditText) findViewById(R.id.edtTitle);
        edtTitle.setTypeface(tf_reg);
        edtLocation = (EditText) findViewById(R.id.edtLocation);
        edtLocation.setTypeface(tf_reg);
        edtEmploymentType = (EditText) findViewById(R.id.edtEmploymentType);
        edtEmploymentType.setTypeface(tf_reg);
        edtDesc = (EditText) findViewById(R.id.edtDesc);
        edtDesc.setTypeface(tf_reg);
        btnPostJob = (Button) findViewById(R.id.btnPostJob);
        btnPostJob.setTypeface(tf_bold);
        btnBrowse = (Button) findViewById(R.id.btnBrowse);
        btnBrowse.setTypeface(tf_bold);
        imgImage = (ImageView) findViewById(R.id.imgImage);
        ImageView ic_back = (ImageView) findViewById(R.id.ic_back);
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        industriesBean = new industriesBean();
        industriesBean.setName("No Experience");
        industriesBean.setId("0");
        list.add(industriesBean);
        industriesBean = new industriesBean();
        industriesBean.setName("1 Year Experience");
        industriesBean.setId("1");
        list.add(industriesBean);
        industriesBean = new industriesBean();
        industriesBean.setName("2 Year Experience");
        industriesBean.setId("2");
        list.add(industriesBean);
        industriesBean = new industriesBean();
        industriesBean.setName("3 Year Experience");
        industriesBean.setId("3");
        list.add(industriesBean);
        industriesBean = new industriesBean();
        industriesBean.setName("4 Year Experience");
        industriesBean.setId("4");
        list.add(industriesBean);
        industriesBean = new industriesBean();
        industriesBean.setName("5 Year Experience");
        industriesBean.setId("5");
        list.add(industriesBean);
        industriesBean = new industriesBean();
        industriesBean.setName("6 Year Experience");
        industriesBean.setId("6");
        list.add(industriesBean);
        industriesBean = new industriesBean();
        industriesBean.setName("7 Year Experience");
        industriesBean.setId("7");
        list.add(industriesBean);
        industriesBean = new industriesBean();
        industriesBean.setName("8 Year Experience");
        industriesBean.setId("8");
        list.add(industriesBean);
        industriesBean = new industriesBean();
        industriesBean.setName("9 Year Experience");
        industriesBean.setId("9");
        list.add(industriesBean);
        industriesBean = new industriesBean();
        industriesBean.setName("9+ Year Experience");
        industriesBean.setId("9+");
        list.add(industriesBean);
        customAdapter = new CustomAdapter(PostAJob.this, list);
        spnr = (Spinner) findViewById(R.id.spnr);
        spnr.setAdapter(customAdapter);
        btnBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED || checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.v("TAG", "Permission is granted");
                        pickPhoto = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, 0);//one can be replaced with any action code

                    } else {

                        Log.v("TAG", "Permission is revoked");
                        ActivityCompat.requestPermissions(PostAJob.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                    }
                } else {

                    Log.v("TAG", "Permission is granted");
                    pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 0);//one can be replaced with any action code
                }
            }
        });

        txtHotJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (chkBox.isChecked()) {
                    chkBox.setChecked(false);
                } else {
                    chkBox.setChecked(true);
                }
            }
        });
        spnr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedExp = list.get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        btnPostJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //    EditText edtTitle, edtLocation, edtDesc, edtEmploymentType;

                if (!ApplicationGlobles.isNullOrEmpty(edtTitle.getText().toString().trim())) {
                    if (!ApplicationGlobles.isNullOrEmpty(edtLocation.getText().toString().trim())) {
                        if (!ApplicationGlobles.isNullOrEmpty(edtDesc.getText().toString().trim())) {
                            if (!ApplicationGlobles.isNullOrEmpty(edtEmploymentType.getText().toString().trim())) {
                                if (chkBox.isChecked()) {
                                    PostJob(edtTitle.getText().toString().trim(), edtLocation.getText().toString().trim(), edtDesc.getText().toString().trim(),
                                            edtEmploymentType.getText().toString().trim(), "1");
                                } else {
                                    PostJob(edtTitle.getText().toString().trim(), edtLocation.getText().toString().trim(), edtDesc.getText().toString().trim(),
                                            edtEmploymentType.getText().toString().trim(), "0");
                                }

                            } else {
                                edtEmploymentType.setError("Required");
                            }
                        } else {
                            edtDesc.setError("Required");
                        }
                    } else {
                        edtLocation.setError("Required");
                    }
                } else {
                    edtTitle.setError("Required");
                }
            }
        });
        getChargeDetails();
    }

    public void getChargeDetails() {
        // final LoaderDiloag loaderDiloag = new LoaderDiloag(PostAJob.this);
        if (ApplicationGlobles.isConnectingToInternet(PostAJob.this)) {
            OkHttpClient client = new OkHttpClient();
            //  Log.e("Constant.token", Constant.token);
            //  Log.e("WebApis.ALLMESSAGE", WebApis.ALLMESSAGE + id);
            Request request = new Request.Builder()
                    .url(WebApis.GETPLANS)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (PostAJob.this == null)
                            return;
                        // loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response", jsonData + "");
                        if (jsonData.length() > 0) {
                            try {
                                JSONArray array = new JSONArray(jsonData);
                                if (array.length() > 0) {
                                   /* for (int i = 0; i < array.length(); i++) {*/
                                    JSONObject jsonObject1 = array.getJSONObject(0);
                                    final String amount = jsonObject1.getString("amount");
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            txtHotJob.setText("Mark as hot job(Charged " + amount + "$)");
                                        }
                                    });
                                    //   String id=jsonObject1.getString("id");
                                    // }

                                }

                            } catch (Exception je) {
                                je.printStackTrace();
                            }
                        } else {
                            // loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //swipeRefreshLayout.setRefreshing(false);
                        //loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(PostAJob.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void PostJob(String edtTitle, String edtLocation, String edtDesc, String edtEmploymentType, String hot) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(PostAJob.this);

        if (ApplicationGlobles.isConnectingToInternet(PostAJob.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            //String params="{\r\n\"description\":\"Write text to post\",\r\n\"postFile\":\"sadc\",\r\n\"extension\":\"jpeg\"\r\n}";
            JSONObject params = new JSONObject();
            try {
                params.put("title", edtTitle);
                params.put("location", edtLocation);
                params.put("description", edtDesc);
                params.put("experience", selectedExp);
                params.put("hot", hot);
                params.put("employment_type", edtEmploymentType);
                params.put("jobPic", encodedString);
                Log.e("param", params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(mediaType, params + "");
            Request request = new Request.Builder()
                    .url(WebApis.POSTJOB)
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (PostAJob.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            showPopup();
                                        }
                                    });
                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(PostAJob.this, "Something went wrong try later...", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                        loaderDiloag.dismissDiloag();
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(PostAJob.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void showPopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.custom_dialog_postjob, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        TextView textView = (TextView) popupView.findViewById(R.id.txtText);
        textView.setTypeface(tf_med);
        Button button = (Button) popupView.findViewById(R.id.btnOk);
        button.setTypeface(tf_bold);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                Constant.update_created = true;
                finish();
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            try {
                Uri selectedFileUri = data.getData();
                Picasso.with(PostAJob.this).load(selectedFileUri.toString()).into(imgImage);
                imgImage.setVisibility(View.VISIBLE);
                String selectedFilePath = FilePath.getPath(PostAJob.this, selectedFileUri);
                // File file = new File(selectedFilePath);
                //bin = new FileBody(file);
                InputStream inputStream = new FileInputStream(selectedFilePath);//You can get an inputStream using any IO API
                byte[] bytes;
                byte[] buffer = new byte[8192];
                int bytesRead;
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        output.write(buffer, 0, bytesRead);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                bytes = output.toByteArray();
                encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);
                Log.e("encodedString", encodedString);

            } catch (Exception e) {
                Log.e("eroore", e.toString());
                e.printStackTrace();
            }
        }

    }

}
