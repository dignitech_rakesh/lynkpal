package com.app.lynkpal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class OtpActivity extends AppCompatActivity {
    private static final String TAG = LoginActivity.class.getSimpleName();

    private EditText et1,et2,et3,et4,et5,et6;
    Button btnOtp;
    TextView tv_resend;
    private String otpString,type,chkOtpUrl,resendOtpUrl,resend_otp;

    int userId;
    boolean resendOtp=false;


    //Old
    private EditText et_otp_;
    private TextView tvResendOtp;
    Button btn_otp;
    private String val;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        userId= getIntent().getExtras().getInt("userId",0);
        type= getIntent().getStringExtra("type");
        if (type.equalsIgnoreCase("ForgetPassActivity")){
//            chkOtpUrl="http://staging.lynkpal.com/api_check_forgotpass_otp/";
//            resendOtpUrl="http://staging.lynkpal.com/api_regenerate_forgotpass_otp/"+userId+"/";
            chkOtpUrl=WebApis.chkOtpUrl_forgot;
            resendOtpUrl=WebApis.resendOtpUrl_forgot + userId+"/";
        }else{
//            chkOtpUrl="http://staging.lynkpal.com/api_check_otp/";
//            resendOtpUrl="http://staging.lynkpal.com/api_regenerate_otp/"+userId+"/";
            chkOtpUrl=WebApis.chkOtpUrl;
            resendOtpUrl=WebApis.resendOtpUrl + userId+"/";
        }

        Log.d("userId",userId+""+type+"--"+ chkOtpUrl);
        btnOtp=(Button)findViewById(R.id.otpBtn);
        tv_resend=(TextView) findViewById(R.id.tv_resend);
        tv_resend.setClickable(false);

        et1=(EditText)findViewById(R.id.et1);
        et2=(EditText)findViewById(R.id.et2);
        et3=(EditText)findViewById(R.id.et3);
        et4=(EditText)findViewById(R.id.et4);
        et5=(EditText)findViewById(R.id.et5);
        et6=(EditText)findViewById(R.id.et6);
        et1.addTextChangedListener(new GenericTextWatcher(et1));
        et2.addTextChangedListener(new GenericTextWatcher(et2));
        et3.addTextChangedListener(new GenericTextWatcher(et3));
        et4.addTextChangedListener(new GenericTextWatcher(et4));
        et5.addTextChangedListener(new GenericTextWatcher(et5));
        et6.addTextChangedListener(new GenericTextWatcher(et6));


        btnOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LoginActivity.isNetworkConnected(OtpActivity.this)) {
                    otpString = et1.getText().toString().trim() + et2.getText().toString().trim() + et3.getText().toString().trim()
                            + et4.getText().toString().trim() + et5.getText().toString().trim() + et6.getText().toString().trim();
                    Log.d("abcString=", otpString + "----" + otpString.length());
                    if (otpString.length() == 6) {
                        checkOtpUrl(OtpActivity.this);
                        et1.setText("");
                        et2.setText("");
                        et3.setText("");
                        et4.setText("");
                        et5.setText("");
                        et6.setText("");

                    } else {
                        Toast.makeText(OtpActivity.this, "Please enter valid six digit Otp", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(OtpActivity.this,"No Internet Connection",Toast.LENGTH_LONG).show();
                    //return;
                }

            }
        });

        new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                tv_resend.setTextColor(getResources().getColor(R.color.redColor));
                tv_resend.setText("seconds remaining: " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                tv_resend.setTextColor(getResources().getColor(R.color.color_white));
                tv_resend.setText("ResendOtp");
                tv_resend.setClickable(true);
                resendOtp=true;
            }

        }.start();

        tv_resend.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               if (LoginActivity.isNetworkConnected(OtpActivity.this)) {
                   if (resendOtp)
                       resendOtpUrl(OtpActivity.this);

               }else{
                   Toast.makeText(OtpActivity.this,"No Internet Connection",Toast.LENGTH_LONG).show();
                   //return;
               }

           }
       });


       // Old

        /*et_otp_=(EditText)findViewById(R.id.otp_et_);
        tvResendOtp=(TextView)findViewById(R.id.resend_otp_);
        btn_otp=(Button)findViewById(R.id.btn_Otp_);


        btn_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_otp_.getText().toString().equalsIgnoreCase("")||et_otp_.getText().toString().equals(null)){
                    et_otp_.setError("Please enter Otp");
                }else
                 val=et_otp_.getText().toString();
                Log.d("valString=",val);

            }
        });*/

    }


    public class GenericTextWatcher implements TextWatcher
    {
        private View view;
        private GenericTextWatcher(View view)
        {
            this.view = view;
        }
@Override
        public void afterTextChanged(Editable editable) {
// TODO Auto-generated method stub
            String text = editable.toString();
            switch(view.getId())
            {
                case R.id.et1:
                    if(text.length()==1)
                        et2.requestFocus();
                    et2.setHint("");
                    break;
                case R.id.et2:
                    if(text.length()==1)
                        et3.requestFocus();
                    et3.setHint("");
                    break;
                case R.id.et3:
                    if(text.length()==1)
                        et4.requestFocus();
                    et4.setHint("");
                    break;
                case R.id.et4:
                    if(text.length()==1)
                        et5.requestFocus();
                    et5.setHint("");
                    break;
                case R.id.et5:
                    if(text.length()==1)
                        et6.requestFocus();
                    et6.setHint("");
                    break;
                case R.id.et6:
                    break;
            }
        }
@Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
// TODO Auto-generated method stub
    et1.setHint("");
        }
@Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
// TODO Auto-generated method stub
        }
    }



    private void checkOtpUrl(final Context context)
    {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(OtpActivity.this);
        loaderDiloag.displayDiloag();

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("userid", userId);
            jsonBody.put("otp", otpString);


            Log.d(TAG,"jsonLogin " + jsonBody.toString());
            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.POST, chkOtpUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    loaderDiloag.dismissDiloag();

                    Log.d("checkOtpUrl", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.contains("success"))
                        {
                            new MaterialDialog.Builder(context)
                                    .title("OTP")
                                    .content(message)
                                    .iconRes(R.drawable.ic_check_circle)
                                    .canceledOnTouchOutside(false)
                                    .positiveText("Ok")
                                    .onPositive(new MaterialDialog.SingleButtonCallback()
                                    {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                        {

                                            dialog.dismiss();
                                            if (type.equalsIgnoreCase("ForgetPassActivity")){
                                                Intent intent=new Intent(OtpActivity.this,ChangePassActivity.class);
                                                intent.putExtra("userId",userId);
                                                //intent.putExtra("type","RegisterActivity");
                                                startActivity(intent);
                                                finish();

                                            }else {
                                                startActivity(new Intent(context, LoginActivity.class));
                                                finish();
                                            }
                                        }
                                    }).show();

                        }
                        else
                        {
                            loaderDiloag.dismissDiloag();
                            Toast.makeText(OtpActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        loaderDiloag.dismissDiloag();
                        Toast.makeText(OtpActivity.this,getResources().getString(R.string.try_again),Toast.LENGTH_SHORT).show();

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    loaderDiloag.dismissDiloag();
                    Toast.makeText(OtpActivity.this,getResources().getString(R.string.try_again),Toast.LENGTH_SHORT).show();

                    Log.e("VOLLEYOut", error.toString());
                }
            })

            {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    HashMap<String,String> param = new HashMap<String, String>();
                    param.put("Content-Type","application/json");
                    return param;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }


            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void resendOtpUrl(final Context context)
    {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(OtpActivity.this);
        loaderDiloag.displayDiloag();

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            /*JSONObject jsonBody = new JSONObject();
            jsonBody.put("userid", userId);
            jsonBody.put("otp", otpString);


            Log.d(TAG,"jsonLogin " + jsonBody.toString());
            final String requestBody = jsonBody.toString();*/

            StringRequest stringRequest = new StringRequest(Request.Method.GET, resendOtpUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    loaderDiloag.dismissDiloag();

                    Log.d("resendOtpUrl", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.contains("success"))
                        {
                            new MaterialDialog.Builder(context)
                                    .title("OTP")
                                    .content(message)
                                    .iconRes(R.drawable.ic_check_circle)
                                    .canceledOnTouchOutside(false)
                                    .positiveText("Ok")
                                    .onPositive(new MaterialDialog.SingleButtonCallback()
                                    {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                        {

                                            dialog.dismiss();
                                            //startActivity(new Intent(context,LoginActivity.class));
                                        }
                                    }).show();

                        }
                        else
                        {
                            loaderDiloag.dismissDiloag();
                            Toast.makeText(OtpActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        loaderDiloag.dismissDiloag();
                        Toast.makeText(OtpActivity.this,getResources().getString(R.string.try_again),Toast.LENGTH_SHORT).show();

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    loaderDiloag.dismissDiloag();
                    Toast.makeText(OtpActivity.this,getResources().getString(R.string.try_again),Toast.LENGTH_SHORT).show();
                    Log.e("VOLLEYOut", error.toString());
                }
            })

            {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    HashMap<String,String> param = new HashMap<String, String>();
                    param.put("Content-Type","application/json");
                    return param;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

               /* @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }*/


            };

            requestQueue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
