package com.app.lynkpal;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
//import com.stripe.android.Stripe;
//import com.stripe.android.TokenCallback;
//import com.stripe.android.model.Card;
//import com.stripe.android.model.Token;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class PayActivity extends AppCompatActivity {
    TextView head;
    String pay, type;
    TextView amount;
//    Stripe stripe;
//    Token tok;
//    Card card;
    String id;
    ImageView mBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);
        head = (TextView) findViewById(R.id.head);
        amount = (TextView) findViewById(R.id.amount);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        head.setTypeface(tf_bold);
        pay = getIntent().getStringExtra("pay");
        type = getIntent().getStringExtra("type");
        id = getIntent().getStringExtra("id");
        amount.setText(pay + "");
      //  stripe = new Stripe(this, "pk_test_3F27d57GgT9zyQ70gTN7J0gL");
        EditText cardNumberField = (EditText) findViewById(R.id.cardNumber);
        final EditText monthField = (EditText) findViewById(R.id.month);
        final EditText yearField = (EditText) findViewById(R.id.year);
        final EditText cvcField = (EditText) findViewById(R.id.cvc);
        mBack = findViewById(R.id.ic_back);
        cardNumberField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i == 15) {
                    monthField.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        monthField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i == 1) {
                    yearField.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        yearField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i == 1) {
                    cvcField.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mBack.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
    }

    public void submitCard(View view){
        Toast.makeText(PayActivity.this,"Sorry! we are unable to provide this service",Toast.LENGTH_SHORT).show();
    }

/*
    public void submitCard(View view) {
        // TODO: replace with your own test key
        EditText cardNumberField = (EditText) findViewById(R.id.cardNumber);
        EditText monthField = (EditText) findViewById(R.id.month);
        EditText yearField = (EditText) findViewById(R.id.year);
        EditText cvcField = (EditText) findViewById(R.id.cvc);
        if (cardNumberField.getText().toString().length() == 16) {
            if (monthField.getText().toString().length() == 2) {
                if (yearField.getText().toString().length() == 2) {
                    if (cvcField.getText().toString().length() > 2) {
                        if (ApplicationGlobles.isConnectingToInternet(PayActivity.this)) {
                            final LoaderDiloag loaderDiloag = new LoaderDiloag(PayActivity.this);
                            loaderDiloag.displayDiloag();
                            card = new Card(
                                    cardNumberField.getText().toString(),
                                    Integer.valueOf(monthField.getText().toString()),
                                    Integer.valueOf(yearField.getText().toString()),
                                    cvcField.getText().toString()
                            );
                            card.setCurrency("usd");
                            stripe.createToken(card, new TokenCallback() {
                                public void onSuccess(Token token) {
                                    // TODO: Send Token information to your backend to initiate a charge
                                    // Toast.makeText(getApplicationContext(), "Token created: " + token.getId(), Toast.LENGTH_LONG).show();
                                    tok = token;
                                    loaderDiloag.dismissDiloag();
                                    Log.e("Token", token.getId() + "Getting token");
                                    PaySuccess(token.getId());

                                }

                                public void onError(Exception error) {
                                    loaderDiloag.dismissDiloag();
                                    Toast.makeText(PayActivity.this, "" + error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                    Log.d("Stripe", error.getLocalizedMessage());
                                }
                            });
                        } else {
                            Toast.makeText(PayActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        Toast.makeText(this, "Please Enter Correct CVV no.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Please Enter Correct Exp Year", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Please Enter Correct Exp Month", Toast.LENGTH_SHORT).show();
            }
        } else {
            cardNumberField.setError("Please Enter Correct Card Number");
        }

        // card.setName("Amit Kumar");
        //  card.setAddressZip("1000");
        */
/*
        card.setNumber(4242424242424242);
        card.setExpMonth(12);
        card.setExpYear(19);
        card.setCVC("123");
        *//*


    }
*/


    public void PaySuccess(String token) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(PayActivity.this);

        if (ApplicationGlobles.isConnectingToInternet(PayActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            //String params="{\r\n\"description\":\"Write text to post\",\r\n\"postFile\":\"sadc\",\r\n\"extension\":\"jpeg\"\r\n}";
            JSONObject params = new JSONObject();
            try {
                int p = Integer.parseInt(pay);
                params.put("token", token);
                params.put("amount", p);
                params.put("currency", "usd");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String url;
            if (type.equals("adv")) {
                url = WebApis.AdvPay + id + "/";
            } else {
                url = WebApis.JobPay + id + "/";
            }
            RequestBody body = RequestBody.create(mediaType, params + "");
            Request request = new Request.Builder()

                    .url(url)
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (PayActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                //  encodedString = null;
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(PayActivity.this, "Payment done successfully", Toast.LENGTH_SHORT).show();
                                            if (!type.equals("adv")) {
                                                Constant.update_created = true;
                                            }
                                            finish();
                                            //   showPopup();
                                        }
                                    });
                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(PayActivity.this, "Something went wrong try later...", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                        loaderDiloag.dismissDiloag();
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(PayActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

}
