package com.app.lynkpal;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Adapter.ChatAdapter;
import com.app.lynkpal.Bean.ChatBean;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Base64;
import com.app.lynkpal.Helper.CircleImageView;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class ChatActivity extends AppCompatActivity {
    private static final int REQUEST_CAMERA_ACCESS_PERMISSION = 5674;
    ImageView back, attachment;
    TextView txt_contact_name;
    EditText messageEdit;
    ImageView send_message;
    ChatBean chatBean;
    Handler handler;
    Bitmap bitmap = null;
    String attachmentStr = "";
    ProgressDialog progressDialog;
    //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    String format = simpleDateFormat.format(new Date());
    ChatAdapter chatAdapter;
    String id, name, image;
    CircleImageView user_image;
    SharedPreferences settings;
    String profile_pic;
    ImageView user_profile;
    private ListView recyclerView;
    private ArrayList<ChatBean> chatBeanArrayList = new ArrayList<>();
    private ArrayList<ChatBean> temp = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatapp);
        Log.d("checkActivity","ChatActivity");

        Constant.update_chat=true;
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        settings = (this).getSharedPreferences(Constant.PREFS_NAME, 0);
        back = (ImageView) findViewById(R.id.back);
        recyclerView = (ListView) findViewById(R.id.chat_recycler);
        user_profile = (ImageView) findViewById(R.id.user_profile);
        attachment = (ImageView) findViewById(R.id.attachment);
        txt_contact_name = (TextView) findViewById(R.id.txt_contact_name);
        txt_contact_name.setTypeface(tf_bold);
        messageEdit = (EditText) findViewById(R.id.messageEdit);
        user_image = (CircleImageView) findViewById(R.id.user_image);
        chatAdapter = new ChatAdapter(chatBeanArrayList, ChatActivity.this);
        recyclerView.setAdapter(chatAdapter);
        chatAdapter.notifyDataSetChanged();
        messageEdit.setTypeface(tf_reg);

        try {
            profile_pic = settings.getString("profile_pic", "");
            Glide.with(this).load(WebApis.userProfileImage + profile_pic).into(user_profile);
           /* Glide.with(this).load(WebApis.userProfileImage + profile_pic).asBitmap().centerCrop()
                    .error(R.drawable.image).placeholder(R.drawable.image).into(new BitmapImageViewTarget(user_profile) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    user_profile.setImageDrawable(circularBitmapDrawable);
                }
            });*/
        } catch (Exception e) {

        }
        Glide.with(this).load(WebApis.userProfileImage + profile_pic).into(user_image);
       /* Glide.with(this).load(WebApis.userProfileImage + image).asBitmap().centerCrop()
                .error(R.drawable.image).placeholder(R.drawable.image).into(new BitmapImageViewTarget(user_image) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                user_image.setImageDrawable(circularBitmapDrawable);
            }
        });*/
        //  Intent intent = getIntent();
        try {
            id = getIntent().getStringExtra("id");
            name = getIntent().getStringExtra("name");
            txt_contact_name.setText(name);
            image = getIntent().getStringExtra("image");
            Log.e("vacds", WebApis.userProfileImage + image);
            Glide.with(this).load(WebApis.userProfileImage + image).into(user_image);
          /*  Glide.with(this).load(WebApis.userProfileImage + image).asBitmap().centerCrop()
                    .error(R.drawable.image).placeholder(R.drawable.image).into(new BitmapImageViewTarget(user_image) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    user_image.setImageDrawable(circularBitmapDrawable);
                }
            });*/
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong. Try Later...", Toast.LENGTH_SHORT).show();
            finish();
        }
        send_message = (ImageView) findViewById(R.id.send_message);
        send_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (messageEdit.getText().toString().trim().length() > 0) {
                    sendMessageToServer(messageEdit.getText().toString(), null);
                    messageEdit.setText("");
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(messageEdit.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);
                } else {
                    Toast.makeText(ChatActivity.this, "Please write something", Toast.LENGTH_SHORT).show();

                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        attachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
        getAllMessage();

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getAllMessage();
                handler.postDelayed(this, 6000);
            }
        }, 6000);
/*
new Handler().
*/

     /*   Handler handler=new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        })*/
    }

    @Override
    protected void onDestroy() {
        handler.removeMessages(0);
        super.onDestroy();
    }

    public void getAllMessage() {
        //  final LoaderDiloag loaderDiloag = new LoaderDiloag(ChatActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(ChatActivity.this)) {
            // loaderDiloag.displayDiloag();
            temp.clear();
            OkHttpClient client = new OkHttpClient();
            //  Log.e("Constant.token", Constant.token);
            //  Log.e("WebApis.ALLMESSAGE", WebApis.ALLMESSAGE + id);
            Request request = new Request.Builder()
                    .url(WebApis.ALLMESSAGE + id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (ChatActivity.this == null)
                            return;
                        // loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");
                        if (jsonData.length() > 0) {
                            try {
                                JSONArray array = new JSONArray(jsonData);
                                if (array.length() > 0) {
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject jsonObject1 = array.getJSONObject(i);
                                        //   String id=jsonObject1.getString("id");
                                        chatBean = new ChatBean();
                                        chatBean.setId(jsonObject1.getString("id"));
                                        chatBean.setReciever_name(jsonObject1.getString("reciever_name"));
                                        chatBean.setReciever_pic(jsonObject1.getString("reciever_pic"));
                                        chatBean.setSender_name(jsonObject1.getString("sender_name"));
                                        chatBean.setSender_pic(jsonObject1.getString("sender_pic"));
                                        chatBean.setAttachments(jsonObject1.getString("image"));
                                        chatBean.setReciever(jsonObject1.getString("reciever"));
                                        chatBean.setSender(jsonObject1.getString("sender"));
                                        chatBean.setMessage(jsonObject1.getString("message"));
                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//2016-01-24T16:00:00.000Z
                                        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                                        long time = 0;
                                        time = sdf.parse(jsonObject1.getString("created_at")).getTime();
                                        long now = System.currentTimeMillis();
                                        CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
                                        chatBean.setCreated_at(String.valueOf(ago));
                                        chatBean.setCurrenttime(format);
                                        temp.add(chatBean);
                                        //chatBeanArrayList.add(chatBean);
                                    }

                                }
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                      /*  if (chatBeanArrayList.size()>0) {
                                            for (int a = 0; a < temp.size(); a++) {
                                                boolean have = false;
                                                for (int b = 0; b < chatBeanArrayList.size(); b++) {
                                                    if (chatBeanArrayList.get(b).getId().equals(temp.get(a).getId())) {
                                                        have = true;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        have = false;
                                                    }

                                                }
                                                if (!have) {
                                                    chatBeanArrayList.add(temp.get(a));

                                                  //  chatAdapter = new ChatAdapter(chatBeanArrayList, ChatActivity.this);

                                                }

                                            }*/

                                        int index = recyclerView.getFirstVisiblePosition();
                                        //int index_last = recyclerView.getLastVisiblePosition();
                                        chatBeanArrayList.clear();
                                        chatAdapter.notifyDataSetChanged();
                                        chatBeanArrayList.addAll(temp);
                                        chatAdapter = new ChatAdapter(chatBeanArrayList, ChatActivity.this);
                                        recyclerView.setAdapter(chatAdapter);
                                        chatAdapter.notifyDataSetChanged();
                                        // recyclerView.setSelection(index);

                                       /* }
                                        else
                                        {
                                            chatBeanArrayList.addAll(temp);
                                            chatAdapter = new ChatAdapter(chatBeanArrayList, ChatActivity.this);
                                            recyclerView.setAdapter(chatAdapter);
                                            chatAdapter.notifyDataSetChanged();
                                        }*/

                                    }
                                });


                            } catch (Exception je) {
                                // swipeRefreshLayout.setRefreshing(false);
                                // loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            // loaderDiloag.dismissDiloag();
                        } else {
                            // loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //swipeRefreshLayout.setRefreshing(false);
                        //loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(ChatActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                            && ActivityCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                REQUEST_CAMERA_ACCESS_PERMISSION);
                    } else {
                        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePicture.resolveActivity(getPackageManager()) != null) {
                            startActivityForResult(takePicture, 1);
                        }
                    }

                } else if (options[item].equals("Choose from Gallery")) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                                == PackageManager.PERMISSION_GRANTED || checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            Log.v("TAG", "Permission is granted");
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            //  Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(photoPickerIntent, 2);//one can be replaced with any action code

                        } else {

                            Log.v("TAG", "Permission is revoked");
                            ActivityCompat.requestPermissions(ChatActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                        }
                    } else {

                        Log.v("TAG", "Permission is granted");
                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                        photoPickerIntent.setType("image/*");
                        //  Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(photoPickerIntent, 2);//one can be replaced with any action code
                    }

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Bundle extras = data.getExtras();
                bitmap = (Bitmap) extras.get("data");
                convertPreImagetBitmapToString(bitmap);

            } else if (requestCode == 2) {
                /*Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                bitmap = (BitmapFactory.decodeFile(picturePath));
                chatBean=new ChatBean();
                chatBean.setBitmap(bitmap);
                chatBean.setCurrenttime(format);
                chatBeanArrayList.add(chatBean);
                Log.e("path of image gallery", picturePath + "");
                bindReyclerView(this,chatBeanArrayList);
                convertPreImagetBitmapToString(bitmap);*/
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = "";
                if (c != null) {
                    picturePath = c.getString(columnIndex);
                    c.close();
                }
                if (picturePath != null) {
                    getThumbnailBitmap(picturePath, 100);
                }
            }
        }
    }

    private Bitmap getThumbnailBitmap(String picturePath, int thumbnailSize) {
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(picturePath, bounds);
        if ((bounds.outWidth == -1) || (bounds.outHeight == -1)) {
            bitmap = null;
        }
        int originalSize = (bounds.outHeight > bounds.outWidth) ? bounds.outHeight
                : bounds.outWidth;
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = originalSize / thumbnailSize;
        bitmap = BitmapFactory.decodeFile(picturePath, opts);
        if (bitmap != null) {
            convertPreImagetBitmapToString(bitmap);

        } else {
            Toast.makeText(this, "Please Select correct Image", Toast.LENGTH_SHORT).show();
        }
        return bitmap;

    }

    public String convertPreImagetBitmapToString(Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 90, stream); //compress to which format you want.
        byte[] byte_arr = stream.toByteArray();
        attachmentStr = Base64.encodeBytes(byte_arr);
        Log.e("imageStr", attachmentStr);
        showPopup(ChatActivity.this, attachmentStr, bmp);
        return attachmentStr;

    }

    private void showPopup(Activity activity, final String attachmentStr, Bitmap bitmap) {
        final Dialog layout = new Dialog(activity);

        // Include dialog.xml file
        layout.setContentView(R.layout.showpopup_attachment_message);

        ImageView attachMent = (ImageView) layout.findViewById(R.id.attachment_image);
        attachMent.setImageBitmap(bitmap);

        final EditText messageEdit = (EditText) layout.findViewById(R.id.new_message);
        // Set dialog title
        layout.setTitle("Send Attachment");
        Button no = (Button) layout.findViewById(R.id.no);
        Button yes = (Button) layout.findViewById(R.id.send);
        layout.show();
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout.dismiss();
            }
        });
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String strMessage = messageEdit.getText().toString();
                    sendMessageToServer(strMessage, attachmentStr);
                    layout.dismiss();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });

        attachMent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
    }

    public void sendMessageToServer(String strMessage, String attachmentStr) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(ChatActivity.this);

        if (ApplicationGlobles.isConnectingToInternet(ChatActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            //String params="{\r\n\"description\":\"Write text to post\",\r\n\"postFile\":\"sadc\",\r\n\"extension\":\"jpeg\"\r\n}";
            JSONObject params = new JSONObject();
            try {
                params.put("talkingto", id);
                params.put("message", strMessage);
                params.put("msgPic", attachmentStr);
                Log.e("param", params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(mediaType, params + "");
            Request request = new Request.Builder()
                    .url(WebApis.SENDMESSAGE)
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (ChatActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    getAllMessage();
                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ChatActivity.this, "Something went wrong try later...", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                        loaderDiloag.dismissDiloag();
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(ChatActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

}
