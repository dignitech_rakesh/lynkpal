package com.app.lynkpal;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.lynkpal.Adapter.CareerAdviceAdapter;
import com.app.lynkpal.Adapter.LynkpalLearningListAdapter;
import com.app.lynkpal.Bean.CareerAdviceModel;
import com.app.lynkpal.Bean.LynkaplLearningModel;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.CareerAdviceInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CareerAdvice extends AppCompatActivity implements CareerAdviceInterface {

    private RecyclerView rvCareerAdvice;
    private SharedPreferences sharedPreferences;
    ArrayList<CareerAdviceModel> careerAdviceList = new ArrayList<>();
    LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_career_advice);
        sharedPreferences = CareerAdvice.this.getSharedPreferences(Constant.PREFS_NAME, 0);

        rvCareerAdvice = (RecyclerView)findViewById(R.id.rvCareerAdvice);
        getCareerAdviceDataApi();
    }


    private void getCareerAdviceDataApi() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(CareerAdvice.this);

        if (ApplicationGlobles.isConnectingToInternet(CareerAdvice.this)) {

            loaderDiloag.displayDiloag();

        com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(CareerAdvice.this);
      /*  Map<String, String> postParam = new HashMap<String, String>();
        postParam.put("toFollow", user);

        Log.d("Training", "frndParam " + postParam.toString());*/


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                WebApis.CAREERADVICE, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response)
                    {
                        loaderDiloag.dismissDiloag();
                        Log.d("CAREERADVICE", "CAREERADVICE " + response.toString());
                        try {
                            //String status = response.getString("status");
                            // String message = response.getString("message");
                            // if (status.contains("success"))
                            String status = response.getString("status");
                            // String message = response.getString("message");
                            if (status.contains("success"))
                            {
                                JSONArray jsonArray=response.getJSONArray("blogs");
                                // Loop through the array elements
                                for(int i=0;i<jsonArray.length();i++){
                                    // Get current json object
                                    CareerAdviceModel careerAdviceModel=new CareerAdviceModel();
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    // Get the current student (json object) data
                                    careerAdviceModel.setId(jsonObject.getInt("id"));
                                    careerAdviceModel.setTitle(jsonObject.getString("title"));
                                    careerAdviceModel.setDescription(jsonObject.getString("description"));
                                    careerAdviceModel.setImage(jsonObject.getString("image"));

                                    careerAdviceModel.setCreated_at(jsonObject.getString("created_at"));

                                    careerAdviceList.add(careerAdviceModel);

                                    Log.d("lynkpalLearningListSize",careerAdviceList.size()+"");
                                }


                                if (careerAdviceList.size() > 0) {
                                   /* listLynkpalLearning.setExpanded(true);
                                    LynkpalLearningListAdapter lynkpalLearningListAdapter = new LynkpalLearningListAdapter(LynkpalLearning.this, lynkpalLearningList);
                                    listLynkpalLearning.setAdapter(lynkpalLearningListAdapter);*/


                                    CareerAdviceAdapter mAdapter = new CareerAdviceAdapter(CareerAdvice.this, careerAdviceList,CareerAdvice.this);
                                    mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                    rvCareerAdvice.setLayoutManager(mLayoutManager);
                                    rvCareerAdvice.setItemAnimator(new DefaultItemAnimator());
                                    rvCareerAdvice.setAdapter(mAdapter);
                                }
                              /*  mFriendAdapter.notifyDataSetChanged();
                                suggestionApi(context, settings.getString("token",""));
                                Toast.makeText(context, "Followed successfully ", Toast.LENGTH_LONG).show();*/
                            } else
                            {

                                Log.d("Training", "frndError " + response.toString());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener()
        {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                loaderDiloag.dismissDiloag();
                error.printStackTrace();
                // if (error.toString().contains(Constant.timeout_error_string));
                // Toast.makeText(AddTrainingActivity.this,getString(R.string.slow_internet), Toast.LENGTH_SHORT).show();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String,String> data = new HashMap<String, String>();
                data.put("Authorization",sharedPreferences.getString("token",""));
                data.put("Content-Type","application/json");
                Log.d("Training","header " + data.toString());
                return data;
            }

        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.VOLLEY_TIMEOUT_MS,
                Constant.VOLLEY_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        jsonObjectRequest.setTag("Training");
        // Adding request to request queue
        requestQueue.add(jsonObjectRequest);

        } else {
            Toast.makeText(CareerAdvice.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void Expand(int pos) {

    }

    @Override
    public void Shrink(int pos) {

        int distanceInPixels;
        View firstVisibleChild = rvCareerAdvice.getChildAt(0);
        int itemHeight = firstVisibleChild.getHeight();
        int currentPosition = rvCareerAdvice.getChildAdapterPosition(firstVisibleChild);
        int p = Math.abs(pos - currentPosition);
        if (p > 5) distanceInPixels = (p - (p - 5)) * itemHeight;
        else       distanceInPixels = p * itemHeight;
        mLayoutManager.scrollToPositionWithOffset(pos, distanceInPixels);
    }
}
