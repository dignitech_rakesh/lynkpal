package com.app.lynkpal;

import android.Manifest;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final int ERROR_DIALOGUE_REQUEST = 9001;
    private final String TAG = MapActivity.class.getSimpleName();
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;

    public Boolean mLocationpermissionGranted = false;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    GoogleMap mMap;
    String address = " ";
    AppCompatButton mUpdateAddress;
    public static LatLng mLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        mUpdateAddress = findViewById(R.id.address_button);
        getLocationPermissions(MapActivity.this);

        mUpdateAddress.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
               if (address=="null")
               {
                   Toast.makeText(MapActivity.this,"Please Mark Any Place",Toast.LENGTH_SHORT).show();
               }
               else
               {
                   startActivity(new Intent(MapActivity.this,UpdateCompanyProfile.class).putExtra("address",address));
                   finish();
               }
            }
        });
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "Map is ready");
        Toast.makeText(MapActivity.this, "map is Ready", Toast.LENGTH_LONG).show();
        mMap = googleMap;
        if (mLocationpermissionGranted)
        {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            else
            {
                getDeviceLocation();
            }
            mMap.setMyLocationEnabled(true);
        }
    }

    private void initMap()
    {
        Log.d(TAG,"initializing Map");
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(MapActivity.this);
    }

    private void getLocationPermissions(Context context)
    {
        Log.d(TAG,"getting Location permissions");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION};
        if (ContextCompat.checkSelfPermission(context,FINE_LOCATION)== PackageManager.PERMISSION_GRANTED)
        {
            if(ContextCompat.checkSelfPermission(context,COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED)
            {
              mLocationpermissionGranted = true;
              initMap();
            }
            else
            {
                ActivityCompat.requestPermissions(this,permissions,LOCATION_PERMISSION_REQUEST_CODE);
            }
        }
        else
        {
            ActivityCompat.requestPermissions(this,permissions,LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "onRequest Permission result called");
        mLocationpermissionGranted = false;
        switch (requestCode)
        {
            case LOCATION_PERMISSION_REQUEST_CODE:
            {
                if (grantResults.length>0)
                {
                    for (int i=0;i<grantResults.length;i++)
                    {
                       if (grantResults[i]!=PackageManager.PERMISSION_GRANTED)
                       {
                           mLocationpermissionGranted = false;
                           return;
                       }
                       mLocationpermissionGranted = true;
                       initMap();
                    }
                }
            }
        }
    }

    public boolean isServicesOk()
    {
        Log.d(TAG, "isServicesOk: ");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(MapActivity.this);
        if (available== ConnectionResult.SUCCESS)
        {
            Log.d(TAG,"Everything is ok");
        }
        return false;
    }

    private void getDeviceLocation()
    {
        Log.d(TAG,"getting current location");
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(MapActivity.this);
        try {
              if (mLocationpermissionGranted)
              {
                  com.google.android.gms.tasks.Task location = mFusedLocationProviderClient.getLastLocation();
                  location.addOnCompleteListener(new OnCompleteListener()
                  {
                      @Override
                      public void onComplete(@NonNull com.google.android.gms.tasks.Task task)
                      {
                          if (task.isSuccessful())
                          {

                              Location currentLocation = (Location) task.getResult();
                              Log.d(TAG, "onComplete: CurrentLocation " + currentLocation);
                             if (currentLocation!=null)
                             {
                                 moveCamera(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()),DEFAULT_ZOOM,"My Location");
                             }
                             else
                             {
                                 Toast.makeText(MapActivity.this,"Mark your Location",Toast.LENGTH_LONG).show();
                                 return;
                             }
                              Log.d(TAG, "onComplete: found location " + currentLocation.toString());

                              mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener()
                              {
                                  @Override
                                  public void onMapClick(LatLng latLng)
                                  {
                                      MarkerOptions markerOptions = new MarkerOptions();

                                      // Setting the position for the marker
                                      markerOptions.position(latLng);
                                      mLocation = latLng;
                                      // Setting the title for the marker.
                                      // This will be displayed on taping the marker

                                      try {
                                          geoLocate(latLng.latitude,latLng.longitude);
                                      } catch (IOException e) {
                                          e.printStackTrace();
                                      }

                                      markerOptions.title(address);

                                      // Clears the previously touched position
                                      mMap.clear();

                                      // Animating to the touched position
                                      mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                                      // Placing a marker on the touched position
                                      mMap.addMarker(markerOptions);


                                  }
                              });

                          }
                          else
                          {
                              Log.d(TAG, "Current location is null");
                          }
                      }
                  });
              }
        }
        catch (SecurityException e)
        {
            Log.d(TAG, "getDeviceLocation: " + e.getMessage());
        }


    }

    private void geoLocate(double latitude,double longitude) throws IOException {
        Log.d(TAG, "geoLocate: geoLocating");
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();

        Log.d(TAG, "geoLocate: Address is " + address);

    }

    private void moveCamera(LatLng latLng,float zoom,String title)
    {
        Log.d(TAG, "moveCamera: latlong " + latLng.longitude + "longitude " + latLng.latitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,zoom));
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
        startActivity(new Intent(MapActivity.this,UpdateCompanyProfile.class));
    }
}
