package com.app.lynkpal;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.github.javiersantos.appupdater.AppUpdater;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import static android.provider.ContactsContract.CommonDataKinds.Website.URL;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = LoginActivity.class.getSimpleName();
    TextView txtEnter,tvForget;
    EditText edtEmail, edtPassword;
    Button btnSignIn;
    TextView btnSignUp;
    String user_id, user_prof, user_firstname, user_lastname, user_email, token, message;
    SharedPreferences settings;
    String user_pass;
    String regId,playerId;
    String userId,resend_otp,messageShow;

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        setContentView(R.layout.activity_login);

       /* AppUpdater appUpdater = new AppUpdater(this);    //https://github.com/javiersantos/AppUpdater


        new AppUpdater(this)
                .setTitleOnUpdateAvailable("Update available")
                .setContentOnUpdateAvailable("Check out the latest version available of my app!")
                .setTitleOnUpdateNotAvailable("Update not available")
                .setContentOnUpdateNotAvailable("No update available. Check for updates again later!")
                .setButtonUpdate("Update now?")
                *//*.setButtonUpdateClickListener(...)
	.setButtonDismiss("Maybe later")
                .setButtonDismissClickListener(...)
	.setButtonDoNotShowAgain("Huh, not interested")
                .setButtonDoNotShowAgainClickListener(...)
	.setIcon(R.drawable.ic_update) // Notification icon *//*
                .setCancelable(false); // Dialog could not be dismissable

        appUpdater.start();*/

        //OneSignal

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                playerId=userId;
                regId=registrationId;
                //Log.d("debug", "User:" + userId);
                if (registrationId != null)
                   // Log.d("debug", "registrationId:" + registrationId);
                    Log.d("debug", "registrationId:"+ playerId +"-----"+ regId);

            }
        });

       // playerId=getIntent().getStringExtra("playerId");

        settings = (this).getSharedPreferences(Constant.PREFS_NAME, 0);

        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        txtEnter = (TextView) findViewById(R.id.textEnter);
        tvForget = (TextView) findViewById(R.id.forget_psw);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPassword = (EditText) findViewById(R.id.edtPwd);
        btnSignIn = (Button) findViewById(R.id.btnSignIn);
        btnSignUp = (TextView) findViewById(R.id.btnSignUp);
        txtEnter.setTypeface(tf_bold);
        edtEmail.setTypeface(tf_reg);
        edtPassword.setTypeface(tf_reg);
        btnSignIn.setTypeface(tf_bold);
        btnSignUp.setTypeface(tf_med);

        tvForget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(LoginActivity.this,ForgetPassActivity.class);
                startActivity(intent);
            }
        });
        btnSignIn.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ApplicationGlobles.isEmailValid(edtEmail.getText().toString())) {
                    if (!ApplicationGlobles.isNullOrEmpty(edtPassword.getText().toString())) {
                       // if (Lynkpal.oneSignalID.length() > 1) i
                        if (edtEmail.getText().toString()!=null || edtPassword.getText().toString()!=null)
                        {
                            if (isNetworkConnected(LoginActivity.this))
                            {
                                callLogin(LoginActivity.this);
                                String email = edtEmail.getText().toString().trim();
                                String email_auth_hash = "NGM3MDQ4MTctODExMC00MjM2LWJmMGEtNzM5MTIyNTE4MDIw";
                                /* OneSignal.setEmail(email, email_auth_hash, new OneSignal.EmailUpdateHandler()
                                 {
                                     @Override
                                     public void onSuccess()
                                     {
                                         Log.d(TAG, "onSuccess: OneSignal");
                                     }

                                     @Override
                                     public void onFailure(OneSignal.EmailUpdateError error)
                                     {
                                         Log.d(TAG, "onFailure: OneSignal " + error.getMessage());
                                     }
                                 });*/

                            }
                            else
                            {
                                Toast.makeText(LoginActivity.this,"No Internet Connection",Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(LoginActivity.this, "Please Wait...", Toast.LENGTH_SHORT).show();
                        }
                        //  startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    } else {
                        edtPassword.setError("Required");
                    }

                } else {
                    edtEmail.setError("Please enter valid e-mail");
                }
            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
    }

    public static boolean isNetworkConnected(Context co) {
        ConnectivityManager cm = (ConnectivityManager)co.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public void callLogin(final Context context)
    {
        user_pass = edtPassword.getText().toString().trim();
        Log.d(TAG, "onResponse: login " + user_pass);
        final LoaderDiloag loaderDiloag = new LoaderDiloag(LoginActivity.this);

        if (ApplicationGlobles.isConnectingToInternet(LoginActivity.this))
       {
           final String android_id = Settings.Secure.getString(LoginActivity.this.getContentResolver(),
                   Settings.Secure.ANDROID_ID);

           /*UUID uuid = UUID.fromString(android_id);
            Log.d(TAG, "callLogin: DeviceID " + uuid);*/

           loaderDiloag.displayDiloag();
           try {
               RequestQueue requestQueue = Volley.newRequestQueue(context);
               final JSONObject jsonBody = new JSONObject();
               jsonBody.put("email", edtEmail.getText().toString());
               jsonBody.put("password",edtPassword.getText().toString());
               if (SplashActivity.mplayerId!=null)
               jsonBody.put("deviceId", SplashActivity.mplayerId);
               else{
                   jsonBody.put("deviceId", playerId);

               }
               //jsonBody.put("deviceId", playerId);
               //jsonBody.put("deviceId", android_id);

               Log.d(TAG,"jsonLogin " + jsonBody.toString());
               final String requestBody = jsonBody.toString();

               StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.POST,WebApis.loginURL, new Response.Listener<String>() {
                   @Override
                   public void onResponse(String response)
                   {
                       JSONObject jsonObject;
                       loaderDiloag.dismissDiloag();
                       Log.i("VOLLEYLogin", response);
                       try {
                            jsonObject = new JSONObject(response);
                           String status = jsonObject.getString("status");
                           String message = jsonObject.getString("message");
                           if (status.equalsIgnoreCase("error")) {
                               resend_otp = jsonObject.optString("resend_otp");
                           }


                           if (status.contains("success"))
                           {
                               user_email = jsonObject.getString("email");
                               user_firstname = jsonObject.getString("firstname");
                               user_lastname = jsonObject.getString("lastname");
                               token = jsonObject.getString("token");
                               user_prof = jsonObject.getString("profilePic");
                               user_id = jsonObject.getString("userid");
                               message = jsonObject.getString("message");
                              String premiumMember = jsonObject.getString("premium_member");

                               SharedPreferences.Editor editor = settings.edit();
                               editor.putString("token", token);
                               editor.putString("email", user_email);
                               editor.putString("firstname", user_firstname);
                               editor.putString("lastname", user_lastname);
                               editor.putString("profilePic", user_prof);
                               editor.putString("userid", user_id);
                               editor.putString("premiumMember",premiumMember );
                               if (edtPassword.getText().toString()!="null")
                               {
                                   user_pass = edtPassword.getText().toString().trim();
                               }
                               editor.putString("user_pass",user_pass);
                               editor.commit();

                               Constant.user_id = user_id;
                               Constant.token = token;
                               Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                               startActivity(intent);
                               finish();
                           }else if(status.equalsIgnoreCase("error")&&resend_otp.equalsIgnoreCase("1")) {
                               resend_otp=jsonObject.optString("resend_otp");
                               userId=jsonObject.optString("user_id");
                               messageShow=jsonObject.optString("message");

                               resendOtpUrl(LoginActivity.this);
                           }else {

                               Toast.makeText(LoginActivity.this,message, Toast.LENGTH_SHORT).show();
                           }

                       } catch (JSONException e) {
                           Toast.makeText(LoginActivity.this,"Something went wrong", Toast.LENGTH_SHORT).show();

                           e.printStackTrace();
                       }
                   }
               }, new Response.ErrorListener() {
                   @Override
                   public void onErrorResponse(VolleyError error)
                   {
                       loaderDiloag.dismissDiloag();
                       Toast.makeText(LoginActivity.this,"Something went wrong", Toast.LENGTH_SHORT).show();

                       Log.e("VOLLEYOut", error.toString());
                   }
               })

               {

                   @Override
                   public Map<String, String> getHeaders() throws AuthFailureError
                   {
                       HashMap<String,String> param = new HashMap<String, String>();
                       param.put("Content-Type","application/json");
                       return param;
                   }

                   @Override
                   public String getBodyContentType() {
                       return "application/json; charset=utf-8";
                   }

                   @Override
                   public byte[] getBody() throws AuthFailureError {
                       try {
                           return requestBody == null ? null : requestBody.getBytes("utf-8");
                       } catch (UnsupportedEncodingException uee) {
                           VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                           return null;
                       }
                   }


               };

               requestQueue.add(stringRequest);
           } catch (JSONException e) {
               e.printStackTrace();
           }
       }
       else
       {
           Toast.makeText(LoginActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

       }
    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }

    private void resendOtpUrl(final Context context)
    {
        String resendOtpUrl=WebApis.resendOtpUrl + userId+"/";
        final LoaderDiloag loaderDiloag = new LoaderDiloag(LoginActivity.this);
        loaderDiloag.displayDiloag();

        try {
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            /*JSONObject jsonBody = new JSONObject();
            jsonBody.put("userid", userId);
            jsonBody.put("otp", otpString);


            Log.d(TAG,"jsonLogin " + jsonBody.toString());
            final String requestBody = jsonBody.toString();*/

            StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.GET, resendOtpUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    loaderDiloag.dismissDiloag();

                    Log.d("resendOtpUrl", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");

                        if (status.contains("success"))
                        {
                            new MaterialDialog.Builder(context)
                                    .title("OTP")
                                    .content(messageShow)
                                    .iconRes(R.drawable.ic_check_circle)
                                    .canceledOnTouchOutside(false)
                                    .cancelable(false)
                                    .positiveText("Ok")
                                    .onPositive(new MaterialDialog.SingleButtonCallback()
                                    {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                        {

                                            Intent intent = new Intent(LoginActivity.this, OtpActivity.class);
                                            intent.putExtra("userId", Integer.parseInt(userId));
                                            intent.putExtra("type", "RegisterActivity");
                                            startActivity(intent);
                                            dialog.dismiss();
                                            //startActivity(new Intent(context,LoginActivity.class));
                                        }
                                    }).show();

                        }
                        else
                        {
                            loaderDiloag.dismissDiloag();
                            Toast.makeText(LoginActivity.this,message,Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        loaderDiloag.dismissDiloag();
                        Toast.makeText(LoginActivity.this,getResources().getString(R.string.try_again),Toast.LENGTH_SHORT).show();

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    loaderDiloag.dismissDiloag();
                    Toast.makeText(LoginActivity.this,getResources().getString(R.string.try_again),Toast.LENGTH_SHORT).show();
                    Log.e("VOLLEYOut", error.toString());
                }
            })

            {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    HashMap<String,String> param = new HashMap<String, String>();
                    param.put("Content-Type","application/json");
                    return param;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

               /* @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }*/


            };

            requestQueue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

