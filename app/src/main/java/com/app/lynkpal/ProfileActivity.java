package com.app.lynkpal;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.app.lynkpal.Adapter.EducationAdapter;
import com.app.lynkpal.Adapter.ExperienceAdapter;
import com.app.lynkpal.Adapter.MultiSelectionList;
import com.app.lynkpal.Bean.ProfileEducationBean;
import com.app.lynkpal.Bean.ProfileExperienceBean;
import com.app.lynkpal.Bean.SkillListBean;
import com.app.lynkpal.Fragment.DialogFragEduProfile;
import com.app.lynkpal.Helper.AlbumStorageDirFactory;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Base64;
import com.app.lynkpal.Helper.BaseAlbumDirFactory;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.ExpandedHightListview;
import com.app.lynkpal.Helper.FilePath;
import com.app.lynkpal.Helper.FroyoAlbumDirFactory;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.EditInterface;
import com.app.lynkpal.Interface.SelectInterface;
import com.app.lynkpal.Interface.deleteEduExpInterface;
import com.bumptech.glide.Glide;

import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class ProfileActivity extends AppCompatActivity implements SelectInterface, EditInterface, deleteEduExpInterface {
    private static final int REQUEST_CAMERA_ACCESS_PERMISSION = 5674;
    private static final String JPEG_FILE_PREFIX = "IMG_";
    private static final String JPEG_FILE_SUFFIX = ".jpg";
    public static List<SkillListBean> list = new ArrayList<SkillListBean>();
    ListView listView;
    ImageView ic_editImage;
    Bitmap bitmap = null;
    String selectedIndustry;
    SkillListBean skillListBean;
    AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    ScrollView scrollView;
    String attachmentStr = "";
    MultiSelectionList customAdapter;
    TextView spnr, head, txtName, edu, exp, myPerDet, txtDOB, Gender;
    String message = "";
    ImageView ic_back, imgImage;
    ProfileEducationBean profileEducationBean;
    EducationAdapter educationAdapter;
    ExperienceAdapter experienceAdapter;
    List<ProfileEducationBean> educationBeanList = new ArrayList<ProfileEducationBean>();
    ProfileExperienceBean profileExperienceBean;
    List<ProfileExperienceBean> experienceBeanArrayList = new ArrayList<ProfileExperienceBean>();
    String profile_slug = "";
    String profile_pic = "";
    String skills = "";
    String dob = "";
    String country = "";
    String gender = "";
    String hobby = "";
    String next_of_kin = "";
    String no_of_children = "";
    String power_statement = "";
    String first_name = "";
    String last_name = "";
    ExpandedHightListview exp_eduList;
    ExpandedHightListview exp_expList;
    EditText edtChild, edtCountry, edtKin, edtHobby, edtDetails;
    RadioButton radMale, radFemale;
    Button btnUpdate;
    Calendar myCalendar, myCalSTREDU, myCalENDEDU, myCalSTREXP, myCalENDEXP;
    ImageView addEdu, addExp;
    EditText edtFirstName, edtLastName;
    private String mCurrentPhotoPath;
    FragmentManager fm = getSupportFragmentManager();

    FileBody filepath;
    private String extension;
    Uri camera_photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        customAdapter = new MultiSelectionList(ProfileActivity.this, list, this);
        exp_eduList = (ExpandedHightListview) findViewById(R.id.exp_eduList);
        exp_expList = (ExpandedHightListview) findViewById(R.id.exp_expList);
        scrollView = (ScrollView) findViewById(R.id.scroll);
        head = (TextView) findViewById(R.id.head);
        txtName = (TextView) findViewById(R.id.txtName);
        spnr = (TextView) findViewById(R.id.spnr);
        edu = (TextView) findViewById(R.id.edu);
        exp = (TextView) findViewById(R.id.exp);
        myPerDet = (TextView) findViewById(R.id.myPerDet);
        txtDOB = (TextView) findViewById(R.id.txtDOB);
        Gender = (TextView) findViewById(R.id.Gender);
        ic_back = (ImageView) findViewById(R.id.ic_back);
        ic_editImage = (ImageView) findViewById(R.id.ic_editImage);
        ic_editImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
        addEdu = (ImageView) findViewById(R.id.addEdu);
        addExp = (ImageView) findViewById(R.id.addExp);
        imgImage = (ImageView) findViewById(R.id.imgImage);
        edtChild = (EditText) findViewById(R.id.edtChild);
        edtCountry = (EditText) findViewById(R.id.edtCountry);
        edtKin = (EditText) findViewById(R.id.edtKin);
        edtHobby = (EditText) findViewById(R.id.edtHobby);
        edtDetails = (EditText) findViewById(R.id.edtDetails);
        edtFirstName = (EditText) findViewById(R.id.edtFirstName);
        edtLastName = (EditText) findViewById(R.id.edtLastName);
        radMale = (RadioButton) findViewById(R.id.radMale);
        radFemale = (RadioButton) findViewById(R.id.radFemale);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        addEdu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragEduProfile dialogFragEduProfile=new DialogFragEduProfile();
                Bundle bundle=new Bundle();
                bundle.putString("openFrag","educationFrag");
                dialogFragEduProfile.setArguments(bundle);
                dialogFragEduProfile.show(fm, "Dialog Fragment");

                //showPopupAddEdu();
            }
        });
        addExp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //showPopupAddExp();

                DialogFragEduProfile dialogFragEduProfile=new DialogFragEduProfile();
                Bundle bundle=new Bundle();
                bundle.putString("openFrag","experienceFrag");
                dialogFragEduProfile.setArguments(bundle);
                dialogFragEduProfile.show(fm, "Dialog Fragment");

            }
        });
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                finish();
               /* Bundle bundle = new Bundle();
                bundle.putInt("company",2);
                startActivity(new Intent(ProfileActivity.this,MainActivity.class).putExtras(bundle)
                .putExtra("profile","profile"));*/
            }
        });
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        head.setTypeface(tf_bold);
        txtName.setTypeface(tf_bold);
        edu.setTypeface(tf_bold);
        exp.setTypeface(tf_bold);
        myPerDet.setTypeface(tf_bold);
        btnUpdate.setTypeface(tf_bold);
        spnr.setTypeface(tf_reg);
        txtDOB.setTypeface(tf_reg);
        Gender.setTypeface(tf_med);
        edtChild.setTypeface(tf_reg);
        edtCountry.setTypeface(tf_reg);
        edtKin.setTypeface(tf_reg);
        edtLastName.setTypeface(tf_reg);
        edtFirstName.setTypeface(tf_reg);
        edtHobby.setTypeface(tf_reg);
        edtDetails.setTypeface(tf_reg);
        radMale.setTypeface(tf_reg);
        radFemale.setTypeface(tf_reg);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ApplicationGlobles.isNullOrEmpty(edtFirstName.getText().toString()))
                {
                    if (!ApplicationGlobles.isNullOrEmpty(edtLastName.getText().toString()))
                    {
                        if (!ApplicationGlobles.isNullOrEmpty(edtChild.getText().toString()))
                        {
                            if (!ApplicationGlobles.isNullOrEmpty(edtCountry.getText().toString()))
                            {
                                if (!ApplicationGlobles.isNullOrEmpty(edtKin.getText().toString()))
                                {
                                    if (!ApplicationGlobles.isNullOrEmpty(edtHobby.getText().toString()))
                                    {
                                        String gen = "";
                                        if (radMale.isChecked())
                                        {
                                            gen = "m";
                                        } else if (radFemale.isChecked())
                                        {
                                            gen = "f";
                                        }
                                        if (!gen.equals(""))
                                        {
                                            if (!ApplicationGlobles.isNullOrEmpty(edtDetails.getText().toString()))
                                            {
                                                SharedPreferences pref = getSharedPreferences("Location",MODE_PRIVATE);
                                                SharedPreferences.Editor editor = pref.edit();
                                                editor.clear();
                                                editor.apply();
                                                UpdatePersonal(edtFirstName.getText().toString(), edtLastName.getText().toString(), txtDOB.getText().toString(), edtChild.getText().toString(), edtCountry.getText().toString(), edtKin.getText().toString(), edtHobby.getText().toString(),
                                                        gen, edtDetails.getText().toString());
                                            }
                                            else
                                                {
                                                edtDetails.setError("Required");
                                            }
                                        }
                                        else {
                                            Toast.makeText(ProfileActivity.this, "Please Select Your Gender", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    else
                                        {
                                        edtHobby.setError("Required");
                                    }
                                } else {
                                    edtKin.setError("Required");
                                }
                            } else {
                                edtCountry.setError("Required");
                            }
                        } else {
                            edtChild.setError("Required");
                        }
                    } else {
                        edtLastName.setError("Required");
                    }

                } else {
                    edtFirstName.setError("Required");
                }
            }
        });
      /*  edtDetails.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        edtDetails.requestFocus();
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        scrollView.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return scrollView.onTouchEvent(motionEvent);
            }
        });*/
        edtDetails.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                scrollView.requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        spnr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String s = spnr.getText().toString();
                List<String> myList = new ArrayList<String>(Arrays.asList(s.split(",")));
                Log.e("myList.size()", myList.size() + "");
                for (int a = 0; a < list.size(); a++) {
                    boolean have = false;
                    for (int b = 0; b < myList.size(); b++) {
                        if (myList.get(b).equals(list.get(a).getName())) {
                            have = true;
                            break;
                        } else {
                            have = false;
                        }
                    }
                    if (have) {
                        list.get(a).setSelected(true);
                    } else {
                        list.get(a).setSelected(false);
                    }
                }
                if (list.size() > 0) {
                    showPopup();
                } else {
                    Toast.makeText(ProfileActivity.this, "No Skills Available for Select", Toast.LENGTH_SHORT).show();
                }
            }
        });
        getSkills();
        getProfileInformaition();
        myCalendar = Calendar.getInstance();
        myCalSTREDU = Calendar.getInstance();
        myCalENDEDU = Calendar.getInstance();
        myCalSTREXP = Calendar.getInstance();
        myCalENDEXP = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateStartLabel();
            }

        };
        txtDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DatePickerDialog(ProfileActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                            && ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                REQUEST_CAMERA_ACCESS_PERMISSION);
                    } else {
                       /* Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePicture.resolveActivity(getPackageManager()) != null) {
                            startActivityForResult(takePicture, 1);
                        }*/
                        dispatchTakePictureIntent();
                    }

                } else if (options[item].equals("Choose from Gallery")) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                                == PackageManager.PERMISSION_GRANTED || checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            Log.v("TAG", "Permission is granted");
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            //  Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(photoPickerIntent, 2);//one can be replaced with any action code

                        } else {

                            Log.v("TAG", "Permission is revoked");
                            ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                        }
                    } else {

                        Log.v("TAG", "Permission is granted");
                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                        photoPickerIntent.setType("image/*");
                        //  Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(photoPickerIntent, 2);//one can be replaced with any action code
                    }

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void dispatchTakePictureIntent() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);


        File f = null;
       /* try {
            Uri photoURI = FileProvider.getUriForFile(ProfileActivity.this,
                    BuildConfig.APPLICATION_ID + ".provider",
                    setUpPhotoFile());
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        } catch (IOException e) {
            e.printStackTrace();
        }*/


        startActivityForResult(takePictureIntent, 1);
    }

    private File setUpPhotoFile() throws IOException {

        File f = createImageFile();
        mCurrentPhotoPath = f.getAbsolutePath();

        return f;
    }

    private void handleBigCameraPhoto() {

        if (mCurrentPhotoPath != null) {
            setPic();
            galleryAddPic();
            mCurrentPhotoPath = null;
        }

        setPic();
        galleryAddPic();

    }

    private void setPic() {

		/* There isn't enough memory to open up more than a camera photos */
        /* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView */
        int targetW = 500;//.getWidth();
        int targetH = 500;////mImageView.getHeight();

		/* Get the size of the image */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

		/* Figure out which way needs to be reduced less */
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        }

		/* Set bitmap options to scale the image decode target */
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        /* Decode the JPEG file into a Bitmap */
        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        imgImage.setImageBitmap(bitmap);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream); //compress to which format you want.
        byte[] byte_arr = stream.toByteArray();
        attachmentStr = Base64.encodeBytes(byte_arr);

    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
        return imageF;
    }

    private File getAlbumDir() {
        File storageDir = null;

        storageDir = new File(Environment.getExternalStorageDirectory()
                + "/lynkpal");

        return storageDir;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                //  Bundle extras = data.getExtras();
                // bitmap = (Bitmap) extras.get("data");
                //convertPreImagetBitmapToString(bitmap);
                //handleBigCameraPhoto();

                //new
                Bitmap photo = (Bitmap) data.getExtras().get("data");

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

               Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                imgImage.setImageBitmap(bmp);
                attachmentStr = Base64.encodeBytes(byteArray);
               // getImageUri(ProfileActivity.this,bmp);
                //convertImage(ProfileActivity.this,camera_photo);


            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = "";
                if (c != null) {
                    picturePath = c.getString(columnIndex);
                    c.close();
                }
                if (picturePath != null) {
                    getThumbnailBitmap(picturePath, 400);
                }
            }
        }
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        camera_photo = Uri.parse(path);
        return camera_photo;
    }

    private void convertImage(Context context, Uri uri)
    {
        String selectedFilePath = FilePath.getPath(context,uri);
        File file = new File(selectedFilePath);
        filepath = new FileBody(file);
        extension = "png";
    }

    private Bitmap getThumbnailBitmap(String picturePath, int thumbnailSize) {
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(picturePath, bounds);
        if ((bounds.outWidth == -1) || (bounds.outHeight == -1)) {
            bitmap = null;
        }
        int originalSize = (bounds.outHeight > bounds.outWidth) ? bounds.outHeight
                : bounds.outWidth;
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = originalSize / thumbnailSize;
        bitmap = BitmapFactory.decodeFile(picturePath, opts);
        if (bitmap != null) {
            convertPreImagetBitmapToString(bitmap);

        } else {
            Toast.makeText(this, "Please Select correct Image", Toast.LENGTH_SHORT).show();
        }
        return bitmap;

    }

    public String convertPreImagetBitmapToString(Bitmap bmp) {
        try {
            imgImage.setImageBitmap(bmp);
        } catch (Exception e) {

        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream); //compress to which format you want.
        byte[] byte_arr = stream.toByteArray();
        attachmentStr = Base64.encodeBytes(byte_arr);
        Log.e("imageStr", attachmentStr);
        // showPopup(ChatActivity.this, attachmentStr, bmp);
        return attachmentStr;

    }

    private void updateStartLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        txtDOB.setText(sdf.format(myCalendar.getTime()));
    }

    public void showPopup() {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        Typeface tf = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        TextView textView = (TextView) popupView.findViewById(R.id.txtMain);
        textView.setTypeface(tf);
        Button btnOK = (Button) popupView.findViewById(R.id.btnOK);
        btnOK.setTypeface(tf);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        btnCancel.setTypeface(tf);
        listView = (ListView) popupView.findViewById(R.id.list);
        Log.e("list", list.size() + "");
        listView.setAdapter(customAdapter);
        customAdapter = new MultiSelectionList(ProfileActivity.this, list, this);
        customAdapter.notifyDataSetChanged();
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nothing = "";
                String temp = "";
                for (int a = 0; a < list.size(); a++) {
                    Log.e("list", String.valueOf(list.get(a).getSelected()));
                    if (list.get(a).getSelected()) {
                        if (nothing.equals("")) {
                            nothing = list.get(a).getId();
                            temp = list.get(a).getName();
                        } else {
                            nothing = nothing + "," + list.get(a).getId();
                            temp = temp + "," + list.get(a).getName();
                        }
                    }
                    if (a == list.size() - 1) {
                        popupWindow.dismiss();
                        Log.e("nothing", nothing + "");
                        SkillsUploader(nothing, temp);
                        break;
                    }
                }


               /* String s="java,kishan";
                List<String> myList = new ArrayList<String>(Arrays.asList(s.split(",")));
                Log.e("myList.size()",myList.size()+"");*/
               /* popupWindow.dismiss();*/
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
    }

    public void showPopupAddEdu() {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.showpopupaddeducation, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        TextView textView = (TextView) popupView.findViewById(R.id.txtMain);
        textView.setTypeface(tf_bold);
        final TextView txtStartDate = (TextView) popupView.findViewById(R.id.txtStartDate);
        txtStartDate.setTypeface(tf_reg);
        final TextView txtEndDate = (TextView) popupView.findViewById(R.id.txtEndDate);
        txtEndDate.setTypeface(tf_reg);
        final EditText edtUni = (EditText) popupView.findViewById(R.id.edtUni);
        edtUni.setTypeface(tf_reg);
        final EditText edtClass = (EditText) popupView.findViewById(R.id.edtClass);
        edtClass.setTypeface(tf_reg);
        final EditText edtSub = (EditText) popupView.findViewById(R.id.edtSub);
        edtSub.setTypeface(tf_reg);
        Button btnOK = (Button) popupView.findViewById(R.id.btnOK);
        btnOK.setTypeface(tf_bold);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        btnCancel.setTypeface(tf_bold);
        final DatePickerDialog.OnDateSetListener dateStartEdu = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalSTREDU.set(Calendar.YEAR, year);
                myCalSTREDU.set(Calendar.MONTH, monthOfYear);
                myCalSTREDU.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                txtStartDate.setText(sdf.format(myCalSTREDU.getTime()));
            }

        };
        final DatePickerDialog.OnDateSetListener dateEndEdu = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalENDEDU.set(Calendar.YEAR, year);
                myCalENDEDU.set(Calendar.MONTH, monthOfYear);
                myCalENDEDU.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                txtEndDate.setText(sdf.format(myCalENDEDU.getTime()));
            }

        };
        txtStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edtSub.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);
                } catch (Exception e) {
                }
                new DatePickerDialog(ProfileActivity.this, dateStartEdu, myCalSTREDU
                        .get(Calendar.YEAR), myCalSTREDU.get(Calendar.MONTH),
                        myCalSTREDU.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
        txtEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edtSub.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);
                } catch (Exception e) {
                }
                new DatePickerDialog(ProfileActivity.this, dateEndEdu, myCalENDEDU
                        .get(Calendar.YEAR), myCalENDEDU.get(Calendar.MONTH),
                        myCalENDEDU.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ApplicationGlobles.isNullOrEmpty(edtUni.getText().toString())) {
                    if (!ApplicationGlobles.isNullOrEmpty(edtClass.getText().toString())) {
                        if (!ApplicationGlobles.isNullOrEmpty(edtSub.getText().toString())) {
                            if (!txtStartDate.getText().toString().equals("Start Date")) {
                                if (!txtEndDate.getText().toString().equals("End Date")) {
                                    AddEducation(edtUni.getText().toString(), edtClass.getText().toString(),
                                            edtSub.getText().toString(), txtStartDate.getText().toString(),
                                            txtEndDate.getText().toString());
                                    popupWindow.dismiss();
                                } else {
                                    Toast.makeText(ProfileActivity.this, "Please Select End Date", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(ProfileActivity.this, "Please Select Start Date", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(ProfileActivity.this, "Please Enter Subject or Course Name", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ProfileActivity.this, "Please Enter Class or Degree Name", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ProfileActivity.this, "Please Enter University or School Name", Toast.LENGTH_SHORT).show();
                }

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
    }


    public void showPopupAddExp() {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.showpopupaddeducation, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        TextView textView = (TextView) popupView.findViewById(R.id.txtMain);
        textView.setText("Add New Experience Detail");
        textView.setTypeface(tf_bold);
        final TextView txtStartDate = (TextView) popupView.findViewById(R.id.txtStartDate);
        txtStartDate.setTypeface(tf_reg);
        final TextView txtEndDate = (TextView) popupView.findViewById(R.id.txtEndDate);
        txtEndDate.setTypeface(tf_reg);
        final EditText edtUni = (EditText) popupView.findViewById(R.id.edtUni);
        edtUni.setHint("Job Title");
        edtUni.setTypeface(tf_reg);
        final EditText edtClass = (EditText) popupView.findViewById(R.id.edtClass);
        edtClass.setHint("Company Name");
        edtClass.setTypeface(tf_reg);
        final EditText edtSub = (EditText) popupView.findViewById(R.id.edtSub);
        edtSub.setHint("Job Location");
        edtSub.setTypeface(tf_reg);
        Button btnOK = (Button) popupView.findViewById(R.id.btnOK);
        btnOK.setTypeface(tf_bold);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        btnCancel.setTypeface(tf_bold);
        final DatePickerDialog.OnDateSetListener dateStartEdu = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalSTREXP.set(Calendar.YEAR, year);
                myCalSTREXP.set(Calendar.MONTH, monthOfYear);
                myCalSTREXP.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                txtStartDate.setText(sdf.format(myCalSTREXP.getTime()));
            }

        };
        final DatePickerDialog.OnDateSetListener dateEndEdu = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalENDEXP.set(Calendar.YEAR, year);
                myCalENDEXP.set(Calendar.MONTH, monthOfYear);
                myCalENDEXP.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                txtEndDate.setText(sdf.format(myCalENDEXP.getTime()));
            }

        };
        txtStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edtSub.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);
                } catch (Exception e) {
                }
                new DatePickerDialog(ProfileActivity.this, dateStartEdu, myCalSTREXP
                        .get(Calendar.YEAR), myCalSTREXP.get(Calendar.MONTH),
                        myCalSTREXP.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
        txtEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edtSub.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);
                } catch (Exception e) {
                }
                new DatePickerDialog(ProfileActivity.this, dateEndEdu, myCalENDEXP
                        .get(Calendar.YEAR), myCalENDEXP.get(Calendar.MONTH),
                        myCalENDEXP.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ApplicationGlobles.isNullOrEmpty(edtUni.getText().toString())) {
                    if (!ApplicationGlobles.isNullOrEmpty(edtClass.getText().toString())) {
                        if (!ApplicationGlobles.isNullOrEmpty(edtSub.getText().toString())) {
                            if (!txtStartDate.getText().toString().equals("Start Date")) {
                                if (!txtEndDate.getText().toString().equals("End Date")) {
                                    AddExperience(edtUni.getText().toString(), edtClass.getText().toString(),
                                            edtSub.getText().toString(), txtStartDate.getText().toString(),
                                            txtEndDate.getText().toString());
                                    popupWindow.dismiss();
                                } else {
                                    Toast.makeText(ProfileActivity.this, "Please Select End Date", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(ProfileActivity.this, "Please Select Start Date", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(ProfileActivity.this, "Please Enter Job Location", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ProfileActivity.this, "Please Enter Company Name", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ProfileActivity.this, "Please Enter Job Title", Toast.LENGTH_SHORT).show();
                }

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
    }

    public void AddExperience(String sc, String cl, String sub, String SD, String ED) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(ProfileActivity.this);

        if (ApplicationGlobles.isConnectingToInternet(ProfileActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            JSONObject params = new JSONObject();
            try {
                params.put("title", sc);
                params.put("location", sub);
                params.put("end_date", ED);
                params.put("start_date", SD);
                params.put("company", cl);

                Log.e("param", params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(mediaType, params + "");
            Request request = new Request.Builder()
                    .url(WebApis.ADDEXPERIENCE)
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (ProfileActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    message = jsonObject.getString("message");
                                    ProfileActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ProfileActivity.this, message + "", Toast.LENGTH_SHORT).show();
                                            getProfileInformaition();
                                            //  finish();
                                        }
                                    });
                                } else {
                                    message = jsonObject.getString("message");
                                    ProfileActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ProfileActivity.this, message + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                ProfileActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(ProfileActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void AddEducation(String sc, String cl, String sub, String SD, String ED) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(ProfileActivity.this);

        if (ApplicationGlobles.isConnectingToInternet(ProfileActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            JSONObject params = new JSONObject();
            try {
                params.put("board_university", sc);
                params.put("class_degree", cl);
                params.put("end_date", ED);
                params.put("start_date", SD);
                params.put("subject_course", sub);

                Log.e("param", params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(mediaType, params + "");
            Request request = new Request.Builder()
                    .url(WebApis.ADDEDUCATION)
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (ProfileActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    message = jsonObject.getString("message");
                                    ProfileActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ProfileActivity.this, message + "", Toast.LENGTH_SHORT).show();
                                            getProfileInformaition();
                                            //  finish();
                                        }
                                    });
                                } else {
                                    message = jsonObject.getString("message");
                                    ProfileActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ProfileActivity.this, message + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                ProfileActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(ProfileActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void UpdatePersonal(final String first_name, final String last_name, final String dob, final String child, final String country, final String kin, final String hobby, final String gen, final String power_statement) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(ProfileActivity.this);
        final MaterialDialog.Builder newDialog = new MaterialDialog.Builder(ProfileActivity.this);

        if (ApplicationGlobles.isConnectingToInternet(ProfileActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            JSONObject params = new JSONObject();
            try {
                params.put("dob", dob);
                params.put("no_of_children", child);
                params.put("next_of_kin", kin);
                params.put("hobby", hobby);
                params.put("gender", gen);
                params.put("power_statement", power_statement);
                params.put("country", country);
                params.put("first_name", first_name);
                params.put("last_name", last_name);
                params.put("profilePic", attachmentStr);

                Log.e("param", params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(mediaType, params + "");
            Request request = new Request.Builder()
                    .url(WebApis.UPDATEPERSONALINFO)
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (ProfileActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success"))
                                {
                                    message = jsonObject.getString("message");
                                    ProfileActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run()
                                        {
                                            attachmentStr = null;
                                            newDialog.title("Success")
                                                    .content(message)
                                                    .positiveText("OK")
                                                    .iconRes(R.drawable.ic_check_circle)
                                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                                        {
                                                            dialog.dismiss();
                                                            finish();
                                                        }
                                                    })
                                                    //.icon(R.drawable.ic_check_circle)
                                                    .show();
                                            Toast.makeText(ProfileActivity.this, message + "", Toast.LENGTH_SHORT).show();

                                        }
                                    });
                                } else {
                                    message = jsonObject.getString("message");
                                    ProfileActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run()
                                        {
                                            newDialog.title("Fail")
                                                    .content(message)
                                                    .positiveText("OK")
                                                    .iconRes(R.drawable.ic_cancel)
                                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
                                                        {
                                                            dialog.dismiss();
                                                        }
                                                    })
                                                    //.icon(R.drawable.ic_check_circle)
                                                    .show();
                                            Toast.makeText(ProfileActivity.this, message + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                ProfileActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(ProfileActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void getProfileInformaition() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(ProfileActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(ProfileActivity.this)) {
            loaderDiloag.displayDiloag();
            educationBeanList.clear();
            experienceBeanArrayList.clear();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", Constant.token);
            Request request = new Request.Builder()
                    .url(WebApis.USERPROFILE + Constant.user_id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (ProfileActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                JSONObject jsonObject = new JSONObject(jsonData);
                                final String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("personal_profile");
                                    profile_slug = jsonObject1.getString("profile_slug");
                                    profile_pic = jsonObject1.getString("profile_pic");
                                    skills = jsonObject1.getString("skills");
                                    dob = jsonObject1.getString("dob");
                                    country = jsonObject1.getString("country");
                                    gender = jsonObject1.getString("gender");
                                    hobby = jsonObject1.getString("hobby");
                                    next_of_kin = jsonObject1.getString("next_of_kin");
                                    no_of_children = jsonObject1.getString("no_of_children");
                                    power_statement = jsonObject1.getString("power_statement");
                                    first_name = jsonObject1.getString("first_name");
                                    last_name = jsonObject1.getString("last_name");

                                    JSONArray array = jsonObject.getJSONArray("education");
                                    for (int a = 0; a < array.length(); a++) {
                                        JSONObject jsonObject2 = array.getJSONObject(a);
                                        String id = jsonObject2.getString("id");
                                        String board_university = jsonObject2.getString("board_university");
                                        String class_degree = jsonObject2.getString("class_degree");
                                        String end_date = jsonObject2.getString("end_date");
                                        String start_date = jsonObject2.getString("start_date");
                                        String subject_course = jsonObject2.getString("subject_course");
                                        profileEducationBean = new ProfileEducationBean();
                                        profileEducationBean.setId(id);
                                        profileEducationBean.setBoard_university(board_university);
                                        profileEducationBean.setClass_degree(class_degree);
                                        profileEducationBean.setEnd_date(parseDateToddMMyyyy(end_date));
                                        profileEducationBean.setStart_date(parseDateToddMMyyyy(start_date));
                                        profileEducationBean.setSubject_course(subject_course);
                                        profileEducationBean.setDeleteEdit(true);
                                        educationBeanList.add(profileEducationBean);
                                    }
                                    JSONArray array1 = jsonObject.getJSONArray("experience");
                                    for (int a = 0; a < array1.length(); a++) {
                                        JSONObject jsonObject3 = array1.getJSONObject(a);
                                        String id = jsonObject3.getString("id");
                                        String title = jsonObject3.getString("title");
                                        String company = jsonObject3.getString("company");
                                        String location = jsonObject3.getString("location");
                                        String start_date = jsonObject3.getString("start_date");
                                        String end_date = jsonObject3.getString("end_date");
                                        profileExperienceBean = new ProfileExperienceBean();
                                        profileExperienceBean.setId(id);
                                        profileExperienceBean.setTitle(title);
                                        profileExperienceBean.setCompany(company);
                                        profileExperienceBean.setLocation(location);
                                        profileExperienceBean.setStart_date(parseDateToddMMyyyy(start_date));
                                        profileExperienceBean.setEnd_date(parseDateToddMMyyyy(end_date));
                                        profileExperienceBean.setDeleteEdit(true);
                                        experienceBeanArrayList.add(profileExperienceBean);
                                    }

                                }

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        loaderDiloag.dismissDiloag();
                                        if (!skills.equals("null")) {
                                            spnr.setText(skills + "");
                                        }
                                        if (!dob.equals("null")) {
                                            txtDOB.setText(dob + "");
                                        }
                                        if (!country.equals("null")) {
                                            edtCountry.setText(country + "");
                                        }
                                        if (!no_of_children.equals("null")) {
                                            edtChild.setText(no_of_children + "");
                                        }
                                        if (!next_of_kin.equals("null")) {
                                            edtKin.setText(next_of_kin + "");
                                        }
                                        if (!hobby.equals("null")) {
                                            edtHobby.setText(hobby + "");
                                        }
                                        if (!power_statement.equals("null")) {
                                            edtDetails.setText(power_statement + "");
                                        }
                                        if (!first_name.equals("null")) {
                                            edtFirstName.setText(first_name + "");
                                        }
                                        if (!last_name.equals("null")) {
                                            edtLastName.setText(last_name + "");
                                        }
                                        if (!gender.equals("null")) {
                                            if (gender.toLowerCase().equals("m")) {
                                                radMale.setChecked(true);
                                                radFemale.setChecked(false);
                                            } else {
                                                radMale.setChecked(false);
                                                radFemale.setChecked(true);
                                            }
                                        }

                                        if (!profile_slug.equals("null")) {
                                            // profile_slug = profile_slug.replace("-", " ");
                                            // profile_slug = profile_slug.replace(" " + Constant.user_id, "");
                                            txtName.setText(first_name + " " + last_name);
                                        }
                                        if (!profile_pic.equals("null")) {
                                            Glide.with(ProfileActivity.this).load(WebApis.userProfileImage + profile_pic)/*.centerCrop()*/.into(imgImage);
                                        } else {
                                            // imgImage.setImageResource(R.drawable.image);
                                        }
                                        if (exp_eduList.getAdapter() == null) {
                                            if (educationBeanList.size() > 0) {
                                                exp_eduList.setExpanded(true);
                                                educationAdapter = new EducationAdapter(ProfileActivity.this, educationBeanList, ProfileActivity.this, ProfileActivity.this);
                                                exp_eduList.setAdapter(educationAdapter);
                                            }
                                        } else {
                                            try {
                                                educationAdapter.notifyDataSetChanged();
                                            } catch (Exception e) {
                                            }
                                        }
                                        if (exp_expList.getAdapter() == null) {
                                            if (experienceBeanArrayList.size() > 0) {
                                                exp_expList.setExpanded(true);
                                                experienceAdapter = new ExperienceAdapter(ProfileActivity.this, experienceBeanArrayList, ProfileActivity.this, ProfileActivity.this);
                                                exp_expList.setAdapter(experienceAdapter);
                                            }
                                        } else {
                                            try {
                                                experienceAdapter.notifyDataSetChanged();
                                            } catch (Exception e) {
                                            }
                                        }
                                    }
                                });


                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(ProfileActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        String outputPattern = "yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public void SkillsUploader(final String nothing, final String temp) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(ProfileActivity.this);

        if (ApplicationGlobles.isConnectingToInternet(ProfileActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            JSONObject params = new JSONObject();
            try {
                params.put("skill_ids", nothing);
                Log.e("param", params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(mediaType, params + "");
            Request request = new Request.Builder()
                    .url(WebApis.ADDSKILLS)
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (ProfileActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    message = jsonObject.getString("message");
                                    ProfileActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                          /*  layImage.setVisibility(View.GONE);
                                            bin = null;
                                            postText = "";
                                            encodedString = "";
                                            edtSEND.setText("");*/
                                            spnr.setText(temp + "");
                                            Toast.makeText(ProfileActivity.this, message + "", Toast.LENGTH_SHORT).show();
//                                            /*getAllPost();*/
                                        }
                                    });
                                } else {
                                    message = jsonObject.getString("message");
                                    ProfileActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ProfileActivity.this, message + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                ProfileActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(ProfileActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    public void getSkills() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(ProfileActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(ProfileActivity.this)) {
            loaderDiloag.displayDiloag();
            list.clear();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", Constant.token);
            Request request = new Request.Builder()
                    .url(WebApis.USERPROFILE)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (ProfileActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.d("skills response ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try
                            {
                               JSONObject jsonObject = new JSONObject(jsonData);
                               String status = jsonObject.getString("status");
                               if (status.contains("success"))
                               {
                                   JSONObject object = new JSONObject("personal_profile");
                                   String skills = object.getString("skills");
                               }

                              /*  JSONArray array = new JSONArray(jsonData);
                                if (array.length() > 0) {
                                    for (int i = 0; i < array.length(); i++)
                                    {
                                        JSONObject jsonObject1 = array.getJSONObject(i);
                                        //   String id=jsonObject1.getString("id");
                                        String id = jsonObject1.getString("id");
                                        String name = jsonObject1.getString("name");
                                        skillListBean = new SkillListBean();
                                        skillListBean.setId(id);
                                        skillListBean.setName(name);
                                        skillListBean.setSelected(false);
                                        list.add(skillListBean);

                                    }
                                    runOnUiThread(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            loaderDiloag.dismissDiloag();
                                            //  showPopup();
                                        }
                                    });

                                }*/
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(ProfileActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    @Override
    public void select(String position, int yes) {
        if (yes == 1) {
            list.get(Integer.parseInt(position)).setSelected(true);
            customAdapter.notifyDataSetChanged();
        } else {
            list.get(Integer.parseInt(position)).setSelected(false);
            customAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void Edit(String id, String type) {
        if (type.equals("Edu")) {
            getEduDetails(id);
        } else {
            getExpDetails(id);
        }
    }

    public void getEduDetails(String id) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(ProfileActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(ProfileActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", Constant.token);
            Request request = new Request.Builder()
                    .url(WebApis.GETEDUCATION + id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (ProfileActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    JSONArray array = jsonObject.getJSONArray("education");
                                    {
                                        JSONObject jsonObject1 = array.getJSONObject(0);
                                        {
                                            final String id = jsonObject1.getString("id");
                                            final String board_university = jsonObject1.getString("board_university");
                                            final String class_degree = jsonObject1.getString("class_degree");
                                            final String end_date = jsonObject1.getString("end_date");
                                            final String start_date = jsonObject1.getString("start_date");
                                            final String subject_course = jsonObject1.getString("subject_course");
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    showPopupUpdateEdu(id, board_university, class_degree, yyMMdd(start_date), yyMMdd(end_date), subject_course);
                                                }
                                            });
                                        }
                                    }


                                }

                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(ProfileActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void getExpDetails(String id) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(ProfileActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(ProfileActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", Constant.token);
            Request request = new Request.Builder()
                    .url(WebApis.GETEXPERIENCE + id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (ProfileActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    JSONArray array = jsonObject.getJSONArray("experience");
                                    {
                                        JSONObject jsonObject1 = array.getJSONObject(0);
                                        {
                                            final String id = jsonObject1.getString("id");
                                            final String title = jsonObject1.getString("title");
                                            final String location = jsonObject1.getString("location");
                                            final String end_date = jsonObject1.getString("end_date");
                                            final String start_date = jsonObject1.getString("start_date");
                                            final String company = jsonObject1.getString("company");
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    showPopupUpdateExp(id, title, location, yyMMdd(start_date), yyMMdd(end_date), company);
                                                }
                                            });
                                        }
                                    }


                                }

                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(ProfileActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public String yyMMdd(String time) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        String outputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public void showPopupUpdateEdu(final String id, String board_university, String class_degree, String start_date, String end_date, String subject_course) {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.showpopupaddeducation, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        TextView textView = (TextView) popupView.findViewById(R.id.txtMain);
        textView.setTypeface(tf_bold);
        textView.setText("Update Education Details");
        final TextView txtStartDate = (TextView) popupView.findViewById(R.id.txtStartDate);
        txtStartDate.setTypeface(tf_reg);
        txtStartDate.setText(start_date);
        final TextView txtEndDate = (TextView) popupView.findViewById(R.id.txtEndDate);
        txtEndDate.setTypeface(tf_reg);
        txtEndDate.setText(end_date);
        final EditText edtUni = (EditText) popupView.findViewById(R.id.edtUni);
        edtUni.setTypeface(tf_reg);
        edtUni.setText(board_university);
        final EditText edtClass = (EditText) popupView.findViewById(R.id.edtClass);
        edtClass.setTypeface(tf_reg);
        edtClass.setText(class_degree);
        final EditText edtSub = (EditText) popupView.findViewById(R.id.edtSub);
        edtSub.setTypeface(tf_reg);
        edtSub.setText(subject_course);
        Button btnOK = (Button) popupView.findViewById(R.id.btnOK);
        btnOK.setTypeface(tf_bold);
        btnOK.setText("Update");
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        btnCancel.setTypeface(tf_bold);
        final DatePickerDialog.OnDateSetListener dateStartEdu = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalSTREDU.set(Calendar.YEAR, year);
                myCalSTREDU.set(Calendar.MONTH, monthOfYear);
                myCalSTREDU.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                txtStartDate.setText(sdf.format(myCalSTREDU.getTime()));
            }

        };
        final DatePickerDialog.OnDateSetListener dateEndEdu = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalENDEDU.set(Calendar.YEAR, year);
                myCalENDEDU.set(Calendar.MONTH, monthOfYear);
                myCalENDEDU.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                txtEndDate.setText(sdf.format(myCalENDEDU.getTime()));
            }

        };
        txtStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ProfileActivity.this, dateStartEdu, myCalSTREDU
                        .get(Calendar.YEAR), myCalSTREDU.get(Calendar.MONTH),
                        myCalSTREDU.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
        txtEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ProfileActivity.this, dateEndEdu, myCalENDEDU
                        .get(Calendar.YEAR), myCalENDEDU.get(Calendar.MONTH),
                        myCalENDEDU.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ApplicationGlobles.isNullOrEmpty(edtUni.getText().toString())) {
                    if (!ApplicationGlobles.isNullOrEmpty(edtClass.getText().toString())) {
                        if (!ApplicationGlobles.isNullOrEmpty(edtSub.getText().toString())) {
                            if (!txtStartDate.getText().toString().equals("Start Date")) {
                                if (!txtEndDate.getText().toString().equals("End Date")) {
                                    UpdateEducation(id, edtUni.getText().toString(), edtClass.getText().toString(),
                                            edtSub.getText().toString(), txtStartDate.getText().toString(),
                                            txtEndDate.getText().toString());
                                    popupWindow.dismiss();
                                } else {
                                    Toast.makeText(ProfileActivity.this, "Please Select End Date", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(ProfileActivity.this, "Please Select Start Date", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(ProfileActivity.this, "Please Enter Subject or Course Name", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ProfileActivity.this, "Please Enter Class or Degree Name", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ProfileActivity.this, "Please Enter University or School Name", Toast.LENGTH_SHORT).show();
                }

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
    }

    public void showPopupUpdateExp(final String id, String title, String location, String start_date, String end_date, String company) {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.showpopupaddeducation, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        TextView textView = (TextView) popupView.findViewById(R.id.txtMain);
        textView.setTypeface(tf_bold);
        textView.setText("Update Experience Details");
        final TextView txtStartDate = (TextView) popupView.findViewById(R.id.txtStartDate);
        txtStartDate.setTypeface(tf_reg);
        txtStartDate.setText(start_date);
        final TextView txtEndDate = (TextView) popupView.findViewById(R.id.txtEndDate);
        txtEndDate.setTypeface(tf_reg);
        txtEndDate.setText(end_date);
        final EditText edtUni = (EditText) popupView.findViewById(R.id.edtUni);
        edtUni.setHint("Job Title");
        edtUni.setText(title);
        edtUni.setTypeface(tf_reg);
        final EditText edtClass = (EditText) popupView.findViewById(R.id.edtClass);
        edtClass.setHint("Company Name");
        edtClass.setText(company);
        edtClass.setTypeface(tf_reg);
        final EditText edtSub = (EditText) popupView.findViewById(R.id.edtSub);
        edtSub.setHint("Job Location");
        edtSub.setText(location);
        edtSub.setTypeface(tf_reg);
        Button btnOK = (Button) popupView.findViewById(R.id.btnOK);
        btnOK.setTypeface(tf_bold);
        btnOK.setText("Update");
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        btnCancel.setTypeface(tf_bold);
        final DatePickerDialog.OnDateSetListener dateStartEdu = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalSTREDU.set(Calendar.YEAR, year);
                myCalSTREDU.set(Calendar.MONTH, monthOfYear);
                myCalSTREDU.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                txtStartDate.setText(sdf.format(myCalSTREDU.getTime()));
            }

        };
        final DatePickerDialog.OnDateSetListener dateEndEdu = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalENDEDU.set(Calendar.YEAR, year);
                myCalENDEDU.set(Calendar.MONTH, monthOfYear);
                myCalENDEDU.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                txtEndDate.setText(sdf.format(myCalENDEDU.getTime()));
            }

        };
        txtStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ProfileActivity.this, dateStartEdu, myCalSTREDU
                        .get(Calendar.YEAR), myCalSTREDU.get(Calendar.MONTH),
                        myCalSTREDU.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
        txtEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ProfileActivity.this, dateEndEdu, myCalENDEDU
                        .get(Calendar.YEAR), myCalENDEDU.get(Calendar.MONTH),
                        myCalENDEDU.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ApplicationGlobles.isNullOrEmpty(edtUni.getText().toString())) {
                    if (!ApplicationGlobles.isNullOrEmpty(edtClass.getText().toString())) {
                        if (!ApplicationGlobles.isNullOrEmpty(edtSub.getText().toString())) {
                            if (!txtStartDate.getText().toString().equals("Start Date")) {
                                if (!txtEndDate.getText().toString().equals("End Date")) {
                                    UpdateExperience(id, edtUni.getText().toString(),
                                            edtSub.getText().toString(),
                                            edtClass.getText().toString(),
                                            txtStartDate.getText().toString(),
                                            txtEndDate.getText().toString());
                                    popupWindow.dismiss();
                                } else {
                                    Toast.makeText(ProfileActivity.this, "Please Select End Date", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(ProfileActivity.this, "Please Select Start Date", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(ProfileActivity.this, "Please Enter Job Location", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ProfileActivity.this, "Please Enter Company Name", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ProfileActivity.this, "Please Enter Job Title", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
    }

    public void UpdateExperience(String id, String title, String location, String company, String txtStartDate, String txtEndDate) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(ProfileActivity.this);

        if (ApplicationGlobles.isConnectingToInternet(ProfileActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            JSONObject params = new JSONObject();
            try {
                params.put("title", title);
                params.put("location", location);
                params.put("end_date", txtEndDate);
                params.put("start_date", txtStartDate);
                params.put("company", company);

                Log.e("param", params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(mediaType, params + "");
            Request request = new Request.Builder()
                    .url(WebApis.UPDATEEXPERIENCE + id + "/")
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (ProfileActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    message = jsonObject.getString("message");
                                    ProfileActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ProfileActivity.this, message + "", Toast.LENGTH_SHORT).show();
                                            getProfileInformaition();
                                            //  finish();
                                        }
                                    });
                                } else {
                                    message = jsonObject.getString("message");
                                    ProfileActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ProfileActivity.this, message + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                ProfileActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(ProfileActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    public void UpdateEducation(String id, String edtUni, String edtClass, String edtSub, String txtStartDate, String txtEndDate) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(ProfileActivity.this);

        if (ApplicationGlobles.isConnectingToInternet(ProfileActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            JSONObject params = new JSONObject();
            try {
                params.put("board_university", edtUni);
                params.put("class_degree", edtClass);
                params.put("end_date", txtEndDate);
                params.put("start_date", txtStartDate);
                params.put("subject_course", edtSub);

                Log.e("param", params.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody body = RequestBody.create(mediaType, params + "");
            Request request = new Request.Builder()
                    .url(WebApis.UPDATEEDUCATION + id + "/")
                    .post(body)
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (ProfileActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    message = jsonObject.getString("message");
                                    ProfileActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ProfileActivity.this, message + "", Toast.LENGTH_SHORT).show();
                                            getProfileInformaition();
                                            //  finish();
                                        }
                                    });
                                } else {
                                    message = jsonObject.getString("message");
                                    ProfileActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ProfileActivity.this, message + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                ProfileActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(ProfileActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }


    @Override
    public void delete(String id, int pos, String type) {
        if (type.equals("Edu")) {
            showPopup(id, pos, type);

        } else {
            showPopup(id, pos, type);
        }
    }

    public void showPopup(final String id, final int pos, final String type) {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_group_join_leave, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        final TextView textText = (TextView) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        textTitle.setText("DELETE");
        if (type.equals("Edu")) {
            textText.setText("Are you sure, you want to delete Education?");
            // JoinOpenGroup(id);
        } else {
            textText.setText("Are you sure, you want to delete Experience?");
            //  JoinClosedGroup(id);
        }
        // textText.setText("Are you sure, you want to leave " + '"' + text + '"' + " Group?");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.equals("Edu")) {
                    deleteEducation(id, pos);
                    // JoinOpenGroup(id);
                } else {
                    deleteExperience(id, pos);
                    //  JoinClosedGroup(id);
                }
                popupWindow.dismiss();

            }
        });
    }

    public void deleteExperience(String id, int pos) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(ProfileActivity.this);

        if (ApplicationGlobles.isConnectingToInternet(ProfileActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(WebApis.DELETEEXPERIENCE + id /*+ "/"*/)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (ProfileActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    message = jsonObject.getString("message");
                                    ProfileActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ProfileActivity.this, message + "", Toast.LENGTH_SHORT).show();
                                            getProfileInformaition();
                                            //  finish();
                                        }
                                    });
                                } else {
                                    message = jsonObject.getString("message");
                                    ProfileActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ProfileActivity.this, message + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                ProfileActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(ProfileActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }


    public void deleteEducation(String id, int pos) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(ProfileActivity.this);

        if (ApplicationGlobles.isConnectingToInternet(ProfileActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(WebApis.DELETEEDUCATION + id /*+ "/"*/)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (ProfileActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                if (status.equals("success")) {
                                    message = jsonObject.getString("message");
                                    ProfileActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ProfileActivity.this, message + "", Toast.LENGTH_SHORT).show();
                                            getProfileInformaition();
                                            //  finish();
                                        }
                                    });
                                } else {
                                    message = jsonObject.getString("message");
                                    ProfileActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ProfileActivity.this, message + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                ProfileActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(ProfileActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }
}
