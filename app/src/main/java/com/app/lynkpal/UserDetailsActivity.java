package com.app.lynkpal;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Address;
import android.location.Geocoder;
import android.os.Handler;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.lynkpal.Adapter.CompanyFollowersAdapter;
import com.app.lynkpal.Adapter.CompanyReviewAdapter;
import com.app.lynkpal.Adapter.GalleryGridAdapter;
import com.app.lynkpal.Adapter.GridViewAdapter;
import com.app.lynkpal.Adapter.SearchAdapter;
import com.app.lynkpal.Adapter.UserFollowerAdapter;
import com.app.lynkpal.Bean.CompanyFollowersBean;
import com.app.lynkpal.Bean.CompanyReviewsBean;
import com.app.lynkpal.Bean.GridBean;
import com.app.lynkpal.Fragment.Profile;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.ExpandadHeightGridview;
import com.app.lynkpal.Helper.ExpandedHightListview;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.Searchit;
import com.app.lynkpal.Interface.deleteFollowers;
import com.app.lynkpal.Interface.showImagePopup;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class UserDetailsActivity extends AppCompatActivity implements View.OnClickListener, showImagePopup,OnMapReadyCallback,Searchit {
    private static final String TAG = UserDetailsActivity.class.getSimpleName();
    ImageView mProfileImage;
    ImageView imgDetails, imgReviews, imgAddress, imgGallery, imgFollowers, imgCover, imgProfile, imgEdit, imgSave, ic_back, follow_unfollow;
    TextView txtDetails, txtReviews, txtAddress, txtGallery, txtFollowers, txtAddMore, txtAddMoreReview;
    TextView txtMainName, txtSubName, txtFollowing, textContent, textLocation, txtSave, txtCancel;
    LinearLayout linDetails, linReviews, linAddress, linGallery, linFollowers,addMore, linSaveCancel,lin_Location;
    RelativeLayout mLayout;
    TextView mCompanytxt,mFollowtxt,mProfessiontxt;
    String mUser_id = " ";
    String status,mUserImage,mCompany,mFollow,mProffession,company_link,company_description,company_lon,followers_count,mobile;
    String company_lat,company_banner,address;
    ExpandadHeightGridview gridView;
    ExpandedHightListview expReviewsList;
    RecyclerView expFollowersList;
    TextView mCompanyDescription;
    CompanyReviewsBean companyReviewsBean;
    CompanyFollowersBean companyFollowersBean;
    List<CompanyReviewsBean> companyReviewsArrayList = new ArrayList<CompanyReviewsBean>();
    List<CompanyFollowersBean> companyFollowersBeanList = new ArrayList<CompanyFollowersBean>();
    CompanyReviewAdapter companyReviewAdapter;
    SearchAdapter userFollowerAdapter;
    int height,width;
    GridBean gridBean;
    List<GridBean>gridBeanList = new ArrayList<>();
    GalleryGridAdapter gridViewAdapter;
    LinearLayout mFragLayout;
    LatLng mylocationValue;
    GoogleMap mMap;
    LoaderDiloag loaderDiloag;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        mUser_id = getIntent().getStringExtra("User_id");
        Log.d(TAG, "onCreate: Useridd" + mUser_id);
        mFragLayout = findViewById(R.id.frag_layout);

        init();
        initMap();
        loaderDiloag = new LoaderDiloag(this);
        linDetails.setOnClickListener(this);
        linReviews.setOnClickListener(this);
        linAddress.setOnClickListener(this);
        linGallery.setOnClickListener(this);
        linFollowers.setOnClickListener(this);
//        imgEdit.setOnClickListener(this);
      //  addMore.setOnClickListener(this);
      //  linSaveCancel.setOnClickListener(this);
      //  txtSave.setOnClickListener(this);
      //  txtCancel.setOnClickListener(this);
       // txtFollowing.setOnClickListener(this);

        mFollowtxt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
              followUnfollowApi(UserDetailsActivity.this,mUser_id);
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

    }

    private void followUnfollowApi(final Context context, String user) {


        com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(context);
        Map<String, String> postParam = new HashMap<String, String>();
        postParam.put("toFollow", user);

        Log.d(TAG, "frndParam " + postParam.toString());


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.POST,
                WebApis.FollowUnfollow, new JSONObject(postParam),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response)
                    {
                        Log.d(TAG, "FrndResponse " + response.toString());
                        try {
                            String status = response.getString("status");
                            String message = response.getString("message");
                            if (status.contains("success"))
                            {
                               if (mFollowtxt.getText().toString()=="Followed")
                               {
                                   mFollowtxt.setText("Follow");
                                   Toast.makeText(UserDetailsActivity.this,message,Toast.LENGTH_SHORT).show();

                               }
                               else
                               {
                                   mFollowtxt.setText("Followed");
                                   Toast.makeText(UserDetailsActivity.this,message,Toast.LENGTH_SHORT).show();
                               }

                            }
                            else
                                {
                                Log.d(TAG, "frndError " + response.toString());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener()
        {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                error.printStackTrace();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String,String> data = new HashMap<String, String>();
                data.put("Authorization",Constant.token);
                data.put("Content-Type","application/json");
                Log.d(TAG,"header " + data.toString());
                return data;
            }

        };

        jsonObjReq.setTag(TAG);
        // Adding request to request queue
        requestQueue.add(jsonObjReq);
    }

    private void init()
    {
        mProfileImage = findViewById(R.id.imguserProfile);
        mLayout = findViewById(R.id.follow_layout);
        mCompanytxt = findViewById(R.id.company_name_text);
        mFollowtxt = findViewById(R.id.follow_txt);
        mProfessiontxt = findViewById(R.id.proffession_txt);


        initializeTextView();
        initializeImage();
        initializeLayout();
    }

    /*private void initMap() {
        Log.d(TAG, "initializing Map");
        MapFragment mapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(UserDetailsActivity.this);
    }*/

    private void initializeImage()
    {
        imgDetails = (ImageView) findViewById(R.id.imgDetail);
        imgReviews = (ImageView) findViewById(R.id.imgReviews);
        imgAddress = (ImageView) findViewById(R.id.imgAddress);
        imgGallery = (ImageView) findViewById(R.id.imgGallery);
        imgFollowers = (ImageView) findViewById(R.id.imgFollowers);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        imgCover = (ImageView) findViewById(R.id.imgCover);
        imgEdit = (ImageView) findViewById(R.id.imgEdit);
        imgSave = (ImageView) findViewById(R.id.imgSave);
        ic_back = (ImageView) findViewById(R.id.ic_back);
        follow_unfollow = (ImageView) findViewById(R.id.follow_unfollow);
    }

    private void initializeTextView()
    {
        mCompanyDescription = findViewById(R.id.company_description_text);
        txtMainName = (TextView) findViewById(R.id.txtMainName);
        txtSubName = (TextView) findViewById(R.id.txtSubName);
        txtFollowing = (TextView) findViewById(R.id.txtFollowing);

        txtDetails = (TextView) findViewById(R.id.txtDetails);
        txtReviews = (TextView) findViewById(R.id.txtReviews);
        txtAddress = (TextView) findViewById(R.id.txtAddress);
        txtGallery = (TextView) findViewById(R.id.txtGallery);
        txtFollowers = (TextView) findViewById(R.id.txtFollowers);
        txtSave = (TextView) findViewById(R.id.txtSave);
        txtCancel = (TextView) findViewById(R.id.txtCancel);
        txtAddMore = (TextView) findViewById(R.id.txtAddMore);
        txtAddMoreReview = (TextView) findViewById(R.id.txtAddMoreReview);
    }

    private void initializeLayout()
    {
        linDetails = (LinearLayout) findViewById(R.id.linDetail);
        linReviews = (LinearLayout) findViewById(R.id.linReviews);
        linAddress = (LinearLayout) findViewById(R.id.linAddress);
        linGallery = (LinearLayout) findViewById(R.id.linGallery);
        linFollowers = (LinearLayout) findViewById(R.id.linFollowers);

        addMore = (LinearLayout) findViewById(R.id.addMore);
        linSaveCancel = (LinearLayout) findViewById(R.id.linSaveCancel);


        gridView = (ExpandadHeightGridview) findViewById(R.id.grid);
        expReviewsList = (ExpandedHightListview) findViewById(R.id.expReviewsList);
        expFollowersList =  findViewById(R.id.expFollowersList);





        changeColor();




    }

    public void ChangeColorImageView(ImageView imageView) {
        imgDetails.setColorFilter(getResources().getColor(R.color.black_overlay));
        imgReviews.setColorFilter(getResources().getColor(R.color.black_overlay));
        imgAddress.setColorFilter(getResources().getColor(R.color.black_overlay));
        imgGallery.setColorFilter(getResources().getColor(R.color.black_overlay));
        imgFollowers.setColorFilter(getResources().getColor(R.color.black_overlay));
        imageView.setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_IN);

    }

    public void ChangeColorTextView(TextView txtView) {
        txtDetails.setTextColor(getResources().getColor(R.color.black_overlay));
        txtReviews.setTextColor(getResources().getColor(R.color.black_overlay));
        txtAddress.setTextColor(getResources().getColor(R.color.black_overlay));
        txtGallery.setTextColor(getResources().getColor(R.color.black_overlay));
        txtFollowers.setTextColor(getResources().getColor(R.color.black_overlay));
        txtView.setTextColor(Color.parseColor("#ffffff"));

    }

    public void ChangeColorLinear(LinearLayout linView) {
        linDetails.setBackgroundColor(Color.parseColor("#ffffff"));
        linReviews.setBackgroundColor(Color.parseColor("#ffffff"));
        linAddress.setBackgroundColor(Color.parseColor("#ffffff"));
        linGallery.setBackgroundColor(Color.parseColor("#ffffff"));
        linFollowers.setBackgroundColor(Color.parseColor("#ffffff"));
        linView.setBackgroundColor(Color.parseColor("#375cc8"));

    }



    private void changeColor()
    {
        ChangeColorImageView(imgDetails);
        ChangeColorTextView(txtDetails);
        ChangeColorLinear(linDetails);
    }

    public void getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null)
            {
                return;
            }
            else
            {
                Address location = address.get(0);
                mylocationValue = new LatLng(location.getLatitude(), location.getLongitude() );
                Log.d(TAG, "getLocationFromAddress: " + mylocationValue);
            }





        } catch (IOException ex) {

            ex.printStackTrace();
        }


    }



    private void getDetails()
    {
        final LoaderDiloag loader = new LoaderDiloag(UserDetailsActivity.this);
        loader.displayDiloag();
        StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.GET, WebApis.CompanyDetails + mUser_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                loader.dismissDiloag();
                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    if (status.contains("success"))
                    {
                        mCompany = jsonObject.getString("company_name");
                        Log.d(TAG, "onResponse: Company Name " + mCompany);
                        if (mCompany!="null")
                        {
                            mCompanytxt.setText(mCompany);
                        }

                        mUserImage = jsonObject.getString("profile_pic");
                        String userImage = WebApis.userProfileImage + mUserImage;
                        if (mUserImage!="null")
                        {
                            Glide.with(UserDetailsActivity.this).load(userImage).into(mProfileImage);
                        }
                        else
                        {
                            mProfileImage.setImageResource(R.drawable.image);
                        }
                        mFollow = jsonObject.getString("follow_status");
                        if (mFollow.equals("1"))
                        {
                            mFollowtxt.setText("Followed");
                        }
                        else
                        {
                            mFollowtxt.setText("Follow");
                        }
                        mProffession = jsonObject.getString("industryname");
                        mProfessiontxt.setText(mProffession);
                        company_link = jsonObject.getString("company_link");
                        company_description = jsonObject.getString("company_description");
                        if (company_description!="null")
                        {
                            mCompanyDescription.setText(company_description);
                        }
                        else
                        {
                            mCompanyDescription.setText("User haven’t added this information yet.");
                        }
                        mobile = jsonObject.getString("mobile");
                        company_lon = jsonObject.getString("company_lon");
                        followers_count = jsonObject.getString("followers_count");
                        address = jsonObject.getString("address");
                        company_lat = jsonObject.getString("company_lat");
                        company_banner = jsonObject.getString("company_banner");
                        if (address!="null")
                        {
                            getLocationFromAddress(UserDetailsActivity.this,address);
                        }

                        Log.d(TAG, "onResponse: MyLocation " + mylocationValue);
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                loader.dismissDiloag();
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String,String> data = new HashMap<String, String>();
                data.put("authorization",Constant.token);
                return data;
            }
        }
                ;

        Volley.newRequestQueue(UserDetailsActivity.this).add(stringRequest);
    }


    public void getGallery()
    {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(UserDetailsActivity.this);
        loaderDiloag.displayDiloag();
        if (ApplicationGlobles.isConnectingToInternet(UserDetailsActivity.this)) {

            OkHttpClient client = new OkHttpClient();
            //status = "false";
            gridBeanList.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.GalleryAPI", WebApis.GalleryAPI + mUser_id);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.GalleryAPI + mUser_id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e)
                    {


                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getDetails", jsonData + "");
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {
                                JSONObject jsonObject = array.getJSONObject(a);
                                gridBean = new GridBean();
                                gridBean.setName(jsonObject.getString("created_at"));
                                gridBean.setImage(jsonObject.getString("image"));
                                gridBeanList.add(gridBean);
                            }

                        } catch (Exception e) {
                            loaderDiloag.dismissDiloag();
                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                                if (gridBeanList.size() > 0)
                                {
                                    Log.d(TAG, "Gallery");
                                    gridView.setExpanded(true);
                                    gridViewAdapter = new GalleryGridAdapter(UserDetailsActivity.this, gridBeanList, height,UserDetailsActivity.this);
                                    gridView.setAdapter(gridViewAdapter);
                                } else if (gridBeanList.size() == 0) {
                                    Log.d(TAG, "GalleryNo");

                                }
                            }
                        });


                    }
                });

            } catch (Exception e)
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(UserDetailsActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }




    public void getFollowers(Context context) {

        if (ApplicationGlobles.isConnectingToInternet(UserDetailsActivity.this)) {

            OkHttpClient client = new OkHttpClient();
            //status = "false";
            companyFollowersBeanList.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.Followers", WebApis.Followers + mUser_id);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.Followers + mUser_id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {



                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException
                    {

                        String jsonData = response.body().string();
                        Log.e("getDetails", jsonData + "");
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {
                                JSONObject jsonObject = array.getJSONObject(a);
                                companyFollowersBean = new CompanyFollowersBean();
                                companyFollowersBean.setFullName(jsonObject.getString("fullname"));
                                companyFollowersBean.setProfilePic(jsonObject.getString("profile_pic"));
                                companyFollowersBean.setUserid(jsonObject.getString("userid"));
                                companyFollowersBean.setDescription("No Parameter in API");
                                companyFollowersBeanList.add(companyFollowersBean);
                            }

                        } catch (Exception e) {

                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (companyFollowersBeanList.size() > 0)
                                {

                                    userFollowerAdapter = new SearchAdapter(UserDetailsActivity.this, companyFollowersBeanList,UserDetailsActivity.this );
                                    expFollowersList.setLayoutManager(new LinearLayoutManager(UserDetailsActivity.this));
                                    expFollowersList.setAdapter(userFollowerAdapter);
                                    expFollowersList.setNestedScrollingEnabled(false);


                                } else if (companyFollowersBeanList.size() == 0)
                                {

                                }
                            }
                        });


                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(UserDetailsActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }



    @Override
    protected void onResume()
    {
        super.onResume();

        getFollowers(UserDetailsActivity.this);
        getDetails();
        getReviews(UserDetailsActivity.this);
        getGallery();
    }

    @Override
    public void onClick(View view)
    {
        if (view == linDetails)
        {
            mFragLayout.setVisibility(View.GONE);
            expFollowersList.setVisibility(View.GONE);
            if (!company_description.equals("null"))
            {
                Log.d(TAG, "Address on: ");
               if (mCompanyDescription.getVisibility()==View.GONE)
                   mCompanyDescription.setVisibility(View.VISIBLE);
               mCompanyDescription.setText(company_description);
            }
            else if (company_description=="null")
            {
                Log.d(TAG, "Address off: ");
                mCompanyDescription.setVisibility(View.VISIBLE);
                gridView.setVisibility(View.GONE);
                mCompanyDescription.setText("User haven’t added this information yet.");
            }

            if (expReviewsList.getVisibility()==View.VISIBLE)
            {
                expReviewsList.setVisibility(View.GONE);
                gridView.setVisibility(View.GONE);
            }


            ChangeColorImageView(imgDetails);
            ChangeColorTextView(txtDetails);
            ChangeColorLinear(linDetails);

        } else if (view == linReviews)
        {
            mFragLayout.setVisibility(View.GONE);

            gridView.setVisibility(View.GONE);
            expFollowersList.setVisibility(View.GONE);
            if (companyReviewsArrayList.size()<=0)
            {
                Log.d(TAG, "Review off");
                mCompanyDescription.setVisibility(View.VISIBLE);
               mCompanyDescription.setText("Oops, No Review Found!");
            }
            else
                {
                    Log.d(TAG, "Review on");
                    if (expReviewsList.getVisibility()==View.GONE)
                    {
                        mCompanyDescription.setVisibility(View.GONE);
                        expReviewsList.setVisibility(View.VISIBLE);
                    }
            }




            ChangeColorImageView(imgReviews);
            ChangeColorTextView(txtReviews);
            ChangeColorLinear(linReviews);


        }
        else if (view == linAddress)
        {

            expReviewsList.setVisibility(View.GONE);
            expFollowersList.setVisibility(View.GONE);
            gridView.setVisibility(View.GONE);

            if (!address.equals("null"))
            {
                mFragLayout.setVisibility(View.VISIBLE);
                mCompanyDescription.setText(address);
                if (mylocationValue!=null)
                {

                    Marker marker = mMap.addMarker(new MarkerOptions().position(mylocationValue).title(address));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mylocationValue,15));
                    marker.showInfoWindow();

                }
            }
            else
            {
                mFragLayout.setVisibility(View.GONE);
                mCompanyDescription.setVisibility(View.VISIBLE);
                mCompanyDescription.setText("User haven’t added Location information yet.");
            }



            ChangeColorImageView(imgAddress);
            ChangeColorTextView(txtAddress);
            ChangeColorLinear(linAddress);


        } else if (view == linGallery)
        {
            mFragLayout.setVisibility(View.GONE);

            expFollowersList.setVisibility(View.GONE);
            expReviewsList.setVisibility(View.GONE);
            if (gridBeanList.size()<=0)
            {
                mCompanyDescription.setVisibility(View.VISIBLE);
                mCompanyDescription.setText("Oops, No Gallery Found!");
            }
            else
            {
                mCompanyDescription.setVisibility(View.GONE);
            }
            gridView.setVisibility(View.VISIBLE);
            ChangeColorImageView(imgGallery);
            ChangeColorTextView(txtGallery);
            ChangeColorLinear(linGallery);

        } else if (view == linFollowers)
        {

            mFragLayout.setVisibility(View.GONE);
            expReviewsList.setVisibility(View.GONE);
            if (companyFollowersBeanList.size()<=0)
            {
                mCompanyDescription.setVisibility(View.VISIBLE);
                mCompanyDescription.setText("Oops, No Follower Found!");
            }
            else
            {
                mCompanyDescription.setVisibility(View.GONE);
            }
            expFollowersList.setVisibility(View.VISIBLE);
            gridView.setVisibility(View.GONE);
            ChangeColorImageView(imgFollowers);
            ChangeColorTextView(txtFollowers);
            ChangeColorLinear(linFollowers);

        } else if (view == txtFollowers)
        {

        }  else if (view == txtCancel)
        {
            linSaveCancel.setVisibility(View.GONE);
            addMore.setVisibility(View.VISIBLE);
           // bin = null;
           // encodedString = "";
        } else if (view == txtSave) {
            //  ImageUploader();
        } /*else if (view == addMoreReview) {
            //showPopup();

        }*/ else if (view == ic_back) {
            finish();
        } else if (view == txtFollowing)
        {
            // followUnfollow();
        }
    }


    public String parseDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//2016-01-24T16:00:00.000Z
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd,yyyy hh:mm a");
        String newFormat = formatter.format(testDate);
        return newFormat;
    }

    public void getReviews(Context context)
    {
        txtGallery.setVisibility(View.GONE);
        final LoaderDiloag loaderDiloag = new LoaderDiloag(context);
        if (ApplicationGlobles.isConnectingToInternet(UserDetailsActivity.this))
        {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            //companyReviewsArrayList.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.CompanyReviews", WebApis.CompanyReviews + mUser_id);
            Request request = new Request.Builder()
                    // .url(WebApis.APPLICANTS+id)
                    .url(WebApis.CompanyReviews + mUser_id)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {

                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException
                    {
                        companyReviewsArrayList.clear();
                        loaderDiloag.dismissDiloag();
                        String jsonData = response.body().string();
                        Log.d(TAG, "onResponse: GetReview" + jsonData);
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {
                                JSONObject jsonObject = array.getJSONObject(a);
                                companyReviewsBean = new CompanyReviewsBean();
                                companyReviewsBean.setFullname(jsonObject.getString("fullname"));
                                companyReviewsBean.setProfile_pic(jsonObject.getString("profile_pic"));
                                companyReviewsBean.setUser(jsonObject.getString("user"));
                                companyReviewsBean.setComment(jsonObject.getString("comment"));
                                companyReviewsBean.setRating(jsonObject.getString("rating"));
                                companyReviewsBean.setCreated_at(parseDate(jsonObject.getString("created_at")));
                                companyReviewsArrayList.add(companyReviewsBean);
                            }

                        } catch (Exception e) {
                            loaderDiloag.dismissDiloag();
                        }


                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                loaderDiloag.dismissDiloag();
                                if (companyReviewsArrayList.size() > 0)
                                {
                                    Log.d(TAG, "Reviewss");
                                    expReviewsList.setExpanded(true);
                                    companyReviewAdapter = new CompanyReviewAdapter(UserDetailsActivity.this, companyReviewsArrayList, height);
                                    expReviewsList.setAdapter(companyReviewAdapter);
                                } else if (companyReviewsArrayList.size() == 0)
                                {


                                }
                            }
                        });


                    }
                });

            } catch (Exception e)
            {
               /* getActivity().runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        loaderDiloag.dismissDiloag();
                    }
                });*/
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            //Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    @Override
    public void image(String path)
    {

    }

    private void initMap() {
        Log.d(TAG, "initializing Map");
        MapFragment mapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(UserDetailsActivity.this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;
        Log.d(TAG, "onMapReady: true");
    }

    @Override
    public void onSearch(String id) {

    }
}
