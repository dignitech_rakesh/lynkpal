package com.app.lynkpal;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.lynkpal.Bean.LynkaplLearningModel;
import com.app.lynkpal.Bean.MyTrainingModel;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.WebApis;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TrainingDetailActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private String type;
    ImageView ivTrainingImage,ivProfileImage;
    private TextView tvCreatedAt,tvUserName,tvDetailName,tvDetailDuration,tvDetailNoOfLec,tvDetailAmt,tvDetailSpecialisation,
            tvAlrdyPurchased,tvDetailSubmit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_detail);

        sharedPreferences = TrainingDetailActivity.this.getSharedPreferences(Constant.PREFS_NAME, 0);
        type=getIntent().getStringExtra("type");
        final MyTrainingModel myTrainingModel = (MyTrainingModel) getIntent().getSerializableExtra("model");
       final LynkaplLearningModel allTrainingModel = (LynkaplLearningModel) getIntent().getSerializableExtra("LynkaplLearningModel");
        Log.d("type",type );

        tvCreatedAt = (TextView)findViewById(R.id.tvCreatedAt);
        tvUserName = (TextView)findViewById(R.id.tvUserName);
        tvDetailName = (TextView)findViewById(R.id.tvDetailName);
        tvDetailDuration = (TextView)findViewById(R.id.tvDetailDuration);
        tvDetailNoOfLec = (TextView)findViewById(R.id.tvDetailNoOfLec);
        tvDetailAmt = (TextView)findViewById(R.id.tvDetailAmt);
        tvDetailSpecialisation = (TextView)findViewById(R.id.tvDetailSpecialisation);
        tvAlrdyPurchased = (TextView)findViewById(R.id.tvAlrdyPurchased);
        tvDetailSubmit = (TextView)findViewById(R.id.tvDetailSubmit);

        ivTrainingImage = (ImageView) findViewById(R.id.ivTrainingImage);
        ivProfileImage = (ImageView) findViewById(R.id.ivProfileImage);


       if (type.equalsIgnoreCase("myTraining")){

          // Glide.with(TrainingDetailActivity.this).load(WebApis.userProfileImage + sharedPreferences.getString("profile_pic",""))
           Glide.with(TrainingDetailActivity.this).load(WebApis.userProfileImage +myTrainingModel.getProfile_pic())
                   .diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.image).error(R.drawable.image).into(ivProfileImage);
           Glide.with(TrainingDetailActivity.this).load(WebApis.BaseURL+myTrainingModel.getTraining_image()).centerCrop().into(ivTrainingImage);

           String createdAt="Added: ";
           String createdAT = myTrainingModel.getCreated_at();
           String createdAtFull= createdAt + createdAT;
           tvCreatedAt.setText(createdAtFull);
           String firstName= myTrainingModel.getFirst_name();
           String lastName= myTrainingModel.getLast_name();
           String fullName=firstName + " " + lastName;
           tvUserName.setText(fullName);
           tvDetailName.setText(myTrainingModel.getName());
           tvDetailDuration.setText(String.valueOf(myTrainingModel.getDuration()));
           tvDetailNoOfLec.setText(String.valueOf(myTrainingModel.getLectures()));
           tvDetailAmt.setText(String.valueOf(myTrainingModel.getAmount()));
           tvDetailSpecialisation.setText(myTrainingModel.getSpecialization_name());

           try {
               SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//2016-01-24T16:00:00.000Z
               sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
               String created = String.valueOf(myTrainingModel.getCreated_at());
               long time = sdf.parse(created).getTime();
               long now = System.currentTimeMillis();
               CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
               //postBean.setCreated(String.valueOf(ago));

               if (String.valueOf(ago).toLowerCase().equals("0 minutes ago") || String.valueOf(ago).toLowerCase().equals("in 0 minutes")) {
                   tvCreatedAt.setText("Just Now");
               } else {
                   String add= "Added: ";
                   String full= add + String.valueOf(ago);

                   tvCreatedAt.setText(full);

               }
           }catch (Exception e){
               e.printStackTrace();
           }

           tvDetailSubmit.setText("See Your Trainings");
           tvDetailSubmit.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   finish();
               }
           });

       }else{
           Glide.with(TrainingDetailActivity.this).load(WebApis.userProfileImage + allTrainingModel.getProfile_pic())
                   .diskCacheStrategy(DiskCacheStrategy.SOURCE).error(R.drawable.image).into(ivProfileImage);
           Glide.with(TrainingDetailActivity.this).load(WebApis.BaseURL+allTrainingModel.getTraining_image()).centerCrop().into(ivTrainingImage);

          // tvCreatedAt.setText(allTrainingModel.getCreated_at());
           String createdAt="Added: ";
           String createdAT = allTrainingModel.getCreated_at();
           String createdAtFull= createdAt + createdAT;
           tvCreatedAt.setText(createdAtFull);
           String firstName= allTrainingModel.getFirst_name();
           String lastName= allTrainingModel.getLast_name();
           String fullName=firstName + " " + lastName;
           tvUserName.setText(fullName);
           tvDetailName.setText(allTrainingModel.getName());
           tvDetailDuration.setText(String.valueOf(allTrainingModel.getDuration()));
           tvDetailNoOfLec.setText(String.valueOf(allTrainingModel.getLectures()));
           tvDetailAmt.setText(String.valueOf(allTrainingModel.getAmount()));
           tvDetailSpecialisation.setText(allTrainingModel.getSpecialization_name());

           try {
               SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//2016-01-24T16:00:00.000Z
               sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
               String created = String.valueOf(allTrainingModel.getCreated_at());
               long time = sdf.parse(created).getTime();
               long now = System.currentTimeMillis();
               CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
               //postBean.setCreated(String.valueOf(ago));

               if (String.valueOf(ago).toLowerCase().equals("0 minutes ago") || String.valueOf(ago).toLowerCase().equals("in 0 minutes")) {
                   tvCreatedAt.setText("Just Now");
               } else {
                   String add= "Added: ";
                   String full= add + String.valueOf(ago);

                   tvCreatedAt.setText(full);

               }
           }catch (Exception e){
               e.printStackTrace();
           }

           if (allTrainingModel.getPurchase_status()==0){
               tvDetailSubmit.setText("Pay For This Training");
               tvAlrdyPurchased.setVisibility(View.GONE);

           }else{
               tvDetailSubmit.setText("See More Trainings");
               tvAlrdyPurchased.setVisibility(View.VISIBLE);

           }

           tvDetailSubmit.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   if (allTrainingModel.getPurchase_status()==0){
                       Intent intent = new Intent(TrainingDetailActivity.this,CardDetailActivity.class);
                       intent.putExtra("LynkaplLearningModel",allTrainingModel);
                       intent.putExtra("trainingKey","TrainingDetail");

                       startActivity(intent);
                   }else{
                       finish();
                   }
               }
           });
       }
    }

    public static CharSequence getRelativeDate(final Date date) {
        long diff = System.currentTimeMillis() - date.getTime();
        if (System.currentTimeMillis() - date.getTime() >= android.text.format.DateUtils.WEEK_IN_MILLIS) {
            return diff / android.text.format.DateUtils.DAY_IN_MILLIS + " days ago";
        } else
            return android.text.format.DateUtils.getRelativeTimeSpanString(date.getTime());
    }
}
