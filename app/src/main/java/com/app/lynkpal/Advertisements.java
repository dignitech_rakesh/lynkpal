package com.app.lynkpal;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.app.lynkpal.Adapter.AdvertismentAdapter;
import com.app.lynkpal.Bean.AdvertisementsBean;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.EditInterface;
import com.app.lynkpal.Interface.deleteEduExpInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class Advertisements extends AppCompatActivity implements EditInterface, deleteEduExpInterface {
    FloatingActionButton floatAdd;
    TextView head;
    AdvertisementsBean advertisementsBean;
    List<AdvertisementsBean> advertisementsBeanList = new ArrayList<>();
    ListView list;
    AdvertismentAdapter advertismentAdapter;
    ImageView ic_back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        setContentView(R.layout.activity_advertisment);
        head = (TextView) findViewById(R.id.head);
        list = (ListView) findViewById(R.id.list);
        ic_back = (ImageView) findViewById(R.id.ic_back);

        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        head.setTypeface(tf_bold);
        floatAdd = (FloatingActionButton) findViewById(R.id.floatAdd);
        floatAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Advertisements.this, CreateAdvertisement.class));
            }
        });

    }


    public void getAdvertisment() {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(Advertisements.this);
        if (ApplicationGlobles.isConnectingToInternet(Advertisements.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            //status = "false";
            advertisementsBeanList.clear();
            Log.e("Constant.token", Constant.token);
            Log.e("WebApis.ALL_ADV", WebApis.ALL_ADV);
            Request request = new Request.Builder()
                    .url(WebApis.ALL_ADV)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("cache-control", "no-cache")
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (Advertisements.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("getDetails", jsonData + "");
                        loaderDiloag.dismissDiloag();
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            for (int a = 0; a < array.length(); a++) {

                                JSONObject jsonObject = array.getJSONObject(a);
                                advertisementsBean = new AdvertisementsBean();
                                advertisementsBean.setId(jsonObject.getString("id"));
                                advertisementsBean.setName(jsonObject.getString("name"));
                                advertisementsBean.setTags(jsonObject.getString("tags"));
                                advertisementsBean.setLink(jsonObject.getString("link"));
                                String startDate = parseDateToddMMyyyy(jsonObject.getString("startdate"));
                                String endDate = parseDateToddMMyyyy(jsonObject.getString("enddate"));
                                advertisementsBean.setStartdate(startDate);
                                advertisementsBean.setEnddate(endDate);
                                advertisementsBean.setAmount(jsonObject.getString("amount"));
                                advertisementsBean.setPaid(jsonObject.getString("paid"));
                                advertisementsBean.setAd_image(jsonObject.getString("ad_image"));
                                advertisementsBean.setRead_count(jsonObject.getString("read_count"));
                                advertisementsBeanList.add(advertisementsBean);
                            }


                        } catch (Exception e) {

                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                                if (advertisementsBeanList.size() > 0) {
                                    advertismentAdapter = new AdvertismentAdapter(Advertisements.this, advertisementsBeanList, Advertisements.this, Advertisements.this);
                                    list.setAdapter(advertismentAdapter);
                                }
                            }
                        });


                    }
                });

            } catch (Exception e) {
                Advertisements.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(Advertisements.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAdvertisment();
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        String outputPattern = "MMM dd,yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Override
    public void Edit(String id, String type) {
        int pos = Integer.parseInt(id);
        Intent intent = new Intent(Advertisements.this, EditAdvertisment.class);
        intent.putExtra("name", advertisementsBeanList.get(pos).getName());
        intent.putExtra("tag", advertisementsBeanList.get(pos).getTags());
        intent.putExtra("link", advertisementsBeanList.get(pos).getLink());
        intent.putExtra("image", advertisementsBeanList.get(pos).getAd_image());
        intent.putExtra("s_date", advertisementsBeanList.get(pos).getStartdate());
        intent.putExtra("e_date", advertisementsBeanList.get(pos).getEnddate());
        intent.putExtra("charge", advertisementsBeanList.get(pos).getAmount());
        intent.putExtra("id", advertisementsBeanList.get(pos).getId());
        startActivity(intent);

    }

    @Override
    public void delete(String id, int pos, String type) {

        showPopup(id, pos);
    }

    public void showPopup(final String id, final int pos) {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.show_popup_group_join_leave, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout linearLayout = (LinearLayout) popupView.findViewById(R.id.lin1);
        popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.update();
        popupWindow.getContentView().setFocusableInTouchMode(true);
        TextView textTitle = (TextView) popupView.findViewById(R.id.text_title);
        final TextView textText = (TextView) popupView.findViewById(R.id.txtText);
        Button btnCancel = (Button) popupView.findViewById(R.id.btnCancel);
        Button btnSumbmit = (Button) popupView.findViewById(R.id.btnContinue);
        Typeface tf_reg = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface tf_med = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), "Roboto-Bold.ttf");
        textTitle.setTypeface(tf_bold);
        textText.setTypeface(tf_reg);
        btnCancel.setTypeface(tf_bold);
        btnSumbmit.setTypeface(tf_bold);
        textTitle.setText("Delete Advertisement");
        textText.setText("Are you sure, you want to delete Advertisement?");
        // textText.setText("Are you sure, you want to leave " + '"' + text + '"' + " Group?");
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });
        btnSumbmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteExperience(id, pos);
                popupWindow.dismiss();

            }
        });
    }


    public void deleteExperience(String id, final int pos) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(Advertisements.this);

        if (ApplicationGlobles.isConnectingToInternet(Advertisements.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(WebApis.DELETE_ADVERTISMENT + id /*+ "/"*/)
                    .get()
                    .addHeader("authorization", Constant.token)
                    .addHeader("content-type", "application/json")
                    .build();

            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (Advertisements.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("response withme ", jsonData + "");


                        if (jsonData.length() > 0) {
                            try {
                                final JSONObject jsonObject = new JSONObject(jsonData);
                                String status = jsonObject.getString("status");
                                String message = "";
                                if (status.equals("success")) {
                                    message = jsonObject.getString("message");
                                    final String finalMessage = message;
                                    Advertisements.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                advertisementsBeanList.remove(pos);
                                                advertismentAdapter.notifyDataSetChanged();
                                            } catch (Exception e) {
                                            }
                                            Toast.makeText(Advertisements.this, finalMessage + "", Toast.LENGTH_SHORT).show();
                                            // getAdvertisment();
                                            //  finish();
                                        }
                                    });
                                } else {
                                    message = jsonObject.getString("message");
                                    final String finalMessage1 = message;
                                    Advertisements.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(Advertisements.this, finalMessage1 + "", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                Log.e("json", jsonObject.toString());
                            } catch (Exception je) {
                                loaderDiloag.dismissDiloag();
                                je.printStackTrace();
                            }
                            loaderDiloag.dismissDiloag();
                        } else {
                            loaderDiloag.dismissDiloag();
                        }
                    }
                });

            } catch (Exception e) {
                Advertisements.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(Advertisements.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }
}
