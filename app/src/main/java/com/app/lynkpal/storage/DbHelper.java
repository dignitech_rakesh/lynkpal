package com.app.lynkpal.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME="POST_DATABASE";
    public static final String TABLE_NAME="POST_TABLE";
    public static final String COL_0 ="table_id";

    public static final String COL_1 = "likestatus";
    public static final String COL_2 = "id";
    public static final String COL_3 = "description";
    public static final String COL_4 = "file";
    public static final String COL_5 = "created_at";
    public static final String COL_6 = "user";
    public static final String COL_7 = "parent_post";

    public static final String COL_8 = "sharer_msg";
    public static final String COL_9 = "fullname";
    public static final String COL_10 = "profile_pic";
    public static final String COL_11 = "likes_count";
    public static final String COL_12 = "comments_count";
    public static final String COL_13 = "owner_fullname";
    public static final String COL_14 = "owner_profile_pic";
    public static final String COL_15 = "frame_name";
    public static final String COL_16 = "file_type";
    public static final String COL_17 = "page_no";
    public static final String COL_18 = "shrdPref_userId";

    public static final String COL_19 = "org_profile_pic";
    public static final String COL_20 = "org_file";
    public static final String COL_21 = "org_owner_profile_pic";
    public static final String COL_22 = "org_frame_name";

    public static final String COL_23 = "adv_id";
    public static final String COL_24 = "adv_name";
    public static final String COL_25 = "adv_tags";
    public static final String COL_26 = "adv_link";
    public static final String COL_27 = "adv_startDate";
    public static final String COL_28 = "adv_endDate";
    public static final String COL_29 = "adv_amount";
    public static final String COL_30 = "adv_paid";
    public static final String COL_31 = "adv_image";
    public static final String COL_32 = "adv_readCount";

   public DbHelper(Context context){
       super(context, DATABASE_NAME, null, 1);
   }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME +" (table_id INTEGER PRIMARY KEY AUTOINCREMENT,likestatus TEXT,id INTEGER," +
                "description TEXT,file TEXT,created_at TEXT,user INTEGER,parent_post TEXT,sharer_msg TEXT,fullname TEXT,profile_pic TEXT," +
                "likes_count TEXT,comments_count TEXT,owner_fullname TEXT,owner_profile_pic TEXT,frame_name TEXT,file_type TEXT,page_no INTEGER,shrdPref_userId TEXT," +
                "org_profile_pic TEXT,org_file TEXT,org_owner_profile_pic TEXT,org_frame_name TEXT," +
                "adv_id INTEGER, adv_name TEXT, adv_tags TEXT, adv_link TEXT, adv_startDate TEXT, adv_endDate TEXT, " +
                " adv_amount INTEGER, adv_paid INTEGER, adv_image TEXT, adv_readCount TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }

    public boolean insertData(String likestatus,int id,String description,String file,String created_at,int user,String parent_post,String sharer_msg,
                              String fullname,String profile_pic,String likes_count,String comments_count,String owner_fullname,
                              String owner_profile_pic,String frame_name,String file_type,int page_no,String shrdPref_userId,
                              String org_profile_pic,String org_file,String org_owner_profile_pic,String org_frame_name,
                              int adv_id,String adv_name, String adv_tags, String adv_link, String adv_startDate, String adv_endDate,
                              int adv_amount, int adv_paid, String adv_image, String adv_readCount) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1,likestatus);
        contentValues.put(COL_2,id);
        contentValues.put(COL_3,description);
        contentValues.put(COL_4,file);
        contentValues.put(COL_5,created_at);
        contentValues.put(COL_6,user);
        contentValues.put(COL_7,parent_post);
        contentValues.put(COL_8,sharer_msg);
        contentValues.put(COL_9,fullname);
        contentValues.put(COL_10,profile_pic);
        contentValues.put(COL_11,likes_count);
        contentValues.put(COL_12,comments_count);
        contentValues.put(COL_13,owner_fullname);
        contentValues.put(COL_14,owner_profile_pic);
        contentValues.put(COL_15,frame_name);
        contentValues.put(COL_16,file_type);
        contentValues.put(COL_17,page_no);
        contentValues.put(COL_18,shrdPref_userId);

        contentValues.put(COL_19,org_profile_pic);
        contentValues.put(COL_20,org_file);
        contentValues.put(COL_21,org_owner_profile_pic);
        contentValues.put(COL_22,org_frame_name);

        contentValues.put(COL_23,adv_id);
        contentValues.put(COL_24,adv_name);
        contentValues.put(COL_25,adv_tags);
        contentValues.put(COL_26,adv_link);
        contentValues.put(COL_27,adv_startDate);
        contentValues.put(COL_28,adv_endDate);
        contentValues.put(COL_29,adv_amount);
        contentValues.put(COL_30,adv_paid);
        contentValues.put(COL_31,adv_image);
        contentValues.put(COL_32,adv_readCount);


        long result = db.insert(TABLE_NAME,null ,contentValues);
        if(result == -1)
            return false;
        else
            return true;
    }

    public boolean chkData(int post_id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        String sql ="SELECT * FROM "+TABLE_NAME+" WHERE id="+post_id;
        cursor= db.rawQuery(sql,null);
        Log.d("Cursor Count : " , cursor.getCount()+"");

        if(cursor.getCount()>0){
            //PID Found
            return true;
        }else{
            //PID Not Found
            return false;
        }
        //cursor.close();
    }

    public boolean chkAdvData(int adv_id, int page){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        String sql ="SELECT * FROM "+TABLE_NAME+" WHERE adv_id="+adv_id + " AND page_no = "+  page ;
        cursor= db.rawQuery(sql,null);
        Log.d("CursorAdvCount : " , cursor.getCount()+"");

        if(cursor.getCount()>0){
            //PID Found
            return true;
        }else{
            //PID Not Found
            return false;
        }
        //cursor.close();
    }

    public Cursor getAllData(int a,String shrdPref_userId,Integer adv_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res ;
        //res= db.rawQuery("select * from "+TABLE_NAME,null);
        //res= db.rawQuery("select * from "+TABLE_NAME + " limit 0, 10",null);
        //res= db.rawQuery("select * from "+TABLE_NAME + " limit"+ "?,?",new String[]{a,b},null);
       // res= db.rawQuery("select * from "+TABLE_NAME +" WHERE "+ " page_no " + " = " + a,null);
        res= db.rawQuery("select * from "+TABLE_NAME +" WHERE "+ " page_no = " +  a +" AND shrdPref_userId = "+  shrdPref_userId +  " AND adv_id = "+  adv_id +
                " ORDER BY " + COL_5 + " DESC ",null);
//        int count = res.getCount();
//        res.close();

        return res;
    }

    public Cursor getAllAddData(int a,String shrdPref_userId,Integer postId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res ;
        //res= db.rawQuery("select * from "+TABLE_NAME,null);
        //res= db.rawQuery("select * from "+TABLE_NAME + " limit 0, 10",null);
        //res= db.rawQuery("select * from "+TABLE_NAME + " limit"+ "?,?",new String[]{a,b},null);
        // res= db.rawQuery("select * from "+TABLE_NAME +" WHERE "+ " page_no " + " = " + a,null);
        res= db.rawQuery("select * from "+TABLE_NAME +" WHERE "+ " page_no = " +  a +" AND shrdPref_userId = "+  shrdPref_userId + " AND id = "+  postId +
                " ORDER BY " + COL_5 + " DESC ",null);
//        int count = res.getCount();
//        res.close();

        return res;
    }


    public boolean updateData(String likestatus,int id,String description,String file,String created_at,int user,String parent_post,String sharer_msg,
                              String fullname,String profile_pic,String likes_count,String comments_count,String owner_fullname,
                              String owner_profile_pic,String frame_name,String file_type,int page_no,String shrdPref_userId,
                              String org_profile_pic,String org_file,String org_owner_profile_pic,String org_frame_name,
                              int adv_id,String adv_name, String adv_tags, String adv_link, String adv_startDate, String adv_endDate,
                              int adv_amount, int adv_paid, String adv_image, String adv_readCount) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1,likestatus);
        contentValues.put(COL_2,id);
        contentValues.put(COL_3,description);
        contentValues.put(COL_4,file);
        contentValues.put(COL_5,created_at);
        contentValues.put(COL_6,user);
        contentValues.put(COL_7,parent_post);
        contentValues.put(COL_8,sharer_msg);
        contentValues.put(COL_9,fullname);
        contentValues.put(COL_10,profile_pic);
        contentValues.put(COL_11,likes_count);
        contentValues.put(COL_12,comments_count);
        contentValues.put(COL_13,owner_fullname);
        contentValues.put(COL_14,owner_profile_pic);
        contentValues.put(COL_15,frame_name);
        contentValues.put(COL_16,file_type);
        contentValues.put(COL_17,page_no);
        contentValues.put(COL_18,shrdPref_userId);

        contentValues.put(COL_19,org_profile_pic);
        contentValues.put(COL_20,org_file);
        contentValues.put(COL_21,org_owner_profile_pic);
        contentValues.put(COL_22,org_frame_name);

        contentValues.put(COL_23,adv_id);
        contentValues.put(COL_24,adv_name);
        contentValues.put(COL_25,adv_tags);
        contentValues.put(COL_26,adv_link);
        contentValues.put(COL_27,adv_startDate);
        contentValues.put(COL_28,adv_endDate);
        contentValues.put(COL_29,adv_amount);
        contentValues.put(COL_30,adv_paid);
        contentValues.put(COL_31,adv_image);
        contentValues.put(COL_32,adv_readCount);



        //db.update(TABLE_NAME, contentValues, "ID = ?",new String[] { id });
        db.update(TABLE_NAME, contentValues, "id="+id, null);
        return true;
    }


    public boolean updateDataAdv(String likestatus,int id,String description,String file,String created_at,int user,String parent_post,String sharer_msg,
                              String fullname,String profile_pic,String likes_count,String comments_count,String owner_fullname,
                              String owner_profile_pic,String frame_name,String file_type,int page_no,String shrdPref_userId,
                              String org_profile_pic,String org_file,String org_owner_profile_pic,String org_frame_name,
                              int adv_id,String adv_name, String adv_tags, String adv_link, String adv_startDate, String adv_endDate,
                              int adv_amount, int adv_paid, String adv_image, String adv_readCount) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1,likestatus);
        contentValues.put(COL_2,id);
        contentValues.put(COL_3,description);
        contentValues.put(COL_4,file);
        contentValues.put(COL_5,created_at);
        contentValues.put(COL_6,user);
        contentValues.put(COL_7,parent_post);
        contentValues.put(COL_8,sharer_msg);
        contentValues.put(COL_9,fullname);
        contentValues.put(COL_10,profile_pic);
        contentValues.put(COL_11,likes_count);
        contentValues.put(COL_12,comments_count);
        contentValues.put(COL_13,owner_fullname);
        contentValues.put(COL_14,owner_profile_pic);
        contentValues.put(COL_15,frame_name);
        contentValues.put(COL_16,file_type);
        contentValues.put(COL_17,page_no);
        contentValues.put(COL_18,shrdPref_userId);

        contentValues.put(COL_19,org_profile_pic);
        contentValues.put(COL_20,org_file);
        contentValues.put(COL_21,org_owner_profile_pic);
        contentValues.put(COL_22,org_frame_name);

        contentValues.put(COL_23,adv_id);
        contentValues.put(COL_24,adv_name);
        contentValues.put(COL_25,adv_tags);
        contentValues.put(COL_26,adv_link);
        contentValues.put(COL_27,adv_startDate);
        contentValues.put(COL_28,adv_endDate);
        contentValues.put(COL_29,adv_amount);
        contentValues.put(COL_30,adv_paid);
        contentValues.put(COL_31,adv_image);
        contentValues.put(COL_32,adv_readCount);



        //db.update(TABLE_NAME, contentValues, "ID = ?",new String[] { id });
        db.update(TABLE_NAME, contentValues, "adv_id="+adv_id, null);
        return true;
    }

    public int getCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
       // cursor.close();

        // return count
        return cursor.getCount();
    }


    public int matchData(int post_id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        int temp=-1;
        String sql ="SELECT * FROM "+TABLE_NAME+" WHERE id="+post_id;
       // cursor= db.rawQuery(sql,null);
        cursor= db.rawQuery("select * from "+TABLE_NAME +" WHERE "+ " id = " +  post_id ,null);
        Log.d("CursorCount:" , cursor.getCount()+"");

        if(cursor.getCount()>0){
            //PID Found
           // while (cursor.moveToNext()) {
                //return cursor.getInt(Integer.parseInt(COL_2));
                //temp= Integer.parseInt(cursor.getString(2));
            return post_id;
           // }
        }else {
            return temp;
        }

        //cursor.close();

        //Log.d("temp",temp+"");
       // return temp;

    }

    public void deleteTable(){
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("delete from "+ TABLE_NAME);
    }

    public void deletePostByPostId(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(TABLE_NAME, COL_2 + "=" + id, null);
    }

    public void deletePostByAdvId(int adv_id){
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(TABLE_NAME, COL_23 + "=" + adv_id, null);
    }

    //Profile.java1812
    public void deleteAdvByPageNo(int page,int id){
        SQLiteDatabase db = this.getWritableDatabase();
        //db.delete(TABLE_NAME, COL_2 + "=" + id, null);
        //db.rawQuery("delete from "+TABLE_NAME +" WHERE "+ " page_no = " +  page  + " AND id = "+  id,null);
        db.delete(TABLE_NAME, COL_2 + "=" + id + " AND "+  COL_17 + "=" + page, null);
    }

    public void deleteDataPageNo(int page){
        SQLiteDatabase db = this.getWritableDatabase();
        //db.delete(TABLE_NAME, COL_2 + "=" + id, null);
        //db.rawQuery("delete from "+TABLE_NAME +" WHERE "+ " page_no = " +  page  + " AND id = "+  id,null);
        db.delete(TABLE_NAME,  COL_17 + "=" + page, null);
    }

    //Profile.java1812
    public void deletePostByUserId(int user_id){
        SQLiteDatabase db = this.getReadableDatabase();
        //db.delete(TABLE_NAME, COL_2 + "=" + id, null);
         db.rawQuery("delete from "+TABLE_NAME +" WHERE "+ " user = " +  user_id ,null);
    }


    public Cursor matchData2(int post_id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        int temp=-1;
        String sql ="SELECT * FROM "+TABLE_NAME+" WHERE id="+post_id;
        // cursor= db.rawQuery(sql,null);
        cursor= db.rawQuery("select * from "+TABLE_NAME +" WHERE "+ " id = " +  post_id ,null);
        Log.d("CursorCount:" , cursor.getCount()+"");

        if(cursor.getCount()>0){

            return cursor;
        }else {
            return cursor;
        }

        //cursor.close();


    }
}
