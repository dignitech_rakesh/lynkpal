package com.app.lynkpal;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.lynkpal.Adapter.CommentListAdapter;
import com.app.lynkpal.Adapter.ReplyAdapter;
import com.app.lynkpal.Bean.CommentReplies;
import com.app.lynkpal.Bean.CommentsBean;
import com.app.lynkpal.Helper.ApplicationGlobles;
import com.app.lynkpal.Helper.Constant;
import com.app.lynkpal.Helper.LoaderDiloag;
import com.app.lynkpal.Helper.WebApis;
import com.app.lynkpal.Interface.InterCmntRply;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class ReplyActivity extends AppCompatActivity implements InterCmntRply {

    public String TAG = CmntReplyActivity.class.getSimpleName();
    ReplyAdapter replyAdapter;
    ArrayList<CommentsBean> commentsBeenList_reply = new ArrayList<>();
    ArrayList<CommentsBean> commentsBeenList = new ArrayList<>();
    ExpandableListView expandableListView;
    CommentsBean commentsBean,group;

    EditText etUserRply;
    ImageView ivPostRply;
    String id,token;
    int comment_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply);


        group=(CommentsBean) getIntent().getSerializableExtra("group");
         comment_id =  getIntent().getExtras().getInt("comment_id",0);
         id =  getIntent().getStringExtra("id");
        token =  getIntent().getStringExtra("token");

        expandableListView = (ExpandableListView) findViewById(R.id.reply_exp_list);
        etUserRply=(EditText)findViewById(R.id.user_Reply);
        ivPostRply=(ImageView) findViewById(R.id.sendReply);

        Log.d("group",group.getMessage()+"--"+ group.getId()+"--"+id+"--"+token+"--"+comment_id);
        expandableListView.setGroupIndicator(null);
        ImageView ic_back = (ImageView) findViewById(R.id.ic_back);
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
              //  Intent intent = new Intent(ReplyActivity.this,CmntReplyActivity.class);
               // startActivity(intent);
            }
        });

        commentsBeenList_reply.add(group);

        replyAdapter = new ReplyAdapter(ReplyActivity.this, commentsBeenList_reply);
        //expandableListView.setExpanded(true);
        expandableListView.setAdapter(replyAdapter);
        replyAdapter.notifyDataSetChanged();


        ivPostRply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ReplyVolley(ReplyActivity.this,etUserRply.getText().toString(),Integer.parseInt(group.getId()));
                etUserRply.setText("");
            }
        });


    }

    public void getAllComments() {

        final LoaderDiloag loaderDiloag = new LoaderDiloag(ReplyActivity.this);
        loaderDiloag.displayDiloag();
        commentsBeenList.clear();
        OkHttpClient client = new OkHttpClient();
        // Log.e("Constant.token", "852fdcdbf8eccb01a7b0c5c52bf67834e4126f4e");
        Log.e("Constant.token", token);
        Request request = new Request.Builder()
                .url(WebApis.AllCommentPOST + id + "/")
                .get()
                .addHeader("authorization", token)
                .addHeader("cache-control", "no-cache")
                .build();


        try {
            okhttp3.Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(okhttp3.Call call, IOException e) {
                    loaderDiloag.dismissDiloag();
                    if (ReplyActivity.this == null)
                        return;



                }

                @Override
                public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                    String jsonData = response.body().string();
                    Log.e("response withme ", jsonData + "");

                    loaderDiloag.dismissDiloag();

                    commentsBeenList.clear();

                    if (jsonData.length() > 0) {
                        try {
                            JSONArray array = new JSONArray(jsonData);
                            if (array.length() > 0) {
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsonObject1 = array.getJSONObject(i);
                                    //   String id=jsonObject1.getString("id");
                                    Integer id = jsonObject1.getInt("id");
                                    Integer user = jsonObject1.getInt("user");
                                    String created = jsonObject1.getString("created_at");
                                    String message = jsonObject1.getString("message");
                                    // String message = Utils.decodeMessage(jsonObject1.getString("message"));
                                    String fullname = jsonObject1.getString("fullname");
                                    String profile_pic = jsonObject1.getString("profile_pic");
                                    commentsBean = new CommentsBean();
                                    commentsBean.setId(String.valueOf(id));

                                    commentsBean.setUser(String.valueOf(user));
                                    commentsBean.setProfile_pic(profile_pic);
                                    commentsBean.setFullname(fullname);
                                    commentsBean.setMessage(message);

                                    commentsBean.setLikes_count(jsonObject1.getString("likes_count"));
                                    commentsBean.setReplies_count(jsonObject1.getString("replies_count"));
                                    commentsBean.setCommentLikeStatus(jsonObject1.getInt("commentlikestatus"));


                                    JSONArray jsonRplyArray=jsonObject1.getJSONArray("commentreplies");
                                    JSONObject jsonRplyObj;
                                    CommentReplies cmntRply ;
                                    ArrayList<CommentReplies> cmntRplyArrayList =new ArrayList<CommentReplies>();
                                    for (int j=0;j<jsonRplyArray.length();j++){
                                        cmntRply =new CommentReplies();
                                        jsonRplyObj=jsonRplyArray.getJSONObject(j);
                                        cmntRply.setId_rply(jsonRplyObj.getInt("id"));
                                        cmntRply.setLevel_rply(jsonRplyObj.getInt("level"));
                                        cmntRply.setFullname_rply(jsonRplyObj.getString("fullname"));
                                        cmntRply.setMessage_rply(jsonRplyObj.getString("message"));
                                        cmntRplyArrayList.add(cmntRply);
                                    }




                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//2016-01-24T16:00:00.000Z
                                    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                                    long time = sdf.parse(created).getTime();
                                    long now = System.currentTimeMillis();
                                    CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
                                    commentsBean.setCreated_at(String.valueOf(ago));
                                    commentsBean.setCommentRepliesArrayList(cmntRplyArrayList);
                                    commentsBeenList.add(commentsBean);

                                }
                                ReplyActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                       /* commentAdapterr = new CommentListAdapter(ReplyActivity.this, commentsBeenList);

                                        expandableListView.setAdapter(commentAdapterr);
                                        commentAdapterr.notifyDataSetChanged();*/


                                       for (int i=0;i<commentsBeenList.size();i++){
                                           if (commentsBeenList.get(i).getId().equalsIgnoreCase(group.getId())){
                                               group=commentsBeenList.get(i);
                                               commentsBeenList_reply.clear();
                                               commentsBeenList_reply.add(group);

                                               replyAdapter = new ReplyAdapter(ReplyActivity.this, commentsBeenList_reply);
                                               //expandableListView.setExpanded(true);
                                               expandableListView.setAdapter(replyAdapter);
                                               replyAdapter.notifyDataSetChanged();
                                           }

                                       }

                                    }
                                });

                            }
                        } catch (Exception je) {
                            loaderDiloag.dismissDiloag();
                            je.printStackTrace();
                        }

                    } else {
                        loaderDiloag.dismissDiloag();
                    }
                }
            });

        } catch (Exception e) {
            ReplyActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    //loaderDiloag.dismissDiloag();
                }
            });
            e.printStackTrace();
            loaderDiloag.dismissDiloag();
        }


        return;
    }



    private void ReplyVolley(final Context context, String reply, int cmnt_id) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(ReplyActivity.this);
        loaderDiloag.displayDiloag();

        com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(context);
        Map<String, String> postParam = new HashMap<String, String>();
        postParam.put("message", reply);

        Log.d("frndParam " ,postParam.toString());


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(com.android.volley.Request.Method.POST,
                WebApis.ReplyComment + cmnt_id + "/", new JSONObject(postParam),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonObject)
                    {
                        loaderDiloag.dismissDiloag();

                        Log.d(TAG, "FrndResponse " + jsonObject.toString());
                        try {
                            //JSONObject jsonObject = new JSONObject(jsonData);
                            String success = jsonObject.getString("status");
                            if (success.equals("success")) {
                                //txtCommentsCount.setText(jsonObject.getInt("comment_count") + "");
                                //commentsBean.setReplies_count(String.valueOf(jsonObject.getInt("reply_count")));

                                getAllComments();
                                // commentAdapterr.refreshData();

                                       /* final Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                //Do something after 100ms
                                                getAllComments();
                                                // commentAdapterr.notifyDataSetChanged();
                                                commentAdapterr.refreshData();
                                            }
                                        }, 2000);*/

                            }
                        } catch (Exception e) {
                            loaderDiloag.dismissDiloag();

                        }
                        loaderDiloag.dismissDiloag();
                    }
                }, new Response.ErrorListener()
        {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                error.printStackTrace();
                loaderDiloag.dismissDiloag();

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String,String> data = new HashMap<String, String>();
                data.put("Authorization", Constant.token);
                data.put("Content-Type","application/json");
                Log.d(TAG,"header " + data.toString());
                return data;
            }

        };

        jsonObjReq.setTag(TAG);
        // Adding request to request queue
        requestQueue.add(jsonObjReq);
    }


    public void likeCommentUrl(int cmnt_id) {
        final LoaderDiloag loaderDiloag = new LoaderDiloag(ReplyActivity.this);
        if (ApplicationGlobles.isConnectingToInternet(ReplyActivity.this)) {
            loaderDiloag.displayDiloag();
            OkHttpClient client = new OkHttpClient();
            Log.e("Constant.token", token + "");
            Request request = new Request.Builder()
                    .url(WebApis.LikeComment + cmnt_id)
                    .get()
                    .addHeader("authorization", token)
                    .build();


            try {
                okhttp3.Call call = client.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {
                        if (ReplyActivity.this == null)
                            return;
                        loaderDiloag.dismissDiloag();

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                        String jsonData = response.body().string();
                        Log.e("respons", jsonData + "");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loaderDiloag.dismissDiloag();
                                getAllComments();
                            }
                        });
                    }
                });

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loaderDiloag.dismissDiloag();
                    }
                });
                e.printStackTrace();
                //  loaderDiloag.dismissDiloag();
            }
        } else {
            Toast.makeText(ReplyActivity.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

        }

        return;
    }

    @Override
    public void likeComment(int comment_id) {
        Log.d("cmntId",comment_id+"");
        likeCommentUrl(comment_id);

    }

    @Override
    public void replyClick(int comment_id, CommentsBean group) {

    }
}
